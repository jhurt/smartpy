# image:../logo-transp.png[Logo,50] Editor Help - SmartPy.io
:nofooter:
:source-highlighter: coderay
:linkattrs:

## Editor

SmartPy is a Python library designed to describe and handle Smart Contracts.

The link:index.html[Editor] enables programmers to design smart contracts and add tests that can yield
to different treatments.

To run the script, one can click on the `Run` or type `Ctrl-Enter` or `Cmd-Enter`.
To run the script without tests, one can type `Shift-Ctrl-Enter` or `Shift-Cmd-Enter`.

It then evaluates the script and possibly registers new links or buttons that can be executed by clicking on
them.

## Interface

The interface comprises three zones:

- Menus and buttons.
- The SmartPy editor.
- The output panel.

## Output Panel
This output is typically populated by a link:reference.html#_tests_and_test_scenarios[testScenario, window="_blank"] in a test.

## Contract Viewer
In the output panel, a usual output for a smart contract, the contract viewer comprises several tabs.

- SmartPy. An actual SmartPy script. Syntax is currently evolving so may be unstable at this exact moment.
- Storage. A pretty-print of the storage only.
- Types. Information about types inferred for storage and entry points.
- All. SmartPy + Storage + Types.
- Michelson. Compiled Michelson smart contract with a link to origination page.
- X. To minimize the contract output.
