# Copyright 2019-2020 Smart Chain Arena LLC.

SOURCES:=$(shell find  ./smartML/ \( ! -name ".\#*" \) -name "*.ml" -o -iname "*.mli")
DUNES:=$(shell find  ./smartML/ \( ! -name ".\#*" \) -name "dune")

GIT_HOOKS=pre-commit pre-push
TEMPLATES=$(wildcard python/templates/*.py)

all: .phony smartML/build_ok build-local-tezos

init:
	./env/current/init

distclean:
	rm -rf _build _opam env/current env/{naked,nix}/_opam package{,_lock}.json smartML/build_ok

full: .phony
	$(MAKE)
	$(MAKE) test-quick
	$(MAKE) doc
	$(MAKE) referenceManual
	$(MAKE) build-local-tezos
	@echo -e '\n\n\n'
	@echo -e 'To launch full tests:\n ./with_env make test'
	@echo -e 'To launch a webserver:\n  ./with_env make www'

clean: .phony
	rm -f smartML/build_ok
	dune clean

SMARTML_EXE=_build/default/smartML/app/smartml.exe

smartML/build_ok: $(SOURCES) $(DUNES)
	@rm -f smartML/build_ok
	dune build smartML/web_js/smartmljs.bc.js smartML/app/smartml.exe smartML/test/main.exe smartML/cli_js/smartml-cli.js
	@$(MAKE) fmt-check
	@touch smartML/build_ok

smartML.loop: .phony $(SMARTML_EXE)
	dune build smartML/web_js/smartmljs.bc.js smartML/app/smartml.exe -w

PORT=8000
www: .phony
	(sleep 1; open http://localhost:$(PORT)/demo) &
	cd site && python3 -m http.server $(PORT)

fmt: .phony
	@ocamlformat --inplace $(SOURCES)

fmt-check: .phony
	ocamlformat --check $(SOURCES)

build: .phony smartML/build_ok

SCENARIO_OK=$(TEMPLATES:python/templates/%.py=test_baselines/scenario/%.ok)

-include test_baselines/compile.mk
-include test_baselines/scenario_sandbox.mk
-include smartML/test/decompile.mk

test_baselines/compile.mk: smartML/test/basic-app-test.sh
	smartML/test/basic-app-test.sh prepare-compile

test_baselines/scenario_sandbox.mk: smartML/test/basic-app-test.sh
	smartML/test/basic-app-test.sh prepare-scenario-sandbox

test_baselines/scenario/%.ok: python/templates/%.py smartML/build_ok
	@grep -q EXCLUDE_FROM_TESTS $< || \
          ./test_wrapper test_baselines/scenario/$* smartpy-cli/SmartPy.sh test python/templates/$*.py test_baselines/scenario/$*

test_baselines/scenario_sandbox/%.ok: smartML/build_ok python/templates/%.py
	@./test_wrapper test_baselines/scenario_sandbox/$* smartpy-cli/SmartPy.sh test-sandbox python/templates/$*.py test_baselines/scenario_sandbox/$*

test_baselines/sandbox-scenarios/%_scenario.tsv: build-local-tezos smartML/build_ok python/templates/%.py
	$(MAKE) kill_sandbox
	$(MAKE) test_baselines/scenario/$*.ok
	@method=local ./with_env smartML/test/sandbox-scenarios.sh fullrun $*

smartML/test/decompile/%.ok: smartML/build_ok smartML/test/tz/%.tz.json
	@./test_wrapper smartML/test/decompile/$* $(SMARTML_EXE) decompile smartML/test/tz/$*.tz.json smartML/test/decompile/$*/contract.michel

test-sandbox-short:
	$(MAKE) smartML/build_ok
	dune build _build/default/ext/tezos/tezos-master/src/bin_node/main.exe _build/default/ext/tezos/tezos-master/src/bin_client/main_client.exe _build/default/ext/tezos/tezos-master/src/bin_client/main_admin.exe
	rm -rf test_baselines/scenario_sandbox
	$(MAKE) test_baselines/scenario_sandbox/welcome.ok
	$(MAKE) test_baselines/scenario_sandbox/stringManipulations.ok
	$(MAKE) test_baselines/scenario_sandbox/FA1.2.ok
	$(MAKE) test_baselines/scenario_sandbox/FA2.ok
	$(MAKE) test_baselines/scenario_sandbox/calculator.ok
	$(MAKE) test_baselines/scenario_sandbox/chess.ok

test-sandbox-full:
	$(MAKE) test-sandbox-short
	$(MAKE) test_baselines/scenario_sandbox/atomicSwap.ok
	$(MAKE) test_baselines/scenario_sandbox/checkLanguage.ok
	$(MAKE) test_baselines/scenario_sandbox/decompilation.ok
	$(MAKE) test_baselines/scenario_sandbox/fifo.ok
	$(MAKE) test_baselines/scenario_sandbox/jingleBells.ok
	$(MAKE) test_baselines/scenario_sandbox/minikitties.ok
	$(MAKE) test_baselines/scenario_sandbox/multisig.ok
	$(MAKE) test_baselines/scenario_sandbox/nim.ok
	$(MAKE) test_baselines/scenario_sandbox/nimLift.ok
	$(MAKE) test_baselines/scenario_sandbox/storeValue.ok
	$(MAKE) test_baselines/scenario_sandbox/syntax.ok
	$(MAKE) test_baselines/scenario_sandbox/testEmpty.ok
	$(MAKE) test_baselines/scenario_sandbox/testHashFunctions.ok
	$(MAKE) test_baselines/scenario_sandbox/testSend1.ok
	$(MAKE) test_baselines/scenario_sandbox/testSend2.ok
	$(MAKE) test_baselines/scenario_sandbox/testSend3.ok
	$(MAKE) test_baselines/scenario_sandbox/testVoid1.ok
	$(MAKE) test_baselines/scenario_sandbox/testVoid2.ok
	$(MAKE) test_baselines/scenario_sandbox/tictactoe.ok
	$(MAKE) test_baselines/scenario_sandbox/tictactoeFactory.ok
	$(MAKE) test_baselines/scenario_sandbox/voting.ok

smartML/test/value-pack.ok: smartML/build_ok smartML/test/value-pack.sh
	@./test_wrapper smartML/test/value-pack smartML/test/value-pack.sh smartML/test/value-pack

test-clean: .phony smartML/build_ok
	smartML/test/basic-app-test.sh clean
	@$(MAKE) test-compile-purge
	@$(MAKE) test-scenario-purge
	@$(MAKE) test-decompile-purge
	rm -f smartML/build_ok
	rm -f smartML/test/value-pack.ok
	rm -f doc/all_ok

test-build: .phony smartML/build_ok
	@dune build smartML/test/main.exe

test-unit: .phony smartML/build_ok
	@dune build @smartML/test/runtest

michelsons: .phony smartML/build_ok
	smartML/test/basic-app-test.sh michelsons

test-prepare:
	smartML/test/basic-app-test.sh prepare-compile
	smartML/test/basic-app-test.sh prepare-scenario-sandbox

test-compile-purge: .phony
	rm -rf test_baselines/compile

test-decompile-purge: .phony
	rm -rf smartML/test/decompile

test-scenario-purge: .phony
	rm -rf test_baselines/scenario

test_baselines/compile/all_ok: smartML/build_ok $(COMPILE_OK)
	@touch $@

test_baselines/scenario/all_ok: smartML/build_ok $(SCENARIO_OK)
	@touch $@

test_baselines/scenario_sandbox/all_ok: smartML/build_ok $(SCENARIO_SANDBOX_OK)
	@touch $@

smartML/test/decompile/all_ok: smartML/build_ok $(DECOMPILE_OK)
	@touch $@

test-compile-incremental: .phony
	@$(MAKE) test_baselines/compile/all_ok

test-scenario-incremental: .phony
	@$(MAKE) test_baselines/scenario/all_ok

test-scenario-sandbox-incremental: .phony
	@$(MAKE) test_baselines/scenario_sandbox/all_ok

test-decompile-incremental: .phony
	@$(MAKE) smartML/test/decompile/all_ok

test-compile: .phony
	$(MAKE) test-compile-purge
	$(MAKE) test-compile-incremental

test-scenario: .phony
	$(MAKE) test-scenario-purge
	$(MAKE) test-scenario-incremental

test-value-pack: .phony
	@$(MAKE) smartML/test/value-pack.ok

test-decompile: .phony
	$(MAKE) test-decompile-purge
	$(MAKE) test-decompile-incremental

build-local-tezos: .phony
	@smartML/test/sandbox-scenarios.sh buildlocal

kill_sandbox: .phony
	@lsof -i :20000 -t | xargs kill || :

test-sandbox-scenarios: .phony build-local-tezos
	$(MAKE) kill_sandbox
	@method=local smartML/test/sandbox-scenarios.sh fullrun
	git status

test-user-installation: .phony
	sh scripts/test-user-installation.sh

test: .phony
	@$(MAKE) test-quick
	@$(MAKE) test-sandbox-scenarios
	@$(MAKE) test-user-installation

test-quick: .phony
	@$(MAKE) smartML/build_ok
	@$(MAKE) test-scenario
	@$(MAKE) test-compile
	@$(MAKE) test-unit
	@$(MAKE) test-value-pack
	@$(MAKE) test-decompile
	git status

test-quick-incremental: .phony
	@$(MAKE) smartML/build_ok
	@$(MAKE) test-scenario-incremental
	@$(MAKE) test-compile-incremental
	@$(MAKE) test-unit
	@$(MAKE) test-decompile-incremental
	git status

incremental: .phony
	@$(MAKE) fmt
	@$(MAKE) doc/all_ok
	@$(MAKE) test-quick-incremental

git-hooks-install:
	for h in $(GIT_HOOKS); do ln -s ../../.githooks/$$h .git/hooks || :; done

.phony:

.PHONY: .phony

doc/all_ok: doc/*.md site/*.md
	asciidoctor doc/reference.md      -a toc=left -a linkcss --destination-dir site/demo/
	asciidoctor doc/faq.md            -a toc=left -a linkcss --destination-dir site/demo/
	asciidoctor doc/editor_help.md    -a toc=left -a linkcss --destination-dir site/demo/
	touch doc/all_ok
	asciidoctor site/releases.md      -a toc=left -a linkcss --destination-dir site/
	python private/build_completion.py

referenceManual: .phony
	@rm -f doc/all_ok
	$(MAKE) doc/all_ok

doc: .phony
	dune build @doc
	@echo -e '\n\n\n'
	@echo -e 'SmartML doc:\n  open _build/default/_doc/_html/smart-ml/index.html'
	@echo -e 'SmartPy doc:\n  open site/demo/reference.html'

latest_release:
	git log -n1 release


ifeq (,$(wildcard env/current))
  $(error Please run './env/nix/init' or './env/naked/init' before running make)
endif

ifndef SMARTPY_ENV
  ifeq ($(shell readlink env/current),nix/)
    $(warning Initializing a new nix shell for each command. Run 'make' under './envsh' or directly as './with_env make' for better performance.)
  endif
  SHELL=./envsh
endif
