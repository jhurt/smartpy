{ stdenv }:

stdenv.mkDerivation {
  name = "g_plus_plus";
  unpackPhase = "true";
  buildPhase = "true";
  installPhase = "mkdir -p $out/bin && ln -s ${stdenv.cc}/bin/c++ $out/bin/g++";
}
