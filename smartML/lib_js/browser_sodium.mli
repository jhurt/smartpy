(* Copyright 2019-2020 Smart Chain Arena LLC. *)

(** Bindings to libsodium (generated with [gen_js_api]). *)

(** The type of the main [sodium] object setup by the JS library. *)
type sodium

val instance : sodium [@@js.global "sodium"]

(** Encapsulation of the byte-arrays used by the library. *)
module Crypto_bytes : sig
  type t = private Ojs.t

  val to_hex : t -> string [@@js.global "sodium.to_hex"]

  val of_hex : string -> t [@@js.global "sodium.from_hex"]

  val pp : t -> unit [@@js.global "console.log"]

  val append : t -> t -> t [@@js.global "eztz.utility.mergebuf"]

  module B58_prefix : sig
    type t = private Ojs.t

    val edsig : t [@@js.global "eztz.prefix.edsig"]

    val spsig1 : t [@@js.global "eztz.prefix.spsig1"]

    val p2sig : t [@@js.global "eztz.prefix.p2sig"]

    val generic_sig : t [@@js.global "eztz.prefix.sig"]

    val edpk : t [@@js.global "eztz.prefix.edpk"]

    val sppk : t [@@js.global "eztz.prefix.sppk"]

    val p2pk : t [@@js.global "eztz.prefix.p2pk"]

    val tz1 : t [@@js.global "eztz.prefix.tz1"]

    val tz2 : t [@@js.global "eztz.prefix.tz2"]

    val tz3 : t [@@js.global "eztz.prefix.tz3"]

    val kt : t [@@js.global "eztz.prefix.KT"]

    val edsk32 : t
      [@@js.global "eztz.prefix.edsk2"]
    (** Ed25519 Secret-keys in the tezos-style (a.k.a. “seed”). *)

    val edsk64 : t
      [@@js.global "eztz.prefix.edsk"]
    (** Ed25519 Secret-keys in the NaCl-style (public and secret keys
        concatenated). *)
  end

  val to_b58_check : t -> prefix:B58_prefix.t -> string
    [@@js.global "eztz.utility.b58cencode"]

  val of_b58_check : string -> prefix:B58_prefix.t -> t
    [@@js.global "eztz.utility.b58cdecode"]
end

val crypto_hash_sha256 : Crypto_bytes.t -> Crypto_bytes.t
  [@@js.global "sodium.crypto_hash_sha256"]

val crypto_hash_sha512 : Crypto_bytes.t -> Crypto_bytes.t
  [@@js.global "sodium.crypto_hash_sha512"]

val crypto_hash_blake_2b : size:int -> Crypto_bytes.t -> Crypto_bytes.t
  [@@js.global "sodium.crypto_generichash"]
(** Implementation of BLAKE2B, the [~size] argument seems was only
    tested with [32] and [64]. *)

module Ed25519 : sig
  val verify_signature :
       Crypto_bytes.t
    -> message:Crypto_bytes.t
    -> public_key:Crypto_bytes.t
    -> bool
    [@@js.global "sodium.crypto_sign_verify_detached"]

  val sign :
    message:Crypto_bytes.t -> secret_key:Crypto_bytes.t -> Crypto_bytes.t
    [@@js.global "sodium.crypto_sign_detached"]

  type keypair =
    { publicKey : Crypto_bytes.t
    ; privateKey : Crypto_bytes.t
    ; keyType : string }

  val keypair_of_seed : string -> keypair
    [@@js.global "sodium.crypto_sign_seed_keypair"]
  (** Make a deterministic key-pair from a 32-byte C-string.
      {[
        sodium.crypto_sign_seed_keypair('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
        {publicKey: Uint8Array(32), privateKey: Uint8Array(64), keyType: "ed25519"}
     ]} *)
end
