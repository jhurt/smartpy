(* Copyright 2019-2020 Smart Chain Arena LLC. *)

module Error : sig
  type t = {message : Base.string}

  val make : string -> t

  val pp_quick : Format.formatter -> t -> unit
end

module Decorated_result : sig
  type content =
    [ `Code of string list
    | `Error of Error.t
    | `Int of int
    | `O of (string * content) list
    | `Path of string
    | `Text of string
    ]

  type 'a t =
    { result : ('a, Error.t) result
    ; attach : content list }

  val return : 'a -> 'a t

  val fail : Error.t -> 'a t

  val bind : 'a t -> f:('a -> 'b t) -> 'b t

  val attach : a:content list -> 'a t -> 'a t
end

module type Tezos_client = sig
  module Io : sig
    type 'a t

    val return : 'a -> 'a t

    val bind : 'a t -> f:('a -> 'b t) -> 'b t
  end

  type state

  val originate :
       state
    -> id:int
    -> contract:string
    -> storage:string
    -> string Decorated_result.t Io.t

  val transfer :
       ?arg:string
    -> ?entry_point:string
    -> ?amount:int
    -> ?sender:Basics.account_or_address
    -> ?source:Basics.account_or_address
    -> state
    -> dst:string
    -> unit Decorated_result.t Io.t

  val get_contract_storage :
    state -> address:string -> string Decorated_result.t Io.t

  val run_script :
    state -> contract:string -> parameter:string -> unit Decorated_result.t Io.t
end

module History_event : sig
  type t = private
    { actions : Basics.action list
    ; status : [ `Failure | `None | `Success ]
    ; expecting_success : bool
    ; attachements : Decorated_result.content list }

  val make :
       ?actions:Basics.action list
    -> ?status:[ `Failure | `None | `Success ]
    -> ?expecting_success:bool
    -> ?attachements:Decorated_result.content list
    -> unit
    -> t
end

module Make_interpreter (Client : Tezos_client) (Prims : Primitives.Primitives) : sig
  module State : sig
    type t = private
      { client : Client.state
      ; smartml : Basics.scenario_state
      ; history : History_event.t Queue.t
      ; log_advancement : string -> unit }

    val of_smartml :
         ?log_advancement:(string -> unit)
      -> client:Client.state
      -> Basics.scenario_state
      -> t

    val fresh : client:Client.state -> unit -> t
  end

  val run :
    State.t -> Typing.env -> Scenario.t -> (unit, Error.t) result Client.Io.t
end
