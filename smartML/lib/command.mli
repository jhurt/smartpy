(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t = tcommand

val ifte : line_no:int -> texpr -> t -> t -> t

val ifteSome : line_no:int -> Expr.t -> t -> t -> t

val mk_match : line_no:int -> texpr -> (string * string * t) list -> t

val sp_failwith : line_no:int -> texpr -> t

val never : line_no:int -> texpr -> t

val verify : line_no:int -> texpr -> bool -> texpr option -> t

val forGroup : line_no:int -> string -> texpr -> t -> t

val whileLoop : line_no:int -> texpr -> t -> t

val delItem : line_no:int -> texpr -> texpr -> t

val updateSet : line_no:int -> texpr -> texpr -> bool -> t

val set : line_no:int -> texpr -> texpr -> t

val defineLocal : line_no:int -> string -> texpr -> t

val bind : line_no:int -> string option -> t -> t -> t

val seq : line_no:int -> t list -> t

val setType : line_no:int -> texpr -> Type.t -> t

val result : line_no:int -> texpr -> t

val mk_match_cons : line_no:int -> texpr -> string -> t -> t -> t

val comment : line_no:int -> string -> t
