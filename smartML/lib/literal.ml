(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils

type dynamic_id = {dynamic_id : int}
[@@deriving eq, ord, show {with_path = false}]

type static_id = {static_id : int}
[@@deriving eq, ord, show {with_path = false}]

type contract_id =
  | C_static  of static_id
  | C_dynamic of dynamic_id
[@@deriving eq, ord, show {with_path = false}]

type address =
  | Real  of string
  | Local of contract_id
[@@deriving show, eq, ord]

type sapling_test_state =
  { test : bool
  ; elements : (string * Bigint.t) list }
[@@deriving show, eq, ord]

type sapling_test_transaction =
  { source : string option
  ; target : string option
  ; amount : Bigint.t }
[@@deriving show, eq, ord]

type 'type_ f =
  | Unit
  | Bool                     of bool
  | Int                      of
      { i : Bigint.t
      ; is_nat : bool Type.unknown_ref
            [@equal fun _ _ -> true] [@compare fun _ _ -> 0] }
  | String                   of string
  | Bytes                    of string
  | Chain_id                 of string
  | Timestamp                of Bigint.t
  | Mutez                    of Bigint.t
  | Address                  of address * string option (* <- entry-point *)
  | Contract                 of
      address * string option (* <- entry-point *) * 'type_
  | Key                      of string
  | Secret_key               of string
  | Key_hash                 of string
  | Signature                of string
  | Sapling_test_state       of sapling_test_state
  | Sapling_test_transaction of sapling_test_transaction
[@@deriving eq, ord, show {with_path = false}, map, fold]

type t = Type.t f [@@deriving show {with_path = false}]

let equal = equal_f (fun _ _ -> true)

let compare = compare_f (fun _ _ -> 0)

let map_type = map_f

let type_of = function
  | Unit -> Type.unit
  | Bool _ -> Type.bool
  | Int {is_nat} -> Type.int_raw ~isNat:is_nat
  | String _ -> Type.string
  | Bytes _ -> Type.bytes
  | Chain_id _ -> Type.chain_id
  | Timestamp _ -> Type.timestamp
  | Mutez _ -> Type.token
  | Address _ -> Type.address
  | Contract (_, _, t) -> Type.contract t
  | Key _ -> Type.key
  | Secret_key _ -> Type.secret_key
  | Key_hash _ -> Type.key_hash
  | Signature _ -> Type.signature
  | Sapling_test_state _ -> Type.sapling_state
  | Sapling_test_transaction _ -> Type.sapling_transaction

let unit = Unit

let bool x = Bool x

let int i = Int {i; is_nat = ref (Type.UnValue false)}

let nat i = Int {i; is_nat = ref (Type.UnValue true)}

let intOrNat t i =
  match Type.getRepr t with
  | TInt {isNat} -> Int {i; is_nat = isNat}
  | _ -> assert false

let small_int i = int (Bigint.of_int i)

let small_nat i = nat (Bigint.of_int i)

let string s = String s

let bytes s = Bytes s

let chain_id s = Chain_id s

let timestamp i = Timestamp i

let mutez i = Mutez i

let meta_address ?entry_point s = Address (s, entry_point)

let address ?entry_point s = meta_address ?entry_point (Real s)

let local_address ?entry_point s = meta_address ?entry_point (Local s)

let meta_contract ?entry_point s t = Contract (s, entry_point, t)

let contract ?entry_point s = meta_contract ?entry_point (Real s)

let local_contract ?entry_point s = meta_contract ?entry_point (Local s)

let key s = Key s

let secret_key s = Secret_key s

let key_hash s = Key_hash s

let signature s = Signature s

let sapling_test_state elements = Sapling_test_state {test = true; elements}

let sapling_state_real () = Sapling_test_state {test = false; elements = []}

let sapling_test_transaction source target amount =
  Sapling_test_transaction {source; target; amount}

let unBool = function
  | Bool b -> b
  | _ -> failwith "Not a bool"

let unInt = function
  | Int {i} -> i
  | _ -> failwith "Not a int"
