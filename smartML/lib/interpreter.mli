(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Basics

type context_ =
  { sender : Literal.address
  ; source : Literal.address
  ; chain_id : string
  ; time : int
  ; amount : Bigint.t
  ; line_no : int
  ; debug : bool
  ; contract_id : Literal.contract_id option }

(** The initial state of the execution environment, a.k.a. this
    provides a “pseudo-blockchain.” *)
type context

val context_sender : context -> Literal.address

val context_time : context -> int

val context_line_no : context -> int

val context_debug : context -> bool

val context :
     ?contract_id:Literal.contract_id
  -> ?sender:Literal.address
  -> ?source:Literal.address
  -> ?chain_id:string
  -> time:int
  -> amount:Bigint.t
  -> line_no:int
  -> debug:bool
  -> unit
  -> context
(** Build a {!context}. *)

val pack_value : (module Primitives.Primitives) option -> Value.t -> string
(** Serialize a value into the same binary format as Michelson in the
    protocol.  *)

val interpret_message :
     primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> env:Typing.env
  -> context
  -> value_tcontract
  -> tmessage
  -> (value_tcontract * tcommand) option
     * tvalue operation list
     * Execution.error option
     * Execution.step list
(** Evaluation of a contract call ({!tmessage}) within a {!context}. *)

val interpret_expr_external :
     primitives:(module Primitives.Primitives)
  -> no_env:Basics.smart_except list
  -> scenario_state:scenario_state
  -> texpr
  -> tvalue
(** Evaluation of an expression ({!texpr}) within a {!context}. *)

val reducer :
     primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> line_no:int
  -> texpr
  -> texpr
