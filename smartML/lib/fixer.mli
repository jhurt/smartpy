(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

val fix_value_contract :
     Mangler.mangle_env
  -> Typing.env
  -> balance:tvalue
  -> ?storage:tvalue
  -> baker:tvalue
  -> entry_points_layout:Type.layout Type.unknown ref
  -> entry_points:tcommand entry_point list
  -> tstorage:Type.t
  -> tparameter:Type.t
  -> flags:Basics.flag list
  -> global_variables:(string * texpr) list
  -> value_tcontract

val fix_expr_contract :
  Mangler.mangle_env -> Typing.env -> tcontract -> tcontract
