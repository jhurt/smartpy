(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t =
  | Raw of string
  | Div of string * t list

let rec pp_render ppf = function
  | Raw x -> Format.pp_print_string ppf x
  | Div (args, xs) ->
      Format.fprintf ppf "<div %s>" args;
      List.iter (pp_render ppf) xs;
      Format.pp_print_string ppf "</div>"

let render x = Format.asprintf "%a" pp_render x

let div ?(args = "") xs = Div (args, xs)

type tab_ =
  { name : string
  ; tablink : string
  ; content : t
  ; lazy_tab : t Lazy.t option }

type tab = int -> int -> tab_

let tab_functions = Hashtbl.create 100

let call_tab id global_id =
  match Hashtbl.find_opt tab_functions (id, global_id) with
  | Some f -> Lazy.force f
  | None -> Raw ""

let tab ?active ?lazy_tab name inner global_id (id : int) =
  ( match lazy_tab with
  | None -> Hashtbl.remove tab_functions (id, global_id)
  | Some lazy_tab -> Hashtbl.replace tab_functions (id, global_id) lazy_tab );
  let call_f =
    if 0 <= global_id
    then Printf.sprintf "openTabLazy(event, %i, %i);" id global_id
    else Printf.sprintf "openTab(event, %i);" id
  in
  let tablink =
    Printf.sprintf
      "<button name='button_generatedMichelson' class='tablinks%s' \
       onclick='%s'>%s</button>"
      (if active <> None then " active" else "")
      call_f
      name
  in
  let content =
    div
      ~args:
        (Printf.sprintf
           "class='tabcontent' style='display: %s;'"
           (if active = None then "none" else "block"))
      [inner]
  in
  {name; tablink; lazy_tab; content}

let tabs ?(global_id = -1) title tabs =
  let tabs = List.mapi (fun i x -> x global_id i) tabs in
  Raw
    (Printf.sprintf
       "<div class='tabs'><div class='tab'><span \
        class='title'>%s</span>%s</div>%s</div>"
       title
       (String.concat "" (List.map (fun x -> x.tablink) tabs))
       (String.concat "" (List.map (fun x -> render x.content) tabs)))

let copy_div ~id ?(className = "white-space-pre") name inner =
  let r =
    Format.asprintf
      "<div class='menu'> <button \
       onClick='copyMichelson(this);'>Copy</button></div><div id='%s%s' \
       class='michelson %s'>%a</div>"
      name
      id
      className
      pp_render
      inner
  in
  Raw (Base.String.substr_replace_all ~pattern:"\n" ~with_:"<br>" r)

let contract_sizes_html
    ~primitives ~codeJson ~simplifiedCodeJson ~storageJson ~nb_bigmaps =
  let module P = (val primitives : Primitives.Primitives) in
  let codeSize = P.Tezize.calculateSize codeJson in
  let storageSize =
    match storageJson with
    | None -> 0.
    | Some storageJson -> P.Tezize.calculateSize storageJson
  in
  let pp name size =
    let percentage =
      Printf.sprintf "%f%%" (size *. 100.0 /. P.Tezize.sizeLimit)
    in
    Printf.sprintf
      "<tr><td class='pad10'><span class='item'>%s: </span></td><td \
       class='pad10'><div class='progress' style='height: 20px; width: \
       150px;'><div class='progress-bar' style='width:%s;' role='progressbar' \
       style='aria-valuenow='10'></div></div></td><td class='pad10, \
       code_right'>%.2f Bytes</td></tr>"
      name
      percentage
      size
  in
  let bigmapSize = float_of_int (nb_bigmaps * 32) in
  let sep, simplifiedCode, simplifiedCombined =
    match simplifiedCodeJson with
    | None -> ("", "", "")
    | Some simplifiedCodeJson ->
        let simplifiedCodeSize = P.Tezize.calculateSize simplifiedCodeJson in
        ( "<tr><td><hr></td><td></td><td></td></tr>"
        , pp "Simplified Code" simplifiedCodeSize
        , pp
            "Simplified Combined"
            (simplifiedCodeSize +. storageSize +. bigmapSize) )
  in
  let code = pp "Code" codeSize in
  let combined = pp "Combined" (codeSize +. storageSize +. bigmapSize) in
  let html =
    if 0. < storageSize +. float_of_int nb_bigmaps
    then
      Printf.sprintf
        "<table>%s%s%s%s%s%s%s%s</table>"
        (pp "Storage" storageSize)
        (pp (Printf.sprintf "Big map originations (%i)" nb_bigmaps) bigmapSize)
        sep
        code
        combined
        sep
        simplifiedCode
        simplifiedCombined
    else Printf.sprintf "<table>%s%s</table>" code simplifiedCode
  in
  Raw html

let michelson_html
    ~title ~lazy_tabs ~id ?simplified_contract ~primitives contract =
  let open Michelson in
  let storageJson =
    Base.Option.map
      contract.Michelson.Michelson_contract.storage
      ~f:(fun storage ->
        Format.asprintf
          "%a"
          (Micheline.pp_as_json ())
          (To_micheline.literal storage))
  in
  let codeJson =
    Format.asprintf
      "%a"
      (Micheline.pp_as_json ())
      (Michelson_contract.to_micheline contract)
  in
  let initialStorageHtml =
    copy_div
      ~id
      ~className:"white-space-pre-wrap"
      "storageCode"
      (Raw
         (Base.Option.value_map
            contract.Michelson.Michelson_contract.storage
            ~default:"missing storage"
            ~f:Michelson.string_of_instr_mliteral))
  in
  let codeHtml =
    lazy
      (div
         [ Raw "<h2>Michelson Code</h2>"
         ; div
             [ copy_div
                 ~id
                 "contractCode"
                 (Raw (Michelson_contract.to_html contract)) ] ])
  in
  let initialStorageHtmlJson =
    copy_div
      ~id
      "storageCodeJson"
      (Raw
         (Format.asprintf
            "<div class='white-space-pre'>%s</div>"
            (Option.value ~default:"missing storage" storageJson)))
  in
  let codeHtmlJson =
    copy_div
      ~id
      "contractCodeJson"
      (Raw
         ( try
             Format.asprintf "<div class='white-space-pre'>%s</div>" codeJson
           with
         | e ->
             Format.asprintf
               "<div class='white-space-pre'>Error: %s\n%s</div>"
               (Printexc.to_string e)
               (Printexc.get_backtrace ()) ))
  in
  let simplified =
    match simplified_contract with
    | Some simplified
      when not
             (Michelson.equal_tinstr
                contract.code
                simplified.Michelson_contract.code) ->
        Some simplified
    | _ -> None
  in
  let simplifiedCodeHtml =
    match simplified with
    | None -> None
    | Some simplified ->
        Some
          ( lazy
            (div
               [ Raw "<h2>Michelson Code</h2>"
               ; div
                   [ copy_div
                       ~id
                       "simplifiedContractCode"
                       (Raw (Michelson_contract.to_html simplified)) ] ]) )
  in
  let simplifiedCodeJson, simplifiedCodeHtmlJson =
    match simplified with
    | None -> (None, None)
    | Some simplified ->
      ( try
          let simplified =
            Format.asprintf
              "%a"
              (Micheline.pp_as_json ())
              (Michelson_contract.to_micheline simplified)
          in
          ( Some simplified
          , Some
              (copy_div
                 ~id
                 "simplifiedContractCodeJson"
                 (Raw
                    (Printf.sprintf
                       "<div class='white-space-pre'>%s</div>"
                       simplified))) )
        with
      | e ->
          ( None
          , Some
              (Raw
                 (Format.asprintf
                    "<div class='white-space-pre'>Error: %s\n%s</div>"
                    (Printexc.to_string e)
                    (Printexc.get_backtrace ()))) ) )
  in
  let nb_bigmaps =
    match contract.Michelson.Michelson_contract.storage with
    | None -> 0
    | Some storage -> Michelson.MLiteral.nb_bigmaps storage
  in
  let sizes =
    contract_sizes_html
      ~primitives
      ~codeJson
      ~storageJson
      ~simplifiedCodeJson
      ~nb_bigmaps
  in
  let tabs =
    tabs
      ~global_id:0
      title
      ( [ tab
            ~active:()
            "Sizes"
            (div [Raw "<h2>Sizes</h2>"; copy_div ~id "contractSizes" sizes])
        ; tab
            "Storage"
            (div [Raw "<h2>Initial Storage</h2>"; div [initialStorageHtml]])
        ; ( if lazy_tabs
          then tab "Code" ~lazy_tab:codeHtml (Raw "")
          else tab "Code" (Lazy.force codeHtml) ) ]
      @ ( match simplifiedCodeHtml with
        | Some simplifiedCodeHtml ->
            [ ( if lazy_tabs
              then tab "Simplified Code" ~lazy_tab:simplifiedCodeHtml (Raw "")
              else tab "Simplified Code" (Lazy.force simplifiedCodeHtml) ) ]
        | None -> [] )
      @ [ tab
            "Storage JSON"
            (div [Raw "<h2>Initial Storage</h2>"; div [initialStorageHtmlJson]])
        ; tab
            "Code JSON"
            (div [Raw "<h2>JSON Representation</h2>"; div [codeHtmlJson]]) ]
      @
      match simplifiedCodeHtmlJson with
      | Some simplifiedCodeHtmlJson ->
          [ tab
              "Simplified Code JSON"
              (div
                 [ Raw "<h2>JSON Representation</h2>"
                 ; div [simplifiedCodeHtmlJson] ]) ]
      | None -> [] )
  in
  tabs

let michelson_html_comp primitives ~accept_missings contract id =
  let tabs =
    michelson_html
      ~title:"Generated Michelson:"
      ~lazy_tabs:false
      ~id
      ~primitives
      contract
  in
  let michelson =
    Printf.sprintf
      "<button class='centertextbutton extramarginbottom' \
       onClick='gotoOrigination(contractSizes%s.innerHTML, \
       storageCode%s.innerText, contractCode%s.innerText, \
       storageCodeJson%s.innerText, \
       contractCodeJson%s.innerText,storageCode%s.innerHTML, \
       contractCode%s.innerHTML, storageCodeJson%s.innerHTML, \
       contractCodeJson%s.innerHTML)'>Deploy Michelson Contract</button>%s"
      id
      id
      id
      id
      id
      id
      id
      id
      id
      (render tabs)
  in
  (michelson, Michelson.Michelson_contract.has_error ~accept_missings contract)

let showLine line_no =
  Raw
    (Printf.sprintf
       "<button class=\"text-button\" onClick='showLine(%i)'>(line %i)</button>"
       line_no
       line_no)

let full_html
    ~primitives
    ~contract
    ~compiled_contract
    ~def
    ~onlyDefault
    ~id
    ~line_no
    ~accept_missings =
  let myName = "Contract" in
  let tab title content =
    tab ?active:(if def = title then Some () else None) title content
  in
  let michelson, has_error =
    michelson_html_comp primitives ~accept_missings compiled_contract id
  in
  let myTabs =
    [ tab
        "SmartPy"
        (Raw
           ( ( match contract.value_tcontract.unknown_parts with
             | Some msg ->
                 Printf.sprintf
                   "<span class='partialType'>Warning: unknown types or type \
                    errors: %s.</span><br>"
                   msg
             | None -> "" )
           ^ ( if has_error
             then
               "<span class='partialType'>Warning: errors in the Michelson \
                generated code.</span><br>"
             else "" )
           ^ Printer.tcontract_to_string ~options:Printer.Options.html contract
           ^ render (showLine line_no) ))
      (* ; tab
       *     "Storage"
       *     (Raw
       *        (Printf.sprintf
       *           "<div class='contract'>%s</div>"
       *           (Base.Option.value_map
       *              ~default:"missing storage"
       *              ~f:(Printer.html_of_data Printer.Options.html)
       *              contract.storage))) *)
    ; tab
        "Types"
        (Raw
           (Printf.sprintf
              "<div class='contract'><h3>Storage:</h3>%s<p><h3>Entry \
               points:</h3>%s</div>"
              (Printer.type_to_string
                 ~toplevel:()
                 ~options:Printer.Options.types
                 contract.value_tcontract.tstorage)
              (Printer.type_to_string
                 ~toplevel:()
                 ~options:Printer.Options.types
                 contract.value_tcontract.tparameter)))
      (* ; tab
       *     "Details"
       *     (Raw
       *        (Printer.contract_to_string
       *           ~options:Printer.Options.analysis
       *           contract)) *)
    ; tab "Deploy Michelson Contract" (Raw michelson)
    ; tab "&times;" (Raw "") ]
  in
  if onlyDefault
  then
    div
      ~args:"class='tabs'"
      (List.mapi
         (fun i tab ->
           let tab = tab (-1) i in
           if tab.name = def
           then div ~args:"class='tabcontentshow'" [tab.content]
           else Raw "")
         myTabs)
  else tabs myName myTabs

let nextInputGuiId = Value.nextId "inputGui_"

let nextOutputGuiId = Value.nextId "outputGui_"

let nextLazyOutputGuiId = Value.nextId "lazyOutputGui_"

let contextSimulationType =
  let t1 =
    Type.record_default_layout
      [ ("sender", Type.string)
      ; ("source", Type.string)
      ; ("timestamp", Type.string)
      ; ( "amount"
        , Type.variant_default_layout
            [("Tez", Type.string); ("Mutez", Type.string)] ) ]
  in
  let t2 =
    Type.record_default_layout [("debug", Type.bool); ("full_output", Type.bool)]
  in
  Type.record_default_layout [("context", t1); ("simulation", t2)]

let inputGui sim_id t tstorage buttonText ~line_no =
  let output = nextOutputGuiId () in
  let id = nextInputGuiId () ^ "_" in
  let nextId = Value.nextId id in
  let input = Value_gui.inputGuiR ~nextId t in
  let tstorage = Type.option tstorage in
  let input_storage = Value_gui.inputGuiR ~nextId tstorage in
  let contextInput = Value_gui.inputGuiR ~nextId contextSimulationType in
  div
    ~args:"class='simulationBuilder'"
    [ Raw (Printf.sprintf "<form><h3>Simulation Builder</h3>")
    ; Raw "<h4>Context</h4>"
    ; Raw contextInput.gui
    ; Raw "<h4>Edit Storage</h4>"
    ; Raw input_storage.gui
    ; Raw "<h4>Transaction</h4>"
    ; Raw input.gui
    ; Raw
        (Printf.sprintf
           "<button type='button' class='explorer_button' onClick=\"var t = \
            smartmlCtx.call_exn_handler('importType', '%s'); var tstorage = \
            smartmlCtx.call_exn_handler('importType', '%s');if (t) \
            smartmlCtx.call_exn_handler('callGui', '%s', %i, '%s', t, \
            tstorage,%i)\">%s</button></form>\n\
            <div id='%s'></div>"
           (Export.export_type t)
           (Export.export_type tstorage)
           id
           sim_id
           output
           line_no
           buttonText
           output) ]

let delayedInputGui t =
  let output = nextLazyOutputGuiId () in
  let id = nextInputGuiId () ^ "." in
  Raw
    (Printf.sprintf
       "<button type='button' \
        onClick=\"smartmlCtx.call_exn_handler('addInputGui', '%s', '%s', \
        smartmlCtx.call_exn_handler('importType', '%s'))\">%s</button>\n\
        <div id='%s'></div>"
       id
       output
       (Export.export_type t)
       "Add Another Step"
       output)

let simulatedContracts = Hashtbl.create 100

let simulation c id ~line_no =
  Hashtbl.replace simulatedContracts id c;
  let c = c.value_tcontract in
  inputGui id c.tparameter c.tstorage "Simulation" ~line_no
