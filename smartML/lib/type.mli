(* Copyright 2019-2020 Smart Chain Arena LLC. *)

type layout =
  | Layout_leaf of
      { source : string
      ; target : string }
  | Layout_pair of layout * layout
[@@deriving eq, show, ord]

type 'a unknown =
  | UnUnknown of string
  | UnValue   of 'a
  | UnRef     of 'a unknown ref
[@@deriving eq, show, ord]

type 'a unknown_ref = 'a unknown ref
[@@deriving eq, show {with_path = false}, ord]

type 't f = private
  | TUnit
  | TBool
  | TInt                of {isNat : bool unknown ref [@equal ( == )]}
  | TTimestamp
  | TString
  | TBytes
  | TRecord             of
      { layout : layout unknown ref [@equal ( == )]
      ; row : (string * 't) list }
  | TVariant            of
      { layout : layout unknown ref [@equal ( == )]
      ; row : (string * 't) list }
  | TSet                of {telement : 't}
  | TMap                of
      { big : bool unknown ref [@equal ( == )]
      ; tkey : 't
      ; tvalue : 't }
  | TAddress
  | TContract           of 't
  | TKeyHash
  | TKey
  | TSignature
  | TToken
  | TUnknown            of 't unknownType_f ref [@equal ( == )]
  | TPair               of 't * 't
  | TList               of 't
  | TLambda             of 't * 't
  | TChainId
  | TSecretKey
  | TOperation
  | TSaplingState
  | TSaplingTransaction
  | TNever

and 't unknownType_f =
  | UUnknown of string
  | URecord  of (string * 't) list
  | UVariant of (string * 't) list
  | UExact   of 't
[@@deriving eq, show, ord]

type t = {t : t f} [@@deriving eq, show, ord]

type unknownType = t unknownType_f

val cata : ('a f -> 'a) -> t -> 'a

type tvariable = string * t [@@deriving eq, show]

val default_layout : string list -> layout

val default_layout_of_row : (string * t) list -> layout

val comb_layout : [ `Left | `Right ] -> string list -> layout

val comb_layout_of_row : [ `Left | `Right ] -> (string * t) list -> layout

val layout_length : layout -> int

val full_unknown : unit -> t

val getRef : 'a unknown ref -> 'a unknown ref

val getRefOption : 'a unknown ref -> 'a option

val getRepr : t -> t f

val unit : t

val address : t

val contract : t -> t

val bool : t

val bytes : t

val key_hash : t

val int_raw : isNat:bool unknown ref -> t

val int : unit -> t

val nat : unit -> t

val intOrNat : unit -> t

val key : t

val key_value : t -> t -> t

val head_tail : t -> t -> t

val chain_id : t

val secret_key : t

val operation : t

val sapling_state : t

val sapling_transaction : t

val never : t

val map : big:bool unknown ref -> tkey:t -> tvalue:t -> t

val set : telement:t -> t

val record : layout unknown ref -> (string * t) list -> t

val variant : layout unknown ref -> (string * t) list -> t

val record_default_layout : (string * t) list -> t

val variant_default_layout : (string * t) list -> t

val record_or_unit : layout unknown ref -> (string * t) list -> t

val signature : t

val option : t -> t

val tor : t -> t -> t

val string : t

val timestamp : t

val token : t

val uvariant : string -> t -> t

val urecord : string -> t -> t

val unknown_raw : unknownType ref -> t

val account : t

val pair : t -> t -> t

val list : t -> t

val lambda : t -> t -> t

val unknown_record_layout : layout -> t

val unknown_variant_layout : layout -> t

val has_unknowns : t -> bool

val is_bigmap : t -> bool
