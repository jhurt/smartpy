(* Copyright 2019-2020 Smart Chain Arena LLC. *)

(** Debug printing functions. *)
module Dbg : sig
  val on : bool ref
  (** Off by default, use [Dbg.on := true] to activate. *)

  val p : (Format.formatter -> unit -> unit) -> unit

  val f : ('a, Format.formatter, unit, unit) format4 -> 'a

  val summary : ?len:int -> string -> string
end

module type JsonGetter = sig
  val null : bool

  val get : string -> Yojson.Basic.t

  val string : string -> string

  val string_option : string -> string option

  val int : string -> int

  val bool : string -> bool
end

val json_getter : Yojson.Basic.t -> (module JsonGetter)

val json_sub : (module JsonGetter) -> string -> (module JsonGetter)

(** Shared Utilities on hexadecimal encodings. *)
module Hex : sig
  val hexcape : string -> string

  val unhex : string -> string
end

val ( <|> ) : 'a option -> (unit -> 'a option) -> 'a option

val memoize : ?clear_after:int -> (('a -> 'b) -> 'a -> 'b) -> 'a -> 'b

val pp_with_max_indent :
  Format.formatter -> int -> (unit -> unit) -> unit -> unit

val pp_with_margin : Format.formatter -> int -> (unit -> unit) -> unit -> unit
