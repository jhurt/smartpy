(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Type

type env =
  { unknowns : (string, Type.t) Hashtbl.t
  ; constraints : (int * typing_constraint) list ref
  ; contract_data_types : (Literal.contract_id, Type.t) Hashtbl.t }

let init_env ?contract_data_types () =
  let contract_data_types =
    match contract_data_types with
    | Some c -> c
    | None -> Hashtbl.create 5
  in
  {unknowns = Hashtbl.create 10; constraints = ref []; contract_data_types}

let unknown importEnv i =
  if i = ""
  then failwith "empty unknown"
  else
    match Hashtbl.find_opt importEnv.unknowns i with
    | Some t -> t
    | None ->
        let r = unknown_raw (ref (UUnknown i)) in
        Hashtbl.add importEnv.unknowns i r;
        r

let intType isNat =
  match getRefOption isNat with
  | None -> `Unknown
  | Some true -> `Nat
  | Some false -> `Int

let add_constraint ~line_no env c =
  env.constraints := (line_no, c) :: !(env.constraints)

let assertEqual ~line_no ~pp ~env t1 t2 =
  if t1.t != t2.t then add_constraint ~line_no env (AssertEqual (t1, t2, pp))

let for_variant ~line_no ~env name t =
  match name with
  | "None" ->
      assertEqual ~line_no ~env t Type.unit ~pp:(fun () ->
          [`Text "Argument to None must be unit."]);
      option (full_unknown ())
  | "Some" -> option t
  | "Left" -> tor t (full_unknown ())
  | "Right" -> tor (full_unknown ()) t
  | _ -> uvariant name t

let reset_constraints env = {env with constraints = ref []}
