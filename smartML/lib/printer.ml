(* Copyright 2019-2020 Smart Chain Arena LLC. *)
open Utils
open Basics

module Options = struct
  type t =
    { html : bool
    ; stripStrings : bool
    ; stripped : bool
    ; types : bool
    ; analysis : bool }

  let string =
    { html = false
    ; stripStrings = false
    ; stripped = false
    ; types = false
    ; analysis = false }

  let html = {string with html = true}

  let htmlStripStrings = {html with stripStrings = true}

  let types = {html with types = true}

  let analysis = {types with analysis = true}
end

let rec layout_to_string = function
  | Type.Layout_leaf {source; target} ->
      if source = target
      then Printf.sprintf "%S" source
      else Printf.sprintf "\"%s as %s\"" source target
  | Layout_pair (l1, l2) ->
      Printf.sprintf "(%s, %s)" (layout_to_string l1) (layout_to_string l2)

let layout_to_string layout =
  match Type.getRefOption layout with
  | None -> ""
  | Some l -> Printf.sprintf ".layout(%s)" (layout_to_string l)

let rec type_to_string ?toplevel ?(options = Options.string) ?(indent = "") t =
  let open Type in
  let shiftIdent = if options.html then "&nbsp;&nbsp;" else "  " in
  match getRepr t with
  | TBool -> "sp.TBool"
  | TString -> "sp.TString"
  | TTimestamp -> "sp.TTimestamp"
  | TBytes -> "sp.TBytes"
  | TInt {isNat} ->
    ( match Typing.intType isNat with
    | `Unknown -> "sp.TIntOrNat"
    | `Nat -> "sp.TNat"
    | `Int -> "sp.TInt" )
  | TRecord {row; layout} ->
      if options.html
      then
        Printf.sprintf
          "%s%s<span class='record'>{</span>%s<br>%s<span \
           class='record'>}%s</span>"
          (if toplevel = Some () then "" else "<br>")
          indent
          (String.concat
             ""
             (List.map
                (fun (s, t) ->
                  Printf.sprintf
                    "<br>%s<span class='record'>%s</span>: %s;"
                    (indent ^ shiftIdent)
                    s
                    (type_to_string
                       ~options
                       ~indent:(indent ^ shiftIdent ^ shiftIdent)
                       t))
                row))
          indent
          (layout_to_string layout)
      else
        Printf.sprintf
          "sp.TRecord(%s)%s"
          (String.concat
             ", "
             (List.map
                (fun (s, t) ->
                  Printf.sprintf "%s = %s" s (type_to_string ~options t))
                row))
          (layout_to_string layout)
  | TVariant {row; layout} ->
    ( match row with
    | [("None", _unit); ("Some", t)] ->
        Printf.sprintf
          "sp.TOption(%s)"
          (type_to_string ~options ~indent:(indent ^ shiftIdent ^ shiftIdent) t)
    | _ ->
        if options.html
        then
          let ppVariant (s, t) =
            let t =
              match getRepr t with
              | TUnit -> ""
              | _ ->
                  Printf.sprintf
                    " %s"
                    (type_to_string ~options ~indent:(indent ^ shiftIdent) t)
            in
            Printf.sprintf "%s<span class='variant'>| %s</span>%s" indent s t
          in
          (if toplevel = Some () then "" else "<br>")
          ^ String.concat "<br>" (List.map ppVariant row)
        else
          Printf.sprintf
            "sp.TVariant(%s)%s"
            (String.concat
               ", "
               (List.map
                  (fun (s, t) ->
                    Printf.sprintf "%s = %s" s (type_to_string ~options t))
                  row))
            (layout_to_string layout) )
  | TSet {telement} ->
      Printf.sprintf
        "sp.TSet(%s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) telement)
  | TMap {big; tkey; tvalue} ->
      let name =
        match getRefOption big with
        | None -> "sp.TMap??"
        | Some true -> "sp.TBigMap"
        | Some false -> "sp.TMap"
      in
      Printf.sprintf
        "%s(%s, %s)"
        name
        (type_to_string ~options ~indent:(indent ^ shiftIdent) tkey)
        (type_to_string ~options ~indent:(indent ^ shiftIdent) tvalue)
  | TToken -> "sp.TMutez"
  | TUnit -> "sp.TUnit"
  | TAddress -> "sp.TAddress"
  | TKeyHash -> "sp.TKeyHash"
  | TKey -> "sp.TKey"
  | TSecretKey -> "sp.TSecretKey"
  | TChainId -> "sp.TChainId"
  | TSignature -> "sp.TSignature"
  | TContract t ->
      Printf.sprintf
        "sp.TContract(%s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t)
  | TUnknown {contents = UExact t} -> type_to_string t
  | TUnknown {contents = UUnknown _} ->
      if options.html
      then "<span class='partialType'>unknown</span>"
      else Printf.sprintf "unknown"
  | TUnknown {contents = URecord l} ->
      Printf.sprintf
        "TRecord++(%s)"
        (String.concat
           ", "
           (List.map
              (fun (s, t) ->
                Printf.sprintf "%s = %s" s (type_to_string ~options t))
              l))
  | TUnknown {contents = UVariant l} ->
      Printf.sprintf
        "TVariant++(%s)"
        (String.concat
           " | "
           (List.map
              (fun (s, t) ->
                Printf.sprintf "%s %s" s (type_to_string ~options t))
              l))
  | TPair (t1, t2) ->
      Printf.sprintf
        "sp.TPair(%s, %s)"
        (type_to_string ~options t1)
        (type_to_string ~options t2)
  | TList t ->
      Printf.sprintf
        "sp.TList(%s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t)
  | TLambda (t1, t2) ->
      Printf.sprintf
        "sp.TLambda(%s, %s)"
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t1)
        (type_to_string ~options ~indent:(indent ^ shiftIdent) t2)
  | TOperation -> "sp.TOperation"
  | TSaplingState -> "sp.TSaplingState"
  | TSaplingTransaction -> "sp.TSaplingTransaction"
  | TNever -> "sp.TNever"

let type_to_string ?toplevel ?(options = Options.string) t =
  if options.html
  then
    Printf.sprintf
      "<span class='type'>%s</span>"
      (type_to_string ?toplevel ~options t)
  else type_to_string ?toplevel ~options t

let ppAmount html amount =
  let oneMillion = Bigint.of_int 1000000 in
  let quotient, modulo = Big_int.quomod_big_int amount oneMillion in
  if html
  then
    let mutez = Big_int.string_of_big_int amount in
    let mutez =
      if String.length mutez < 7
      then String.sub "0000000" 0 (7 - String.length mutez) ^ mutez
      else mutez
    in
    Printf.sprintf
      "%s.%s<img height=20 width=20 src='./svgs/tezos-xtz-logo.svg' alt='tz'/>"
      (String.sub mutez 0 (String.length mutez - 6))
      (String.sub mutez (String.length mutez - 6) 6)
  else if Big_int.compare_big_int modulo Big_int.zero_big_int = 0
  then Printf.sprintf "sp.tez(%s)" (Big_int.string_of_big_int quotient)
  else Printf.sprintf "sp.mutez(%s)" (Big_int.string_of_big_int amount)

let string_of_contract_id = function
  | Literal.C_static {static_id} -> Printf.sprintf "%i" static_id
  | Literal.C_dynamic {dynamic_id} -> Printf.sprintf "Dyn_%i" dynamic_id

let literal_to_string ~html ?strip_strings =
  let to_string (l : Literal.t) =
    let open Format in
    let entry_point_opt ppf = function
      | None -> fprintf ppf ""
      | Some ep -> fprintf ppf "%%%s" ep
    in
    match l with
    | Unit -> "sp.unit"
    | Bool x -> String.capitalize_ascii (string_of_bool x)
    | Int {i} -> Big_int.string_of_big_int i
    | String s when strip_strings = Some () -> s
    | String s -> sprintf "'%s'" s
    | Bytes s -> sprintf "sp.bytes('0x%s')" (Misc.Hex.hexcape s)
    | Chain_id s -> sprintf "sp.chain_id('0x%s')" (Misc.Hex.hexcape s)
    | Mutez i -> ppAmount html i
    | Address (Real s, epo) ->
        asprintf "sp.address('%s%a')" s entry_point_opt epo
    | Address (Local l, epo) ->
        asprintf
          "sp.contract_address(Contract%s%a)"
          (string_of_contract_id l)
          entry_point_opt
          epo
    | Contract (Real s, epo, t) ->
        asprintf
          "sp.contract(%s, sp.address('%s%a')).open_some()"
          (type_to_string t)
          s
          entry_point_opt
          epo
    | Contract (Local l, epo, t) ->
        asprintf
          "sp.contract(%s, sp.contract_address(Contract%s%a)).open_some()"
          (type_to_string t)
          (string_of_contract_id l)
          entry_point_opt
          epo
    | Timestamp i -> sprintf "sp.timestamp(%s)" (Big_int.string_of_big_int i)
    | Key s -> sprintf "sp.key('%s')" s
    | Secret_key s -> sprintf "sp.secret_key('%s')" s
    | Key_hash s -> sprintf "sp.key_hash('%s')" s
    | Signature s -> sprintf "sp.signature('%s')" s
    | Sapling_test_state _ -> "sapling test state"
    | Sapling_test_transaction _ -> "sapling test transaction"
  in

  to_string

let unrec = function
  | {v = Record l} -> List.map snd l
  | x -> [x]

let is_range_keys l =
  let rec aux n = function
    | [] -> true
    | ({v = Literal (Int {i})}, _) :: l ->
        Big_int.eq_big_int (Bigint.of_int n) i && aux (n + 1) l
    | (_, _) :: _ -> false
  in
  aux 0 l

let is_vector v =
  match v.v with
  | Map l -> if is_range_keys l then Some (List.map snd l) else None
  | _ -> None

let is_matrix l =
  match is_vector l with
  | None -> None
  | Some vect ->
      let new_line lines a =
        match (lines, is_vector a) with
        | None, _ | _, None -> None
        | Some lines, Some v -> Some (v :: lines)
      in
      ( match List.fold_left new_line (Some []) vect with
      | Some lines
        when 2 <= List.length lines
             && List.exists (fun line -> 2 <= List.length line) lines ->
          Some ([], List.rev lines)
      | _ -> None )

let html_of_record_list (columns, data) f =
  let ppRow row =
    Printf.sprintf
      "<tr>%s</tr>"
      (String.concat
         ""
         (List.map
            (fun s -> Printf.sprintf "<td class='data'>%s</td>" (f s))
            row))
  in
  Printf.sprintf
    "<table class='recordList'><tr>%s</tr>%s</table>"
    (String.concat
       "\n"
       (List.map
          (fun s ->
            Printf.sprintf
              "<td class='dataColumn'>%s</td>"
              (String.capitalize_ascii s))
          columns))
    (String.concat "\n" (List.map ppRow data))

let is_record_list = function
  | {v = Record _} :: _ -> true
  | _ -> false

let rec value_to_string
    ?(deep = false) ?(noEmptyList = false) ?(options = Options.string) v =
  match v.v with
  | Literal (Bool x) -> String.capitalize_ascii (string_of_bool x)
  | Literal (Int {i}) ->
      let pp = Big_int.string_of_big_int i in
      if options.html && not deep
      then Printf.sprintf "<input type='text' value='%s' readonly></input>" pp
      else pp
  | Literal (String s) ->
      if options.stripStrings then s else Printf.sprintf "'%s'" s
  | Literal (Bytes s) ->
      if options.html
      then Printf.sprintf "<span class='bytes'>0x%s</span>" (Misc.Hex.hexcape s)
      else Printf.sprintf "sp.bytes('0x%s')" (Misc.Hex.hexcape s)
  | Literal (Chain_id s) ->
      if options.html
      then Printf.sprintf "<span class='bytes'>0x%s</span>" (Misc.Hex.hexcape s)
      else Printf.sprintf "sp.chain_id('0x%s')" (Misc.Hex.hexcape s)
  | Record l ->
      let rec f = function
        | Type.Layout_leaf {source; target} -> [(source, target)]
        | Layout_pair (l1, l2) -> f l1 @ f l2
      in
      let layout =
        match Type.getRepr v.t with
        | TRecord {layout} -> Some layout
        | _ -> None
      in
      let fields =
        let layout =
          match layout with
          | None -> None
          | Some layout -> Type.getRefOption layout
        in
        match layout with
        | None -> List.map (fun (x, _) -> (x, x)) l
        | Some layout -> f layout
      in
      if options.html
      then
        html_of_record_list
          ( List.map snd fields
          , [List.map (fun (source, _) -> List.assoc source l) fields] )
          (fun s -> value_to_string ~noEmptyList:true ~options s)
      else
        Printf.sprintf
          "sp.record(%s)"
          (String.concat
             ", "
             (List.map
                (fun (n, _) ->
                  let x = List.assoc n l in
                  Printf.sprintf "%s = %s" n (value_to_string ~options x))
                fields))
  | Variant (name, v) when options.html ->
      Printf.sprintf
        "<div class='subtype'><select class='selection'><option \
         value='%s'>%s</option></select>%s</div>"
        name
        (String.capitalize_ascii name)
        (value_to_string v ~deep ~noEmptyList ~options)
  | Variant ("None", {v = Literal Unit}) ->
      if options.html then "None" else "sp.none"
  | Variant (name, {v = Literal Unit}) -> name
  | Variant ("Some", v) ->
      if options.html
      then value_to_string ~options v (* TODO ? *)
      else Printf.sprintf "sp.some(%s)" (value_to_string ~options v)
  | Variant (x, v) -> Printf.sprintf "%s(%s)" x (value_to_string ~options v)
  | List [] when noEmptyList -> ""
  | List l when deep && not (is_record_list l) ->
      if options.html
      then
        Printf.sprintf
          "[%s]"
          (String.concat
             ", "
             (List.map (value_to_string ~options ~deep:true) l))
      else
        Printf.sprintf
          "sp.list([%s])"
          (String.concat ", " (List.map (value_to_string ~options) l))
  | List l when options.html ->
      let l =
        match l with
        | {v = Record r} :: _ as l ->
            ( List.map fst r
            , List.map
                (function
                  | {v = Record r} -> List.map snd r
                  | _ -> assert false)
                l )
        | _ -> ([""], List.map (fun x -> [x]) l)
      in
      html_of_record_list l (fun s ->
          value_to_string ~noEmptyList:true ~deep:true ~options s)
  | List l ->
      Printf.sprintf
        "sp.list([%s])"
        (String.concat ", " (List.map (value_to_string ~options) l))
  | Set [] when noEmptyList -> ""
  | Set set ->
      if options.html
      then
        html_of_record_list
          ([""], List.map (fun x -> [x]) set)
          (fun s -> value_to_string ~noEmptyList:true ~deep:true ~options s)
      else
        Printf.sprintf
          "sp.set([%s])"
          (String.concat ", " (List.map (value_to_string ~options) set))
  | Map map ->
      if options.html
      then
        match is_matrix v with
        | Some (columns, l) ->
            html_of_record_list (columns, l) (fun s ->
                value_to_string ~noEmptyList:true ~options s)
        | None ->
            let result =
              match Type.getRepr v.t with
              | TMap {tvalue} ->
                ( match Type.getRepr tvalue with
                | TRecord {row} ->
                    Some
                      (html_of_record_list
                         ( "Key" :: List.map fst row
                         , List.map (fun (x, y) -> x :: unrec y) map )
                         (fun s -> value_to_string ~noEmptyList:true ~options s))
                | _ -> None )
              | _ -> None
            in
            ( match result with
            | None ->
                html_of_record_list
                  (["Key"; "Value"], List.map (fun (x, y) -> [x; y]) map)
                  (fun s -> value_to_string ~noEmptyList:true ~options s)
            | Some t -> t )
      else
        Printf.sprintf
          "{%s}"
          (String.concat
             ", "
             (List.map
                (fun (k, v) ->
                  Printf.sprintf
                    "%s : %s"
                    (value_to_string ~options k)
                    (value_to_string ~options v))
                map))
  | Literal Unit -> if options.html then "" else "Unit"
  | Literal (Key_hash s) ->
      if options.html
      then Printf.sprintf "<span class='key'>%s</span>" s
      else Printf.sprintf "sp.key_hash('%s')" s
  | Literal (Key s) ->
      if options.html
      then Printf.sprintf "<span class='key'>%s</span>" s
      else Printf.sprintf "sp.key('%s')" s
  | Literal (Secret_key s) ->
      if options.html
      then Printf.sprintf "<span class='key'>%s</span>" s
      else Printf.sprintf "sp.secret_key('%s')" s
  | Literal (Signature s) ->
      if options.html
      then Printf.sprintf "<span class='signature'>%s</span>" s
      else Printf.sprintf "sp.signature('%s')" s
  | Literal ((Address (Real s, _) | Contract (Real s, _, _)) as lit) ->
      if options.html
      then Printf.sprintf "<span class='address'>%s</span>" s
      else literal_to_string ~html:false lit
  | Literal ((Address (Local s, ep) | Contract (Local s, ep, _)) as lit) ->
      if options.html
      then
        Printf.sprintf
          "<span class='address'>Contract-%s%s</span>"
          (string_of_contract_id s)
          (Base.Option.value_map ~default:"" ep ~f:(Printf.sprintf "%%%s"))
      else literal_to_string ~html:false lit
  | Literal (Timestamp i) ->
      if options.html
      then
        Printf.sprintf
          "<span class='timestamp'>timestamp(%s)</span>"
          (Big_int.string_of_big_int i)
      else Printf.sprintf "sp.timestamp(%s)" (Big_int.string_of_big_int i)
  | Literal (Mutez i) ->
      let amount = ppAmount options.html i in
      if options.html
      then Printf.sprintf "<span class='token'>%s</span>" amount
      else amount
  | Pair (v1, v2) ->
      if options.html
      then
        html_of_record_list
          ([], [[v1; v2]])
          (fun s -> value_to_string ~noEmptyList:true ~options s)
      else
        Printf.sprintf
          "(%s, %s)"
          (value_to_string ~noEmptyList:true ~options v1)
          (value_to_string ~noEmptyList:true ~options v2)
  | Closure _ -> Printf.sprintf "lambda(%s)" (type_to_string v.t)
  | Operation _ -> assert false
  | Literal (Sapling_test_state {test = false}) ->
      if options.html
      then html_of_record_list (["SaplingState"], []) (fun s -> s)
      else
        Printf.sprintf
          "[%s]"
          (String.concat "; " (List.map (String.concat ",") []))
  | Literal (Sapling_test_state {elements}) ->
      let l =
        List.map
          (fun (key, amount) -> [key; Bigint.string_of_big_int amount])
          (List.sort compare elements)
      in
      if options.html
      then html_of_record_list (["key"; "amount"], l) (fun s -> s)
      else
        Printf.sprintf
          "[%s]"
          (String.concat "; " (List.map (String.concat ",") l))
  | Literal (Sapling_test_transaction {source; target; amount}) ->
      Printf.sprintf
        "sp.sapling_test_transaction(%S, %S, %s)"
        (Utils.Option.default "" source)
        (Utils.Option.default "" target)
        (Bigint.string_of_big_int amount)

let vclass_to_string = function
  | Storage -> "storage"
  | Local -> "local"
  | Param -> "param"
  | Iter -> "iter"
  | ListMap -> "list_map"
  | MatchCons -> "match_cons"

let with_vclass ~html vclass x =
  if html
  then Printf.sprintf "<span class='%s'>%s</span>" (vclass_to_string vclass) x
  else x

let tvariable_to_string ?(options = Options.string) ?protect (s, t) vclass =
  let prot s = if protect = Some () then Printf.sprintf "(%s)" s else s in
  match options with
  | {html = false; types = false} -> s
  | {html; types = true} ->
      prot
        (Printf.sprintf
           "%s : <span class='type'>%s</span>"
           (with_vclass ~html vclass s)
           (type_to_string t))
  | {types = false} ->
      Printf.sprintf "<span class='%s'>%s</span>" (vclass_to_string vclass) s

let variable_to_string ?(options = Options.string) ?protect s vclass =
  let prot s = if protect = Some () then Printf.sprintf "(%s)" s else s in
  match options with
  | {html = false; types = false} -> s
  | {html; types = true} -> prot (with_vclass ~html vclass s)
  | {types = false} ->
      Printf.sprintf "<span class='%s'>%s</span>" (vclass_to_string vclass) s

let string_of_binOpInfix = function
  | BNeq -> "!="
  | BEq -> "=="
  | BAnd -> "&"
  | BOr -> "|"
  | BAdd -> "+"
  | BSub -> "-"
  | BDiv -> "//"
  | BEDiv -> "ediv"
  | BMul -> "*"
  | BMod -> "%"
  | BLt -> "<"
  | BLe -> "<="
  | BGt -> ">"
  | BGe -> ">="
  | BLsl -> "<<"
  | BLsr -> ">>"
  | BXor -> "^"

let string_of_binOpPrefix = function
  | BMax -> "max"
  | BMin -> "min"

let rec expr_to_string_with_lambdas
    lambdas ?(options = Options.string) ?protect e =
  let prot s = if protect = Some () then Printf.sprintf "(%s)" s else s in
  let htmlClass name s =
    if options.html
    then Printf.sprintf "<span class='%s'>%s</span>" name s
    else s
  in
  let putSelf s = Printf.sprintf "%s.%s" (htmlClass "self" "self") s in
  let to_string = expr_to_string_with_lambdas lambdas ~options in
  match e.expr with
  | EPrim0 prim ->
    begin
      match prim with
      | ECst v ->
          htmlClass
            "constant"
            (literal_to_string
               ~html:false
               ?strip_strings:(if options.stripStrings then Some () else None)
               v)
      | EBalance -> "sp.balance"
      | ESender -> "sp.sender"
      | ESource -> "sp.source"
      | EAmount -> "sp.amount"
      | ENow -> "sp.now"
      | EChain_id -> "sp.chain_id"
      | EIter x -> with_vclass Iter ~html:options.html x
      | EMatchCons x -> with_vclass MatchCons ~html:options.html x
      | EParams _ -> Printf.sprintf "params"
      | ELocal "__operations__" -> "sp.operations()"
      | ELocal "__storage__" -> putSelf "data"
      | ELocal name -> Printf.sprintf "%s.value" name
      | EGlobal name -> Printf.sprintf "self.%s" name
      | EVariant_arg arg_name -> arg_name
      | ESelf _ -> "sp.self"
      | ESelf_entry_point (name, _) ->
          Printf.sprintf "sp.self_entry_point('%s')" name
      | ESaplingEmptyState -> "sp.sapling_empty_state()"
      | EContract_balance id ->
          Printf.sprintf "sp.contract_balance(%s)" (string_of_contract_id id)
      | EContract_baker id ->
          Printf.sprintf "sp.contract_baker(%s)" (string_of_contract_id id)
      | EContract_data id ->
          Printf.sprintf "sp.contract_data(%s)" (string_of_contract_id id)
      | EScenario_var (id, _) -> Printf.sprintf "sp.scenario_var(%d)" id
      | EAccount_of_seed {seed} -> Printf.sprintf "sp.test_account(%S)" seed
    end
  | EPrim1 (prim, x) ->
    begin
      match prim with
      | EReduce -> Printf.sprintf "sp.reduce(%s)" (to_string x)
      | EListRev -> Printf.sprintf "%s.rev()" (to_string ~protect:() x)
      | EListItems false ->
          Printf.sprintf "%s.items()" (to_string ~protect:() x)
      | EListKeys false -> Printf.sprintf "%s.keys()" (to_string ~protect:() x)
      | EListValues false ->
          Printf.sprintf "%s.values()" (to_string ~protect:() x)
      | EListElements false ->
          Printf.sprintf "%s.elements()" (to_string ~protect:() x)
      | EListItems true ->
          Printf.sprintf "%s.rev_items()" (to_string ~protect:() x)
      | EListKeys true ->
          Printf.sprintf "%s.rev_keys()" (to_string ~protect:() x)
      | EListValues true ->
          Printf.sprintf "%s.rev_values()" (to_string ~protect:() x)
      | EListElements true ->
          Printf.sprintf "%s.rev_elements()" (to_string ~protect:() x)
      | EPack -> Printf.sprintf "sp.pack(%s)" (to_string x)
      | ENot -> prot (Printf.sprintf "~ %s" (to_string ~protect:() x))
      | EAbs -> prot (Printf.sprintf "abs(%s)" (to_string x))
      | EToInt -> prot (Printf.sprintf "sp.to_int(%s)" (to_string x))
      | EIsNat -> prot (Printf.sprintf "sp.is_nat(%s)" (to_string x))
      | ENeg -> prot (Printf.sprintf "- %s" (to_string ~protect:() x))
      | ESign -> prot (Printf.sprintf "sp.sign(%s)" (to_string x))
      | ESum -> Printf.sprintf "sp.sum(%s)" (to_string x)
      | EUnpack t ->
          Printf.sprintf "sp.unpack(%s, %s)" (to_string x) (type_to_string t)
      | EHash_key -> Printf.sprintf "sp.hash_key(%s)" (to_string x)
      | EHash algo ->
          Printf.sprintf
            "sp.%s(%s)"
            (String.lowercase_ascii (string_of_hash_algo algo))
            (to_string x)
      | EContract_address -> Printf.sprintf "sp.to_address(%s)" (to_string x)
      | EImplicit_account ->
          Printf.sprintf "sp.implicit_account(%s)" (to_string x)
      | EFirst -> Printf.sprintf "sp.fst(%s)" (to_string x)
      | ESecond -> Printf.sprintf "sp.snd(%s)" (to_string x)
      | EConcat_list -> Printf.sprintf "sp.concat(%s)" (to_string x)
      | ESize -> Printf.sprintf "sp.len(%s)" (to_string x)
      | ESetDelegate ->
          Printf.sprintf "sp.set_delegate_operation(%s)" (to_string x)
      | EType_annotation t ->
          Printf.sprintf
            "sp.set_type_expr(%s, %s)"
            (to_string x)
            (type_to_string t)
      | EAttr name -> Printf.sprintf "%s.%s" (to_string ~protect:() x) name
      | EOpenVariant name ->
        begin
          match (name, x) with
          | "Some", {expr = EPrim1 (EIsNat, x)} ->
              Printf.sprintf "sp.as_nat(%s)" (to_string x)
          | "Some", x -> Printf.sprintf "%s.open_some()" (to_string x)
          | name, x -> Printf.sprintf "%s.open_variant('%s')" (to_string x) name
        end
      | EIsVariant "Some" ->
          Printf.sprintf "%s.is_some()" (to_string ~protect:() x)
      | EIsVariant name ->
          Printf.sprintf "%s.is_variant('%s')" (to_string x) name
      | EVariant name ->
        begin
          match (name, x) with
          | "None", {expr = EPrim0 (ECst Unit)} -> "sp.none"
          | "Some", x -> Printf.sprintf "sp.some(%s)" (to_string x)
          | name, x -> Printf.sprintf "variant('%s', %s)" name (to_string x)
        end
    end
  | ECallLambda (lambda, parameter) ->
      Printf.sprintf
        "%s(%s)" (* "%s.__call__(%s)" *)
        (to_string ~protect:() lambda)
        (to_string parameter)
  | EApplyLambda (lambda, parameter) ->
      Printf.sprintf
        "%s.apply(%s)"
        (to_string ~protect:() lambda)
        (to_string parameter)
  | ELambda {id; body = {command = CResult r}} ->
      Printf.sprintf "sp.build_lambda(lambda lparams_%i: %s)" id (to_string r)
  | ELambda {id; body} ->
      let l = Printf.sprintf "f%d" id in
      let v = Printf.sprintf "lparams_%d" id in
      lambdas := (l, v, body) :: !lambdas;
      Printf.sprintf "sp.build_lambda(%s)" l
  | EMapFunction {f; l} ->
      Printf.sprintf "%s.map(%s)" (to_string ~protect:() l) (to_string f)
  | ELambdaParams {id} -> Printf.sprintf "lparams_%i" id
  | ECreate_contract _ -> Printf.sprintf "create contract ..."
  | EBinOpPre (f, x, y) ->
      Printf.sprintf
        "sp.%s(%s, %s)"
        (string_of_binOpPrefix f)
        (to_string x)
        (to_string y)
  | EBinOpInf (BEDiv, x, y) ->
      prot (Printf.sprintf "sp.ediv(%s, %s)" (to_string x) (to_string y))
  | EBinOpInf (op, x, y) ->
      prot
        (Printf.sprintf
           "%s %s %s"
           (to_string ~protect:() x)
           (string_of_binOpInfix op)
           (to_string ~protect:() y))
  | EItem {items; key; default_value = None; missing_message = None} ->
      Printf.sprintf "%s[%s]" (to_string ~protect:() items) (to_string key)
  | EItem {items; key; default_value = Some d} ->
      Printf.sprintf
        "%s.get(%s, default_value = %s)"
        (to_string ~protect:() items)
        (to_string key)
        (to_string d)
  | EItem {items; key; default_value = None; missing_message = Some message} ->
      Printf.sprintf
        "%s.get(%s, message = %s)"
        (to_string ~protect:() items)
        (to_string key)
        (to_string message)
  | ESplit_tokens
      ( {expr = EPrim0 (ECst (Mutez tok))}
      , quantity
      , {expr = EPrim0 (ECst (Int {i}))} )
    when Big_int.eq_big_int tok (Bigint.of_int 1000000)
         && Big_int.eq_big_int i (Bigint.of_int 1) ->
      Printf.sprintf "sp.tez(%s)" (to_string quantity)
  | ESplit_tokens
      ( {expr = EPrim0 (ECst (Mutez tok))}
      , quantity
      , {expr = EPrim0 (ECst (Int {i}))} )
    when Big_int.eq_big_int tok (Bigint.of_int 1)
         && Big_int.eq_big_int i (Bigint.of_int 1) ->
      Printf.sprintf "sp.mutez(%s)" (to_string quantity)
  | ESplit_tokens (e1, e2, e3) ->
      Printf.sprintf
        "sp.split_tokens(%s, %s, %s)"
        (to_string e1)
        (to_string e2)
        (to_string e3)
  | ERange (a, b, {expr = EPrim0 (ECst (Int {i = step}))})
    when Big_int.eq_big_int step (Bigint.of_int 1) ->
      Printf.sprintf "sp.range(%s, %s)" (to_string a) (to_string b)
  | ECons (e1, e2) ->
      Printf.sprintf "sp.cons(%s, %s)" (to_string e1) (to_string e2)
  | ERange (e1, e2, e3) ->
      Printf.sprintf
        "sp.range(%s, %s, %s)"
        (to_string e1)
        (to_string e2)
        (to_string e3)
  | EAdd_seconds (e1, e2) ->
      Printf.sprintf "sp.add_seconds(%s, %s)" (to_string e1) (to_string e2)
  | ECheck_signature (e1, e2, e3) ->
      Printf.sprintf
        "sp.check_signature(%s, %s, %s)"
        (to_string e1)
        (to_string e2)
        (to_string e3)
  | EUpdate_map (e1, e2, e3) ->
      Printf.sprintf
        "sp.update_map(%s, %s, %s)"
        (to_string e1)
        (to_string e2)
        (to_string e3)
  | ERecord entries ->
      Printf.sprintf
        "sp.record(%s)"
        (String.concat
           ", "
           (List.map
              (fun (n, e) -> Printf.sprintf "%s = %s" n (to_string e))
              entries))
  | EList l ->
      Printf.sprintf "sp.list([%s])" (String.concat ", " (List.map to_string l))
  | EMap (_, l) ->
      Printf.sprintf
        "{%s}"
        (String.concat
           ", "
           (List.map
              (fun (k, e) ->
                Printf.sprintf "%s : %s" (to_string k) (to_string e))
              l))
  | ESet l ->
      Printf.sprintf "sp.set([%s])" (String.concat ", " (List.map to_string l))
  | EContains (items, member) ->
      prot
        (Printf.sprintf
           "%s.contains(%s)"
           (to_string ~protect:() items)
           (to_string member))
  | EContract {arg_type; entry_point; address} ->
      Printf.sprintf
        "sp.contract(%s, %s%s)"
        (type_to_string arg_type)
        (to_string address)
        (Base.Option.value_map entry_point ~default:"" ~f:(fun ep ->
             Printf.sprintf ", entry_point='%s'" ep))
  | EPair (e1, e2) -> Printf.sprintf "(%s, %s)" (to_string e1) (to_string e2)
  | ESlice {offset; length; buffer} ->
      prot
        (Printf.sprintf
           "sp.slice(%s, %s, %s)"
           (to_string buffer)
           (to_string offset)
           (to_string length))
  | EMake_signature {secret_key; message; message_format} ->
      Printf.sprintf
        "sp.make_signature(secret_key = %s, message = %s, message_format = %s)"
        (to_string secret_key)
        (to_string message)
        ( match message_format with
        | `Hex -> "'Hex'"
        | `Raw -> "'Raw'" )
  | EMichelson (michelson, exprs) ->
      let mich ({name} : _ inline_michelson) exprs =
        Printf.sprintf
          "mi.%s(%s)"
          name
          (String.concat ", " (List.map to_string exprs))
      in
      ( match (michelson, exprs) with
      | [x], _ -> mich x exprs
      | michs, [] ->
          Printf.sprintf
            "mi.seq(%s)"
            (String.concat ", " (List.map (fun x -> mich x []) michs))
      | michs, _ ->
          Printf.sprintf
            "mi.seq(%s, %s)"
            (String.concat ", " (List.map (fun x -> mich x []) michs))
            (String.concat ", " (List.map to_string exprs)) )
  | ETransfer {arg; amount; destination} ->
      Printf.sprintf
        "sp.transfer_operation(%s, %s, %s)"
        (to_string arg)
        (to_string amount)
        (to_string destination)
  | EIf (cond, a, b) ->
      Printf.sprintf
        "sp.eif(%s, %s, %s)"
        (to_string cond)
        (to_string a)
        (to_string b)
  | EMatch (scrutinee, clauses) ->
      Printf.sprintf
        "sp.ematch(%s, %s)"
        (to_string scrutinee)
        (String.concat
           ", "
           (List.map
              (fun (cons, rhs) ->
                Printf.sprintf "(\"%s\", %s)" cons (to_string rhs))
              clauses))
  | ESaplingVerifyUpdate {transaction; state} ->
      Printf.sprintf
        "%s.verify_update(%s)"
        (to_string state)
        (to_string transaction)

let expr_to_string = expr_to_string_with_lambdas (ref [])

let rec pp_command ?(indent = "") ?(options = Options.string) ppf command =
  let pp_command = pp_command ~options in
  let shiftIdent = if options.html then "&nbsp;&nbsp;" else "  " in
  let lambdas = ref [] in
  let expr_to_string = expr_to_string_with_lambdas ~options lambdas in
  let r ppf =
    match command.command with
    | CNever message ->
        Format.fprintf ppf "%ssp.never(%s)" indent (expr_to_string message)
    | CFailwith message ->
        Format.fprintf ppf "%ssp.failwith(%s)" indent (expr_to_string message)
    | CIf (c, t, e) ->
        if options.html
        then (
          Format.fprintf
            ppf
            "%ssp.if %s:<br>%a"
            indent
            (expr_to_string c)
            (pp_command ~indent:(indent ^ shiftIdent))
            t;
          match e.command with
          | CResult {expr = EPrim0 (ECst Literal.Unit)} -> ()
          | _ ->
              Format.fprintf
                ppf
                "<br>%ssp.else:<br>%a"
                indent
                (pp_command ~indent:(indent ^ shiftIdent))
                e )
        else (
          Format.fprintf
            ppf
            "%ssp.if %s:\n%a"
            indent
            (expr_to_string c)
            (pp_command ~indent:(indent ^ shiftIdent))
            t;

          match e.command with
          | CResult {expr = EPrim0 (ECst Literal.Unit)} -> ()
          | _ ->
              Format.fprintf
                ppf
                "\n%ssp.else:\n%a"
                indent
                (pp_command ~indent:(indent ^ shiftIdent))
                e )
    | CMatch (scrutinee, [(constructor, arg_name, c)]) ->
        if options.html
        then
          Format.fprintf
            ppf
            "%swith %s.match('%s') as %s:<br>%a"
            indent
            (expr_to_string ~protect:() scrutinee)
            constructor
            arg_name
            (pp_command ~indent:(indent ^ shiftIdent))
            c
        else
          Format.fprintf
            ppf
            "%swith %s.match('%s') as %s:\n%a"
            indent
            (expr_to_string ~protect:() scrutinee)
            constructor
            arg_name
            (pp_command ~indent:(indent ^ shiftIdent))
            c
    | CMatch (scrutinee, cases) ->
        if options.html
        then
          Format.fprintf
            ppf
            "%swith %s.match_cases() as arg:<br>%a"
            indent
            (expr_to_string ~protect:() scrutinee)
            (fun ppf ->
              List.iter (fun (constructor, arg_name, c) ->
                  Format.fprintf
                    ppf
                    "%swith arg.match('%s') as %s:<br>%a<br>"
                    (indent ^ shiftIdent)
                    constructor
                    arg_name
                    (pp_command ~indent:(indent ^ shiftIdent ^ shiftIdent))
                    c))
            cases
        else
          Format.fprintf
            ppf
            "%swith %s.match_cases() as arg:\n%a"
            indent
            (expr_to_string ~protect:() scrutinee)
            (fun ppf ->
              List.iter (fun (constructor, arg_name, c) ->
                  Format.fprintf
                    ppf
                    "%swith arg.match('%s') as %s:\n%a\n"
                    (indent ^ shiftIdent)
                    constructor
                    arg_name
                    (pp_command ~indent:(indent ^ shiftIdent ^ shiftIdent))
                    c))
            cases
    | CMatchCons matcher ->
        if options.html
        then
          Format.fprintf
            ppf
            "%swith sp.match_cons(%s) as %s:<br>%a<br>%selse:<br>%a"
            indent
            (expr_to_string ~protect:() matcher.expr)
            matcher.id
            (pp_command ~indent:(indent ^ shiftIdent))
            matcher.ok_match
            indent
            (pp_command ~indent:(indent ^ shiftIdent))
            matcher.ko_match
        else
          Format.fprintf
            ppf
            "%swith sp.match_cons(%s) as %s:\n%a\n%selse:\n%s%a"
            indent
            (expr_to_string ~protect:() matcher.expr)
            matcher.id
            (pp_command ~indent:(indent ^ shiftIdent))
            matcher.ok_match
            indent
            indent
            (pp_command ~indent:(indent ^ shiftIdent))
            matcher.ko_match
    | CBind (None, c1, c2) ->
        if options.html
        then (
          pp_command ~indent ppf c1;
          Format.fprintf ppf "<br> %a" (pp_command ~indent) c2 )
        else (
          pp_command ~indent ppf c1;
          Format.fprintf ppf "\n%a" (pp_command ~indent) c2 )
    | CBind (Some x, {command = CResult e}, c2) ->
        if options.html
        then (
          Format.fprintf
            ppf
            "%s%s = sp.local(%S, %s)\n<br>"
            indent
            x
            x
            (expr_to_string e);
          pp_command ~indent ppf c2 )
        else (
          Format.fprintf
            ppf
            "%s%s = sp.local(%S, %s)\n"
            indent
            x
            x
            (expr_to_string e);
          pp_command ~indent ppf c2 )
    | CBind (Some x, c1, c2) ->
        if options.html
        then (
          Format.fprintf ppf "%s%s = sp.seq(%S)\n<br>" indent x x;
          Format.fprintf ppf "%swith %s:\n<br>" indent x;
          pp_command ~indent:(indent ^ shiftIdent) ppf c1;
          Format.fprintf ppf "\n<br>";
          Format.fprintf ppf "%swith sp.bind(%s):\n<br>" indent x;
          pp_command ~indent:(indent ^ shiftIdent) ppf c2 )
        else (
          Format.fprintf ppf "%s%s = sp.seq(%S)\n" indent x x;
          Format.fprintf ppf "%swith %s:\n" indent x;
          pp_command ~indent:(indent ^ shiftIdent) ppf c1;
          Format.fprintf ppf "\n";
          Format.fprintf ppf "%swith sp.bind(%s):\n" indent x;
          pp_command ~indent:(indent ^ shiftIdent) ppf c2 )
    | CResult {expr = EPrim0 (ECst Literal.Unit)} ->
        Format.fprintf ppf "%spass" indent
    | CResult r ->
        Format.fprintf ppf "%ssp.result(%s)" indent (expr_to_string r)
    | CFor (var, e, c) ->
        if options.html
        then
          Format.fprintf
            ppf
            "%ssp.for %s in %s:\n<br>%a"
            indent
            (variable_to_string ~options var Iter)
            (expr_to_string e)
            (pp_command ~indent:(indent ^ shiftIdent))
            c
        else
          Format.fprintf
            ppf
            "%ssp.for %s in %s:\n%a"
            indent
            (variable_to_string ~options var Iter)
            (expr_to_string e)
            (pp_command ~indent:(indent ^ shiftIdent))
            c
    | CWhile (e, c) ->
        if options.html
        then
          Format.fprintf
            ppf
            "%ssp.while %s:\n<br>%a"
            indent
            (expr_to_string e)
            (pp_command ~indent:(indent ^ shiftIdent))
            c
        else
          Format.fprintf
            ppf
            "%ssp.while %s:\n%a"
            indent
            (expr_to_string e)
            (pp_command ~indent:(indent ^ shiftIdent))
            c
    | CDefineLocal (name, {expr = EPrim1 (EType_annotation t, e)}) ->
        Format.fprintf
          ppf
          "%s%s = sp.local(%S, %s, %s)"
          indent
          (tvariable_to_string ~options (name, t) Local)
          (tvariable_to_string ~options (name, t) Local)
          (expr_to_string e)
          (type_to_string t)
    | CDefineLocal (name, e) ->
        Format.fprintf
          ppf
          "%s%s = sp.local(%S, %s)"
          indent
          (variable_to_string ~options name Local)
          (variable_to_string ~options name Local)
          (expr_to_string e)
    | CSetVar (s, t) ->
      ( match t.expr with
      | ECons (u, s') when s = s' ->
        ( match (s.expr, u.expr) with
        | ( EPrim0 (ELocal "__operations__")
          , ETransfer
              { destination =
                  { expr =
                      EPrim1
                        ( EOpenVariant "Some"
                        , {expr = EContract {arg_type = {t = TUnit}; address}}
                        ) }
              ; amount } ) ->
            Format.fprintf
              ppf
              "%ssp.send(%s, %s)"
              indent
              (expr_to_string address)
              (expr_to_string amount)
        | EPrim0 (ELocal "__operations__"), ETransfer {arg; amount; destination}
          ->
            Format.fprintf
              ppf
              "%ssp.transfer(%s, %s, %s)"
              indent
              (expr_to_string arg)
              (expr_to_string amount)
              (expr_to_string destination)
        | EPrim0 (ELocal "__operations__"), EPrim1 (ESetDelegate, e) ->
            Format.fprintf ppf "%ssp.set_delegate(%s)" indent (expr_to_string e)
        | _ ->
            Format.fprintf
              ppf
              "%s%s.push(%s)"
              indent
              (expr_to_string s)
              (expr_to_string u) )
      | EBinOpInf (((BAdd | BSub | BMul | BDiv) as op), s', u)
        when equal_expr s s' ->
          Format.fprintf
            ppf
            "%s%s %s= %s"
            indent
            (expr_to_string s)
            (string_of_binOpInfix op)
            (expr_to_string u)
      | _ ->
        ( match s.expr with
        | EItem _ | EPrim1 (EAttr _, _) ->
            Format.fprintf
              ppf
              "%s%s = %s"
              indent
              (expr_to_string s)
              (expr_to_string t)
        | EPrim0 (ELocal _) ->
            Format.fprintf
              ppf
              "%s%s = %s"
              indent
              (expr_to_string s)
              (expr_to_string t)
        | _ ->
            Format.fprintf
              ppf
              "%s%s.set(%s)"
              indent
              (expr_to_string s)
              (expr_to_string t) ) )
    | CDelItem (expr, item) ->
        Format.fprintf
          ppf
          "%s%s %s[%s]"
          indent
          (if options.html then "<span class='keyword'>del</span>" else "del")
          (expr_to_string expr)
          (expr_to_string item)
    | CUpdateSet (expr, element, add) ->
        Format.fprintf
          ppf
          "%s%s.%s(%s)"
          indent
          (expr_to_string expr)
          (if add then "add" else "remove")
          (expr_to_string element)
    | CVerify (e, ghost, message) ->
        Format.fprintf
          ppf
          "%ssp.%s(%s%s)"
          indent
          (if ghost then "staticVerify" else "verify")
          (expr_to_string e)
          ( match message with
          | None -> ""
          | Some msg -> Format.sprintf ", message = %s" (expr_to_string msg) )
    | CComment s -> Format.fprintf ppf "%s# %s" indent s
    | CSetType (e, t) ->
        Format.fprintf
          ppf
          "%ssp.set_type(%s, %s)"
          indent
          (expr_to_string e)
          (type_to_string t)
  in

  let r = Format.asprintf "%t" r in
  let print_lambda (l, v, body) =
    let ppc = pp_command ~indent:(indent ^ shiftIdent) in
    if options.html
    then Format.fprintf ppf "%sdef %s(%s):\n<br>%a\n<br>" indent l v ppc body
    else Format.fprintf ppf "%sdef %s(%s):\n%a\n" indent l v ppc body
  in
  List.iter print_lambda !lambdas;
  Format.fprintf ppf "%s" r

let command_to_string ?indent ?options c =
  Format.asprintf "%a" (pp_command ?indent ?options) c

let unRecordList = function
  | {v = Record l} -> (List.map fst l, [List.map snd l])
  | _data -> (["data"], [])

let html_of_data options data =
  html_of_record_list (unRecordList data) (fun s ->
      value_to_string ~noEmptyList:true ~options s)
  ^
  (*  Value.html_of_record_list (unRecordList data) (fun s -> value_to_string ~noEmptyList:true ~options (!checkImport s)) ^*)
  match options with
  | {html = true; types = true} ->
      type_to_string ~options data.t ^ "<br><br><br>"
  | _ -> ""

let init_html_of_data options (x, v) =
  Printf.sprintf
    "%s = %s"
    (tvariable_to_string ~options x Storage)
    (value_to_string v)

let init_html_of_data options : tvalue -> string list = function
  | {v = Record l} ->
      List.map (fun (n, v) -> init_html_of_data options ((n, v.t), v)) l
  | {v = Literal Unit} -> []
  | v -> [value_to_string v]

let unMutez = function
  | {v = Literal (Mutez b)} -> b
  | _ -> failwith "Not a mutez"

let pp_contract
    ?(options = Options.string)
    ppf
    {value_contract = {balance; storage; tstorage; entry_points}} =
  if options.html
  then (
    let init =
      if options.stripped
      then ""
      else
        match storage with
        | None ->
            Printf.sprintf
              "<div class='on'><span class='keyword'>def</span> \
               __init__(self)<span class='keyword'>:</span><div \
               class='indent5'>&nbsp;&nbsp;self.init_type(%s)</div></div><br>"
              (type_to_string tstorage)
        | Some storage ->
            Printf.sprintf
              "<div class='on'><span class='keyword'>def</span> \
               __init__(self)<span class='keyword'>:</span><div \
               class='indent5'>&nbsp;&nbsp;self.init(%s)</div></div><br>"
              (String.concat
                 ",<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                 (init_html_of_data options storage))
    in
    Format.fprintf
      ppf
      "<div class='contract'><h3>Balance: %s</h3>\n\
       <h3>Storage:</h3>\n\
       %s\n\
       <h3>Entry points:</h3>\n\
       %s\n"
      (ppAmount true (unMutez balance))
      (Base.Option.value_map ~default:"" ~f:(html_of_data options) storage)
      init;

    List.iteri
      (fun i {channel; originate; body} ->
        if i > 0 then Format.fprintf ppf "\n<br>";
        Format.fprintf
          ppf
          "<div class='on'>%s<span class='keyword'>def</span> %s(<span \
           class='self'>self</span>, params)<span \
           class='keyword'>:</span><br><div class='indent5'>%a</div></div>"
          ( if options.stripped
          then ""
          else if originate
          then "@sp.entry_point<br>"
          else "@sp.private_entry_point<br>" )
          channel
          (fun ppf c -> pp_command ~options ~indent:"&nbsp;&nbsp;" ppf c)
          body)
      entry_points;

    Format.fprintf ppf "</div>" )
  else
    let init =
      if options.stripped
      then ""
      else
        match storage with
        | None ->
            Printf.sprintf
              "  def __init__(self):\n    self.init_type(%s)\n"
              (type_to_string tstorage)
        | Some storage ->
            Printf.sprintf
              "  def __init__(self):\n    self.init(%s)\n"
              (String.concat ", " (init_html_of_data options storage))
    in
    Format.fprintf
      ppf
      "import smartpy as sp\n\nclass Contract(sp.Contract):\n%s"
      init;

    List.iteri
      (fun i {channel; body} ->
        if i > 0 then Format.fprintf ppf "\n";
        Format.fprintf
          ppf
          "%s  def %s(self, params):\n%a"
          (if options.stripped then "" else "\n  @sp.entry_point\n")
          channel
          (fun ppf c -> pp_command ~options ~indent:"    " ppf c)
          body)
      entry_points

let contract_to_string ?options c =
  Format.asprintf "%a" (pp_contract ?options) c

let operation_to_string ?(options = Options.string) operation =
  match operation with
  | Transfer {arg; destination; amount} ->
      Printf.sprintf
        "<div class='operation'>Transfer %s to  %s<br>%s</div>"
        (value_to_string ~options amount)
        (value_to_string ~options destination)
        (value_to_string ~options arg)
  | SetDelegate None -> "<div class='operation'>Remove Delegate</div>"
  | SetDelegate (Some d) ->
      Printf.sprintf
        "<div class='operation'>Set Delegate(%s)</div>"
        (value_to_string ~options d)
  | CreateContract {id; baker; contract} ->
      Printf.sprintf
        "<div class='operation'>Create Contract(id: %s, baker: %s)%s</div>"
        (string_of_contract_id id)
        (value_to_string baker)
        (Base.Option.value_map
           ~f:(value_to_string ~options)
           ~default:""
           contract.storage)

let pp_tcontract ?options fmt c =
  pp_contract ?options fmt (erase_types_contract (layout_records_contract c))

let texpr_to_string ?options ?protect x =
  expr_to_string ?options ?protect (erase_types_expr (layout_records_expr x))

let tcommand_to_string ?indent ?options x =
  command_to_string
    ?indent
    ?options
    (erase_types_command (layout_records_command x))

let tcontract_to_string ?options c =
  contract_to_string ?options (erase_types_contract (layout_records_contract c))

let ppType html t =
  if html
  then Printf.sprintf "<span class='type'>%s</span>" (type_to_string t)
  else type_to_string t

let ppExpr html e =
  if html
  then
    let pp =
      if 0 <= e.el
      then
        Printf.sprintf
          "<button class='text-button' onClick='showLine(%i)'>%s</button>"
          e.el
      else fun x -> x
    in
    pp (Printf.sprintf "(%s : %s)" (texpr_to_string e) (ppType html e.et))
  else
    Printf.sprintf
      "(%s : %s)%s"
      (texpr_to_string e)
      (ppType html e.et)
      (if 0 <= e.el then Printf.sprintf " (line %i)" e.el else "")

let rec pp_smart_except html = function
  | `Literal literal -> literal_to_string ~html literal
  | `Value value -> value_to_string value
  | `Expr expr -> ppExpr html expr
  | `Exprs exprs -> String.concat ", " (List.map (ppExpr html) exprs)
  | `Line i ->
      if html
      then
        Printf.sprintf
          "<button class='text-button' onClick='showLine(%i)'>line %i</button>"
          i
          i
      else Printf.sprintf "(line %i)" i
  | `Text s -> s
  | `Type t -> ppType html t
  | `Br -> if html then "<br>" else "\n"
  | `Rec l -> String.concat " " (List.map (pp_smart_except html) l)

let error_to_string ?(options = Options.string) operation =
  let open Execution in
  if options.html
  then
    match operation with
    | Exec_error v ->
        Printf.sprintf
          "<div class='error'>%s</div>"
          (pp_smart_except options.html (`Rec v))
    | Exec_channel_not_found s ->
        Printf.sprintf "<div class='error'>ChannelNotFound: '%s'</div>" s
    | Exec_wrong_condition (e, line_no, message) ->
        let message =
          match message with
          | None -> ""
          | Some msg -> Printf.sprintf " [%s]" (value_to_string msg)
        in
        Printf.sprintf
          "<div class='error'>WrongCondition in <button class='text-button' \
           onClick='showLine(%i)'>line %i</button>: %s%s</div>"
          line_no
          line_no
          (texpr_to_string ~options e)
          message
  else
    match operation with
    | Exec_error v -> Printf.sprintf "%s" (pp_smart_except false (`Rec v))
    | Exec_channel_not_found s -> Printf.sprintf "ChannelNotFound: '%s'" s
    | Exec_wrong_condition (e, line_no, message) ->
        let message =
          match message with
          | None -> ""
          | Some msg -> Printf.sprintf " [%s]" (value_to_string msg)
        in
        Printf.sprintf
          "WrongCondition in line %i: %s%s"
          line_no
          (texpr_to_string ~options e)
          message

let exception_to_string html = function
  | Failure s -> s
  | SmartExcept l -> pp_smart_except html (`Rec l)
  | Yojson.Basic.Util.Type_error (msg, _t) -> "Yojson exception - " ^ msg
  | e -> Printexc.to_string e
