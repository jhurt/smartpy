(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t =
  | Int       of string
  | String    of string
  | Bytes     of string
  | Primitive of
      { name : string
      ; annotations : string list
      ; arguments : t list }
  | Sequence  of t list
[@@deriving show]

val unAnnot : string list -> string

val pretty : string -> t -> string

val to_json : t -> Yojson.Basic.t

val pp_as_json :
  ?margin:int -> ?max_indent:int -> unit -> Format.formatter -> t -> unit

val left : t -> t

val right : t -> t

val annotName : string -> string

val extractAnnot : string -> string list -> string

val identity : t -> 'a

val error : string -> t -> tvalue

val to_value : (module Primitives.Primitives) -> Type.t -> t -> Value.t

val unString : [> `String of 'a ] -> 'a

val parse : Yojson.Basic.t -> t

val int : string -> t

val string : string -> t

val bytes : string -> t

(* val chain_id : string -> t *)

val primitive : string -> ?annotations:string list -> t list -> t

val sequence : t list -> t
