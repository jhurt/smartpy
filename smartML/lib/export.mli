(* Copyright 2019-2020 Smart Chain Arena LLC. *)

val export_type : Type.t -> string
