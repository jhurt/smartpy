(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Control
open Basics

let full_types_and_tags = false

type 'm mtype_f =
  | MTunit
  | MTbool
  | MTnat
  | MTint
  | MTmutez
  | MTstring
  | MTbytes
  | MTchain_id
  | MTtimestamp
  | MTaddress
  | MTkey
  | MTkey_hash
  | MTsignature
  | MToperation
  | MTsapling_state
  | MTsapling_transaction
  | MTnever
  | MToption              of 'm
  | MTlist                of 'm
  | MTset                 of 'm
  | MTcontract            of 'm
  | MTpair                of
      { fst : 'm
      ; snd : 'm
      ; annot1 : string option
      ; annot2 : string option }
  | MTor                  of
      { left : 'm
      ; right : 'm
      ; annot1 : string option
      ; annot2 : string option }
  | MTlambda              of 'm * 'm
  | MTmap                 of 'm * 'm
  | MTbig_map             of 'm * 'm
  | MTmissing             of string
[@@deriving eq, ord, fold, map, show {with_path = false}]

type mtype =
  { mt : mtype mtype_f
  ; annot_type : string option (* :a *)
  ; annot_variable : string option (* @a *)
  ; annot_singleton : string option }
[@@deriving eq, ord, show {with_path = false}]

let mk_mtype ?annot_type ?annot_variable ?annot_singleton mt =
  {mt; annot_type; annot_variable; annot_singleton}

let rec cata_mtype f {mt; annot_type; annot_variable; annot_singleton} =
  f ?annot_type ?annot_variable ?annot_singleton (map_mtype_f (cata_mtype f) mt)

let cata_mtype_stripped f =
  cata_mtype (fun ?annot_type:_ ?annot_variable:_ ?annot_singleton:_ x -> f x)

let has_missing_type =
  cata_mtype_stripped (function
      | MTmissing _ -> true
      | x -> fold_mtype_f ( || ) false x)

let mt_unit = mk_mtype MTunit

let mt_bool = mk_mtype MTbool

let mt_nat = mk_mtype MTnat

let mt_int = mk_mtype MTint

let mt_mutez = mk_mtype MTmutez

let mt_string = mk_mtype MTstring

let mt_chain_id = mk_mtype MTchain_id

let mt_bytes = mk_mtype MTbytes

let mt_timestamp = mk_mtype MTtimestamp

let mt_address = mk_mtype MTaddress

let mt_key = mk_mtype MTkey

let mt_key_hash = mk_mtype MTkey_hash

let mt_signature = mk_mtype MTsignature

let mt_operation = mk_mtype MToperation

let mt_sapling_state = mk_mtype MTsapling_state

let mt_never = mk_mtype MTnever

let mt_sapling_transaction = mk_mtype MTsapling_transaction

let mt_option t = mk_mtype (MToption t)

let mt_list t = mk_mtype (MTlist t)

let mt_set t = mk_mtype (MTset t)

let mt_contract t = mk_mtype (MTcontract t)

let mt_pair ?annot1 ?annot2 fst snd =
  mk_mtype (MTpair {fst; snd; annot1; annot2})

let mt_or ?annot1 ?annot2 left right =
  mk_mtype (MTor {left; right; annot1; annot2})

let mt_lambda t1 t2 = mk_mtype (MTlambda (t1, t2))

let mt_map t1 t2 = mk_mtype (MTmap (t1, t2))

let mt_big_map t1 t2 = mk_mtype (MTbig_map (t1, t2))

let mt_missing s = mk_mtype (MTmissing s)

let remove_annots =
  cata_mtype_stripped (function
      | MTpair {fst; snd} -> mt_pair fst snd
      | MTor {left; right} -> mt_or left right
      | x -> mk_mtype x)

type ad_step =
  | A
  | D
[@@deriving eq, show {with_path = false}]

module MLiteral = struct
  type tezos_int = Bigint.t [@@deriving eq, ord, show {with_path = false}]

  type seq_kind =
    | SKmap
    | SKlist
    | SKbigmap
    | SKset
    | SKinstr
    | SKunknown
  [@@deriving eq, ord, show {with_path = false}, map, fold]

  type 'i t =
    | Int      of tezos_int
    | Bool     of bool
    | String   of string
    | Bytes    of string
    | Chain_id of string
    | Unit
    | Pair     of 'i t * 'i t
    | None
    | Left     of 'i t
    | Right    of 'i t
    | Some     of 'i t
    | Seq      of seq_kind * 'i t list
    | Elt      of ('i t * 'i t)
    | Instr    of 'i
  [@@deriving eq, ord, show {with_path = false}, map, fold]

  let rec nb_bigmaps = function
    | Int _ | Bool _ | String _ | Bytes _ | Chain_id _ | Instr _ | Unit | None
      ->
        0
    | Pair (x, y) | Elt (x, y) -> nb_bigmaps x + nb_bigmaps y
    | Left x | Right x | Some x -> nb_bigmaps x
    | Seq (kind, l) ->
        (if kind = SKbigmap then 1 else 0)
        + List.fold_left (fun acc x -> acc + nb_bigmaps x) 0 l

  let compare x y =
    compare (fun _ _ -> failwith "MLiteral.compare: cannot compare lambdas") x y

  let int i = Int i

  let small_int i = Int (Bigint.of_int i)

  let bool x = Bool x

  let string x = String x

  let bytes x = Bytes x

  let chain_id x = Chain_id x

  let unit = Unit

  let left x = Left x

  let right x = Right x

  let some x = Some x

  let pair x1 x2 = Pair (x1, x2)

  let none = None

  let list xs = Seq (SKlist, xs)

  let set xs = Seq (SKset, Base.List.dedup_and_sort ~compare xs)

  let mk_map big xs =
    Seq
      ( (if big then SKbigmap else SKmap)
      , List.map
          (fun (k, v) -> Elt (k, v))
          (Base.List.dedup_and_sort
             ~compare:(fun (k1, _) (k2, _) -> compare k1 k2)
             xs) )

  let sapling_empty_state = Seq (SKunknown, [])

  let instr body = Instr body

  let rec has_missing = function
    | Pair (t, u) | Elt (t, u) -> has_missing t || has_missing u
    | Seq (_, xs) -> List.exists has_missing xs
    | Left t | Right t | Some t -> has_missing t
    | _ -> false

  let rec to_michelson_string instr_to_string ~protect =
    let continue ~protect = to_michelson_string instr_to_string ~protect in
    let open Printf in
    let prot ~protect s = if protect then Printf.sprintf "(%s)" s else s in
    function
    | Int i -> Big_int.string_of_big_int i
    | Unit -> "Unit"
    | String s -> Printf.sprintf "%S" s
    (* | Hash s -> Some (Printf.sprintf "\"%s\"" (String.escaped s))
     * | Address s -> Some (Printf.sprintf "\"%s\"" (String.escaped s)) *)
    | Bool true -> "True"
    | Bool false -> "False"
    | Pair (l, r) ->
        prot
          ~protect
          (sprintf
             "Pair %s %s"
             (continue ~protect:true l)
             (continue ~protect:true r))
    | None -> "None"
    | Left l -> prot ~protect (sprintf "Left %s" (continue ~protect:true l))
    | Right l -> prot ~protect (sprintf "Right %s" (continue ~protect:true l))
    | Some l -> prot ~protect (sprintf "Some %s" (continue ~protect:true l))
    | Bytes string_bytes -> "0x" ^ Misc.Hex.hexcape string_bytes
    | Chain_id string_bytes -> "0x" ^ Misc.Hex.hexcape string_bytes
    | Seq (_, xs) ->
        sprintf
          "{%s}"
          (String.concat "; " (List.map (continue ~protect:false) xs))
    | Elt (k, v) ->
        sprintf
          "Elt %s %s"
          (continue ~protect:true k)
          (continue ~protect:true v)
    | Instr i -> instr_to_string i

  let to_michelson_string = to_michelson_string ~protect:true
end

type target =
  | T_params
  | T_lambda_parameter of int
  | T_local            of string
  | T_iter             of string
  | T_match_cons       of string * bool (* head *)
  | T_variant_arg      of string
  | T_global           of string
  | T_entry_points
  | T_self_address
[@@deriving eq, show {with_path = false}]

type stack_tag =
  | ST_none
  | ST_target of target
  | ST_pair   of stack_tag * stack_tag
[@@deriving eq, show {with_path = false}]

type stack_element =
  { se_type : mtype
  ; se_tag : stack_tag }
[@@deriving eq, show {with_path = false}]

type stack =
  | Stack_ok     of stack_element list
  | Stack_failed
[@@deriving eq, show {with_path = false}]

type 'i instr_f =
  | MIerror                 of string
  | MIcomment               of string list
  | MImich                  of mtype Basics.inline_michelson
  | MIdip                   of 'i
  | MIdipn                  of int * 'i
  | MIloop                  of 'i
  | MIiter                  of 'i
  | MImap                   of 'i
  | MIdrop
  | MIdropn                 of int
  | MIdup
  | MIdig                   of int
  | MIdug                   of int
  | MIfailwith
  | MIif                    of 'i * 'i
  | MIif_left               of 'i * 'i
  | MIif_some               of 'i * 'i
  | MIif_cons               of 'i * 'i
  | MInil                   of mtype
  | MIempty_set             of mtype
  | MIempty_bigmap          of mtype * mtype
  | MIempty_map             of mtype * mtype
  | MIcons
  | MInone                  of mtype
  | MIsome
  | MIpair                  of string option * string option
  | MIleft                  of string option * string option * mtype
  | MIright                 of string option * string option * mtype
  | MIpush                  of mtype * 'i MLiteral.t
  | MIseq                   of 'i list
  | MIswap
  | MIunpair
  | MIunit
  | MIfield                 of ad_step list
  | MIsetField              of ad_step list
  | MIcontract              of string option * mtype
  | MIcast                  of mtype * mtype
  | MIexec
  | MIapply
  | MIlambda                of mtype * mtype * 'i
  | MIcreate_contract       of
      { tparameter : mtype
      ; tstorage : mtype
      ; code : 'i }
  | MIself                  of string option
  | MIaddress
  | MIimplicit_account
  | MItransfer_tokens
  | MIcheck_signature
  | MIset_delegate
  | MIsapling_empty_state
  | MIsapling_verify_update
  | MInever
  | MIeq
  | MIneq
  | MIle
  | MIlt
  | MIge
  | MIgt
  | MIcompare
  | MImul
  | MIadd
  | MIsub
  | MIediv
  | MInot
  | MIand
  | MIor
  | MIlsl
  | MIlsr
  | MIxor
  | MIconcat                of {arity : [ `Unary | `Binary ] option}
  | MIslice
  | MIsize
  | MIget
  | MIupdate
  | MIsender
  | MIsource
  | MIamount
  | MIbalance
  | MInow
  | MIchain_id
  | MImem
  | MIhash_key
  | MIblake2b
  | MIsha256
  | MIsha512
  | MIabs
  | MIneg
  | MIint
  | MIisnat
  | MIpack
  | MIunpack                of mtype
[@@deriving eq, show {with_path = false}, map, fold]

type instr = {instr : instr instr_f} [@@deriving eq]

(** Print instr as if we had it defined as [type instr = instr
   instr_f] (using [-rectypes]), i.e. without [{instr=...}] at each
   level. *)
let rec pp_instr pp {instr} = pp_instr_f pp_instr pp instr

let show_instr {instr} = show_instr_f pp_instr instr

type tinstr =
  { tinstr : tinstr instr_f
  ; stack : stack Result.t }
[@@deriving eq, show {with_path = false}]

let rec forget_types : tinstr -> instr =
 fun {tinstr} -> {instr = map_instr_f forget_types tinstr}

let string_of_ad_path p =
  String.concat
    ""
    (List.map
       (function
         | A -> "A"
         | D -> "D")
       p)

let s_expression_of_mtype ?full ?human =
  let is_full = full = Some () in
  let is_human = human = Some () in
  let open Base.Sexp in
  let f ?annot_type ?annot_variable ?annot_singleton mt =
    let annots =
      let get pref = function
        | None -> None
        | Some s -> Some (Atom (pref ^ s))
      in
      Base.List.filter_opt
        [ get ":" annot_type
        ; get "@" annot_variable
        ; (if is_full then get "!" annot_singleton else None) ]
    in
    let mk = function
      | [] -> assert false
      | [x] -> x
      | xs -> List xs
    in
    let atom s = mk (Atom s :: annots) in
    let call s l = List ((Atom s :: annots) @ l) in
    let insert_field_annot a e =
      match a with
      | None -> e
      | Some a ->
          let a = Atom ("%" ^ a) in
          ( match e with
          | Atom s -> List [Atom s; a]
          | List (Atom s :: xs) -> List (Atom s :: a :: xs)
          | _ -> assert false )
    in
    match annot_variable with
    | Some a when is_human ->
      ( match Base.String.split ~on:'.' a with
      | [] -> assert false
      | hd :: xs ->
          let rec collapse = function
            | ("left" | "right") :: xs -> collapse xs
            | [last] -> Atom ("@" ^ hd ^ "%" ^ last)
            | _ -> Atom ("@" ^ a)
          in
          collapse xs )
    | _ ->
      ( match mt with
      | MTkey -> atom "key"
      | MTunit -> atom "unit"
      | MTsignature -> atom "signature"
      | MToption t -> call "option" [t]
      | MTlist t -> call "list" [t]
      | MTset t -> call "set" [t]
      | MToperation -> atom "operation"
      | MTsapling_state -> atom "sapling_state"
      | MTsapling_transaction -> atom "sapling_transaction"
      | MTnever -> atom "never"
      | MTcontract t -> call "contract" [t]
      | MTpair {annot1; annot2; fst; snd} ->
          call
            "pair"
            [insert_field_annot annot1 fst; insert_field_annot annot2 snd]
      | MTor {annot1; annot2; left; right} ->
          call
            "or"
            [insert_field_annot annot1 left; insert_field_annot annot2 right]
      | MTlambda (t1, t2) -> call "lambda" [t1; t2]
      | MTmap (t1, t2) -> call "map" [t1; t2]
      | MTbig_map (t1, t2) -> call "big_map" [t1; t2]
      | MTmissing s -> call "missing_type_conversion" [Atom s]
      | MTint -> atom "int"
      | MTnat -> atom "nat"
      | MTstring -> atom "string"
      | MTbytes -> atom "bytes"
      | MTmutez -> atom "mutez"
      | MTbool -> atom "bool"
      | MTkey_hash -> atom "key_hash"
      | MTtimestamp -> atom "timestamp"
      | MTaddress -> atom "address"
      | MTchain_id -> atom "chain_id" )
  in
  cata_mtype f

let string_of_mtype ?full ?human ?protect ~html t =
  let s_expr = s_expression_of_mtype ?full ?human t in
  let maybe_escape_string =
    (* See how this is used in `_opam/lib/sexplib0/sexp.ml` *)
    Base.Sexp.Private.mach_maybe_esc_str
  in
  let sexp_to_string_flat sexp =
    let open Base.Sexp in
    let buf = Buffer.create 512 in
    let rec go = function
      | Atom s -> Buffer.add_string buf (maybe_escape_string s)
      | List [] -> Buffer.add_string buf "()"
      | List (h :: t) ->
          let is_partial =
            match h with
            | Atom "missing_type_conversion" when html -> true
            | _ -> false
          in
          Buffer.add_char buf '(';
          if is_partial then Buffer.add_string buf "<span class='partialType'>";
          go h;
          Base.List.iter t ~f:(fun elt ->
              Buffer.add_char buf ' ';
              go elt);
          if is_partial then Buffer.add_string buf "</span>";
          Buffer.add_char buf ')'
    in
    go sexp;
    Buffer.contents buf
  in
  match (protect, s_expr) with
  | None, List l -> List.map sexp_to_string_flat l |> String.concat " "
  | None, Atom a -> maybe_escape_string a
  | Some (), any -> sexp_to_string_flat any

let string_of_target = function
  | T_params -> "params"
  | T_local "__storage__" -> "storage"
  | T_local "__operations__" -> "operations"
  | T_lambda_parameter id -> Printf.sprintf "lambda_parameter(%i)" id
  | T_local n -> Printf.sprintf "local(%s)" n
  | T_global n -> Printf.sprintf "global(%s)" n
  | T_iter n -> Printf.sprintf "iter(%s)" n
  | T_match_cons (n, b) ->
      Printf.sprintf "match_cons(%s).%s" n (if b then "head" else "tail")
  | T_variant_arg arg_name -> Printf.sprintf "variant_arg(%s)" arg_name
  | T_entry_points -> "entry_points"
  | T_self_address -> "self_address"

let rec string_of_stack_tag ?protect =
  let prot x = if protect = Some () then Printf.sprintf "(%s)" x else x in
  function
  | ST_none -> "_"
  | ST_target t -> string_of_target t
  | ST_pair (x, y) ->
      prot
        (Printf.sprintf
           "pair(%s, %s)"
           (string_of_stack_tag x)
           (string_of_stack_tag y))

let memo_string_of_mtype_human =
  Misc.memoize ~clear_after:1000 (fun _f (full, se) ->
      string_of_mtype
        ?full:(if full then Some () else None)
        ~human:()
        ~html:false
        se)

let memo_string_of_mtype_human ?full t =
  memo_string_of_mtype_human (full = Some (), t)

let string_of_stack_element ?full {se_type; se_tag} =
  let t = memo_string_of_mtype_human ?full se_type in
  if full = Some ()
  then Printf.sprintf "%s{%s}" t (string_of_stack_tag se_tag)
  else t

let string_of_ok_stack ?full stack =
  String.concat " : " (List.map (string_of_stack_element ?full) stack)

let string_of_stack ?full = function
  | Stack_ok stack -> string_of_ok_stack ?full stack
  | Stack_failed -> "FAILED"

let strip_annots {mt} = mk_mtype mt

(** {1 Stack helpers} *)
type rule_result =
  | Rule_ok     of int * stack_element list
  | Rule_failed

type instr_spec =
  { name : string
  ; rule : tparams:mtype -> stack_element list -> rule_result option
  ; commutative : bool
  ; arities : (int * int) option }

let mk_spec_tagged name ?commutative ~arities r =
  let arities =
    (* We ensure that every spec declares proper arities.
       Small hack for CONCAT which is non-regular in Michelson. *)
    match arities with
    | -1, -1 -> None
    | _ -> Some arities
  in
  { name
  ; rule =
      (fun ~tparams:_ stack ->
        Base.Option.map ~f:(fun (i, x) -> Rule_ok (i, x)) (r stack))
  ; commutative = commutative = Some ()
  ; arities }

let untagged se_type = {se_type; se_tag = ST_none}

let mk_spec ~arities name ?commutative r =
  mk_spec_tagged ~arities name ?commutative (fun stack ->
      Base.Option.map
        ~f:(fun (i, x) -> (i, List.map untagged x))
        (r (List.map (fun {se_type} -> se_type) stack)))

let mk_spec_const name t = mk_spec ~arities:(0, 1) name (fun _ -> Some (0, [t]))

let spec_on_stack ~tparams {name; rule} stack =
  match rule ~tparams stack with
  | None -> Error (name ^ " on " ^ string_of_stack ~full:() (Stack_ok stack))
  | Some Rule_failed -> Ok Stack_failed
  | Some (Rule_ok (n, xs)) -> Ok (Stack_ok (xs @ Base.List.drop stack n))

let mi_error msg _ = Error msg

let unstack_se err ok = function
  | se :: tail -> (ok se) tail
  | [] -> Error err

let unstack_type err ok = unstack_se err (fun {se_type} -> ok se_type)

let unstack_bool ok =
  unstack_type "empty stack" (function
      | {mt = MTbool} -> ok
      | _ -> mi_error "not a bool")

let unstack_option ok =
  unstack_type "empty stack" (function
      | {mt = MToption t} -> ok t
      | _ -> mi_error "not an option")

let unstack_list ok =
  unstack_type "empty stack" (function
      | {mt = MTlist t} -> ok t
      | _ -> mi_error "not an list")

let unstack_any = unstack_se "empty stack"

let cons_stack x = function
  | Stack_ok stack -> Stack_ok (x :: stack)
  | Stack_failed -> Stack_failed

let prepend_stack xs = function
  | Stack_ok stack -> Stack_ok (xs @ stack)
  | Stack_failed -> Stack_failed

(** {1 Unification}  *)

let unify_annots pref ?tolerant a b =
  match (a, b) with
  | Some a, Some b when a = b -> Ok (Some a)
  | Some a, Some b ->
      if tolerant = Some ()
      then Ok None
      else
        Error
          (Printf.sprintf
             "Cannot unify annotations '%s%s' and '%s%s'"
             pref
             a
             pref
             b)
  | _ -> Ok None

let rec unify_types t u =
  let mk {mt} =
    match
      ( unify_annots ":" t.annot_type u.annot_type
      , unify_annots "@" ~tolerant:() t.annot_variable u.annot_variable
      , unify_annots "!" t.annot_singleton u.annot_singleton )
    with
    | Error e, _, _ | _, Error e, _ | _, _, Error e -> Error e
    | Ok annot_type, Ok annot_variable, Ok annot_singleton ->
        Ok {mt; annot_type; annot_variable; annot_singleton}
  in
  let open Result in
  match (t.mt, u.mt) with
  | MTunit, MTunit -> mk mt_unit
  | MTbool, MTbool -> mk mt_bool
  | MTnat, MTnat -> mk mt_nat
  | MTint, MTint -> mk mt_int
  | MTmutez, MTmutez -> mk mt_mutez
  | MTstring, MTstring -> mk mt_string
  | MTbytes, MTbytes -> mk mt_bytes
  | MTtimestamp, MTtimestamp -> mk mt_timestamp
  | MTaddress, MTaddress -> mk mt_address
  | MTkey, MTkey -> mk mt_key
  | MTkey_hash, MTkey_hash -> mk mt_key_hash
  | MTsignature, MTsignature -> mk mt_signature
  | MToperation, MToperation -> mk mt_operation
  | MTsapling_transaction, MTsapling_transaction -> mk mt_sapling_transaction
  | MTsapling_state, MTsapling_state -> mk mt_sapling_state
  | MTnever, MTnever -> mk mt_never
  | MToption t, MToption u -> mt_option <$> unify_types t u
  | MTlist t, MTlist u -> mt_list <$> unify_types t u
  | MTset t, MTset u -> mt_set <$> unify_types t u
  | MTcontract t, MTcontract u -> mt_contract <$> unify_types t u
  | ( MTpair {fst = t1; snd = t2; annot1 = a1; annot2 = a2}
    , MTpair {fst = u1; snd = u2; annot1 = b1; annot2 = b2} ) ->
      let* annot1 = unify_annots "%" a1 b1 in
      let* v1 = unify_types t1 u1 in
      let* annot2 = unify_annots "%" a2 b2 in
      let* v2 = unify_types t2 u2 in
      mk (mt_pair ?annot1 v1 ?annot2 v2)
  | ( MTor {left = t1; right = t2; annot1 = a1; annot2 = a2}
    , MTor {left = u1; right = u2; annot1 = b1; annot2 = b2} ) ->
      let* annot1 = unify_annots "%" a1 b1 in
      let* v1 = unify_types t1 u1 in
      let* annot2 = unify_annots "%" a2 b2 in
      let* v2 = unify_types t2 u2 in
      mk (mt_or ?annot1 v1 ?annot2 v2)
  | MTlambda (t1, t2), MTlambda (u1, u2) ->
      mt_lambda <$> unify_types t1 u1 <*> unify_types t2 u2 >>= mk
  | MTmap (t1, t2), MTmap (u1, u2) ->
      mt_map <$> unify_types t1 u1 <*> unify_types t2 u2 >>= mk
  | MTbig_map (t1, t2), MTbig_map (u1, u2) ->
      mt_big_map <$> unify_types t1 u1 <*> unify_types t2 u2 >>= mk
  (* TODO Distinguish different missing types? *)
  | MTmissing t_name, MTmissing u_name when t_name = u_name ->
      mk (mt_missing t_name)
  | _ ->
      Error
        (Printf.sprintf
           "Cannot unify types '%s' and '%s'."
           (show_mtype t)
           (show_mtype u))

let unifiable_types t u = Base.Result.is_ok (unify_types t u)

let unify_stack_tags t u =
  match (t, u) with
  | _ when t = u -> t
  | ST_none, _ -> u
  | _, ST_none -> t
  | _ -> ST_none

let unify_stack_elements
    {se_type = t1; se_tag = tag1} {se_type = t2; se_tag = tag2} =
  if remove_annots t1 = remove_annots t2
  then Some {se_type = t1; se_tag = unify_stack_tags tag1 tag2}
  else None

let rec unify_ok_stacks s1 s2 =
  match (s1, s2) with
  | se1 :: s1, se2 :: s2 ->
      Option.map2
        (fun x xs -> x :: xs)
        (unify_stack_elements se1 se2)
        (unify_ok_stacks s1 s2)
  | [], [] -> Some []
  | _ -> None

let unifiable_ok_stacks t u = Option.is_some (unify_ok_stacks t u)

let unify_stacks s1 s2 =
  match (s1, s2) with
  | Stack_ok s1, Stack_ok s2 ->
      Option.map (fun x -> Stack_ok x) (unify_ok_stacks s1 s2)
  | Stack_failed, s2 -> Some s2
  | s1, Stack_failed -> Some s1

(** {1 Michelson instructions} *)

let rec ty_seq_in stack = function
  | [] -> []
  | x :: xs ->
      let x = x stack in
      x :: ty_seq_in x.stack xs

let rec ty_seq_out xs stack =
  match xs with
  | [] -> stack
  | x :: xs -> ty_seq_out xs (Ok x)

let mi_failwith =
  { name = "FAILWITH"
  ; rule = (fun ~tparams:_ _ -> Some Rule_failed)
  ; commutative = false
  ; arities = Some (1, 0) }

let mi_never =
  { name = "NEVER"
  ; rule =
      (fun ~tparams:_ -> function
        | {se_type = {mt = MTnever}} :: _ -> Some Rule_failed
        | _ -> None)
  ; commutative = false
  ; arities = Some (1, 0) }

let ty_if_some_in1 =
  unstack_option (fun t tail ->
      let a =
        match t.annot_variable with
        | Some v -> v ^ ".some"
        | None -> "some"
      in
      let se_type = {t with annot_variable = Some a} in
      Ok (Stack_ok ({se_type; se_tag = ST_none} :: tail)))

let ty_if_some_in2 = unstack_option (fun _ tail -> Ok (Stack_ok tail))

let ty_if_cons_ok =
  unstack_list (fun t tail ->
      Ok
        (Stack_ok
           ( {se_type = t; se_tag = ST_none}
           :: {se_type = mt_list t; se_tag = ST_none}
           :: tail )))

let ty_if_cons_ko = unstack_list (fun _ tail -> Ok (Stack_ok tail))

let ty_if_left_in1, ty_if_left_in2 =
  let aux f =
    unstack_se "empty stack" (fun {se_tag; se_type} tail ->
        match se_type with
        | {mt = MTor {annot1; annot2; left; right}; annot_variable} ->
            let se_tag =
              if se_tag = ST_target T_params
              then ST_target T_params
              else ST_none
            in
            let default, fa, sub = f annot1 annot2 left right in
            let fa = Base.Option.value ~default fa in
            let se_type =
              { sub with
                annot_variable =
                  Base.Option.map ~f:(fun v -> v ^ "." ^ fa) annot_variable }
            in
            Ok (Stack_ok ({se_type; se_tag} :: tail))
        | _ -> Error "not an or")
  in
  (aux (fun la _ l _ -> ("left", la, l)), aux (fun _ ra _ r -> ("right", ra, r)))

let ty_if_in = unstack_bool (fun tail -> Ok (Stack_ok tail))

let ty_if_out l r _stack =
  match unify_stacks l r with
  | Some s -> Ok s
  | None -> Error "cannot unify branches"

let ty_dip_in = unstack_any (fun _ tail -> Ok (Stack_ok tail))

let ty_dip_out body = unstack_any (fun se _ -> Ok (cons_stack se body))

let ty_dipn_in n stack =
  if n < 0
  then Error "negative index"
  else if n > List.length stack
  then Error "stack too short"
  else Ok (Stack_ok (Base.List.drop stack n))

let ty_dipn_out n body stack =
  if n < 0
  then Error "negative index"
  else if n > List.length stack
  then Error "stack too short"
  else Ok (prepend_stack (Base.List.take stack n) body)

let mi_dup =
  mk_spec_tagged "DUP" ~arities:(1, 2) (function
      | [] -> None
      | x :: _ -> Some (1, [x; x]))

let mi_dig n stack =
  let hi, lo = Base.List.split_n stack n in
  match lo with
  | [] -> Error (Printf.sprintf "DIG %i: stack too short" n)
  | x :: lo -> Ok (Stack_ok ((x :: hi) @ lo))

let mi_dug n = function
  | x :: tail ->
      if n > List.length tail
      then Error (Printf.sprintf "DUG %i: stack too short" n)
      else
        let hi, lo = Base.List.split_n tail n in
        Ok (Stack_ok (hi @ (x :: lo)))
  | [] -> Error (Printf.sprintf "DUG %i: stack not long enough" n)

let mi_swap =
  mk_spec_tagged "SWAP" ~arities:(2, 2) (function
      | a :: b :: _ -> Some (2, [b; a])
      | _ -> None)

let mi_drop =
  mk_spec_tagged "DROP" ~arities:(1, 0) (function
      | _ :: _ -> Some (1, [])
      | [] -> None)

let mi_dropn n =
  mk_spec_tagged "DROP" ~arities:(n, 0) (function
      | _ :: _ -> Some (n, [])
      | [] -> None)

let mi_unpair =
  mk_spec_tagged "UNPAIR" ~arities:(1, 2) (function
      | {se_type = {mt = MTpair {fst; snd}}; se_tag} :: _ ->
          let ta, tb =
            match se_tag with
            | ST_pair (ta, tb) -> (ta, tb)
            | _ -> (ST_none, ST_none)
          in
          Some (1, [{se_type = fst; se_tag = ta}; {se_type = snd; se_tag = tb}])
      | _ -> None)

let ty_iter_in =
  let aux se_type tail = Ok (Stack_ok ({se_type; se_tag = ST_none} :: tail)) in
  unstack_type "empty stack" (function
      | {mt = MTmap (k, v)} -> aux (mt_pair k v)
      | {mt = MTlist t} -> aux t
      | {mt = MTset k} -> aux k
      | _ -> fun _ -> Error "not a container")

let ty_iter_out body =
  unstack_type "empty stack" (function
      | {mt = MTmap _ | MTlist _ | MTset _} ->
          fun tail ->
            ( match body with
            | Stack_ok stack' when not (unifiable_ok_stacks tail stack') ->
                Error "stack mismatch"
            | _ -> Ok (Stack_ok tail) )
      | _ -> fun _ -> Error "not a container")

let ty_map_in = ty_iter_in

let ty_lambda_in t_in = Ok (Stack_ok [{se_type = t_in; se_tag = ST_none}])

let ty_lambda_out t1 t2 stack s =
  match stack with
  | Stack_ok [{se_type}] when unifiable_types t2 se_type ->
      Ok (Stack_ok (untagged (mt_lambda t1 t2) :: s))
  | Stack_failed -> Ok (Stack_ok (untagged (mt_lambda t1 t2) :: s))
  | _ ->
      Error
        (Printf.sprintf
           "lambda: incompatible output stack %s %s"
           (show_mtype t2)
           (string_of_stack ~full:() stack))

let ty_map_out body =
  let open Result in
  let aux f tail =
    match body with
    | Stack_ok (_ :: tail') when not (unifiable_ok_stacks tail tail') ->
        Error "stack mismatch"
    | Stack_ok ({se_type = v'} :: _) ->
        let* se_type = f v' in
        return (Stack_ok ({se_type; se_tag = ST_none} :: tail))
    | Stack_ok [] -> Error "empty stack"
    | Stack_failed -> Error "body fails"
  in
  unstack_type "empty stack" (function
      | {mt = MTmap (k, _)} -> aux (fun t -> Ok (mt_map k t))
      | {mt = MTlist _} -> aux (fun t -> Ok (mt_list t))
      | {mt = MTset _} -> aux (fun v -> Ok (mt_set v))
      | _ -> fun _ -> Error "not a container")

let ty_loop_in = unstack_bool (fun tail -> Ok (Stack_ok tail))

let ty_loop_out body =
  unstack_bool (fun stack ->
      match body with
      | Stack_ok (_ :: tail) when not (unifiable_ok_stacks stack tail) ->
          Error "stack mismatch"
      | Stack_ok ({se_type = {mt = MTbool}} :: _) -> Ok (Stack_ok stack)
      | Stack_ok _ -> Error "not a bool"
      | Stack_failed -> Error "body fails")

let mi_pair ?annot1 ?annot2 () =
  mk_spec_tagged "PAIR" ~arities:(2, 1) (function
      | {se_type = a; se_tag = ta} :: {se_type = b; se_tag = tb} :: _ ->
          Some
            ( 2
            , [ { se_type = mt_pair ?annot1 ?annot2 a b
                ; se_tag =
                    ( match (ta, tb) with
                    | ST_none, ST_none -> ST_none
                    | _ -> ST_pair (ta, tb) ) } ] )
      | _ -> None)

let mi_cons =
  mk_spec_tagged "CONS" ~arities:(2, 1) (function
      | {se_type = a} :: {se_type = {mt = MTlist a'}} :: _ ->
        ( match unify_types a a' with
        | Ok t -> Some (2, [untagged (mt_list (strip_annots t))])
        | Error _ -> None )
      | _ -> None)

let mi_get =
  mk_spec_tagged "GET" ~arities:(2, 1) (function
      | {se_type = key} :: {se_type = {mt = MTmap (key', value)}} :: _
        when unifiable_types key key' ->
          Some (2, [untagged (mt_option value)])
      | {se_type = key} :: {se_type = {mt = MTbig_map (key', value)}} :: _
        when unifiable_types key key' ->
          Some (2, [untagged (mt_option value)])
      | _ -> None)

let mi_eq =
  mk_spec "EQ" ~commutative:() ~arities:(1, 1) (function
      | {mt = MTint} :: _ -> Some (1, [mt_bool])
      | _ -> None)

let mi_neq = {mi_eq with name = "NEQ"}

let mi_lt = {mi_neq with name = "LT"; commutative = false}

let mi_le = {mi_lt with name = "LE"}

let mi_gt = {mi_lt with name = "GT"}

let mi_ge = {mi_lt with name = "GE"}

let mi_neg =
  mk_spec "NEG" ~arities:(1, 1) (function
      | {mt = MTnat} :: _ -> Some (1, [mt_int])
      | {mt = MTint} :: _ -> Some (1, [mt_int])
      | _ -> None)

let mi_abs =
  mk_spec "ABS" ~arities:(1, 1) (function
      | {mt = MTint} :: _ -> Some (1, [mt_nat])
      | _ -> None)

let mi_isnat =
  mk_spec "ISNAT" ~arities:(1, 1) (function
      | {mt = MTint} :: _ -> Some (1, [mt_option mt_nat])
      | _ -> None)

let mi_int =
  mk_spec "INT" ~arities:(1, 1) (function
      | {mt = MTnat} :: _ -> Some (1, [mt_int])
      | _ -> None)

let is_simple_comparable mtype =
  match mtype.mt with
  | MTunit | MTbool | MTnat | MTint | MTmutez | MTstring | MTbytes
   |MTchain_id | MTtimestamp | MTaddress | MTkey | MTkey_hash | MTsignature
   |MTnever ->
      true
  | MToperation | MTsapling_state | MTsapling_transaction | MToption _
   |MTlist _ | MTset _ | MTcontract _ | MTpair _ | MTor _ | MTlambda _
   |MTmap _ | MTbig_map _ | MTmissing _ ->
      false

let rec is_comparable = function
  | {mt = MTpair {fst; snd}} -> is_simple_comparable fst && is_comparable snd
  | t -> is_simple_comparable t

let mi_compare =
  mk_spec "COMPARE" ~arities:(2, 1) (function
      | a :: b :: _
        when is_comparable a && is_comparable b && unifiable_types a b ->
          Some (2, [mt_int])
      | _ -> None)

let mi_add =
  mk_spec "ADD" ~commutative:() ~arities:(2, 1) (function
      | {mt = MTint} :: {mt = MTint} :: _ -> Some (2, [mt_int])
      | {mt = MTint} :: {mt = MTnat} :: _ -> Some (2, [mt_int])
      | {mt = MTnat} :: {mt = MTint} :: _ -> Some (2, [mt_int])
      | {mt = MTnat} :: {mt = MTnat} :: _ -> Some (2, [mt_nat])
      | {mt = MTmutez} :: {mt = MTmutez} :: _ -> Some (2, [mt_mutez])
      | {mt = MTtimestamp} :: {mt = MTint} :: _ -> Some (2, [mt_timestamp])
      | {mt = MTint} :: {mt = MTtimestamp} :: _ -> Some (2, [mt_timestamp])
      | _ -> None)

let mi_sub =
  mk_spec "SUB" ~arities:(2, 1) (function
      | {mt = MTint} :: {mt = MTint} :: _ -> Some (2, [mt_int])
      | {mt = MTint} :: {mt = MTnat} :: _ -> Some (2, [mt_int])
      | {mt = MTnat} :: {mt = MTint} :: _ -> Some (2, [mt_int])
      | {mt = MTnat} :: {mt = MTnat} :: _ -> Some (2, [mt_int])
      | {mt = MTmutez} :: {mt = MTmutez} :: _ -> Some (2, [mt_mutez])
      | {mt = MTtimestamp} :: {mt = MTint} :: _ -> Some (2, [mt_timestamp])
      | {mt = MTtimestamp} :: {mt = MTtimestamp} :: _ -> Some (2, [mt_int])
      | _ -> None)

let mi_mul =
  mk_spec "MUL" ~commutative:() ~arities:(2, 1) (function
      | {mt = MTint} :: {mt = MTint} :: _ -> Some (2, [mt_int])
      | {mt = MTint} :: {mt = MTnat} :: _ -> Some (2, [mt_int])
      | {mt = MTnat} :: {mt = MTint} :: _ -> Some (2, [mt_int])
      | {mt = MTnat} :: {mt = MTnat} :: _ -> Some (2, [mt_nat])
      | {mt = MTmutez} :: {mt = MTnat} :: _ -> Some (2, [mt_mutez])
      | {mt = MTnat} :: {mt = MTmutez} :: _ -> Some (2, [mt_mutez])
      | _ -> None)

let mi_ediv =
  mk_spec "EDIV" ~arities:(2, 1) (function
      | {mt = MTint} :: {mt = MTint} :: _ ->
          Some (2, [mt_option (mt_pair mt_int mt_nat)])
      | {mt = MTint} :: {mt = MTnat} :: _ ->
          Some (2, [mt_option (mt_pair mt_int mt_nat)])
      | {mt = MTnat} :: {mt = MTint} :: _ ->
          Some (2, [mt_option (mt_pair mt_int mt_nat)])
      | {mt = MTnat} :: {mt = MTnat} :: _ ->
          Some (2, [mt_option (mt_pair mt_nat mt_nat)])
      | {mt = MTmutez} :: {mt = MTnat} :: _ ->
          Some (2, [mt_option (mt_pair mt_mutez mt_mutez)])
      | {mt = MTmutez} :: {mt = MTmutez} :: _ ->
          Some (2, [mt_option (mt_pair mt_nat mt_mutez)])
      | _ -> None)

let mi_not =
  mk_spec "NOT" ~arities:(1, 1) (function
      | {mt = MTbool} :: _ -> Some (1, [mt_bool])
      | _ -> None)

let mi_and =
  mk_spec "AND" ~commutative:() ~arities:(2, 1) (function
      | {mt = MTbool} :: {mt = MTbool} :: _ -> Some (2, [mt_bool])
      | {mt = MTnat} :: {mt = MTnat} :: _ -> Some (2, [mt_nat])
      | _ -> None)

let mi_or = {mi_and with name = "OR"}

let mi_xor = {mi_or with name = "XOR"}

let mi_lsl =
  mk_spec "LSL" ~arities:(2, 1) (function
      | {mt = MTnat} :: {mt = MTnat} :: _ -> Some (2, [mt_nat])
      | _ -> None)

let mi_lsr =
  mk_spec "LSR" ~arities:(2, 1) (function
      | {mt = MTnat} :: {mt = MTnat} :: _ -> Some (2, [mt_nat])
      | _ -> None)

let mi_unit = mk_spec_const "UNIT" mt_unit

let mi_nil t = mk_spec_const "NIL" (mt_list t)

let mi_empty_set t = mk_spec_const "EMPTY_SET" (mt_set t)

let mi_empty_map k v = mk_spec_const "EMPTY_MAP" (mt_map k v)

let mi_empty_big_map k v = mk_spec_const "EMPTY_BIG_MAP" (mt_big_map k v)

let mi_sapling_empty_state =
  mk_spec_const "SAPLING_EMPTY_STATE" mt_sapling_state

let mi_none t = mk_spec_const "NONE" (mt_option t)

let mi_push t = mk_spec_const "PUSH" t

let mi_some =
  mk_spec "SOME" ~arities:(1, 1) (function
      | t :: _ -> Some (1, [mt_option (strip_annots t)])
      | [] -> None)

let mi_left ?annot1 ?annot2 b =
  mk_spec "LEFT" ~arities:(1, 1) (function
      | a :: _ -> Some (1, [mt_or ?annot1 ?annot2 (strip_annots a) b])
      | [] -> None)

let mi_right ?annot1 ?annot2 a =
  mk_spec "RIGHT" ~arities:(1, 1) (function
      | b :: _ -> Some (1, [mt_or ?annot1 ?annot2 a (strip_annots b)])
      | [] -> None)

(** Select the part of the type designated by the ad_path. *)
let rec ad_path_in_type ops t =
  match (ops, t) with
  | [], _ -> Some t
  | A :: p, {mt = MTpair {fst}} -> ad_path_in_type p fst
  | D :: p, {mt = MTpair {snd}} -> ad_path_in_type p snd
  | _ :: _, _ -> None

let rec ad_path_in_stack_tag ops t =
  match (ops, t) with
  | [], t -> t
  | A :: p, ST_pair (a, _) -> ad_path_in_stack_tag p a
  | D :: p, ST_pair (_, d) -> ad_path_in_stack_tag p d
  | _ :: _, _ -> ST_none

let mi_field steps =
  mk_spec_tagged
    ~arities:(1, 1)
    (Printf.sprintf "C%sR" (string_of_ad_path steps))
    (function
      | {se_type; se_tag} :: _ ->
        ( match ad_path_in_type steps se_type with
        | None -> None
        | Some se_type ->
            Some
              ( 1
              , let se_tag = ad_path_in_stack_tag steps se_tag in
                [{se_type; se_tag}] ) )
      | [] -> None)

let mi_set_field steps =
  mk_spec
    ~arities:(2, 1)
    (Printf.sprintf "SET_C%sR" (string_of_ad_path steps))
    (function
      | t :: x :: _ ->
        ( match ad_path_in_type steps t with
        | Some x' when unifiable_types x x' -> Some (2, [t])
        | _ -> None )
      | _ -> None)

let mi_update =
  mk_spec "UPDATE" ~arities:(3, 1) (function
      | k :: {mt = MTbool} :: {mt = MTset k'} :: _ ->
          Base.Option.map
            ~f:(fun k -> (3, [mt_set k]))
            (Base.Result.ok (unify_types k k'))
      | k :: {mt = MToption v} :: {mt = MTmap (k', v')} :: _ ->
          Option.map2
            (fun k v -> (3, [mt_map k v]))
            (Base.Result.ok (unify_types k k'))
            (Base.Result.ok (unify_types v v'))
      | k :: {mt = MToption v} :: {mt = MTbig_map (k', v')} :: _ ->
          Option.map2
            (fun k v -> (3, [mt_big_map k v]))
            (Base.Result.ok (unify_types k k'))
            (Base.Result.ok (unify_types v v'))
      | _ -> None)

let mi_mem =
  mk_spec "MEM" ~arities:(2, 1) (function
      | k :: {mt = MTset k'} :: _ when unifiable_types k k' ->
          Some (2, [mt_bool])
      | k :: {mt = MTmap (k', _)} :: _ when unifiable_types k k' ->
          Some (2, [mt_bool])
      | k :: {mt = MTbig_map (k', _)} :: _ when unifiable_types k k' ->
          Some (2, [mt_bool])
      | _ -> None)

let mi_exec =
  mk_spec "EXEC" ~arities:(2, 1) (function
      | k :: {mt = MTlambda (k', v)} :: _ ->
          if unifiable_types k k' then Some (2, [v]) else None
      | _ -> None)

let mi_apply =
  mk_spec "APPLY" ~arities:(2, 1) (function
      | k :: {mt = MTlambda ({mt = MTpair {fst = k'; snd = k''}}, v)} :: _
        when unifiable_types k k' ->
          Some (2, [mt_lambda k'' v])
      | _ -> None)

let mi_contract t =
  mk_spec "CONTRACT" ~arities:(1, 1) (function
      | {mt = MTaddress} :: _ -> Some (1, [mt_option (mt_contract t)])
      | _ -> None)

let mi_cast t =
  mk_spec "CAST" ~arities:(1, 1) (function
      | _ :: _ -> Some (1, [t])
      | _ -> None)

let mi_transfer_tokens =
  mk_spec "TRANSFER_TOKENS" ~arities:(3, 1) (function
      | p :: {mt = MTmutez} :: {mt = MTcontract p'} :: _
        when unifiable_types p p' ->
          Some (3, [mt_operation])
      | _ -> None)

let mi_set_delegate =
  mk_spec "SET_DELEGATE" ~arities:(1, 1) (function
      | {mt = MToption {mt = MTkey_hash}} :: _ -> Some (1, [mt_operation])
      | _ -> None)

let mi_sapling_verify_update =
  mk_spec "SAPLING_VERIFY_UPDATE" ~arities:(2, 1) (function
      | {mt = MTsapling_transaction} :: {mt = MTsapling_state} :: _ ->
          Some (2, [mt_option (mt_pair mt_int mt_sapling_state)])
      | _ -> None)

let mi_hash_key =
  mk_spec "HASH_KEY" ~arities:(1, 1) (function
      | {mt = MTkey} :: _ -> Some (1, [mt_key_hash])
      | _ -> None)

let mi_blake2b =
  mk_spec "BLAKE2B" ~arities:(1, 1) (function
      | {mt = MTbytes} :: _ -> Some (1, [mt_bytes])
      | _ -> None)

let mi_sha256 = {mi_blake2b with name = "SHA256"}

let mi_sha512 = {mi_blake2b with name = "SHA512"}

let mi_check_signature =
  mk_spec "CHECK_SIGNATURE" ~arities:(3, 1) (function
      | {mt = MTkey} :: {mt = MTsignature} :: {mt = MTbytes} :: _ ->
          Some (3, [mt_bool])
      | _ -> None)

let mi_sender = mk_spec_const "SENDER" mt_address

let mi_source = mk_spec_const "SOURCE" mt_address

let mi_amount = mk_spec_const "AMOUNT" mt_mutez

let mi_balance = mk_spec_const "BALANCE" mt_mutez

let mi_now = mk_spec_const "NOW" mt_timestamp

let mi_chain_id = mk_spec_const "CHAIN_ID" mt_chain_id

let mi_concat arity =
  let arities =
    (* CONCAT has a variable arity. *)
    match arity with
    | Some `Unary -> (1, 1)
    | Some `Binary -> (2, 1)
    | None -> (-1, -1)
  in
  mk_spec "CONCAT" ~arities (function
      | {mt = MTlist {mt = MTstring}} :: _ -> Some (1, [mt_string])
      | {mt = MTlist {mt = MTbytes}} :: _ -> Some (1, [mt_bytes])
      | {mt = MTstring} :: {mt = MTstring} :: _ -> Some (2, [mt_string])
      | {mt = MTbytes} :: {mt = MTbytes} :: _ -> Some (2, [mt_bytes])
      | _ -> None)

let mi_pack =
  mk_spec "PACK" ~arities:(1, 1) (function
      | _ :: _ -> Some (1, [mt_bytes])
      | [] -> None)

let mi_unpack t =
  mk_spec "UNPACK" ~arities:(1, 1) (function
      | {mt = MTbytes} :: _ -> Some (1, [mt_option t])
      | _ -> None)

let mi_slice =
  mk_spec "SLICE" ~arities:(3, 1) (function
      | {mt = MTnat} :: {mt = MTnat} :: {mt = MTstring} :: _ ->
          Some (3, [mt_option mt_string])
      | {mt = MTnat} :: {mt = MTnat} :: {mt = MTbytes} :: _ ->
          Some (3, [mt_option mt_bytes])
      | _ -> None)

let mi_size =
  mk_spec "SIZE" ~arities:(1, 1) (function
      | {mt = MTstring | MTbytes | MTset _ | MTmap _ | MTbig_map _ | MTlist _}
        :: _ ->
          Some (1, [mt_nat])
      | _ -> None)

let mi_mich ~name ~types_in ~types_out =
  mk_spec
    name
    ~arities:(List.length types_in, List.length types_out)
    (fun stack ->
      if Base.List.is_prefix ~prefix:types_in stack ~equal:equal_mtype
      then Some (List.length types_in, types_out)
      else None)

let mi_self entrypoint =
  { name = "SELF"
  ; rule =
      (fun ~tparams _ ->
        match entrypoint with
        | None -> Some (Rule_ok (0, [untagged (mt_contract tparams)]))
        | Some entrypoint ->
            let rec find_entrypoint mtype =
              match mtype.mt with
              | MTor {left; annot1} when annot1 = Some entrypoint -> Some left
              | MTor {right; annot2} when annot2 = Some entrypoint -> Some right
              | MTor {left; right} ->
                ( match find_entrypoint left with
                | None -> find_entrypoint right
                | x -> x )
              | _ -> None
            in
            ( match find_entrypoint tparams with
            | None ->
                Printf.ksprintf failwith "Missing entry point %S" entrypoint
            | Some t -> Some (Rule_ok (0, [untagged (mt_contract t)])) ))
  ; commutative = false
  ; arities = Some (0, 1) }

let mi_address =
  mk_spec "ADDRESS" ~arities:(1, 1) (function
      | {mt = MTcontract _} :: _ -> Some (1, [mt_address])
      | _ -> None)

let mi_implicit_account =
  mk_spec "IMPLICIT_ACCOUNT" ~arities:(1, 1) (function
      | {mt = MTkey_hash} :: _ -> Some (1, [mt_contract mt_unit])
      | _ -> None)

let mi_create_contract =
  mk_spec "CREATE_CONTRACT" ~arities:(3, 2) (function
      | {mt = MToption {mt = MTkey_hash}} :: {mt = MTmutez} :: _ :: _ ->
          Some (3, [mt_operation; mt_address])
      | _ -> None)

let spec_of_instr = function
  | MIadd -> Some mi_add
  | MIsub -> Some mi_sub
  | MImul -> Some mi_mul
  | MIediv -> Some mi_ediv
  | MInil t -> Some (mi_nil t)
  | MIempty_set t -> Some (mi_empty_set t)
  | MIempty_bigmap (k, v) -> Some (mi_empty_big_map k v)
  | MIempty_map (k, v) -> Some (mi_empty_map k v)
  | MInone t -> Some (mi_none t)
  | MIpush (t, _l) -> Some (mi_push t)
  | MIcontract (_, t) -> Some (mi_contract t)
  | MIcast (_, t) -> Some (mi_cast t)
  | MIconcat {arity} -> Some (mi_concat arity)
  | MIslice -> Some mi_slice
  | MIset_delegate -> Some mi_set_delegate
  | MIsapling_empty_state -> Some mi_sapling_empty_state
  | MIsapling_verify_update -> Some mi_sapling_verify_update
  | MInever -> Some mi_never
  | MIcheck_signature -> Some mi_check_signature
  | MIhash_key -> Some mi_hash_key
  | MIblake2b -> Some mi_blake2b
  | MIsha256 -> Some mi_sha256
  | MIsha512 -> Some mi_sha512
  | MIeq -> Some mi_eq
  | MIneq -> Some mi_neq
  | MIlt -> Some mi_lt
  | MIle -> Some mi_le
  | MIgt -> Some mi_gt
  | MIge -> Some mi_ge
  | MIneg -> Some mi_neg
  | MIabs -> Some mi_abs
  | MIisnat -> Some mi_isnat
  | MIint -> Some mi_int
  | MIcompare -> Some mi_compare
  | MIsender -> Some mi_sender
  | MIsource -> Some mi_source
  | MIamount -> Some mi_amount
  | MIbalance -> Some mi_balance
  | MInow -> Some mi_now
  | MIchain_id -> Some mi_chain_id
  | MIpair (annot1, annot2) -> Some (mi_pair ?annot1 ?annot2 ())
  | MIswap -> Some mi_swap
  | MIdrop -> Some mi_drop
  | MIdropn n -> Some (mi_dropn n)
  | MIunpair -> Some mi_unpair
  | MIunit -> Some mi_unit
  | MIfailwith -> Some mi_failwith
  | MIsome -> Some mi_some
  | MIleft (annot1, annot2, t) -> Some (mi_left ?annot1 ?annot2 t)
  | MIright (annot1, annot2, t) -> Some (mi_right ?annot1 ?annot2 t)
  | MItransfer_tokens -> Some mi_transfer_tokens
  | MIcons -> Some mi_cons
  | MInot -> Some mi_not
  | MIand -> Some mi_and
  | MIor -> Some mi_or
  | MIlsl -> Some mi_lsl
  | MIlsr -> Some mi_lsr
  | MIxor -> Some mi_xor
  | MIfield steps -> Some (mi_field steps)
  | MIsetField steps -> Some (mi_set_field steps)
  | MIpack -> Some mi_pack
  | MIunpack t -> Some (mi_unpack t)
  | MIdup -> Some mi_dup
  | MIget -> Some mi_get
  | MIsize -> Some mi_size
  | MIupdate -> Some mi_update
  | MImem -> Some mi_mem
  | MIself entrypoint -> Some (mi_self entrypoint)
  | MIexec -> Some mi_exec
  | MIapply -> Some mi_apply
  | MIaddress -> Some mi_address
  | MIimplicit_account -> Some mi_implicit_account
  | MIcreate_contract _ -> Some mi_create_contract
  | MImich {name; typesIn; typesOut} ->
      Some (mi_mich ~name ~types_in:typesIn ~types_out:typesOut)
  | MIerror _ | MIcomment _ | MIdip _
   |MIdipn (_, _)
   |MIloop _ | MIiter _ | MImap _ | MIdig _ | MIdug _
   |MIif (_, _)
   |MIif_left (_, _)
   |MIif_some (_, _)
   |MIif_cons (_, _)
   |MIseq _ | MIlambda _ ->
      None

let is_commutative instr =
  match spec_of_instr instr with
  | Some {commutative} -> commutative
  | None -> false

let name_of_instr instr =
  match spec_of_instr instr with
  | Some {name} -> name
  | None -> failwith "name_of_instr"

let instr_on_stack ~tparams instr stack =
  match spec_of_instr instr with
  | Some spec -> spec_on_stack ~tparams spec stack
  | None -> failwith "instr_on_stack"

let mk_concat arity stack =
  let arity =
    match (arity, stack) with
    | Some a, _ -> Some a
    | _, {se_type = {mt = MTlist {mt = MTstring}}} :: _
     |_, {se_type = {mt = MTlist {mt = MTbytes}}} :: _ ->
        Some `Unary
    | _, {se_type = {mt = MTstring}} :: {se_type = {mt = MTstring}} :: _
     |_, {se_type = {mt = MTbytes}} :: {se_type = {mt = MTbytes}} :: _ ->
        Some `Binary
    | _ -> None
  in
  MIconcat {arity}

(** {1 Type checking} *)

module Traversable (A : APPLICATIVE) = struct
  open A

  let rec sequence_literal : 'a t MLiteral.t -> 'a MLiteral.t t =
    let s = sequence_literal in
    function
    | (Int _ | Bool _ | String _ | Bytes _ | Chain_id _ | Unit | None) as s ->
        return s
    | Left x -> A.map (fun x -> MLiteral.Left x) (s x)
    | Right x -> A.map (fun x -> MLiteral.Right x) (s x)
    | Pair (x, y) -> A.map2 (fun x y -> MLiteral.Pair (x, y)) (s x) (s y)
    | Some x -> A.map (fun x -> MLiteral.Some x) (s x)
    | Seq (kind, xs) ->
        A.map (fun x -> MLiteral.Seq (kind, x)) (A.mapA_list s xs)
    | Elt (x, y) -> A.map2 (fun x y -> MLiteral.Elt (x, y)) (s x) (s y)
    | Instr x -> A.map (fun x -> MLiteral.Instr x) x

  let sequence = function
    | MIdip x -> A.map (fun x -> MIdip x) x
    | MIdipn (n, x) -> A.map (fun x -> MIdipn (n, x)) x
    | MIloop x -> A.map (fun x -> MIloop x) x
    | MIiter x -> A.map (fun x -> MIiter x) x
    | MImap x -> A.map (fun x -> MImap x) x
    | MIif (i1, i2) -> A.map2 (fun i1 i2 -> MIif (i1, i2)) i1 i2
    | MIif_left (i1, i2) -> A.map2 (fun i1 i2 -> MIif_left (i1, i2)) i1 i2
    | MIif_some (i1, i2) -> A.map2 (fun i1 i2 -> MIif_some (i1, i2)) i1 i2
    | MIif_cons (i1, i2) -> A.map2 (fun i1 i2 -> MIif_cons (i1, i2)) i1 i2
    | MIlambda (t1, t2, i) -> A.map (fun x -> MIlambda (t1, t2, x)) i
    | MIcreate_contract {tparameter; tstorage; code} ->
        A.map (fun code -> MIcreate_contract {tparameter; tstorage; code}) code
    | MIseq is -> A.map (fun is -> MIseq is) (A.sequenceA_list is)
    | MIpush (t, l) -> A.map (fun l -> MIpush (t, l)) (sequence_literal l)
    | ( MIdrop | MIdropn _ | MIdup | MIfailwith | MIcons | MIsome | MIpair _
      | MIswap | MIunpair | MIunit | MIself _ | MIexec | MIapply | MIaddress
      | MIimplicit_account | MItransfer_tokens | MIcheck_signature
      | MIset_delegate | MIeq | MIneq | MIle | MIlt | MIge | MIgt | MIcompare
      | MImul | MIadd | MIsub | MIediv | MInot | MIand | MIor | MIlsl | MIlsr
      | MIxor | MIconcat _ | MIslice | MIsize | MIget | MIupdate | MIsender
      | MIsource | MIamount | MIbalance | MInow | MImem | MIhash_key | MIblake2b
      | MIsha256 | MIsha512 | MIabs | MIneg | MIint | MIisnat | MIpack
      | MIerror _ | MIcomment _ | MImich _ | MIdig _ | MIdug _ | MInil _
      | MIempty_set _ | MIempty_bigmap _ | MIempty_map _ | MInone _ | MIleft _
      | MIright _ | MIfield _ | MIsetField _ | MIcontract _ | MIcast _
      | MIunpack _ | MIchain_id | MIsapling_empty_state | MInever
      | MIsapling_verify_update ) as instr ->
        return instr

  let traverse f i = sequence (map_instr_f f i)
end

module TR = Traversable (Result)

(** Catamorphisms on `instr`. *)
let rec cata_instr f {instr} = f (map_instr_f (cata_instr f) instr)

let rec cata_tinstr f {tinstr; stack} =
  f (map_instr_f (cata_tinstr f) tinstr) stack

let size_instr = cata_instr (fun i -> fold_instr_f ( + ) 1 i)

let size_tinstr = cata_tinstr (fun i _ -> fold_instr_f ( + ) 1 i)

let rec instr_of_mliteral seq : _ MLiteral.t -> _ = function
  | Instr i -> Some i
  | Seq (_kind, xs) ->
      Option.map seq (Option.mapA_list (instr_of_mliteral seq) xs)
  | Int _ | Bool _ | String _ | Bytes _ | Chain_id _ | Unit | Pair _ | None
   |Left _ | Right _ | Some _ | Elt _ ->
      None

let initial_stack ~tparameter ~tstorage =
  Stack_ok
    [ { se_type =
          mt_pair
            {tparameter with annot_variable = Some "parameter"}
            {tstorage with annot_variable = Some "storage"}
      ; se_tag = ST_pair (ST_target T_params, ST_target (T_local "__storage__"))
      } ]

(** Feeds the appropriate stack into any sub-instructions. *)
let rec feed_literal ~tparameter t l =
  let feed_literal = feed_literal ~tparameter in
  let open MLiteral in
  match (t.mt, l) with
  | MTpair {fst; snd}, Pair (x, y) ->
      Pair (feed_literal fst x, feed_literal snd y)
  | MTor {left}, Left x -> Left (feed_literal left x)
  | MTor {right}, Right x -> Right (feed_literal right x)
  | MToption t, Some x -> Some (feed_literal t x)
  | (MTlist t | MTset t), Seq (kind, xs) ->
      Seq (kind, List.map (feed_literal t) xs)
  | (MTmap (tk, tv) | MTbig_map (tk, tv)), Seq (kind, xs) ->
      let f = function
        | Elt (k, v) -> Elt (feed_literal tk k, feed_literal tv v)
        | _ -> failwith "non-Elt under literal map"
      in
      Seq (kind, List.map f xs)
  | MTlambda (t1, _), _l ->
      let seq xs s = typecheck_f ~tparameter (MIseq xs) s in
      ( match instr_of_mliteral seq l with
      | None -> assert false
      | Some i -> Instr (i (ty_lambda_in t1)) )
  | _, ((Int _ | Bool _ | String _ | Bytes _ | Chain_id _ | Unit | None) as s)
    ->
      s
  | _, Elt _ -> failwith "stray Elt"
  | _, l ->
      let l = MLiteral.map (fun i -> i (Error "dummy")) l in
      let l =
        MLiteral.to_michelson_string (fun x -> show_instr (forget_types x)) l
      in
      failwith
        (Printf.sprintf
           "ill-typed literal %S : %S"
           l
           (string_of_mtype ~html:false t))

and feed ~tparameter stack = function
  | MIdip i -> MIdip (i (ty_dip_in stack))
  | MIdipn (n, i) -> MIdipn (n, i (ty_dipn_in n stack))
  | MIloop i -> MIloop (i (ty_loop_in stack))
  | MIiter i -> MIiter (i (ty_iter_in stack))
  | MImap i -> MImap (i (ty_map_in stack))
  | MIlambda (t1, t2, i) -> MIlambda (t1, t2, i (ty_lambda_in t1))
  | MIif (i1, i2) ->
      let s = ty_if_in stack in
      MIif (i1 s, i2 s)
  | MIif_some (i1, i2) ->
      MIif_some (i1 (ty_if_some_in1 stack), i2 (ty_if_some_in2 stack))
  | MIif_cons (i1, i2) ->
      MIif_cons (i1 (ty_if_cons_ok stack), i2 (ty_if_cons_ko stack))
  | MIif_left (i1, i2) ->
      MIif_left (i1 (ty_if_left_in1 stack), i2 (ty_if_left_in2 stack))
  | MIseq xs -> MIseq (ty_seq_in (Ok (Stack_ok stack)) xs)
  | MIconcat {arity} -> mk_concat arity stack
  | MIpush (t, l) ->
    ( try MIpush (t, feed_literal ~tparameter t l) with
    | Failure f -> MIerror f )
  | MIcreate_contract {tparameter; tstorage; code} ->
      let code = code (Ok (initial_stack ~tparameter ~tstorage)) in
      MIcreate_contract {tparameter; tstorage; code}
  | ( MImich _ | MIswap | MIunpair | MIunit | MIfield _ | MIsetField _
    | MIcontract _ | MIcast _ | MIself _ | MIexec | MIapply | MIaddress
    | MIimplicit_account | MItransfer_tokens | MIcheck_signature
    | MIset_delegate | MIeq | MIneq | MIle | MIlt | MIge | MIgt | MIcompare
    | MImul | MIadd | MIsub | MIediv | MInot | MIand | MIor | MIlsl | MIlsr
    | MIxor | MIslice | MIsize | MIget | MIupdate | MIsource | MIsender
    | MIamount | MIbalance | MInow | MImem | MIhash_key | MIchain_id
    | MIsapling_empty_state | MIsapling_verify_update | MInever | MIblake2b
    | MIsha256 | MIsha512 | MIabs | MIneg | MIint | MIisnat | MIpack
    | MIunpack _ | MIerror _ | MIcomment _ | MIdrop | MIdropn _ | MIdup
    | MIdig _ | MIdug _ | MIfailwith | MInil _ | MIempty_set _
    | MIempty_bigmap _ | MIempty_map _ | MIcons | MInone _ | MIsome | MIpair _
    | MIleft _ | MIright _ ) as s ->
      s

and combine ~tparams = function
  | ( MIadd | MIsub | MImul | MIediv | MInil _ | MIempty_set _
    | MIempty_bigmap _ | MIempty_map _ | MInone _ | MIpush _ | MIcontract _
    | MIcast _ | MIconcat _ | MIslice | MIcheck_signature | MIset_delegate
    | MIeq | MIle | MIge | MIlt | MIgt | MIneq | MIcompare | MIsender | MIsource
    | MIamount | MIbalance | MInow | MIhash_key | MIblake2b | MIsha256
    | MIsha512 | MIabs | MIneg | MIint | MIisnat | MIpair _ | MIswap | MIdrop
    | MIdropn _ | MIunpair | MIunit | MIfailwith | MIsome | MIleft _ | MIright _
    | MIcons | MItransfer_tokens | MInot | MIand | MIor | MIlsl | MIlsr | MIxor
    | MIfield _ | MIsetField _ | MIpack | MIunpack _ | MIdup | MIget | MIsize
    | MIupdate | MImem | MImich _ | MIself _ | MIexec | MIapply | MIaddress
    | MIimplicit_account | MIchain_id | MIsapling_empty_state
    | MIsapling_verify_update | MInever ) as instr ->
      instr_on_stack ~tparams instr
  | MIdip i -> ty_dip_out i
  | MIdipn (n, i) -> ty_dipn_out n i
  | MIloop i -> ty_loop_out i
  | MIiter i -> ty_iter_out i
  | MImap i -> ty_map_out i
  | MIif (l, r) -> ty_if_out l r
  | MIif_left (l, r) -> ty_if_out l r
  | MIif_some (l, r) -> ty_if_out l r
  | MIif_cons (l, r) -> ty_if_out l r
  | MIseq xs -> fun stack -> ty_seq_out xs (Ok (Stack_ok stack))
  | MIerror _ -> fun s -> Ok (Stack_ok s)
  | MIcomment _ -> fun s -> Ok (Stack_ok s)
  | MIdig i -> mi_dig i
  | MIdug i -> mi_dug i
  | MIlambda (t1, t2, i) -> ty_lambda_out t1 t2 i
  | MIcreate_contract _ as instr -> instr_on_stack ~tparams instr

and combine' tparams i stack =
  match TR.traverse (fun x -> x) i with
  | Ok x -> combine ~tparams x stack
  | Error _ -> Error "Typing of sub-instruction failed (see below)"

and typecheck_f ~tparameter i =
  (* let i =
   *   match i with
   *   | MIcreate_contract {tparameter} -> map_instr_f (fun i -> i ~tparameter) i
   *   | _ -> map_instr_f (fun i -> i ~tparameter) i
   * in *)
  function
  | Ok (Stack_ok stack) ->
      let tinstr = feed ~tparameter stack i in
      let stack =
        combine' tparameter (map_instr_f (fun x -> x.stack) tinstr) stack
      in
      {tinstr; stack}
  | Ok Stack_failed ->
      let err = Error "instruction after FAILWITH" in
      {tinstr = map_instr_f (fun x -> x err) i; stack = err}
  | Error _ ->
      let err = Error "previous type error" in
      {tinstr = map_instr_f (fun x -> x err) i; stack = err}

let typecheck ~tparameter stack i =
  cata_instr (typecheck_f ~tparameter) i (Ok stack)

let typecheck_literal ~tparameter t x =
  feed_literal
    ~tparameter
    t
    (MLiteral.map
       (fun i stack ->
         match stack with
         | Error msg ->
             failwith
               (Printf.sprintf
                  "typecheck_literal: %s %s"
                  msg
                  (MLiteral.to_michelson_string (fun _ -> "???") x))
         | Ok s -> typecheck ~tparameter s i)
       x)

(** {1 Printing} *)

let has_error ~accept_missings =
  let has_missing_type t =
    if accept_missings then false else has_missing_type t
  in
  cata_tinstr (fun instr stack ->
      match stack with
      | Error _ -> true
      | Ok _ ->
        ( match instr with
        | MIerror _ -> true
        | MInil t
         |MIempty_set t
         |MInone t
         |MIleft (_, _, t)
         |MIright (_, _, t)
         |MIcontract (_, t)
         |MIcast (_, t)
         |MIunpack t ->
            has_missing_type t
        | MIempty_bigmap (k, v) | MIempty_map (k, v) ->
            has_missing_type k || has_missing_type v
        | MIpush (t, l) -> has_missing_type t || MLiteral.has_missing l
        | MImich {typesIn; typesOut} ->
            List.exists has_missing_type typesIn
            || List.exists has_missing_type typesOut
        | MIlambda (t1, t2, _i) as x ->
            has_missing_type t1
            || has_missing_type t2
            || fold_instr_f ( || ) false x
        | x -> fold_instr_f ( || ) false x ))

let name_of_instr_exn = function
  | ( MIadd | MIsub | MImul | MIediv | MInil _ | MIempty_set _
    | MIempty_bigmap _ | MIempty_map _ | MInone _ | MIpush _ | MIcontract _
    | MIcast _ | MIconcat _ | MIslice | MIcheck_signature | MIset_delegate
    | MIeq | MIle | MIge | MIlt | MIgt | MIneq | MIcompare | MIsender | MIsource
    | MIamount | MIbalance | MInow | MIhash_key | MIblake2b | MIsha256
    | MIsha512 | MIabs | MIneg | MIint | MIisnat | MIpair _ | MIswap | MIdrop
    | MIdropn _ | MIunpair | MIunit | MIfailwith | MIsome | MIleft _ | MIright _
    | MIcons | MItransfer_tokens | MInot | MIor | MIlsl | MIlsr | MIxor | MIand
    | MIfield _ | MIsetField _ | MIpack | MIunpack _ | MIdup | MIget | MIsize
    | MIupdate | MImem | MImich _ | MIself _ | MIexec | MIapply | MIaddress
    | MIimplicit_account | MIchain_id | MIsapling_empty_state
    | MIsapling_verify_update | MInever ) as instr ->
      name_of_instr instr
  | MIdip _ -> "DIP"
  | MIdipn _ -> "DIPN"
  | MIloop _ -> "LOOP"
  | MIiter _ -> "ITER"
  | MImap _ -> "MAP"
  | MIif_left _ -> "IF_LEFT"
  | MIif_some _ -> "IF_SOME"
  | MIif_cons _ -> "IF_CONS"
  | MIif _ -> "IF"
  | MIdig _ -> "DIG"
  | MIdug _ -> "DUG"
  | MIlambda _ -> "LAMBDA"
  | MIerror _ -> failwith "name_of_instr_exn: MIerror"
  | MIcomment _ -> failwith "name_of_instr_exn: MIcomment"
  | MIseq _ -> failwith "name_of_instr_exn: MIseq"
  | MIcreate_contract _ -> "CREATE_CONTRACT"

let two_field_annots = function
  | Some a1, Some a2 -> ["%" ^ a1; "%" ^ a2]
  | Some a1, None -> ["%" ^ a1]
  | None, Some a2 -> ["%"; "%" ^ a2]
  | None, None -> []

let html_spaces =
  let spaces =
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
  in
  let spaces = spaces ^ spaces ^ spaces ^ spaces in
  let spaces = spaces ^ spaces ^ spaces ^ spaces ^ spaces ^ spaces in
  Array.init 256 (fun i -> String.sub spaces 0 (6 * i))

let text_spaces =
  let spaces =
    "                                                                "
  in
  let spaces = spaces ^ spaces ^ spaces ^ spaces in
  Array.init 256 (fun i -> String.sub spaces 0 i)

let string_of_michCode ~html ~show_types ?sub_sequence indent inst =
  let rec string_of_michCode ?sub_sequence indent inst =
    let spaces = if html then html_spaces else text_spaces in
    let get_indent () = spaces.(indent) in
    let ppAlign ?app s =
      let app =
        match app with
        | None -> ""
        | Some s -> Printf.sprintf " %s" s
      in
      Printf.sprintf
        "%s%s%s;%s"
        (get_indent ())
        s
        app
        ( if show_types
        then spaces.(max 0 (10 - String.length s - String.length app))
        else "" )
    in
    let span className text =
      if html
      then Printf.sprintf "<span class='%s'>%s</span>" className text
      else text
    in
    let s =
      match inst.tinstr with
      | MIseq [] -> Printf.sprintf "%s{}" (get_indent ())
      | MIseq l ->
          Printf.sprintf
            "%s{\n%s\n%s}%s"
            (get_indent ())
            (String.concat "\n" (List.map (string_of_michCode (indent + 2)) l))
            (get_indent ())
            (if sub_sequence = Some () then "" else ";")
      | MIdip code ->
          Printf.sprintf
            "%sDIP\n%s;"
            (get_indent ())
            (string_of_michCode ~sub_sequence:() (indent + 2) code)
      | MIdipn (n, code) ->
          Printf.sprintf
            "%sDIP %i\n%s;"
            (get_indent ())
            n
            (string_of_michCode ~sub_sequence:() (indent + 2) code)
      | MIloop code ->
          Printf.sprintf
            "%sLOOP\n%s;"
            (get_indent ())
            (string_of_michCode ~sub_sequence:() (indent + 2) code)
      | MIiter code ->
          Printf.sprintf
            "%sITER\n%s;"
            (get_indent ())
            (string_of_michCode ~sub_sequence:() (indent + 2) code)
      | MImap code ->
          Printf.sprintf
            "%sMAP\n%s;"
            (get_indent ())
            (string_of_michCode ~sub_sequence:() (indent + 2) code)
      | MIif_left (l, r) ->
          Printf.sprintf
            "%sIF_LEFT\n%s\n%s;"
            (get_indent ())
            (string_of_michCode ~sub_sequence:() (indent + 2) l)
            (string_of_michCode ~sub_sequence:() (indent + 2) r)
      | MIif_some (l, r) ->
          Printf.sprintf
            "%sIF_SOME\n%s\n%s;"
            (get_indent ())
            (string_of_michCode ~sub_sequence:() (indent + 2) l)
            (string_of_michCode ~sub_sequence:() (indent + 2) r)
      | MIif_cons (l, r) ->
          Printf.sprintf
            "%sIF_CONS\n%s\n%s;"
            (get_indent ())
            (string_of_michCode ~sub_sequence:() (indent + 2) l)
            (string_of_michCode ~sub_sequence:() (indent + 2) r)
      | MIif (l, r) ->
          Printf.sprintf
            "%sIF\n%s\n%s;"
            (get_indent ())
            (string_of_michCode ~sub_sequence:() (indent + 2) l)
            (string_of_michCode ~sub_sequence:() (indent + 2) r)
      | MIcomment comments ->
          let lines =
            List.concat (List.map (String.split_on_char '\n') comments)
          in
          String.concat
            "\n"
            (List.map
               (fun line ->
                 Printf.sprintf
                   "%s%s"
                   (get_indent ())
                   (span "comment" (Printf.sprintf "# %s" line)))
               lines)
      | MIdig n -> ppAlign (Printf.sprintf "DIG %d" n)
      | MIdug n -> ppAlign (Printf.sprintf "DUG %d" n)
      | MIdropn n -> ppAlign (Printf.sprintf "DROP %d" n)
      | MIerror error ->
          Printf.sprintf
            "%s%s"
            (get_indent ())
            (span "partialType" (Printf.sprintf "MIerror: %s" error))
      | MIempty_set t
       |MInil t
       |MInone t
       |MIcontract (None, t)
       |MIunpack t
       |MIcast (t, _) ->
          ppAlign
            (name_of_instr_exn inst.tinstr)
            ~app:(string_of_mtype ~protect:() ~html t)
      | MIempty_bigmap (k, v) | MIempty_map (k, v) ->
          ppAlign
            (name_of_instr_exn inst.tinstr)
            ~app:
              (Base.String.concat
                 ~sep:" "
                 [ string_of_mtype ~protect:() ~html k
                 ; string_of_mtype ~protect:() ~html v ])
      | MIleft (a1, a2, t) | MIright (a1, a2, t) ->
          ppAlign
            (Base.String.concat
               ~sep:" "
               (name_of_instr_exn inst.tinstr :: two_field_annots (a1, a2)))
            ~app:(string_of_mtype ~protect:() ~html t)
      | MIpair (a1, a2) ->
          ppAlign
            (Base.String.concat
               ~sep:" "
               (name_of_instr_exn inst.tinstr :: two_field_annots (a1, a2)))
      | MIcontract (Some ep, t) ->
          ppAlign
            (Printf.sprintf
               "CONTRACT %%%s %s"
               ep
               (string_of_mtype ~protect:() ~html t))
      | MIpush (t, l) ->
          ppAlign
            (Printf.sprintf
               "PUSH %s %s"
               (string_of_mtype ~protect:() ~html t)
               (MLiteral.to_michelson_string
                  (string_of_michCode (indent + 2))
                  l))
      | MIself (Some entrypoint) ->
          ppAlign (Printf.sprintf "SELF %%%s" entrypoint)
      | MIlambda (t1, t2, l) ->
          Printf.sprintf
            "%sLAMBDA\n%s  %s\n%s  %s\n%s;"
            (get_indent ())
            (get_indent ())
            (string_of_mtype ~protect:() ~html t1)
            (get_indent ())
            (string_of_mtype ~protect:() ~html t2)
            (string_of_michCode ~sub_sequence:() (indent + 2) l)
      | MIcreate_contract {tparameter; tstorage; code} ->
          Printf.sprintf
            "%sCREATE_CONTRACT\n\
             %s { parameter %s;\n\
             %s   storage   %s;\n\
             %s   code\n\
             %s};"
            (get_indent ())
            (get_indent ())
            (string_of_mtype ~protect:() ~html tparameter)
            (get_indent ())
            (string_of_mtype ~protect:() ~html tstorage)
            (get_indent ())
            (string_of_michCode ~sub_sequence:() (indent + 5) code)
      | ( MIdrop | MIdup | MIfailwith | MIcons | MIsome | MIswap | MIunpair
        | MIunit
        | MIself None
        | MIexec | MIapply | MIaddress | MIimplicit_account | MItransfer_tokens
        | MIcheck_signature | MIset_delegate | MIeq | MIneq | MIle | MIlt | MIge
        | MIgt | MIcompare | MImul | MIadd | MIsub | MIediv | MInot | MIand
        | MIor | MIlsl | MIlsr | MIxor | MIconcat _ | MIslice | MIsize | MIget
        | MIupdate | MIsender | MIsource | MIamount | MIbalance | MInow | MImem
        | MIhash_key | MIblake2b | MIsha256 | MIsha512 | MIabs | MIneg | MIint
        | MIisnat | MIpack | MImich _ | MIfield _ | MIsetField _ | MIchain_id
        | MIsapling_empty_state | MIsapling_verify_update | MInever ) as simple
        ->
          ppAlign (name_of_instr_exn simple)
    in
    if sub_sequence = Some ()
    then
      match inst.tinstr with
      | MIseq _ -> s
      | _ ->
          string_of_michCode
            ~sub_sequence:()
            indent
            {tinstr = MIseq [inst]; stack = inst.stack}
    else
      let full =
        match inst.tinstr with
        | MIerror _ -> Some ()
        | _ when full_types_and_tags -> Some ()
        | _ -> None
      in
      if show_types
      then
        match inst.stack with
        | Ok inst ->
            Printf.sprintf
              "%s %s %s"
              s
              (span "comment" "#")
              (span "stack" (Printf.sprintf "%s" (string_of_stack ?full inst)))
        | Error msg ->
            Printf.sprintf
              "%s # %s"
              s
              (span "partialType" (Printf.sprintf "Error stack: %s" msg))
      else s
  in
  string_of_michCode ?sub_sequence indent inst

let rec pretty_mliteral i wrap ppf =
  let open Format in
  let wrap x = if wrap then Format.fprintf ppf "(%t)" x else x ppf in
  let pretty = pretty_mliteral in
  match (i : _ MLiteral.t) with
  | Int i -> fprintf ppf "%s" (Big_int.string_of_big_int i)
  | Unit -> fprintf ppf "Unit"
  | String s -> fprintf ppf "%S" s
  | Bool true -> fprintf ppf "True"
  | Bool false -> fprintf ppf "False"
  | Pair (l, r) ->
      wrap (fun ppf -> fprintf ppf "Pair %t %t" (pretty l true) (pretty r true))
  | None -> fprintf ppf "None"
  | Left x -> wrap (fun ppf -> fprintf ppf "Left %t" (pretty x true))
  | Right x -> wrap (fun ppf -> fprintf ppf "Right %t" (pretty x true))
  | Some x -> wrap (fun ppf -> fprintf ppf "Some %t" (pretty x true))
  | Bytes s | Chain_id s -> fprintf ppf "0x%s" (Misc.Hex.hexcape s)
  | Seq (_kind, xs) ->
      let f i x =
        let x = pretty x false in
        if i = 0 then fprintf ppf "%t" x else fprintf ppf "; %t" x
      in
      fprintf ppf "{";
      List.iteri f xs;
      fprintf ppf "}"
  | Elt (k, v) ->
      wrap (fun ppf -> fprintf ppf "Elt %t %t" (pretty k true) (pretty v true))
  | Instr i -> i true ppf

let pretty_instr_f i wrap ppf =
  let open Format in
  let name ppf = fprintf ppf "%s" (name_of_instr_exn i) in
  let wrap x = if wrap then Format.fprintf ppf "{ %t }" x else x ppf in
  match i with
  | MIseq [] -> fprintf ppf "{}"
  | MIseq [x] -> fprintf ppf "{ %t }" (x false)
  | MIseq (x :: xs) ->
      fprintf ppf "{ %t" (x false);
      List.iter (fun x -> fprintf ppf "; %t" (x false)) xs;
      fprintf ppf " }"
  | MIdipn (n, i) ->
      wrap (fun ppf -> fprintf ppf "D%sP %t" (String.make n 'I') (i true))
  | MIdip i | MIloop i | MIiter i | MImap i -> fprintf ppf "%t %t" name (i true)
  | MIif_left (i1, i2) | MIif_some (i1, i2) | MIif_cons (i1, i2) | MIif (i1, i2)
    ->
      wrap (fun ppf -> fprintf ppf "%t %t %t" name (i1 true) (i2 true))
  | MIdig n | MIdug n | MIdropn n ->
      wrap (fun ppf -> fprintf ppf "%t %d" name n)
  | MIcomment xs -> wrap (fun ppf -> List.iter (fprintf ppf "/* %s */") xs)
  | MIerror msg -> wrap (fun ppf -> fprintf ppf "ERROR %s" msg)
  | MInil t
   |MIempty_set t
   |MInone t
   |MIcontract (None, t)
   |MIunpack t
   |MIcast (t, _)
   |MIleft (_, _, t)
   |MIright (_, _, t) ->
      wrap (fun ppf ->
          fprintf ppf "%t %s" name (string_of_mtype ~protect:() ~html:false t))
  | MIempty_bigmap (k, v) | MIempty_map (k, v) ->
      wrap (fun ppf ->
          fprintf
            ppf
            "%t %s %s"
            name
            (string_of_mtype ~protect:() ~html:false k)
            (string_of_mtype ~protect:() ~html:false v))
  | MIcontract (Some ep, t) ->
      wrap (fun ppf ->
          fprintf
            ppf
            "CONTRACT %%%s %s"
            ep
            (string_of_mtype ~protect:() ~html:false t))
  | MIlambda (t1, t2, c) ->
      wrap (fun ppf ->
          let t1 = string_of_mtype ~protect:() ~html:false t1 in
          let t2 = string_of_mtype ~protect:() ~html:false t2 in
          fprintf ppf "LAMBDA %s %s %t" t1 t2 (c true))
  | MIpush (t, l) ->
      wrap (fun ppf ->
          let t = string_of_mtype ~protect:() ~html:false t in
          let l = pretty_mliteral l true in
          fprintf ppf "PUSH %s %t" t l)
  | MIcreate_contract {tparameter; tstorage; code} ->
      wrap (fun ppf ->
          fprintf
            ppf
            "CREATE_CONTRACT { parameter %s; storage %s; code %t};"
            (string_of_mtype ~protect:() ~html:false tparameter)
            (string_of_mtype ~protect:() ~html:false tstorage)
            (code true))
  | MIdrop | MIdup | MIfailwith | MIcons | MIsome | MIswap | MIunpair | MIunit
   |MIself _ | MIexec | MIapply | MIaddress | MIimplicit_account
   |MItransfer_tokens | MIcheck_signature | MIset_delegate | MIeq | MIneq
   |MIle | MIlt | MIge | MIgt | MIcompare | MImul | MIadd | MIsub | MIediv
   |MInot | MIand | MIor | MIlsl | MIlsr | MIxor | MIconcat _ | MIslice
   |MIsize | MIget | MIupdate | MIsender | MIsource | MIamount | MIbalance
   |MInow | MImem | MIhash_key | MIblake2b | MIsha256 | MIsha512 | MIabs
   |MIneg | MIint | MIisnat | MIpack | MImich _ | MIfield _ | MIsetField _
   |MIchain_id | MIsapling_empty_state | MIsapling_verify_update | MInever
   |MIpair _ ->
      wrap (fun ppf -> fprintf ppf "%t" name)

let pretty_instr = cata_instr pretty_instr_f

let pretty_instr_mliteral m = pretty_mliteral (MLiteral.map pretty_instr m)

let string_of_instr_mliteral m =
  Format.asprintf "%t" (pretty_instr_mliteral m true)

let string_of_tinstr_mliteral =
  MLiteral.to_michelson_string
    (string_of_michCode ~html:false ~show_types:true 0)

module Of_micheline = struct
  open Micheline

  let rec mtype x =
    match mtype_annotated x with
    | mt, None -> mt
    | _, Some _ -> assert false

  and mtype_annotated = function
    | Primitive {name; annotations; arguments} as p ->
        let mt =
          match (name, arguments) with
          | "pair", [t1; t2] ->
              let fst, annot1 = mtype_annotated t1 in
              let snd, annot2 = mtype_annotated t2 in
              mt_pair ?annot1 ?annot2 fst snd
          | "or", [t1; t2] ->
              let left, annot1 = mtype_annotated t1 in
              let right, annot2 = mtype_annotated t2 in
              mt_or ?annot1 ?annot2 left right
          | "unit", [] -> mt_unit
          | "bool", [] -> mt_bool
          | "mutez", [] -> mt_mutez
          | "timestamp", [] -> mt_timestamp
          | "nat", [] -> mt_nat
          | "int", [] -> mt_int
          | "string", [] -> mt_string
          | "key", [] -> mt_key
          | "signature", [] -> mt_signature
          | "bytes", [] -> mt_bytes
          | "chain_id", [] -> mt_chain_id
          | "key_hash", [] -> mt_key_hash
          | "contract", [t] -> mt_contract (mtype t)
          | "address", [] -> mt_address
          | "list", [t] -> mt_list (mtype t)
          | "option", [t] -> mt_option (mtype t)
          | "set", [t] -> mt_set (mtype t)
          | "map", [t1; t2] -> mt_map (mtype t1) (mtype t2)
          | "big_map", [t1; t2] -> mt_big_map (mtype t1) (mtype t2)
          | "lambda", [t1; t2] -> mt_lambda (mtype t1) (mtype t2)
          | "operation", [] -> mt_operation
          | "sapling_state", [] -> mt_sapling_state
          | "sapling_transaction", [] -> mt_sapling_transaction
          | "never", [] -> mt_never
          | _ -> mk_mtype (MTmissing ("Parse type error " ^ pretty "" p))
        in
        List.fold_left
          (fun (mt, fa) a ->
            let update = function
              | Some _ -> failwith "duplicate annotation"
              | None -> Some (String.sub a 1 (String.length a - 1))
            in
            match a.[0] with
            | ':' -> ({mt with annot_type = update mt.annot_type}, fa)
            | '@' -> ({mt with annot_variable = update mt.annot_variable}, fa)
            | '%' -> (mt, update fa)
            | _ -> failwith "cannot parse annotation")
          (mt, None)
          annotations
    | p -> failwith ("Parse type error " ^ pretty "" p)

  let rec literal x : _ MLiteral.t =
    match x with
    | Int i -> Int (Big_int.big_int_of_string i)
    | Bytes s -> Bytes s
    | String s -> String s
    | Primitive {name; annotations = _; arguments} ->
      ( match (name, arguments) with
      | "Unit", [] -> Unit
      | "False", [] -> Bool false
      | "True", [] -> Bool true
      | "Pair", [x; y] -> Pair (literal x, literal y)
      | "None", [] -> None
      | "Some", [x] -> Some (literal x)
      | "Left", [x] -> Left (literal x)
      | "Right", [x] -> Right (literal x)
      | "Elt", [k; v] -> Elt (literal k, literal v)
      | _ -> Instr (instruction x) )
    | Sequence xs -> Seq (SKunknown, List.map literal xs)

  and instruction x =
    let err () =
      MIerror (Printf.sprintf "Cannot parse instruction %S" (Micheline.show x))
    in
    let cmp instr = MIseq [{instr = MIcompare}; {instr}] in
    let fail =
      MIseq [{instr = MIpush (mt_unit, MLiteral.unit)}; {instr = MIfailwith}]
    in
    let if_op instr x y =
      MIseq [{instr}; {instr = MIif (instruction x, instruction y)}]
    in
    let ifcmp_op instr x y =
      MIseq
        [ {instr = MIcompare}
        ; {instr}
        ; {instr = MIif (instruction x, instruction y)} ]
    in
    let assert_op instr =
      MIseq [{instr}; {instr = MIif ({instr = MIseq []}, {instr = fail})}]
    in
    let assert_cmp_op instr =
      MIseq
        [ {instr = MIcompare}
        ; {instr}
        ; {instr = MIif ({instr = MIseq []}, {instr = fail})} ]
    in
    let parse_simple_macro = function
      | "FAIL" -> fail
      | "ASSERT" -> MIif ({instr = MIseq []}, {instr = fail})
      | "CMPEQ" -> cmp MIeq
      | "CMPNEQ" -> cmp MIeq
      | "CMPLT" -> cmp MIlt
      | "CMPGT" -> cmp MIgt
      | "CMPLE" -> cmp MIle
      | "CMPGE" -> cmp MIge
      | "ASSERTEQ" -> assert_op MIeq
      | "ASSERTNEQ" -> assert_op MIeq
      | "ASSERTLT" -> assert_op MIlt
      | "ASSERTGT" -> assert_op MIgt
      | "ASSERTLE" -> assert_op MIle
      | "ASSERTGE" -> assert_op MIge
      | "ASSERT_CMPEQ" -> assert_cmp_op MIeq
      | "ASSERT_CMPNEQ" -> assert_cmp_op MIeq
      | "ASSERT_CMPLT" -> assert_cmp_op MIlt
      | "ASSERT_CMPGT" -> assert_cmp_op MIgt
      | "ASSERT_CMPLE" -> assert_cmp_op MIle
      | "ASSERT_CMPGE" -> assert_cmp_op MIge
      | "ASSERT_NONE" -> MIif_some ({instr = fail}, {instr = MIseq []})
      | "ASSERT_SOME" -> MIif_some ({instr = MIseq []}, {instr = fail})
      | prim
        when String.index_opt prim 'D' = Some 0
             && String.index_opt prim 'P' = Some (String.length prim - 1)
             && 2 < String.length prim
             && Base.String.count prim ~f:(fun c -> c = 'U')
                = String.length prim - 2 ->
          let n = String.length prim - 3 in
          MIseq [{instr = MIdig n}; {instr = MIdup}; {instr = MIdug (n + 1)}]
      | prim
        when String.index_opt prim 'C' = Some 0
             && String.index_opt prim 'R' = Some (String.length prim - 1) ->
          let l =
            Base.String.fold
              (String.sub prim 1 (String.length prim - 2))
              ~init:(Some [])
              ~f:(fun acc c ->
                match (acc, c) with
                | Some acc, 'A' -> Some (A :: acc)
                | Some acc, 'D' -> Some (D :: acc)
                | _ -> None)
          in
          ( match l with
          | Some l -> MIfield (List.rev l)
          | None -> err () )
      | _ -> err ()
    in
    let instr =
      match x with
      | Sequence [x] -> (instruction x).instr
      | Sequence xs -> MIseq (List.map instruction xs)
      | Primitive {name; annotations; arguments} ->
        ( match (name, arguments) with
        | "RENAME", _ -> MIseq [] (* TODO *)
        | "UNIT", [] -> MIunit
        | "EMPTY_MAP", [k; v] ->
            MIpush (mt_map (mtype k) (mtype v), MLiteral.mk_map false [])
        | "DIP", [x] -> MIdip (instruction x)
        | "DIP", [Int i; x] -> MIdipn (int_of_string i, instruction x)
        | prim, [x]
          when String.index_opt prim 'D' = Some 0
               && String.index_opt prim 'P' = Some (String.length prim - 1)
               && 2 < String.length prim
               && Base.String.count prim ~f:(fun c -> c = 'I')
                  = String.length prim - 2 ->
            MIdipn (String.length prim - 2, instruction x)
        | "LOOP", [x] -> MIloop (instruction x)
        | "ITER", [x] -> MIiter (instruction x)
        | "MAP", [x] -> MImap (instruction x)
        | "DROP", [] -> MIdrop
        | "DROP", [Int n] -> MIdropn (int_of_string n)
        | "DUP", [] -> MIdup
        | "DIG", [Int i] -> MIdig (int_of_string i)
        | "DUG", [Int i] -> MIdug (int_of_string i)
        | "FAILWITH", [] -> MIfailwith
        | "IF", [x; y] -> MIif (instruction x, instruction y)
        | "IF_LEFT", [x; y] -> MIif_left (instruction x, instruction y)
        | "IF_RIGHT", [x; y] -> MIif_left (instruction y, instruction x)
        | "IF_SOME", [x; y] -> MIif_some (instruction x, instruction y)
        | "IF_NONE", [x; y] -> MIif_some (instruction y, instruction x)
        | "IF_CONS", [x; y] -> MIif_cons (instruction x, instruction y)
        | "NIL", [t] -> MInil (mtype t)
        | "CONS", [] -> MIcons
        | "NONE", [t] -> MInone (mtype t)
        | "SOME", [] -> MIsome
        | "PAIR", [] -> MIpair (None, None)
        | "LEFT", [t] -> MIleft (None, None, mtype t)
        | "RIGHT", [t] -> MIright (None, None, mtype t)
        | "PUSH", [t; l] -> MIpush (mtype t, literal l)
        | "SWAP", [] -> MIswap
        | "UNPAIR", [] -> MIunpair
        | "CAR", [] -> MIfield [A]
        | "CDR", [] -> MIfield [D]
        | "CONTRACT", [t] ->
            let entry_point =
              match annotations with
              | [] -> None
              | entry_point :: _ ->
                  Base.String.chop_prefix ~prefix:"%" entry_point
            in
            MIcontract (entry_point, mtype t)
        | "CAST", [t] ->
            let t = mtype t in
            MIcast (t, t)
        | "EXEC", [] -> MIexec
        | "APPLY", [] -> MIapply
        | "LAMBDA", [t; u; x] -> MIlambda (mtype t, mtype u, instruction x)
        | "CREATE_CONTRACT", [x] ->
            let tparameter, tstorage, code =
              if false then failwith (Micheline.show x);
              match x with
              | Sequence
                  [ Primitive {name = "parameter"; arguments = [tparameter]}
                  ; Primitive {name = "storage"; arguments = [tstorage]}
                  ; Primitive {name = "code"; arguments = [code]} ] ->
                  (mtype tparameter, mtype tstorage, instruction code)
              | _ -> assert false
            in
            MIcreate_contract {tparameter; tstorage; code}
        | "SELF", [] ->
            let entry_point =
              match annotations with
              | [] -> None
              | entry_point :: _ ->
                  Base.String.chop_prefix ~prefix:"%" entry_point
            in
            MIself entry_point
        | "ADDRESS", [] -> MIaddress
        | "SELF_ADDRESS", [] ->
            MIseq [{instr = MIself None}; {instr = MIaddress}]
        | "IMPLICIT_ACCOUNT", [] -> MIimplicit_account
        | "TRANSFER_TOKENS", [] -> MItransfer_tokens
        | "CHECK_SIGNATURE", [] -> MIcheck_signature
        | "SET_DELEGATE", [] -> MIset_delegate
        | "SAPLING_EMPTY_STATE", [] -> MIsapling_empty_state
        | "SAPLING_VERIFY_UPDATE", [] -> MIsapling_verify_update
        | "NEVER", [] -> MInever
        | "EQ", [] -> MIeq
        | "NEQ", [] -> MIneq
        | "LE", [] -> MIle
        | "LT", [] -> MIlt
        | "GE", [] -> MIge
        | "GT", [] -> MIgt
        | "COMPARE", [] -> MIcompare
        | "MUL", [] -> MImul
        | "ADD", [] -> MIadd
        | "SUB", [] -> MIsub
        | "EDIV", [] -> MIediv
        | "NOT", [] -> MInot
        | "AND", [] -> MIand
        | "OR", [] -> MIor
        | "LSL", [] -> MIlsl
        | "LSR", [] -> MIlsr
        | "XOR", [] -> MIxor
        | "CONCAT", [] -> MIconcat {arity = None}
        | "SLICE", [] -> MIslice
        | "SIZE", [] -> MIsize
        | "GET", [] -> MIget
        | "UPDATE", [] -> MIupdate
        | "SENDER", [] -> MIsender
        | "SOURCE", [] -> MIsource
        | "AMOUNT", [] -> MIamount
        | "BALANCE", [] -> MIbalance
        | "NOW", [] -> MInow
        | "CHAIN_ID", [] -> MIchain_id
        | "MEM", [] -> MImem
        | "HASH_KEY", [] -> MIhash_key
        | "BLAKE2B", [] -> MIblake2b
        | "SHA256", [] -> MIsha256
        | "SHA512", [] -> MIsha512
        | "ABS", [] -> MIabs
        | "NEG", [] -> MIneg
        | "INT", [] -> MIint
        | "ISNAT", [] -> MIisnat
        | "PACK", [] -> MIpack
        | "UNPACK", [t] -> MIunpack (mtype t)
        | prim, [] -> parse_simple_macro prim
        | "IFEQ", [x; y] -> if_op MIeq x y
        | "IFNEQ", [x; y] -> if_op MIeq x y
        | "IFLT", [x; y] -> if_op MIlt x y
        | "IFGT", [x; y] -> if_op MIgt x y
        | "IFLE", [x; y] -> if_op MIle x y
        | "IFGE", [x; y] -> if_op MIge x y
        | "IFCMPEQ", [x; y] -> ifcmp_op MIeq x y
        | "IFCMPNEQ", [x; y] -> ifcmp_op MIeq x y
        | "IFCMPLT", [x; y] -> ifcmp_op MIlt x y
        | "IFCMPGT", [x; y] -> ifcmp_op MIgt x y
        | "IFCMPLE", [x; y] -> ifcmp_op MIle x y
        | "IFCMPGE", [x; y] -> ifcmp_op MIge x y
        (* TODO Macros: ASSERT_SOME, ASSERT_LEFT, ASSERT_RIGHT *)
        | _ -> err () )
      | _ -> err ()
    in
    {instr}
end

module To_micheline = struct
  let mtype =
    cata_mtype (fun ?annot_type ?annot_variable ?annot_singleton:_ t ->
        let annotations =
          let get pref = function
            | None -> None
            | Some s -> Some (pref ^ s)
          in
          Base.List.filter_opt [get ":" annot_type; get "@" annot_variable]
        in
        let open Micheline in
        let prim = primitive ~annotations in
        let insert_field_annot a e =
          match a with
          | None -> e
          | Some a ->
              let a = "%" ^ a in
              ( match e with
              | Primitive {name; annotations; arguments} ->
                  Primitive {name; annotations = a :: annotations; arguments}
              | _ -> assert false )
        in
        match t with
        | MTint -> prim "int" []
        | MTbool -> prim "bool" []
        | MTstring -> prim "string" []
        | MTnat -> prim "nat" []
        | MTaddress -> prim "address" []
        | MTbytes -> prim "bytes" []
        | MTmutez -> prim "mutez" []
        | MTkey_hash -> prim "key_hash" []
        | MTkey -> prim "key" []
        | MTtimestamp -> prim "timestamp" []
        | MTpair {annot1; annot2; fst; snd} ->
            prim
              "pair"
              [insert_field_annot annot1 fst; insert_field_annot annot2 snd]
        | MToperation -> prim "operation" []
        | MTsapling_state -> prim "sapling_state" []
        | MTnever -> prim "never" []
        | MTsapling_transaction -> prim "sapling_transaction" []
        | MTmap (ct, ft) -> prim "map" [ct; ft]
        | MTor {annot1; annot2; left; right} ->
            prim
              "or"
              [insert_field_annot annot1 left; insert_field_annot annot2 right]
        | MTunit -> prim "unit" []
        | MTsignature -> prim "signature" []
        | MToption o -> prim "option" [o]
        | MTlist l -> prim "list" [l]
        | MTset e -> prim "set" [e]
        | MTcontract x -> prim "contract" [x]
        | MTlambda (x, f) -> prim "lambda" [x; f]
        | MTbig_map (k, v) -> prim "big_map" [k; v]
        | MTchain_id -> prim "chain_id" []
        | MTmissing msg ->
            primitive
              "ERROR"
              [Format.kasprintf string "Cannot compile missing type: %s" msg])

  module Macro = struct
    open Micheline

    let dip_seq i = primitive "DIP" [sequence i]

    let rec c_ad_r s =
      (*
         See http://tezos.gitlab.io/mainnet/whitedoc/michelson.html#syntactic-conveniences
         > CA(\rest=[AD]+)R / S  =>  CAR ; C(\rest)R / S
         > CD(\rest=[AD]+)R / S  =>  CDR ; C(\rest)R / S
      *)
      match s.[0] with
      | 'A' -> primitive "CAR" [] :: c_ad_r (Base.String.drop_prefix s 1)
      | 'D' -> primitive "CDR" [] :: c_ad_r (Base.String.drop_prefix s 1)
      | exception _ -> []
      | other ->
          Format.kasprintf
            failwith
            "c_ad_r macro: wrong char: '%c' (of %S)"
            other
            s

    let rec set_c_ad_r s =
      (*
         See http://tezos.gitlab.io/mainnet/whitedoc/michelson.html#syntactic-conveniences
         > SET_CA(\rest=[AD]+)R / S   =>
             { DUP ; DIP { CAR ; SET_C(\rest)R } ; CDR ; SWAP ; PAIR } / S
         > SET_CD(\rest=[AD]+)R / S   =>
             { DUP ; DIP { CDR ; SET_C(\rest)R } ; CAR ; PAIR } / S
         Then,
         > SET_CAR  =>  CDR ; SWAP ; PAIR
         > SET_CDR  =>  CAR ; PAIR
      *)
      match s.[0] with
      | 'A' when String.length s > 1 ->
          [ primitive "DUP" []
          ; dip_seq
              (primitive "CAR" [] :: set_c_ad_r (Base.String.drop_prefix s 1))
          ; primitive "CDR" []
          ; primitive "SWAP" []
          ; primitive "PAIR" [] ]
      | 'A' -> [primitive "CDR" []; primitive "SWAP" []; primitive "PAIR" []]
      | 'D' when String.length s > 1 ->
          [ primitive "DUP" []
          ; dip_seq
              (primitive "CDR" [] :: set_c_ad_r (Base.String.drop_prefix s 1))
          ; primitive "CAR" []
          ; primitive "PAIR" [] ]
      | 'D' -> [primitive "CAR" []; primitive "PAIR" []]
      | exception _ ->
          Format.kasprintf failwith "set_c_r_macro: called with no chars: S" s
      | other ->
          Format.kasprintf
            failwith
            "set_c_r_macro: wrong char: '%c' (of %S)"
            other
            s
  end

  let rec literal lit =
    let open MLiteral in
    let open Micheline in
    match (lit : _ MLiteral.t) with
    | Int bi -> int (Big_int.string_of_big_int bi)
    | Bool false -> primitive "False" []
    | Bool true -> primitive "True" []
    | String s -> string s
    | Unit -> primitive "Unit" []
    | Bytes b -> bytes b
    | Chain_id b -> bytes b
    | Pair (left, right) -> primitive "Pair" [literal left; literal right]
    | None -> primitive "None" []
    | Some l -> primitive "Some" [literal l]
    | Left e -> primitive "Left" [literal e]
    | Right e -> primitive "Right" [literal e]
    | Seq (_, xs) -> xs |> Base.List.map ~f:literal |> sequence
    | Elt (k, v) -> primitive "Elt" [literal k; literal v]
    | Instr x -> sequence (instruction x)

  and instruction (the_instruction : instr) =
    let open Micheline in
    let prim0 ?annotations n = [primitive ?annotations n []] in
    let primn ?annotations n l = [primitive ?annotations n l] in
    let rec_instruction instr = Micheline.sequence (instruction instr) in
    match the_instruction.instr with
    | MIerror s -> primn "ERROR" [string s]
    | MIcomment _comment ->
        []
        (*
          [ primitive "PUSH" [primitive "string" []; string comment]
          ; primitive "DROP" [] ] *)
    | MIdip instr -> primn "DIP" [rec_instruction instr]
    | MIdipn (n, instr) ->
        primn "DIP" [int (Base.Int.to_string n); rec_instruction instr]
    | MIdig n -> primn "DIG" [int (Base.Int.to_string n)]
    | MIdug n -> primn "DUG" [int (Base.Int.to_string n)]
    | MIdropn n -> primn "DROP" [int (Base.Int.to_string n)]
    | MIloop instr -> primn "LOOP" [rec_instruction instr]
    | MIiter instr -> primn "ITER" [rec_instruction instr]
    | MImap instr -> primn "MAP" [rec_instruction instr]
    | MIseq ils -> Base.List.concat_map ils ~f:instruction
    | MIif (t, e) -> primn "IF" [rec_instruction t; rec_instruction e]
    | MIif_left (t, e) -> primn "IF_LEFT" [rec_instruction t; rec_instruction e]
    | MIif_some (t, e) -> primn "IF_NONE" [rec_instruction e; rec_instruction t]
    | MIif_cons (t, e) -> primn "IF_CONS" [rec_instruction t; rec_instruction e]
    | MIpush (mt, lit) -> primn "PUSH" [mtype mt; literal lit]
    | MIself (Some entry_point) ->
        primn ~annotations:["%" ^ entry_point] "SELF" []
    | MIpair (a1, a2) ->
        primn ~annotations:(two_field_annots (a1, a2)) "PAIR" []
    | MIright (a1, a2, mty) ->
        primn ~annotations:(two_field_annots (a1, a2)) "RIGHT" [mtype mty]
    | MIleft (a1, a2, mty) ->
        primn ~annotations:(two_field_annots (a1, a2)) "LEFT" [mtype mty]
    | MInone mty -> primn "NONE" [mtype mty]
    | MInil mty -> primn "NIL" [mtype mty]
    | MIempty_set mty -> primn "EMPTY_SET" [mtype mty]
    | MIempty_map (k, v) -> primn "EMPTY_MAP" [mtype k; mtype v]
    | MIempty_bigmap (k, v) -> primn "EMPTY_BIG_MAP" [mtype k; mtype v]
    | MIcontract (None, mty) -> primn "CONTRACT" [mtype mty]
    | MIcontract (Some entry_point, mty) ->
        primn ~annotations:["%" ^ entry_point] "CONTRACT" [mtype mty]
    | MIcast (t, _) -> primn "CAST" [mtype t]
    | MIlambda (t1, t2, b) ->
        primn "LAMBDA" [mtype t1; mtype t2; rec_instruction b]
    | MIunpack mty -> primn "UNPACK" [mtype mty]
    | MIfield op -> Macro.c_ad_r (string_of_ad_path op)
    | MIsetField op -> Macro.set_c_ad_r (string_of_ad_path op)
    | MIcreate_contract {tparameter; tstorage; code} ->
        primn
          "CREATE_CONTRACT"
          [ sequence
              ( primn "parameter" [mtype tparameter]
              @ primn "storage" [mtype tstorage]
              @ primn "code" [rec_instruction code] ) ]
    | ( MIself None
      | MIexec | MIapply | MIaddress | MIimplicit_account | MIcons | MIsome
      | MItransfer_tokens | MIcheck_signature | MIset_delegate | MIeq
      | MIfailwith | MIdrop | MIdup | MIneq | MIle | MIlt | MIge | MIgt | MIswap
      | MIunpair | MIunit | MIcompare | MImul | MIadd | MIsub | MIediv | MInot
      | MIand | MIor | MIlsl | MIlsr | MIxor | MIconcat _ | MIslice | MIsize
      | MIget | MIupdate | MIsource | MIsender | MIamount | MIbalance | MInow
      | MImem | MIpack | MIhash_key | MIblake2b | MIsha256 | MIsha512 | MIabs
      | MIneg | MIint | MIisnat | MImich _ | MIchain_id | MIsapling_empty_state
      | MIsapling_verify_update | MInever ) as simple ->
      ( try prim0 (name_of_instr_exn simple) with
      | _ -> [sequence [primitive "ERROR-NOT-SIMPLE" []]] )
end

module Michelson_contract = struct
  type t =
    { tparameter : mtype
    ; tstorage : mtype
    ; code : tinstr
    ; lazy_entry_points : tinstr MLiteral.t option
    ; storage : instr MLiteral.t option }
  [@@deriving show {with_path = false}]

  let make ~tparameter ~tstorage ~lazy_entry_points ~storage code =
    {tparameter; tstorage; code; lazy_entry_points; storage}

  let typecheck_and_make ~tparameter ~tstorage ~lazy_entry_points ~storage code
      =
    let code =
      typecheck ~tparameter (initial_stack ~tparameter ~tstorage) code
    in
    {tparameter; tstorage; code; lazy_entry_points; storage}

  let has_error ~accept_missings {tparameter; tstorage; code} =
    let e = has_error ~accept_missings code in
    if accept_missings
    then e
    else e || has_missing_type tstorage || has_missing_type tparameter

  let to_micheline {tstorage; tparameter; code} =
    let open Micheline in
    let code =
      match To_micheline.instruction (forget_types code) with
      | [Sequence _] as l -> l
      | l -> [sequence l]
    in
    sequence
      [ primitive "storage" [To_micheline.mtype tstorage]
      ; primitive "parameter" [To_micheline.mtype tparameter]
      ; primitive "code" code ]

  let of_micheline = function
    | Micheline.Sequence
        [ Micheline.Primitive
            {name = "storage"; annotations = []; arguments = [tstorage]}
        ; Micheline.Primitive
            {name = "parameter"; annotations = []; arguments = [tparameter]}
        ; Micheline.Primitive
            {name = "code"; annotations = []; arguments = [code]} ] ->
        typecheck_and_make
          ~tstorage:(Of_micheline.mtype tstorage)
          ~tparameter:(Of_micheline.mtype tparameter)
          ~lazy_entry_points:None
          ~storage:None
          (Of_micheline.instruction code)
    | _ -> failwith "malformed contract"

  let to_string {tstorage; tparameter; code} =
    Printf.sprintf
      "parameter %s;\nstorage   %s;\ncode\n%s;"
      (string_of_mtype ~protect:() ~html:false tparameter)
      (string_of_mtype ~protect:() ~html:false tstorage)
      (string_of_michCode ~sub_sequence:() ~html:false ~show_types:true 2 code)

  let to_html {tstorage; tparameter; code} =
    Printf.sprintf
      "parameter %s;<br>storage &nbsp;&nbsp;%s;<br><div \
       class='michelsonLine'>code<br>%s;</div>"
      (string_of_mtype ~protect:() ~html:true tparameter)
      (string_of_mtype ~protect:() ~html:true tstorage)
      (string_of_michCode ~sub_sequence:() ~html:true ~show_types:true 2 code)

  let to_html_no_types {tstorage; tparameter; code} =
    Printf.sprintf
      "parameter %s;<br>storage &nbsp;&nbsp;%s;<br><div \
       class='michelsonLine'>code<br>%s;</div>"
      (string_of_mtype ~protect:() ~html:true tparameter)
      (string_of_mtype ~protect:() ~html:true tstorage)
      (string_of_michCode ~sub_sequence:() ~html:true ~show_types:false 2 code)
end

let arities instr =
  match spec_of_instr instr with
  | Some {arities} -> arities
  | _ -> None

let is_mono instr =
  match arities instr with
  | Some (1, 1) -> true
  | _ -> false

let is_bin instr =
  match arities instr with
  | Some (2, 1) -> true
  | Some _ -> false
  | None ->
    ( match instr with
    | MIiter {instr = MIcons} -> true
    | _ -> false )

let is_ternary instr =
  match arities instr with
  | Some (3, 1) -> true
  | _ -> false

let rec mtype_examples t =
  match t.mt with
  | MTunit -> [MLiteral.Unit]
  | MTbool -> [MLiteral.Bool false; MLiteral.Bool true]
  | MTnat ->
      [ MLiteral.small_int 0
      ; MLiteral.small_int 2
      ; MLiteral.small_int 4
      ; MLiteral.small_int 8 ]
  | MTint ->
      [ MLiteral.small_int (-2)
      ; MLiteral.small_int 10
      ; MLiteral.small_int 5
      ; MLiteral.small_int 3 ]
  | MTmutez ->
      [ MLiteral.small_int 0
      ; MLiteral.small_int 1000
      ; MLiteral.small_int 2000000
      ; MLiteral.small_int 3000000 ]
  | MTstring ->
      [ MLiteral.String ""
      ; MLiteral.String "foo"
      ; MLiteral.String "bar"
      ; MLiteral.String "SmartPy" ]
  | MTchain_id | MTbytes ->
      [ MLiteral.String ""
      ; MLiteral.String (Misc.Hex.unhex "00")
      ; MLiteral.String (Misc.Hex.unhex "010203")
      ; MLiteral.String (Misc.Hex.unhex "0FFF") ]
  | MTtimestamp ->
      [ MLiteral.small_int 0
      ; MLiteral.small_int 1000
      ; MLiteral.small_int 2000000
      ; MLiteral.small_int 3000000 ]
  | MTaddress ->
      [ MLiteral.String "tz1..."
      ; MLiteral.String "tz2..."
      ; MLiteral.String "tz3..."
      ; MLiteral.String "KT1..." ]
  | MTkey_hash ->
      [ MLiteral.String "tz1..."
      ; MLiteral.String "tz2..."
      ; MLiteral.String "tz3..." ]
  | MTsignature -> [MLiteral.String "edsigt..."; MLiteral.String "edsigu..."]
  | MTkey ->
      [ MLiteral.String "edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG"
      ; MLiteral.String "edpkvThfdv8Efh1MuqSTUk5EnUFCTjqN6kXDCNXpQ8udN3cKRhNDr2"
      ]
  | MToption t ->
      List.map (fun x -> MLiteral.Some x) (mtype_examples t) @ [MLiteral.None]
  | MTlist t ->
      [MLiteral.Seq (SKlist, mtype_examples t); MLiteral.Seq (SKlist, [])]
  | MTset t -> [MLiteral.Seq (SKset, mtype_examples t)]
  | MTcontract _t ->
      [ MLiteral.String "KT1a..."
      ; MLiteral.String "KT1b..."
      ; MLiteral.String "KT1c..."
      ; MLiteral.String "KT1d..." ]
  | MTpair {fst; snd} ->
      let l1 = mtype_examples fst in
      let l2 = mtype_examples snd in
      begin
        match (l1, l2) with
        | a1 :: a2 :: _, b1 :: b2 :: _ ->
            [ MLiteral.pair a1 b1
            ; MLiteral.pair a2 b2
            ; MLiteral.pair a1 b2
            ; MLiteral.pair a2 b1 ]
        | _ ->
            List.fold_left
              (fun acc b ->
                List.fold_left (fun acc a -> MLiteral.pair a b :: acc) acc l1)
              []
              l2
      end
  | MTor {left; right} ->
      let l1 = mtype_examples left in
      let l2 = mtype_examples right in
      begin
        match (l1, l2) with
        | a1 :: a2 :: _, b1 :: b2 :: _ ->
            [ MLiteral.Left a1
            ; MLiteral.Left a2
            ; MLiteral.Right b1
            ; MLiteral.Right b2 ]
        | _ ->
            List.fold_left
              (fun acc b ->
                List.fold_left (fun acc a -> MLiteral.pair a b :: acc) acc l1)
              []
              l2
      end
  | MTlambda _ -> [MLiteral.Seq (SKinstr, [])]
  | MTmap (k, v) | MTbig_map (k, v) ->
      let l1 = mtype_examples k in
      let l2 = mtype_examples v in
      let rec map2 f acc l1 l2 =
        match (l1, l2) with
        | a1 :: l1, a2 :: l2 -> map2 f (f a1 a2 :: acc) l1 l2
        | _ -> List.rev acc
      in
      let l = map2 (fun a b -> MLiteral.Elt (a, b)) [] l1 l2 in
      [MLiteral.Seq (SKmap, l)]
  | MTmissing s -> [MLiteral.String (Printf.sprintf "no value for %S" s)]
  | MToperation -> [MLiteral.String "operation"]
  | MTsapling_state -> [MLiteral.Seq (SKunknown, [])]
  | MTnever -> [MLiteral.String "no value in type never"]
  | MTsapling_transaction -> [MLiteral.String "sapling_transaction"]
