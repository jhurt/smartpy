(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

val apply_to_contract : env:Typing.env -> tcontract -> tcontract
(**
Specializes certain types to unit in case they are unknown:
- storage, if it is not used
- entry point parameters, if it is not used
- contract parameter, if there are no entry points
*)

val apply_to_scenario : env:Typing.env -> scenario -> scenario
