(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type env =
  { primitives : (module Primitives.Primitives)
  ; scenario_state : scenario_state
  ; storage_expr : Expr.t
  ; entry_point : (string * Type.t) option
  ; entry_points : (string, Type.t) Hashtbl.t
  ; tglobal_params : Type.t option }

let attribute_source name =
  match Base.String.substr_index name ~pattern:" as " with
  | None -> name
  | Some i -> String.sub name 0 i

let attribute_target name =
  match Base.String.substr_index name ~pattern:" as " with
  | None -> name
  | Some i -> String.sub name (i + 4) (String.length name - i - 4)

let rec import_inner_layout = function
  | Base.Sexp.List [Atom leaf] ->
      Type.Layout_leaf
        {source = attribute_source leaf; target = attribute_target leaf}
  | Base.Sexp.List [n1; n2] ->
      Type.Layout_pair (import_inner_layout n1, import_inner_layout n2)
  | l -> failwith ("Layout format error " ^ Base.Sexp.to_string l)

let import_layout l layout =
  match l with
  | [] -> ref (Type.UnUnknown "")
  | _ ->
      let layout =
        match layout with
        | Base.Sexp.Atom "None" -> Type.default_layout_of_row l
        | List [Atom "Some"; Atom "Right"] -> Type.comb_layout_of_row `Right l
        | List [Atom "Some"; l] -> import_inner_layout l
        | l -> failwith ("Layout format error " ^ Base.Sexp.to_string l)
      in
      ref (Type.UnValue layout)

let rec assocList (n : string) = function
  | Base.Sexp.Atom (a : string) :: List l :: _ when Stdlib.(a = n) -> l
  | _ :: l -> assocList n l
  | _ -> failwith ("Cannot find " ^ n)

(** Import the expression in an environment where params and storage
   types aren't known yet. For contract constructors. *)
let early_env primitives scenario_state =
  { primitives
  ; scenario_state
  ; storage_expr = Expr.local ~line_no:(-1) "__storage__"
  ; entry_point = None
  ; entry_points = Hashtbl.create 5
  ; tglobal_params = None }

let rec import_type typing_env =
  let open Type in
  let rec import_type = function
    | Base.Sexp.Atom "unit" -> unit
    | Atom "bool" -> bool
    | Atom "int" -> int ()
    | Atom "nat" -> nat ()
    | Atom "intOrNat" -> intOrNat ()
    | Atom "string" -> string
    | Atom "bytes" -> bytes
    | Atom "mutez" -> token
    | Atom "timestamp" -> timestamp
    | Atom "address" -> address
    | Atom "key_hash" -> key_hash
    | Atom "key" -> key
    | Atom "signature" -> signature
    | Atom "operation" -> operation
    | Atom "sapling_state" -> sapling_state
    | Atom "sapling_transaction" -> sapling_transaction
    | Atom "never" -> never
    | List [Atom "unknown"; Atom id] -> Typing.unknown typing_env ("sp:" ^ id)
    | List [Atom "record"; List l; layout] ->
        let l = List.map importField l in
        record_or_unit (import_layout l layout) l
    | List [Atom "variant"; List l; layout] ->
        let l = List.map importField l in
        variant (import_layout l layout) l
    | List [Atom "list"; t] -> list (import_type t)
    | List [Atom "option"; t] -> option (import_type t)
    | List [Atom "contract"; t] -> contract (import_type t)
    | List [Atom "pair"; t1; t2] -> pair (import_type t1) (import_type t2)
    | List [Atom "set"; t] -> set ~telement:(import_type t)
    | List [Atom "map"; k; v] ->
        map
          ~big:(ref (UnValue false))
          ~tkey:(import_type k)
          ~tvalue:(import_type v)
    | List [Atom "bigmap"; k; v] ->
        map
          ~big:(ref (UnValue true))
          ~tkey:(import_type k)
          ~tvalue:(import_type v)
    | List [Atom "lambda"; t1; t2] -> lambda (import_type t1) (import_type t2)
    | List l as t ->
        failwith
          ( "Type format error list "
          ^ Base.Sexp.to_string t
          ^ "  "
          ^ string_of_int (List.length l) )
    | Atom _ as t -> failwith ("Type format error atom " ^ Base.Sexp.to_string t)
  and importField = function
    | List [Atom name; v] -> (name, import_type v)
    | l -> failwith ("Type field format error " ^ Base.Sexp.to_string l)
  in
  import_type

and import_literal =
  let open Literal in
  function
  | [Base.Sexp.Atom "unit"] -> unit
  | [Atom "string"; Atom n] -> string n
  | [Atom "bytes"; Atom n] ->
      let n =
        Base.(String.chop_prefix n ~prefix:"0x" |> Option.value ~default:n)
      in
      bytes (Misc.Hex.unhex n)
  | [Atom "chain_id_cst"; Atom n] ->
      let n =
        Base.(String.chop_prefix n ~prefix:"0x" |> Option.value ~default:n)
      in
      chain_id (Misc.Hex.unhex n)
  | [Atom "int"; Atom n] -> int (Big_int.big_int_of_string n)
  | [Atom "intOrNat"; Atom n] ->
      intOrNat (Type.intOrNat ()) (Big_int.big_int_of_string n)
  | [Atom "nat"; Atom n] -> nat (Big_int.big_int_of_string n)
  | [Atom "timestamp"; Atom n] -> timestamp (Big_int.big_int_of_string n)
  | [Atom "bool"; Atom "True"] -> bool true
  | [Atom "bool"; Atom "False"] -> bool false
  | [Atom "key"; Atom n] -> key n
  | [Atom "secret_key"; Atom n] -> secret_key n
  | [Atom "signature"; Atom n] -> signature n
  | [Atom "address"; Atom n] -> address n
  | [Atom "local-address"; Atom n] ->
      let static_id = int_of_string n in
      local_address (Literal.C_static {static_id})
  | [Atom "key_hash"; Atom n] -> key_hash n
  | [Atom "mutez"; List l] -> mutez (unInt (import_literal l))
  | [Atom "mutez"; Atom n] -> mutez (Big_int.big_int_of_string n)
  | [Atom "sapling_test_transaction"; Atom source; Atom target; Atom amount; _]
    ->
      let source = if source = "" then None else Some source in
      let target = if target = "" then None else Some target in
      sapling_test_transaction source target (Big_int.big_int_of_string amount)
  | l ->
      Format.kasprintf failwith "Literal format error: %a" Base.Sexp.pp (List l)

and import_expr_inline_michelson import_env typing_env =
  let import_type t = import_type typing_env t in
  let rec import_expr_inline_michelson = function
    | Base.Sexp.List [Atom "call_michelson"; instr; Atom _line_no] ->
        import_expr_inline_michelson instr
    | Base.Sexp.List (Atom "op" :: Atom name :: args) ->
        let module P = (val import_env.primitives : Primitives.Primitives) in
        let parsed = P.Parser.json_of_micheline name in
        let rec extractTypes acc = function
          | [] -> assert false
          | Base.Sexp.Atom "out" :: out ->
              (List.rev acc, List.map import_type out)
          | t :: args -> extractTypes (import_type t :: acc) args
        in
        let typesIn, typesOut = extractTypes [] args in
        {name; parsed; typesIn; typesOut}
    | input ->
        failwith
          (Printf.sprintf
             "Cannot parse inline michelson %s"
             (Base.Sexp.to_string input))
  in
  import_expr_inline_michelson

and import_expr (typing_env : Typing.env) (import_env : env) =
  let import_type t = import_type typing_env t in
  let import_expr_inline_michelson =
    import_expr_inline_michelson import_env typing_env
  in
  let open Expr in
  let rec import_expr = function
    | Base.Sexp.List (Atom f :: args) as input ->
      ( match (f, args) with
      | "sender", [] -> sender
      | "self", [] ->
        ( match import_env.tglobal_params with
        | None -> failwith "import: params type yet unknown"
        | Some t -> self t )
      | "self_entry_point", [Atom name; Atom line_no] ->
          let name =
            if name = ""
            then
              match import_env.entry_point with
              | None -> failwith "import: params type yet unknown"
              | Some (name, _) -> name
            else name
          in
          ( match Hashtbl.find_opt import_env.entry_points name with
          | None ->
              raise
                (SmartExcept
                   [ `Text "Import Error"
                   ; `Text (Printf.sprintf "Missing entry point %s" name)
                   ; `Line (int_of_string line_no) ])
          | Some t -> self_entry_point name t )
      | "chain_id", [] -> chain_id
      | "source", [] -> source
      | "now", [] -> now
      | "amount", [] -> amount
      | "balance", [] -> balance
      | "eq", [e1; e2; Atom line_no] ->
          eq ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "neq", [e1; e2; Atom line_no] ->
          neq ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "le", [e1; e2; Atom line_no] ->
          le ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "lt", [e1; e2; Atom line_no] ->
          lt ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "ge", [e1; e2; Atom line_no] ->
          ge ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "gt", [e1; e2; Atom line_no] ->
          gt ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "add", [e1; e2; Atom line_no] ->
          add ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "sub", [e1; e2; Atom line_no] ->
          sub ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "mul", [e1; e2; Atom line_no] ->
          mul ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "ediv", [e1; e2; Atom line_no] ->
          ediv
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "truediv", [e1; e2; Atom line_no] ->
          div ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "floordiv", [e1; e2; Atom line_no] ->
          div ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "mod", [e1; e2; Atom line_no] ->
          e_mod
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "or", [e1; e2; Atom line_no] ->
          b_or
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "and", [e1; e2; Atom line_no] ->
          b_and
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "max", [e1; e2; Atom line_no] ->
          e_max
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "min", [e1; e2; Atom line_no] ->
          e_min
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "sum", [a; Atom line_no] ->
          sum ~line_no:(int_of_string line_no) (import_expr a)
      | "to_address", [e; Atom line_no] ->
          contract_address ~line_no:(int_of_string line_no) (import_expr e)
      | "implicit_account", [e; Atom line_no] ->
          implicit_account ~line_no:(int_of_string line_no) (import_expr e)
      | "cons", [e1; e2; Atom line_no] ->
          cons
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "range", [e1; e2; e3; Atom line_no] ->
          range
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
            (import_expr e3)
      | "literal", [List l; Atom line_no] ->
          cst ~line_no:(int_of_string line_no) (import_literal l)
      | "list", Atom line_no :: l ->
          build_list
            ~line_no:(int_of_string line_no)
            ~elems:(List.map import_expr l)
      | "first", [e; Atom line_no] ->
          first ~line_no:(int_of_string line_no) (import_expr e)
      | "second", [e; Atom line_no] ->
          second ~line_no:(int_of_string line_no) (import_expr e)
      | "tuple", [e1; e2; Atom line_no] ->
          pair
            ~line_no:(int_of_string line_no)
            (import_expr e1)
            (import_expr e2)
      | "tuple", _ ->
          failwith
            (Printf.sprintf
               "Only supported tuples are pairs: %s"
               (Base.Sexp.to_string input))
      | "neg", [e1; Atom line_no] ->
          negE ~line_no:(int_of_string line_no) (import_expr e1)
      | "abs", [e1; Atom line_no] ->
          absE ~line_no:(int_of_string line_no) (import_expr e1)
      | "toInt", [e1; Atom line_no] ->
          to_int ~line_no:(int_of_string line_no) (import_expr e1)
      | "isNat", [e1; Atom line_no] ->
          is_nat ~line_no:(int_of_string line_no) (import_expr e1)
      | "sign", [e1; Atom line_no] ->
          signE ~line_no:(int_of_string line_no) (import_expr e1)
      | "invert", [e1; Atom line_no] ->
          notE ~line_no:(int_of_string line_no) (import_expr e1)
      | "contractBalance", [Atom id; Atom line_no] ->
          let line_no = int_of_string line_no in
          let static_id = int_of_string id in
          contract_balance ~line_no (C_static {static_id})
      | "contract_baker", [Atom id; Atom line_no] ->
          let line_no = int_of_string line_no in
          let static_id = int_of_string id in
          contract_baker ~line_no (C_static {static_id})
      | "contractData", [Atom id; Atom line_no] ->
          let line_no = int_of_string line_no in
          let static_id = int_of_string id in
          contract_data ~line_no (C_static {static_id})
      | "contract", [Atom entry_point; t; addr; Atom line_no] ->
          let entry_point =
            match entry_point with
            | "" -> None
            | _ -> Some entry_point
          in
          let address = import_expr addr in
          contract
            ~line_no:(int_of_string line_no)
            entry_point
            (import_type t)
            address
      | "data", [] -> import_env.storage_expr
      | "operations", [Atom line_no] ->
          operations ~line_no:(int_of_string line_no)
      | "attr", [x; Atom name; Atom line_no] ->
          attr ~line_no:(int_of_string line_no) (import_expr x) name
      | "match_cons", [_e; Atom line_no] ->
          let name = Printf.sprintf "match_cons_%s" line_no in
          match_cons ~line_no:(int_of_string line_no) name
      | "isVariant", [x; Atom name; Atom line_no] ->
          isVariant ~line_no:(int_of_string line_no) name (import_expr x)
      | "variant_arg", [Atom arg_name; Atom line_no] ->
          variant_arg ~line_no:(int_of_string line_no) arg_name
      | "openVariant", [x; Atom name; Atom line_no] ->
          openVariant ~line_no:(int_of_string line_no) name (import_expr x)
      | "variant", [Atom name; x; Atom line_no] ->
          variant ~line_no:(int_of_string line_no) name (import_expr x)
      | "hashCrypto", [Atom algo; e; Atom line_no] ->
          let algo =
            match algo with
            | "BLAKE2B" -> BLAKE2B
            | "SHA256" -> SHA256
            | "SHA512" -> SHA512
            | _ -> failwith ("Unknown hash algorithm: " ^ algo)
          in
          hashCrypto ~line_no:(int_of_string line_no) algo (import_expr e)
      | "hash_key", [e; Atom line_no] ->
          hash_key ~line_no:(int_of_string line_no) (import_expr e)
      | "pack", [e; Atom line_no] ->
          let e = import_expr e in
          pack ~line_no:(int_of_string line_no) e
      | "unpack", [e; t; Atom line_no] ->
          unpack
            ~line_no:(int_of_string line_no)
            (import_expr e)
            (import_type t)
      | "getLocal", [Atom name; Atom line_no] ->
          local ~line_no:(int_of_string line_no) name
      | "params", [Atom line_no] ->
          let line_no = int_of_string line_no in
          ( match import_env.entry_point with
          | None -> failwith "import_expr: params type yet unknown"
          | Some (_, t) -> params ~line_no t )
      | "update_map", [map; key; v; Atom line_no] ->
          updateMap
            ~line_no:(int_of_string line_no)
            (import_expr map)
            (import_expr key)
            (import_expr v)
      | "getItem", [a; pos; Atom line_no] ->
          item
            ~line_no:(int_of_string line_no)
            (import_expr a)
            (import_expr pos)
            None
            None
      | "getItemDefault", [a; pos; def; Atom line_no] ->
          item
            ~line_no:(int_of_string line_no)
            (import_expr a)
            (import_expr pos)
            (Some (import_expr def))
            None
      | "getItemMessage", [a; pos; message; Atom line_no] ->
          item
            ~line_no:(int_of_string line_no)
            (import_expr a)
            (import_expr pos)
            None
            (Some (import_expr message))
      | "add_seconds", [t; s; Atom line_no] ->
          add_seconds
            ~line_no:(int_of_string line_no)
            (import_expr t)
            (import_expr s)
      | "iter", [Atom name; Atom line_no] ->
          iterator ~line_no:(int_of_string line_no) name
      | "rev", [e; Atom line_no] ->
          listRev ~line_no:(int_of_string line_no) (import_expr e)
      | "items", [e; Atom line_no] ->
          listItems ~line_no:(int_of_string line_no) (import_expr e) false
      | "keys", [e; Atom line_no] ->
          listKeys ~line_no:(int_of_string line_no) (import_expr e) false
      | "values", [e; Atom line_no] ->
          listValues ~line_no:(int_of_string line_no) (import_expr e) false
      | "elements", [e; Atom line_no] ->
          listElements ~line_no:(int_of_string line_no) (import_expr e) false
      | "rev_items", [e; Atom line_no] ->
          listItems ~line_no:(int_of_string line_no) (import_expr e) true
      | "rev_keys", [e; Atom line_no] ->
          listKeys ~line_no:(int_of_string line_no) (import_expr e) true
      | "rev_values", [e; Atom line_no] ->
          listValues ~line_no:(int_of_string line_no) (import_expr e) true
      | "rev_elements", [e; Atom line_no] ->
          listElements ~line_no:(int_of_string line_no) (import_expr e) true
      | "contains", [items; x; Atom line_no] ->
          contains
            ~line_no:(int_of_string line_no)
            (import_expr items)
            (import_expr x)
      | "check_signature", [pk; s; msg; Atom line_no] ->
          check_signature
            ~line_no:(int_of_string line_no)
            (import_expr pk)
            (import_expr s)
            (import_expr msg)
      | "reduce", [e; Atom line_no] ->
          reduce ~line_no:(int_of_string line_no) (import_expr e)
      | "scenario_var", [Atom id; Atom line_no] ->
          let t = Type.full_unknown () in
          scenario_var ~line_no:(int_of_string line_no) (int_of_string id) t
      | "make_signature", [sk; msg; Atom fmt; Atom line_no] ->
          let secret_key = import_expr sk in
          let message = import_expr msg in
          let message_format =
            match String.lowercase_ascii fmt with
            | "raw" -> `Raw
            | "hex" -> `Hex
            | other ->
                Format.kasprintf
                  failwith
                  "make_signature: Wrong message format : %S (l. %s)"
                  other
                  line_no
          in
          make_signature
            ~secret_key
            ~message
            ~message_format
            ~line_no:(int_of_string line_no)
      | "account_of_seed", [Atom seed; Atom line_no] ->
          account_of_seed ~seed ~line_no:(int_of_string line_no)
      | "split_tokens", [mutez; quantity; total; Atom line_no] ->
          split_tokens
            ~line_no:(int_of_string line_no)
            (import_expr mutez)
            (import_expr quantity)
            (import_expr total)
      | "slice", [ofs; len; buf; Atom line_no] ->
          let offset = import_expr ofs in
          let length = import_expr len in
          let buffer = import_expr buf in
          slice ~offset ~length ~buffer ~line_no:(int_of_string line_no)
      | "concat", [l; Atom line_no] ->
          concat_list (import_expr l) ~line_no:(int_of_string line_no)
      | "size", [s; Atom line_no] ->
          size (import_expr s) ~line_no:(int_of_string line_no)
      | "type_annotation", [e; t; Atom line_no] ->
          let e = import_expr e in
          let t = import_type t in
          let line_no = int_of_string line_no in
          type_annotation ~line_no e t
      | "set_record_layout", [e; layout; Atom line_no] ->
          let line_no = int_of_string line_no in
          let e = import_expr e in
          set_record_layout ~env:typing_env e.et (`Expr e) layout line_no;
          e
      | "set_variant_layout", [e; layout; Atom line_no] ->
          let line_no = int_of_string line_no in
          let e = import_expr e in
          set_variant_layout ~env:typing_env e.et (`Expr e) layout line_no;
          e
      | "map", Atom line_no :: entries ->
          build_map
            ~line_no:(int_of_string line_no)
            ~big:false
            ~entries:(List.map import_map_entry entries)
      | "set", Atom line_no :: entries ->
          build_set
            ~line_no:(int_of_string line_no)
            ~entries:(List.map import_expr entries)
      | "big_map", Atom line_no :: entries ->
          build_map
            ~line_no:(int_of_string line_no)
            ~big:true
            ~entries:(List.map import_map_entry entries)
      | "record", Atom line_no :: l ->
          let import_exprField = function
            | Base.Sexp.List [Atom name; e] -> (name, import_expr e)
            | l ->
                failwith
                  ("Expression field format error " ^ Base.Sexp.to_string l)
          in
          record ~line_no:(int_of_string line_no) (List.map import_exprField l)
      | "ematch", Atom line_no :: scrutinee :: l ->
          let import_clause = function
            | Base.Sexp.List [Atom name; v] ->
                let e = import_expr v in
                (name, e)
            | l -> failwith ("Clause format error: " ^ Base.Sexp.to_string l)
          in
          let scrutinee = import_expr scrutinee in
          ematch
            ~line_no:(int_of_string line_no)
            scrutinee
            (List.map import_clause l)
      | "eif", [cond; a; b; Atom line_no] ->
          let cond = import_expr cond in
          let a = import_expr a in
          let b = import_expr b in
          eif ~line_no:(int_of_string line_no) cond a b
      | "call_michelson", instr :: Atom line_no :: args ->
          inline_michelson
            ~line_no:(int_of_string line_no)
            [import_expr_inline_michelson instr]
            (List.map import_expr args)
      | "seq_michelson", Atom line_no :: Atom length :: args ->
          let rec split n acc args =
            if n = 0
            then (List.rev acc, args)
            else
              match args with
              | [] -> assert false
              | a :: args -> split (n - 1) (a :: acc) args
          in
          let ops, args = split (int_of_string length) [] args in
          inline_michelson
            ~line_no:(int_of_string line_no)
            (List.map import_expr_inline_michelson ops)
            (List.map import_expr args)
      | "global", [Atom name; Atom line_no] ->
          global ~line_no:(int_of_string line_no) name
      | "map_function", [l; f; Atom line_no] ->
          map_function
            ~line_no:(int_of_string line_no)
            (import_expr l)
            (Expr.allow_lambda_full_stack (import_expr f))
      | "lambda", [Atom id; Atom name; Atom line_no; List commands] ->
          let clean_stack = true in
          import_lambda id name line_no commands clean_stack
      | "lambdaParams", [Atom id; Atom name; Atom line_no; _tParams] ->
          let line_no = int_of_string line_no in
          lambdaParams ~line_no (int_of_string id) name
      | "call_lambda", [lambda; parameter; Atom line_no] ->
          call_lambda
            ~line_no:(int_of_string line_no)
            (import_expr lambda)
            (import_expr parameter)
      | "apply_lambda", [lambda; parameter; Atom line_no] ->
          apply_lambda
            ~line_no:(int_of_string line_no)
            (import_expr lambda)
            (import_expr parameter)
      | "lshift", [expression; shift; Atom line_no] ->
          lshift
            ~line_no:(int_of_string line_no)
            (import_expr expression)
            (import_expr shift)
      | "rshift", [expression; shift; Atom line_no] ->
          rshift
            ~line_no:(int_of_string line_no)
            (import_expr expression)
            (import_expr shift)
      | "xor", [e1; e2; Atom line_no] ->
          xor ~line_no:(int_of_string line_no) (import_expr e1) (import_expr e2)
      | "set_delegate", [x; Atom line_no] ->
          set_delegate ~line_no:(int_of_string line_no) (import_expr x)
      | "sapling_empty_state", [Atom _line_no] -> sapling_empty_state
      | "sapling_verify_update", [state; transaction; Atom line_no] ->
          sapling_verify_update
            ~line_no:(int_of_string line_no)
            (import_expr state)
            (import_expr transaction)
      | "transfer", [e1; e2; e3; Atom line_no] ->
          transfer
            ~line_no:(int_of_string line_no)
            ~arg:(import_expr e1)
            ~amount:(import_expr e2)
            ~destination:(import_expr e3)
      | ( "create_contract"
        , [ List [Atom "contract"; contract]
          ; List [Atom "storage"; storage]
          ; List [Atom "baker"; baker]
          ; List [Atom "amount"; amount]
          ; Atom line_no ] ) ->
          let contract_template =
            let typing_env = Typing.reset_constraints typing_env in
            import_contract
              ~do_fix:true
              ~primitives:import_env.primitives
              ~scenario_state:import_env.scenario_state
              typing_env
              contract
          in
          let tcontract = contract_template.tcontract in
          let storage =
            match (storage, tcontract.storage) with
            | Atom "None", Some storage -> storage
            | Atom "None", None -> assert false
            | e, _ -> import_expr e
          in
          let balance = import_expr amount in
          let baker =
            match baker with
            | Atom "None" -> Expr.none ~line_no:(-1)
            | e -> import_expr e
          in
          create_contract
            ~line_no:(int_of_string line_no)
            ~baker
            {tcontract = {tcontract with balance; storage = Some storage}}
      | s, l ->
        ( try cst ~line_no:(-1) (import_literal (Atom s :: l)) with
        | _ ->
            failwith
              (Printf.sprintf
                 "Expression format error (a %i) %s"
                 (List.length l)
                 (Base.Sexp.to_string_hum input)) ) )
    | x -> failwith ("Expression format error (b) " ^ Base.Sexp.to_string_hum x)
  and import_map_entry = function
    | Base.Sexp.List [k; v] -> (import_expr k, import_expr v)
    | e ->
        failwith
          (Printf.sprintf "import_map_entry: '%s'" (Base.Sexp.to_string e))
  and import_lambda id name line_no body clean_stack =
    let line_no = int_of_string line_no in
    let tParams = Typing.unknown typing_env (Printf.sprintf "lambda %s" id) in
    let tResult =
      Typing.unknown typing_env (Printf.sprintf "lambda result %s" id)
    in
    lambda
      ~line_no
      (int_of_string id)
      name
      tParams
      tResult
      (Command.seq ~line_no (import_commands typing_env import_env body))
      clean_stack
  in
  import_expr

and import_command typing_env import_env =
  let import_type t = import_type typing_env t in
  let import_expr e = import_expr typing_env import_env e in
  let import_commands cs = import_commands typing_env import_env cs in
  let open Command in
  function
  | Base.Sexp.List (Atom f :: args) as input ->
    ( match (f, args) with
    | "bind", [Atom line_no; Atom x; List c1; List c2] ->
        bind
          ~line_no:(int_of_string line_no)
          (Some x)
          (seq ~line_no:(int_of_string line_no) (import_commands c1))
          (seq ~line_no:(int_of_string line_no) (import_commands c2))
    | "result", [x; Atom line_no] ->
        result ~line_no:(int_of_string line_no) (import_expr x)
    | "failwith", [x; Atom line_no] ->
        sp_failwith ~line_no:(int_of_string line_no) (import_expr x)
    | "never", [x; Atom line_no] ->
        never ~line_no:(int_of_string line_no) (import_expr x)
    | "verify", [x; ghost; Atom line_no] ->
        verify
          ~line_no:(int_of_string line_no)
          (import_expr x)
          (ghost = Atom "True")
          None
    | "verify", [x; ghost; message; Atom line_no] ->
        verify
          ~line_no:(int_of_string line_no)
          (import_expr x)
          (ghost = Atom "True")
          (Some (import_expr message))
    | "defineLocal", [Atom name; expr; Atom line_no] ->
        defineLocal ~line_no:(int_of_string line_no) name (import_expr expr)
    | "whileBlock", [e; List l; Atom line_no] ->
        let line_no = int_of_string line_no in
        whileLoop ~line_no (import_expr e) (seq ~line_no (import_commands l))
    | "forGroup", [Atom name; e; List l; Atom line_no] ->
        let e = import_expr e in
        let line_no = int_of_string line_no in
        forGroup ~line_no name e (seq ~line_no (import_commands l))
    | ( "match"
      , [scrutinee; Atom constructor; Atom arg_name; List body; Atom line_no] )
      ->
        let line_no = int_of_string line_no in
        mk_match
          ~line_no
          (import_expr scrutinee)
          [(constructor, arg_name, seq ~line_no (import_commands body))]
    | "match_cases", [scrutinee; _arg; List cases; Atom line_no] ->
        let line_no = int_of_string line_no in
        let parse_case = function
          | Base.Sexp.List
              [ Atom "match"
              ; _
              ; Atom constructor
              ; Atom arg_name
              ; List body
              ; Atom line_no ] ->
              let line_no = int_of_string line_no in
              (constructor, arg_name, seq ~line_no (import_commands body))
          | input -> failwith ("Bad case parsing: " ^ Base.Sexp.to_string input)
        in
        let cases = List.map parse_case cases in
        mk_match ~line_no (import_expr scrutinee) cases
    | "set_type", [e; t; Atom line_no] ->
        setType ~line_no:(int_of_string line_no) (import_expr e) (import_type t)
    | "set_record_layout", [e; layout; Atom line_no] ->
        let line_no = int_of_string line_no in
        let e = import_expr e in
        set_record_layout ~env:typing_env e.et (`Expr e) layout line_no;
        seq ~line_no []
    | "set_variant_layout", [e; layout; Atom line_no] ->
        let line_no = int_of_string line_no in
        let e = import_expr e in
        set_variant_layout ~env:typing_env e.et (`Expr e) layout line_no;
        seq ~line_no []
    | "set_type_record_layout", [t; layout; Atom line_no] ->
        let line_no = int_of_string line_no in
        let t = import_type t in
        set_record_layout ~env:typing_env t (`Type t) layout line_no;
        seq ~line_no []
    | "set_type_variant_layout", [t; layout; Atom line_no] ->
        let line_no = int_of_string line_no in
        let t = import_type t in
        set_variant_layout ~env:typing_env t (`Type t) layout line_no;
        seq ~line_no []
    | "set", [v; e; Atom line_no] ->
        set ~line_no:(int_of_string line_no) (import_expr v) (import_expr e)
    | "delItem", [e; k; Atom line_no] ->
        delItem ~line_no:(int_of_string line_no) (import_expr e) (import_expr k)
    | "updateSet", [e; k; Atom b; Atom line_no] ->
        updateSet
          ~line_no:(int_of_string line_no)
          (import_expr e)
          (import_expr k)
          (b = "True")
    | _ -> failwith ("Command format error (a) " ^ Base.Sexp.to_string input) )
  | input -> failwith ("Command format error (b) " ^ Base.Sexp.to_string input)

and import_commands typing_env import_env =
  let import_expr e = import_expr typing_env import_env e in
  let import_command c = import_command typing_env import_env c in
  let open Command in
  let rec import_commands = function
    | Base.Sexp.List [Atom "ifBlock"; e; List tBlock; Atom line_no]
      :: List [Atom "elseBlock"; List eBlock] :: rest ->
        let line_no = int_of_string line_no in
        let c =
          ifte
            ~line_no
            (import_expr e)
            (seq ~line_no (import_commands tBlock))
            (seq ~line_no (import_commands eBlock))
        in
        c :: import_commands rest
    | List [Atom "ifBlock"; e; List tBlock; Atom line_no] :: rest ->
        let c =
          let line_no = int_of_string line_no in
          ifte
            ~line_no
            (import_expr e)
            (seq ~line_no (import_commands tBlock))
            (seq ~line_no [])
        in
        c :: import_commands rest
    | List [Atom "ifSomeBlock"; e; Atom _; List tBlock; Atom line_no]
      :: List [Atom "elseBlock"; List eBlock] :: rest ->
        let line_no = int_of_string line_no in
        let c =
          ifteSome
            ~line_no
            (import_expr e)
            (seq ~line_no (import_commands tBlock))
            (seq ~line_no (import_commands eBlock))
        in
        c :: import_commands rest
    | List [Atom "ifSomeBlock"; e; Atom _; List tBlock; Atom line_no] :: rest ->
        let line_no = int_of_string line_no in
        let c =
          ifteSome
            ~line_no
            (import_expr e)
            (seq ~line_no (import_commands tBlock))
            (seq ~line_no [])
        in
        c :: import_commands rest
    | List [Atom "match_cons"; expr; List ok_match; Atom line_no_]
      :: List [Atom "elseBlock"; List ko_match] :: rest ->
        let m = import_match_cons expr ok_match ko_match line_no_ in
        m :: import_commands rest
    | List [Atom "match_cons"; expr; List ok_match; Atom line_no_] :: rest ->
        let m = import_match_cons expr ok_match [] line_no_ in
        m :: import_commands rest
    | x :: l ->
        let x = import_command x in
        (* This sequencing is important for type inferrence. *)
        x :: import_commands l
    | [] -> []
  and import_match_cons expr ok_match ko_match line_no_ =
    (* let t = Type.full_unknown () in *)
    let expr = import_expr expr in
    (* Typing.assertEqual expr.et (import_type t) ~pp:(fun _ -> assert false); *)
    let line_no = int_of_string line_no_ in
    mk_match_cons
      ~line_no
      expr
      (Printf.sprintf "match_cons_%s" line_no_)
      (seq ~line_no (import_commands ok_match))
      (seq ~line_no (import_commands ko_match))
  in
  import_commands

and set_record_layout ~env t msg layout line_no =
  let layout = import_inner_layout layout in
  Typing.assertEqual
    ~line_no
    ~env
    t
    (Type.unknown_record_layout layout)
    ~pp:(fun () -> [msg; `Text "is not compatible with layout"; `Line line_no])

and set_variant_layout ~env t msg layout line_no =
  let layout = import_inner_layout layout in
  Typing.assertEqual
    ~line_no
    ~env
    t
    (Type.unknown_variant_layout layout)
    ~pp:(fun () -> [msg; `Text "is not compatible with layout"; `Line line_no])

and import_contract
    ~do_fix ~primitives ~scenario_state (typing_env : Typing.env) = function
  | Base.Sexp.Atom _ -> failwith "Parse error contract"
  | List l ->
      let storage = assocList "storage" l in
      let storage_type =
        match assocList "storage_type" l with
        | [List []] -> None
        | [t] -> Some t
        | _ -> assert false
      in
      let storage_layout = assocList "storage_layout" l in
      let entry_points_layout = assocList "entry_points_layout" l in
      let messages = assocList "messages" l in
      let flags = assocList "flags" l in
      let global_variables = assocList "globals" l in
      let entry_points = Hashtbl.create 6 in
      let messages =
        List.map
          (function
            | Base.Sexp.List [Atom name; Atom originate; List command] ->
                let originate = originate = "True" in
                let t = Type.full_unknown () in
                Hashtbl.replace entry_points name t;
                ((name, originate, command), t)
            | x -> failwith ("Message format error " ^ Base.Sexp.to_string x))
          messages
      in
      let global_variables =
        List.map
          (function
            | Base.Sexp.List [Atom name; variable] -> (name, variable)
            | x ->
                failwith
                  ("Global variable format error " ^ Base.Sexp.to_string x))
          global_variables
      in
      let env0 = early_env primitives scenario_state in
      let build_global_variable (name, variable) =
        let import_env =
          { primitives
          ; scenario_state
          ; entry_point = None
          ; storage_expr = env0.storage_expr
          ; entry_points = Hashtbl.create 5
          ; tglobal_params = None }
        in
        let expression = import_expr typing_env import_env variable in
        (name, expression)
      in
      let global_variables = List.map build_global_variable global_variables in
      let entry_points_layout =
        match entry_points_layout with
        | [] -> ref (Type.UnUnknown "")
        | _ ->
            ref
              (Type.UnValue
                 (import_inner_layout (Base.Sexp.List entry_points_layout)))
      in
      let tparameter =
        match
          List.filter (fun ((_, originate, _), _) -> originate) messages
        with
        | [] -> Type.unit
        | l ->
            Type.variant
              entry_points_layout
              (List.map (fun ((name, _, _), t) -> (name, t)) l)
      in
      let tstorage, storage, storage_line_no =
        let get_storage storage =
          import_expr typing_env env0 (Base.Sexp.List storage)
        in
        let get_storage_type t = import_type typing_env t in
        match (storage, storage_type) with
        | [], None ->
            raise
              (SmartExcept
                 [ `Text "Import Error"
                 ; `Text
                     (Printf.sprintf
                        "Missing contract storage and storage type")
                 ; `Line (-1) ])
        | [], Some t -> (get_storage_type t, None, -1)
        | storage, None ->
            let storage = get_storage storage in
            (storage.et, Some storage, storage.el)
        | storage, Some t ->
            let storage = get_storage storage in
            let t = get_storage_type t in
            Typing.assertEqual
              ~line_no:storage.el
              ~env:typing_env
              t
              storage.et
              ~pp:(fun () ->
                [ `Text "Type error in storage"
                ; `Expr storage
                ; `Type t
                ; `Line storage.el ]);
            (storage.et, Some storage, storage.el)
      in
      ( match storage_layout with
      | [] -> ()
      | _ ->
          set_record_layout
            ~env:typing_env
            tstorage
            (`Text "Storage layout")
            (Base.Sexp.List storage_layout)
            storage_line_no );
      let build_entry_point ((name, originate, command), t) =
        let import_env =
          { primitives
          ; scenario_state
          ; entry_point = Some (name, t)
          ; storage_expr = env0.storage_expr
          ; entry_points
          ; tglobal_params = Some tparameter }
        in
        let body =
          Command.seq
            ~line_no:(-1)
            (import_commands typing_env import_env command)
        in
        match import_env.entry_point with
        | Some (_, paramsType) -> {channel = name; paramsType; originate; body}
        | None -> assert false
      in
      let entry_points = List.map build_entry_point messages in
      let importFlag = function
        | Base.Sexp.Atom "simplify_via_michel" -> Simplify_via_michel
        | Base.Sexp.Atom "lazy_entry_points_multiple" ->
            Lazy_entry_points_multiple
        | Base.Sexp.Atom "lazy_entry_points" -> Lazy_entry_points_single
        | Base.Sexp.Atom "Exception_FullDebug" ->
            Exception_optimization `FullDebug
        | Base.Sexp.Atom "Exception_DebugMessage" ->
            Exception_optimization `Message
        | Base.Sexp.Atom "Exception_VerifyOrLine" ->
            Exception_optimization `VerifyOrLine
        | Base.Sexp.Atom "Exception_DefaultLine" ->
            Exception_optimization `DefaultLine
        | Base.Sexp.Atom "Exception_Line" -> Exception_optimization `Line
        | Base.Sexp.Atom "Exception_DefaultUnit" ->
            Exception_optimization `DefaultUnit
        | Base.Sexp.Atom "Exception_Unit" -> Exception_optimization `Unit
        | Base.Sexp.Atom "no_comment" -> No_comment
        | x -> failwith ("Unknown Contract Flag: " ^ Base.Sexp.to_string x)
      in
      let flags = List.map importFlag flags in
      let c =
        { tcontract =
            { balance =
                Expr.cst ~line_no:(-1) (Literal.mutez Big_int.zero_big_int)
            ; storage
            ; baker = Expr.none ~line_no:(-1)
            ; tstorage
            ; tparameter
            ; entry_points
            ; entry_points_layout
            ; unknown_parts = None
            ; flags
            ; global_variables } }
      in
      let fixEnv =
        Mangler.init_env
          ~reducer:(Interpreter.reducer ~primitives ~scenario_state)
          ()
      in
      if do_fix then Fixer.fix_expr_contract fixEnv typing_env c else c
