(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Control
open Type
open Binary_tree
open Expr
module TO = Binary_tree.Traversable (Option)

let debug_level = 0

type transformer =
  | Atom          of (texpr -> expr)
  | Fixpoint      of transformer
  | Sequence      of transformer list
  | Label         of string * transformer
  | Precondition  of (expr -> string option) * transformer
  | Postcondition of (expr -> expr -> string option) * transformer

let mk_t label transform conditions =
  Label (label, List.fold_left ( |> ) (Atom transform) conditions)

let mk label f conditions = mk_t label (fun e -> f (erase_types e)) conditions

let _pre (lbl, c) t =
  let c x = if c x then None else Some ("pre: " ^ lbl) in
  Precondition (c, t)

let post (lbl, c) t =
  let c _ y = if c y then None else Some ("post: " ^ lbl) in
  Postcondition (c, t)

let pre_post (lbl, c) t =
  let c x y =
    if c x
    then if c y then None else Some ("post: " ^ lbl)
    else Some ("pre: " ^ lbl)
  in
  Postcondition (c, t)

let preserves (lbl, c) t =
  let c x y = if (not (c x)) || c y then None else Some ("preserves: " ^ lbl) in
  Postcondition (c, t)

let rec run_transformer path ~tparameter env t x =
  let path_str =
    match path with
    | [] -> "(no path)"
    | l :: ls -> List.fold_left (fun x y -> x ^ "." ^ y) l ls
  in
  if debug_level > 0
  then (
    Printf.eprintf "%s: %d\n" path_str (size_expr (erase_types x));
    flush stderr );
  let show lbl e = Format.asprintf "%s:\n%a" lbl print_expr e in
  let before () = show "Before" (erase_types x) in
  match t with
  | Atom transform ->
      let y = transform x in
      ( match typecheck ~tparameter env y with
      | Ok y -> y
      | Error msg ->
          let msg1 = Format.asprintf "%s: %s" path_str msg in
          let msg2 () =
            Format.asprintf "\n\n%s\n\n%s" (before ()) (show "After" y)
          in
          if debug_level >= 2 then failwith (msg1 ^ msg2 ()) else failwith msg1
      )
  | Fixpoint t ->
      let rec fixpoint n x =
        if n = 0
        then failwith (Printf.sprintf "%s: failed to converge" path_str)
        else
          let y = run_transformer path ~tparameter env t x in
          if equal_texpr x y then x else fixpoint (n - 1) y
      in
      fixpoint 100 x
  | Sequence ts ->
      List.fold_left
        ( |> )
        x
        (List.map (run_transformer path ~tparameter env) ts)
  | Label (lbl, t) -> run_transformer (path @ [lbl]) ~tparameter env t x
  | Precondition (c, t) ->
    ( match c (erase_types x) with
    | None -> run_transformer path ~tparameter env t x
    | Some msg ->
        let msg1 = Format.asprintf "%s: condition violated: %s" path_str msg in
        let msg2 () = Format.asprintf "\n\n%s" (before ()) in
        if debug_level >= 2 then failwith (msg1 ^ msg2 ()) else failwith msg1 )
  | Postcondition (c, t) ->
      let y = run_transformer path ~tparameter env t x in
      ( match c (erase_types x) (erase_types y) with
      | None -> y
      | Some msg ->
          let msg1 =
            Format.asprintf "%s: condition violated: %s" path_str msg
          in
          let y = erase_types y in
          let msg2 () =
            Format.asprintf "\n\n%s\n\n%s" (before ()) (show "After" y)
          in
          if debug_level >= 2 then failwith (msg1 ^ msg2 ()) else failwith msg1
      )

let run_transformer = run_transformer []

type state = {var_counter : int ref}

let fresh {var_counter} x =
  let i = !var_counter in
  incr var_counter;
  x ^ string_of_int i

let freshen st s = List.map (fun _ -> fresh st s)

module String_set = Set.Make (struct
  type t = string

  let compare = compare
end)

let may_fail =
  cata_expr (function
      | Prim1 (Failwith, _) -> true
      | e -> fold_expr_f ( || ) false e)

let sum_map = Map.String.union (fun _ x y -> Some (x + y))

let sum_maps = List.fold_left sum_map Map.String.empty

let map_of_set f x =
  Map.String.of_seq (Seq.map (fun k -> (k, f k)) (String_set.to_seq x))

let map_of_list f xs =
  List.fold_left
    (Map.String.union (fun _k x y -> Some (f x y)))
    Map.String.empty
    (List.map (fun (k, v) -> Map.String.singleton k v) xs)

(** How many times is each variable used? *)
let count_occs =
  cata_expr (function
      | Var v -> Map.String.singleton v 1
      | e -> fold_expr_f sum_map Map.String.empty e)

(** How many times is each variable bound? *)
let count_bindings =
  let open Map.String in
  let count_var = Option.cata empty (fun x -> singleton x 1) in
  let count_tree f x = Binary_tree.(fold sum_map empty (map f x)) in
  let f = function
    | Let_in (xs, e1, e2) -> sum_maps (e1 :: e2 :: List.map count_var xs)
    | Lambda (x, _, _, e) -> sum_map (count_var x) e
    | Match_record (rp, e1, e2) -> sum_maps [count_tree count_var rp; e1; e2]
    | Match_variant (e, clauses) ->
        let count_clause {var; rhs} = sum_map (count_var var) rhs in
        sum_map e (count_tree count_clause clauses)
    | Loop (e1, xs, e2) -> sum_maps (e1 :: e2 :: List.map count_var xs)
    | If_some (e1, x, e2, e3) -> sum_maps [e1; count_var x; e2; e3]
    | If_left (e1, x1, e2, x2, e3) ->
        sum_maps [e1; count_var x1; e2; count_var x2; e3]
    | If_cons (e1, x1, x2, e2, e3) ->
        sum_maps [e1; count_var x1; count_var x2; e2; e3]
    | e -> fold_expr_f sum_map Map.String.empty e
  in
  cata_expr f

(** Does every variable binding have a unique name? *)
let bindings_unique e = Map.String.for_all (fun _ n -> n = 1) (count_bindings e)

let bindings_unique = ("bindings_unique", bindings_unique)

let bindings_used = ("bindings_used", Analyser.bindings_used)

let is_linear = ("is_linear", Analyser.is_linear)

let loops_closed = ("loops_closed", Analyser.loops_closed)

(*let bindings_unique_and_used_once*)

let rec is_simple {expr} =
  match expr with
  | Var _ -> true
  | Lit _ -> true
  | Prim0 _ -> true
  | Prim1 ((Car | Cdr), _) -> true
  | Record r -> for_all (fun (_, e) -> is_simple e) r
  | Variant (_, _, e) -> is_simple e
  | _ -> false

(* TODO eta-expand bound variables of record types *)

(* Inlines let-bindings whose variables are used exactly once. *)
let inline ~mode e =
  let occs = count_occs e in
  let f = function
    | Var v -> fun subst -> Option.default (var v) (Map.String.find_opt v subst)
    | Let_in ([Some x], e1, e2) ->
        fun subst ->
          let e1 = e1 subst in
          let single_occ = Map.String.lookup ~default:0 x occs = 1 in
          let simple = is_simple e1 in
          let is_var =
            match e1.expr with
            | Var _ -> true
            | _ -> false
          in
          let do_it =
            match mode with
            | `All -> single_occ || simple
            | `Vars_only -> is_var
          in
          if (not (may_fail e1)) && do_it
          then e2 (Map.String.add x e1 subst)
          else let_in [Some x] e1 (e2 subst)
    | e -> fun subst -> {expr = map_expr_f (fun x -> x subst) e}
  in
  cata_expr f e Map.String.empty

let inline ~mode =
  mk "inline" (inline ~mode) [preserves bindings_unique; preserves is_linear]

(* Replace unused variables with a '_'. *)
let mark_unused e =
  let occs = count_occs e in
  let underscore_var = function
    | Some x when Map.String.lookup ~default:0 x occs = 0 -> None
    | x -> x
  in
  let underscore_rp = Binary_tree.map underscore_var in
  let underscore_vars = List.map underscore_var in
  let underscore_clause c = {c with var = underscore_var c.var} in
  let f = function
    | Let_in (xs, e1, e2) -> let_in (underscore_vars xs) e1 e2
    | Match_record (rp, e1, e2) -> match_record (underscore_rp rp) e1 e2
    | Match_variant (scrutinee, clauses) ->
        match_variant scrutinee (Binary_tree.map underscore_clause clauses)
    | Lambda (x, a, b, e) -> {expr = Lambda (underscore_var x, a, b, e)}
    | Loop (init, xs, step) -> {expr = Loop (init, underscore_vars xs, step)}
    | expr -> {expr}
  in
  cata_expr f e

let mark_unused =
  mk "mark_unused" mark_unused [preserves bindings_unique; post bindings_used]

let field_access =
  let f = function
    | Prim1 (Car, (x, Stack_ok [Type.T_record (Node (Leaf (Some lbl, _), _))]))
      ->
        prim1 (Proj_field lbl) x
    | Prim1 (Cdr, (x, Stack_ok [Type.T_record (Node (_, Leaf (Some lbl, _)))]))
      ->
        prim1 (Proj_field lbl) x
    | e -> {expr = map_expr_f fst e}
  in
  cata_texpr f

let field_access = mk_t "field_access" field_access [preserves bindings_unique]

let flatten_matches e =
  let occs = count_occs e in
  let extract = function
    | { var = Some var
      ; rhs = {expr = Match_variant ({expr = Var scrutinee'}, clauses')} }
      when var = scrutinee' && Map.String.find_opt var occs = Some 1 ->
        clauses'
    | c -> leaf c
  in
  let f = function
    | Match_variant (scrutinee, clauses) ->
        (* TODO do not transcend type boundaries *)
        {expr = Match_variant (scrutinee, join (map extract clauses))}
    | expr -> {expr}
  in
  cata_expr f e

let flatten_matches =
  mk "flatten_matches" flatten_matches [preserves bindings_unique]

let fresh_vars st v = List.map (Option.map (fun _ -> fresh st v))

let singleton_or_record = function
  | Leaf (None, x) -> x
  | xs -> {expr = Record xs}

let is_failwith = function
  | {expr = Prim1 (Failwith, _)} -> true
  | _ -> false

(** A "mostly failing" match is one where at most one branch succeeds. *)
let mostly_fails clauses =
  Option.cata 0 size (filter (fun {rhs} -> not (is_failwith rhs)) clauses) <= 1

let _let_protect st e f =
  let v = fresh st "p" in
  let_in [Some v] e (f (var v))

(** Deletes bindings to '_'. Linearizes tuple-bindings. Propagates '_'-components in let-bindings below match nodes. *)
let mangle st =
  let f = function
    | Let_in ([Some x], e1, {expr = Var x'}) when x = x' -> e1
    | Let_in ([None], e1, e2) when not (may_fail e1) -> e2
    | Let_in (_, ({expr = Prim1 (Failwith, _)} as fail), _) -> fail
    | Let_in (rp1, {expr = Let_in (rp2, e2, e1)}, e3) ->
        let_in rp2 e2 (let_in rp1 e1 e3)
    | Let_in ([rp1; rp2], {expr = Record (Node (r1, r2))}, e2) ->
        let unwrap = function
          | Leaf (_, x) -> x
          | Node _ as x -> {expr = Record x}
        in
        let_in [rp1] (unwrap r1) (let_in [rp2] (unwrap r2) e2)
    | Let_in (rp, {expr = Match_variant (e1, clauses)}, e2)
      when mostly_fails clauses ->
        (* Pull let into match when at most one branch succeeds. *)
        let mk_clause ({cons; var; rhs} as c) =
          if is_failwith rhs then c else {cons; var; rhs = let_in rp rhs e2}
        in
        let r = match_variant e1 (map mk_clause clauses) in
        r
    | Let_in (xs, {expr = Match_variant (e1, clauses)}, e2)
      when List.exists Option.is_none xs ->
        (* Push discarding of unused variables inside clauses. *)
        (* TODO Same thing for Match_record instead of Let_in *)
        let mk_clause ({cons; var; rhs} as c) =
          if is_failwith rhs
          then c
          else
            let xs = fresh_vars st "s" xs in
            let xs_used =
              vector ((List.map Expr.var) (List.filter_map id xs))
            in
            let rhs = let_in xs rhs xs_used in
            {cons; var; rhs}
        in
        let xs = List.map Option.some (List.somes xs) in
        let_in xs (match_variant e1 (map mk_clause clauses)) e2
    | expr -> {expr}
  in
  cata_expr f

let mangle st = mk "mangle" (mangle st) [preserves bindings_unique]

let fold_constants =
  let f = function
    | Prim1 (Car, {expr = Record (Node (Leaf (_, e1), _))}) -> e1
    | Prim1 (Car, {expr = Record (Node (t1, _))}) -> {expr = Record t1}
    | Prim1 (Cdr, {expr = Record (Node (_, Leaf (_, e2)))}) -> e2
    | Prim1 (Cdr, {expr = Record (Node (_, t2))}) -> {expr = Record t2}
    | Prim1 (Car, {expr = Prim2 (Pair, x, _)}) -> x
    | Prim1 (Cdr, {expr = Prim2 (Pair, _, y)}) -> y
    | expr -> {expr}
  in
  cata_expr f

let fold_constants =
  mk "fold_constants" fold_constants [preserves bindings_unique]

let pp_tree_skel x =
  print
    (fun ppf -> Format.fprintf ppf " | ")
    (fun ppf _ -> Format.fprintf ppf "_")
    x

let infer_constructors =
  let open Type in
  let f = function
    | Match_variant ((scrutinee, Stack_ok [T_variant r_scrutinee]), clauses) ->
      ( match matches clauses r_scrutinee with
      | None ->
          failwith
            (Format.asprintf
               "infer_constructors:\n  <%a>\n  (%a)"
               (print_row ~sep:";")
               r_scrutinee
               pp_tree_skel
               clauses)
      | Some clauses ->
          let put_cons = function
            | {cons = None; var; rhs = rhs, _}, Leaf (Some cons, _) ->
                {cons = Some cons; var; rhs}
            | {cons; var; rhs = rhs, _}, _ -> {cons; var; rhs}
          in
          let clauses = map put_cons clauses in
          {expr = Match_variant (scrutinee, clauses)} )
    | e -> {expr = map_expr_f fst e}
  in
  cata_texpr f

let infer_constructors =
  mk_t "infer_constructors" infer_constructors [preserves bindings_unique]

let cons x xs = x :: xs

(* A lens that allows us to look through a chain of lets (as well as
   mostly failing matches). Returns a list of bound variables. *)
let rec lens_lets bound wrap {expr} =
  match expr with
  | Comment (xs, e) ->
      let wrap e' = wrap {expr = Comment (xs, e')} in
      lens_lets bound wrap e
  | Let_in (xs, e1, e2) ->
      let wrap e2' = wrap {expr = Let_in (xs, e1, e2')} in
      lens_lets (List.somes xs @ bound) wrap e2
  | Match_variant (e, clauses) when mostly_fails clauses ->
    ( match
        List.find_opt (fun {rhs} -> not (is_failwith rhs)) (to_list clauses)
      with
    | None -> (bound, {expr}, wrap)
    | Some {var; rhs} ->
        let wrap rhs' =
          let f ({cons; var; rhs} as c) =
            if is_failwith rhs then c else {cons; var; rhs = rhs'}
          in
          wrap (match_variant e (map f clauses))
        in
        lens_lets (Option.cata id cons var bound) wrap rhs )
  | _ -> (bound, {expr}, wrap)

let lens_lets = lens_lets [] id

let apply_deep f x =
  let _, focus, close = lens_lets x in
  close (f focus)

(* Substitute invariant loop states with initial state.  Does not
   preserve linearity. *)
let inline_invariant_loop_state =
  let f = function
    | Loop (init, xs, step) as expr ->
      (* If a variable is the same in xs and in step, we can replace
         it with its initial value. *)
      ( match (lens_lets init, lens_lets step) with
      | (_, {expr = Vector (_ :: init)}, _), (_, {expr = Vector (_ :: step)}, _)
        ->
          let s =
            let f x s i =
              match (x, s, i) with
              | Some x, {expr = Var s}, {expr = Var _} when x = s ->
                  Some (x, i) (* x is invariant *)
              (* TODO If x is not a variable, wrap it in a let. *)
              | _ -> None
            in
            assert (eq3 (List.length xs) (List.length step) (List.length init));
            List.map3 f xs step init |> List.somes
          in
          Expr.substitute s {expr}
      | _ -> {expr} )
    | expr -> {expr}
  in
  cata_expr f

let inline_invariant_loop_state =
  mk
    "inline_invariant_loop_state"
    inline_invariant_loop_state
    [preserves bindings_unique]

let remove_in_vector st used e =
  let xs = List.map (fun u -> if u then Some (fresh st "s") else None) used in
  let_in xs e (vector (List.map var (List.somes xs)))

let remove used xs =
  assert (List.length used = List.length xs);
  List.map2 Control.pair used xs |> List.filter fst |> List.map snd

(* Remove unused parts from the loop state. *)
let unused_loop_state st =
  let f = function
    | Let_in (rs, {expr = Loop (init, xs, step)}, e) as expr ->
        assert (List.length rs = List.length xs);
        let used = List.map2 Option.(fun x r -> is_some x || is_some r) rs xs in
        if List.exists not used
        then
          let rs = remove used rs in
          let xs = remove used xs in
          let init = remove_in_vector st (true :: used) init in
          let step = remove_in_vector st (true :: used) step in
          let_in rs (loop init xs step) e
        else {expr}
    | expr -> {expr}
  in
  cata_expr f

let unused_loop_state st =
  mk "unused_loop_state" (unused_loop_state st) [preserves bindings_unique]

let remove_invariant_loop_state st =
  let f = function
    | Let_in (rs, {expr = Loop (init, xs, step)}, e) as expr ->
      ( match lens_lets step with
      | _, {expr = Vector (s0 :: step')}, close_step ->
          assert (List.length xs = List.length step');
          let is_needed r x s =
            match (r, x, s) with
            | None, Some x, {expr = Var x'}
              when x = x'
                   && Map.String.lookup ~default:0 x (count_occs step) = 1 ->
                false
            | _ -> true
          in
          assert (eq3 (List.length rs) (List.length xs) (List.length step'));
          let needed = List.map3 is_needed rs xs step' in
          if List.exists not needed
          then
            let rs = remove needed rs in
            let xs = remove needed xs in
            let init = remove_in_vector st (true :: needed) init in
            let step' = remove needed step' in
            let step' = close_step (vector (s0 :: step')) in
            let_in rs (loop init xs step') e
          else {expr}
      | _ -> {expr} )
    | expr -> {expr}
  in
  cata_expr f

let remove_invariant_loop_state st =
  mk
    "remove_invariant_loop_state"
    (remove_invariant_loop_state st)
    [preserves bindings_unique]

let unify_fields x y =
  match (x, y) with
  | Some x, Some y -> if x = y then Some x else failwith "unify_fields"
  | x, None -> x
  | None, y -> y

let common_parts x y =
  common x y
  |> map (function
         | Leaf (fld_x, Some x), Leaf (fld_y, Some y) when equal_expr x y ->
             Leaf (unify_fields fld_x fld_y, Some x)
         | Leaf (fld_x, _), Leaf (fld_y, _) ->
             Leaf (unify_fields fld_x fld_y, None)
         | _ -> Leaf (None, None))
  |> join

let prune pat x =
  matches pat x
  |> Option.of_some
  |> map_some (function p, x -> if p then Some x else None)
  |> Option.map join

let factorise_clauses st =
  let f = function
    | Match_variant (scrutinee, clauses) as expr ->
        let open Option.Monad_syntax in
        (let* results =
           (* TODO use lens_lets *)
           TO.mapA
             (function
               | {rhs = {expr = Record r}} -> Some (Some r)
               | {rhs = {expr = Prim1 (Failwith, _)}} -> Some None
               | _ -> None)
             clauses
         in
         let* results = map_some id results in
         let* _ = if mostly_fails clauses then None else Some () in
         (* This avoids pulling out bound variables. TODO Be more precise. *)
         let shared =
           fold1 common_parts (map (map (map_snd Option.some)) results)
         in
         let b = exists (fun x -> Option.is_some (snd x)) shared in
         let* _ = if b then Some () else None in
         let open Either in
         let out =
           shared
           |> map (function
                  | _, None -> Left (fresh st "o")
                  | fld, Some x -> Right (fld, x))
         in
         let x =
           map_some of_left out |> Option.cata (Leaf None) (map Option.some)
         in
         let clauses =
           let pat = map Either.is_left out in
           let prune_rhs = function
             | {expr = Record r} ->
                 singleton_or_record
                   (prune pat r |> Option.default (Leaf (None, unit)))
             | {expr = Prim1 (Failwith, _)} as rhs -> rhs
             | _ -> assert false
           in
           clauses |> map (map_match_clause prune_rhs)
         in
         let out =
           out
           |> map (function
                  | Left x -> (None, var x)
                  | Right e -> e)
         in
         Some
           (match_record
              x
              (match_variant scrutinee clauses)
              (singleton_or_record out)))
        |> Option.default {expr}
    | expr -> {expr}
  in
  cata_expr f

let factorise_clauses st =
  mk "factorise_clauses" (factorise_clauses st) [preserves bindings_unique]

let reify_booleans =
  let f = function
    | Match_variant
        ( x
        , Node
            ( Leaf {cons = Some "True"; var = None; rhs = r1}
            , Leaf {cons = Some "False"; var = None; rhs = r2} ) ) as expr ->
      ( match () with
      | _ when equal_expr r2 false_ -> prim2 And x r1
      | _ when equal_expr r1 true_ -> prim2 Or x r1
      | _ -> {expr} )
    | expr -> {expr}
  in
  cata_expr f

let reify_booleans =
  mk "reify_booleans" reify_booleans [preserves bindings_unique]

let binarize_matches st =
  let f = function
    | Match_variant (s, Node ((Node _ as l), r)) ->
        let ls = fresh st "l" in
        let rhs = {expr = Match_variant ({expr = Var ls}, l)} in
        let l = Leaf {cons = None; var = Some ls; rhs} in
        {expr = Match_variant (s, Node (l, r))}
    | Match_variant (s, Node (l, (Node _ as r))) ->
        let rs = fresh st "r" in
        let rhs = {expr = Match_variant ({expr = Var rs}, r)} in
        let r = Leaf {cons = None; var = Some rs; rhs} in
        {expr = Match_variant (s, Node (l, r))}
    | expr -> {expr}
  in
  cata_expr f

let binarize_matches st =
  mk "binarize_matches" (binarize_matches st) [preserves bindings_unique]

let two_clauses c1 c2 = Binary_tree.Node (Leaf c1, Leaf c2)

let unfold_ifs =
  let f = function
    | If (cond, l, r) ->
        match_variant
          cond
          (two_clauses
             {cons = Some "True"; var = None; rhs = l}
             {cons = Some "False"; var = None; rhs = r})
    | If_left (scrutinee, xl, l, xr, r) ->
        match_variant
          scrutinee
          (two_clauses
             {cons = None; var = xl; rhs = l}
             {cons = None; var = xr; rhs = r})
    | If_some (scrutinee, xl, l, r) ->
        match_variant
          scrutinee
          (two_clauses
             {cons = Some "Some"; var = xl; rhs = l}
             {cons = Some "None"; var = None; rhs = r})
    | expr -> {expr}
  in
  cata_expr f

let unfold_ifs = mk "unfold_ifs" unfold_ifs [preserves bindings_unique]

let fold_ifs =
  let f = function
    | Match_variant
        ( scrutinee
        , Node
            ( Leaf {cons = Some "True"; var = None; rhs = l}
            , Leaf {cons = Some "False"; var = None; rhs = r} ) ) ->
        if_ scrutinee l r
    | Match_variant
        ( scrutinee
        , Node
            ( Leaf {cons = Some "Some"; var = xl; rhs = l}
            , Leaf {cons = Some "None"; var = None; rhs = r} ) ) ->
        if_some scrutinee (xl, l) r
    | Match_variant
        (scrutinee, Node (Leaf {var = xl; rhs = l}, Leaf {var = xr; rhs = r}))
      ->
        if_left scrutinee (xl, l) (xr, r)
    | Match_variant (_, Node _) -> assert false (* run binarize_matches first *)
    | Match_variant (_, Leaf _) -> assert false
    | Variant (Some "True", Node_left (Hole, Leaf (Some "False", _)), _) ->
        lit True
    | Variant (Some "False", Node_right (Leaf (Some "True", _), Hole), _) ->
        lit False
    | Variant (Some "Left", Node_left (Hole, Leaf (Some "Right", t)), e) ->
        left t e
    | Variant (Some "Right", Node_right (Leaf (Some "Left", t), Hole), e) ->
        right t e
    | Variant (Some "None", Node_left (Hole, Leaf (Some "Some", _)), e) ->
        some e
    | Variant (Some "Some", Node_right (Leaf (Some "None", _), Hole), _) -> unit
    | Record (Node (Leaf (_, x), Leaf (_, y))) -> pair x y
    | expr -> {expr}
  in
  cata_expr f

let fold_ifs = mk "fold_ifs" fold_ifs [preserves bindings_unique]

let unvectortize_lets =
  let f = function
    | Let_in (xs, {expr = Vector es}, e) ->
        assert (List.length xs = List.length es);
        let bs = List.map2 (fun x e -> ([x], e)) xs es in
        List.fold_right (uncurry let_in) bs e
    | expr -> {expr}
  in
  cata_expr f

let unvectortize_lets =
  mk "unvectortize_lets" unvectortize_lets [preserves bindings_unique]

let single_lets st =
  let f = function
    | Match_record ((Node _ as rp), e1, e2) ->
        let x = fresh st "x" in
        let rec mk_vars v = function
          | Leaf n -> [(Option.default (fresh st "x") n, v)]
          | Node (x, y) -> mk_vars (prim1 Car v) x @ mk_vars (prim1 Cdr v) y
        in
        let bindings = (x, e1) :: mk_vars (var x) rp in
        List.fold_right (fun (x, e) r -> let_in [Some x] e r) bindings e2
    | expr -> {expr}
  in
  cata_expr f

let single_lets st =
  mk "single_lets" (single_lets st) [preserves bindings_unique]

let binarize_projs =
  let f = function
    | Prim1 (Proj_field fld, (e, Stack_ok [T_record r])) ->
        let rec proj r p other =
          match r with
          | Leaf (Some n, _) when fld = n -> p
          | Leaf _ -> other ()
          | Node (x, y) ->
              proj
                x
                {expr = Prim1 (Car, p)}
                (fun () -> proj y {expr = Prim1 (Cdr, p)} other)
        in
        proj r e (fun () -> assert false)
    | e -> {expr = map_expr_f fst e}
  in
  cata_texpr f

let binarize_projs =
  mk_t "binarize_projs" binarize_projs [preserves bindings_unique]

let name_all st =
  let name_var x = Some (Option.default (fresh st "m") x) in
  let name_rp = Binary_tree.map name_var in
  let f = function
    | Let_in (xs, e1, e2) -> {expr = Let_in (List.map name_var xs, e1, e2)}
    | Match_variant (scrutinee, clauses) ->
        let f c = {c with var = name_var c.var} in
        {expr = Match_variant (scrutinee, map f clauses)}
    | Match_record (rp, e1, e2) -> {expr = Match_record (name_rp rp, e1, e2)}
    | Lambda (rp, a, b, e) -> {expr = Lambda (name_var rp, a, b, e)}
    | Loop (init, xs, step) -> {expr = Loop (init, List.map name_var xs, step)}
    | expr -> {expr}
  in
  cata_expr f

let name_all st = mk "name_all" (name_all st) [preserves bindings_unique]

let build_dup_chain st x xs =
  let open List in
  let tmps = init (length xs - 2) (fun _ -> fresh st "tmp") in
  let xs', xs_last = unsnoc xs in
  let ys = tmps @ [xs_last] in
  let zs = x :: tmps in
  map3
    ~err:"build_dup_chain"
    (fun x y z -> ([Some x; Some y], dup (var z)))
    xs'
    ys
    zs

(* TODO: put vector on RHS *)
let drop_vars xs =
  List.fold_right (fun x e -> let_in [None] (var x) e) (String_set.elements xs)

module W = Writer (struct
  type t = string * string
end)

module T = Expr.Traversable (W)

let get_dups st all_occs occs =
  String_set.filter (fun x -> Map.String.lookup ~default:0 x all_occs > 1) occs
  |> String_set.elements
  |> List.map (fun x -> (x, fresh st (x ^ "d")))

(* Generates and applies replacements for variables that occur both in
   e (i.e. in occs) and elsewhere (i.e. in all_occs). A list of
   applied replacement is returned via the writer monad. *)
let dup_expr st all_occs (occs, e) =
  let open W in
  let dups = get_dups st all_occs occs in
  W.mapA_list_ write dups
  >> return (substitute (List.map (map_snd Expr.var) dups) e)

(* Converts a list of replacements into let-bindings and puts them in
   front of the given expression. *)
let make_dups st bs e =
  let chains =
    bs
    |> List.map (map_snd (fun x -> [x]))
    |> map_of_list ( @ )
    |> Map.String.bindings
    |> List.map (fun (x, xs) -> build_dup_chain st x xs)
    |> List.concat
  in
  lets chains e

let linearize_generic st e =
  let all_occs =
    map_expr_f (fun (occs, _) -> map_of_set (fun _ -> 1) occs) e
    |> fold_expr_f sum_map Map.String.empty
  in
  let bs, expr = W.run (T.sequenceA (map_expr_f (dup_expr st all_occs) e)) in
  ( fold_expr_f String_set.union String_set.empty (map_expr_f fst e)
  , make_dups st bs {expr} )

let linearize_generic_without st e xs =
  let occs, e = linearize_generic st e in
  let occs = String_set.diff occs (String_set.of_list (List.somes xs)) in
  (occs, e)

(* Add elements to the end of a vector. *)
let extend st xs len e =
  if len = 1
  then vector (e :: xs)
  else
    match e.expr with
    | Vector es -> vector (es @ xs)
    | _ ->
        let es = List.init len (fun _ -> fresh st "e") in
        let_in (List.map Option.some es) e (vector (List.map var es @ xs))

let extend st xs len e = apply_deep (extend st xs len) e

(* List.take on vectors *)
let take_vector st n len xs =
  if n = len
  then xs
  else
    let es = List.init n (fun _ -> fresh st "t") in
    let ignored = List.init (len - n) (fun _ -> None) in
    let_in
      (List.map Option.some es @ ignored)
      xs
      (vector (List.map var (List.take n es)))

let set_of_map m =
  let f (k, v) =
    assert (v >= 0);
    if v > 0 then Some k else None
  in
  String_set.of_list (List.filter_map f (Map.String.bindings m))

(* Modify loops so that their bodies don't refer to the outer scope. *)
let close_loops st =
  let open Analyser in
  let open String_set in
  let f e =
    let generic () =
      ( count_free_vars_f mode_occurrences (map_expr_f fst e)
      , {expr = map_expr_f snd e} )
    in
    match e with
    | Loop ((_, init), xs, (occs_step, step)) ->
        let n = List.length xs in
        let outer =
          let xs = of_list (List.somes xs) in
          elements (diff (set_of_map occs_step) xs)
        in
        if outer = []
        then generic ()
        else
          let outer_fresh = List.map (fun x -> (x, fresh st "v")) outer in
          let init = extend st (List.map var outer) (n + 1) init in
          let xs = xs @ List.map (fun (_, v) -> Option.some v) outer_fresh in
          let step =
            let sigma = List.map (fun (x, v) -> (x, var v)) outer_fresh in
            substitute sigma step
          in
          let step =
            extend st (List.map (fun (_, v) -> var v) outer_fresh) (n + 1) step
          in
          let e = take_vector st n (List.length xs) (loop init xs step) in
          (count_free_vars mode_occurrences e, e)
    | _ -> generic ()
  in
  fun e -> snd (cata_expr f e)

let close_loops st =
  mk
    "close_loops"
    (close_loops st)
    [preserves bindings_unique; post loops_closed]

let linearize st =
  let open String_set in
  let f = function
    | Var x as expr -> (singleton x, {expr})
    | Let_in (xs, _, _) as e -> linearize_generic_without st e xs
    | Match_record (rp, _, _) as e ->
        linearize_generic_without st e (Binary_tree.to_list rp)
    | Lambda (x, _, _, _) as e -> linearize_generic_without st e [x]
    | Loop ((occs_init, init), xs, (occs_step, step)) ->
        let xs' = of_list (List.somes xs) in
        let to_drop = diff xs' occs_step in
        let step = drop_vars to_drop step in
        let occs_step = diff occs_step xs' in
        let e = Loop ((occs_init, init), xs, (occs_step, step)) in
        let e = linearize_generic st e in
        e
    | Match_variant ((scrutinee_occs, scrutinee), clauses) ->
        let clauses =
          clauses
          |> Binary_tree.map (fun ({var; rhs = occs, e} as clause) ->
                 let occs = Option.cata id remove var occs in
                 {clause with rhs = (occs, e)})
        in
        let all_rhs_occs =
          Binary_tree.map (fun {rhs = occs, _} -> occs) clauses
          |> Binary_tree.fold union empty
        in
        let clauses =
          clauses
          |> Binary_tree.map (fun ({rhs = occs, e} as clause) ->
                 let to_drop = diff all_rhs_occs occs in
                 let e = drop_vars to_drop e in
                 {clause with rhs = e})
        in
        let all_occs =
          sum_map
            (map_of_set (fun _ -> 1) scrutinee_occs)
            (map_of_set (fun _ -> 1) all_rhs_occs)
        in
        let dups1 = get_dups st all_occs scrutinee_occs in
        let dups2 = get_dups st all_occs all_rhs_occs in
        let dups1' = List.map (map_snd Expr.var) dups1 in
        let dups2' = List.map (map_snd Expr.var) dups2 in
        let scrutinee = substitute dups1' scrutinee in
        let clauses =
          clauses
          |> Binary_tree.map (fun ({rhs = e} as clause) ->
                 {clause with rhs = substitute dups2' e})
        in
        let e =
          make_dups
            st
            (dups1 @ dups2)
            {expr = Match_variant (scrutinee, clauses)}
        in
        (union scrutinee_occs all_rhs_occs, e)
    | If _ | If_left _ | If_cons _ | If_some _ -> assert false (* unsupported *)
    | ( Lit _ | Prim0 _ | Prim1 _ | Prim2 _ | Prim3 _ | Stack_op _ | Record _
      | Comment _ | Variant _ | List _ | Set _ | Map _ | Vector _ ) as e ->
        linearize_generic st e
  in
  fun e -> snd (cata_expr f e)

let linearize st =
  mk "linearize" (linearize st) [preserves bindings_unique; post is_linear]

let linearize st = Sequence [close_loops st; linearize st]

let linearize_tuples st =
  let f = function
    | Match_record (Node (rp1, rp2), e1, e2) ->
        let x = fresh st "t" in
        let_in
          [Some x]
          e1
          (match_record
             rp1
             (prim1 Car (var x))
             (match_record rp2 (prim1 Cdr (var x)) e2))
    | Record t -> Binary_tree.cata snd pair t
    | expr -> {expr}
  in
  cata_expr f

let _linearize_tuples st =
  mk "linearize_tuples" (linearize_tuples st) [preserves bindings_unique]

let vector1 = function
  | [x] -> x
  | xs -> vector xs

let unfold_stack_ops ~discarded_only =
  let f expr =
    match expr with
    | Let_in
        ( (None :: x :: xs | x :: None :: xs)
        , {expr = Stack_op (Dup, {expr = Vector es})}
        , e2 ) ->
        let_in (x :: xs) (vector es) e2
    | Let_in
        ( Some x1 :: Some x2 :: xs
        , {expr = Stack_op (Dup, {expr = Vector (e1 :: es)})}
        , e2 )
      when not discarded_only ->
        let_in
          [Some x1]
          e1
          (let_in [Some x2] (var x1) (let_in xs (vector es) e2))
    | Let_in ([x1; x2], {expr = Stack_op (Dup, e)}, e2) ->
      ( match e.expr with
      | Vector _ -> {expr}
      | _ -> let_in [x1; x2] (dup (vector [e])) e2 )
    | Let_in ((x1 :: _ as xs), {expr = Stack_op (Dig n, {expr = Vector es})}, e)
      ->
        let discarded = Option.is_none x1 in
        if (not discarded_only) || discarded
        then
          match List.split_at n es with
          | es1, e' :: es2 -> let_in xs (vector ((e' :: es1) @ es2)) e
          | _ -> assert false
        else {expr}
    | Let_in ((x1 :: _ as xs), {expr = Stack_op (Dug n, {expr = Vector es})}, e)
      ->
      ( match List.split_at (n + 1) es with
      | e' :: es1, es2 ->
          let discarded = Option.is_none x1 in
          if (not discarded_only) || discarded
          then let_in xs (vector (es1 @ (e' :: es2))) e
          else {expr}
      | _ -> assert false )
    | Let_in (x1 :: x2 :: xs, {expr = Stack_op (Swap, es)}, e) ->
        let discarded = Option.is_none x1 || Option.is_none x2 in
        if (not discarded_only) || discarded
        then let_in (x2 :: x1 :: xs) es e
        else {expr}
    | Let_in (xs, {expr = Stack_op (Drop n, {expr = Vector es})}, e)
      when List.length es > n ->
        let es1, es2 = List.split_at n es in
        let_in
          (List.map (fun _ -> None) es1)
          (vector1 es1)
          (let_in xs (vector1 es2) e)
    | _ -> {expr}
  in
  cata_expr f

let unfold_stack_ops ~discarded_only =
  mk
    "unfold_stack_ops"
    (unfold_stack_ops ~discarded_only)
    [pre_post bindings_unique]

let chop_stack_ops =
  let f expr =
    match expr with
    | Let_in
        ( x1 :: x2 :: xs
        , {expr = Stack_op (Swap, {expr = Vector (e1 :: e2 :: es)})}
        , e )
      when List.length xs > 0 ->
        let_in
          [x1; x2]
          (stack_op Swap (vector [e1; e2]))
          (let_in xs (vector1 es) e)
    | Let_in
        (x1 :: x2 :: xs, {expr = Stack_op (Dup, {expr = Vector (e1 :: es)})}, e)
      when List.length xs > 0 ->
        let_in [x1; x2] (stack_op Dup (vector [e1])) (let_in xs (vector1 es) e)
    | Let_in (xs, {expr = Stack_op (Dug n, {expr = Vector es})}, e)
      when List.length xs > n + 1 ->
        let xs1, xs2 = List.split_at (n + 1) xs in
        let es1, es2 = List.split_at (n + 1) es in
        let_in xs1 (stack_op (Dug n) (vector1 es1)) (let_in xs2 (vector1 es2) e)
    | Let_in (xs, {expr = Stack_op (Dig n, {expr = Vector es})}, e)
      when List.length xs > n + 1 ->
        let xs1, xs2 = List.split_at (n + 1) xs in
        let es1, es2 = List.split_at (n + 1) es in
        let_in xs1 (stack_op (Dig n) (vector1 es1)) (let_in xs2 (vector1 es2) e)
    | _ -> {expr}
  in
  cata_expr f

let chop_stack_ops =
  mk "chop_stack_ops" chop_stack_ops [pre_post bindings_unique]

let elim_drops =
  let f = function
    | Let_in ([], {expr = Vector []}, e) -> e
    | Let_in (([None; x] | [x; None]), {expr = Stack_op (Dup, e1)}, e2) ->
        let_in [x] e1 e2
    | Let_in (xs, {expr = Vector es}, e) ->
        lets (List.map2 ~err:"let" (fun x e -> ([x], e)) xs es) e
    | Let_in ([None], e1, e2) when not (may_fail e1) -> e2
    | Let_in
        ( rs
        , {expr = Match_variant (scrutinee, Binary_tree.(Node (Leaf x, Leaf y)))}
        , e ) as expr ->
        let x_bound, x_rhs, close_x = lens_lets x.rhs in
        let y_bound, y_rhs, close_y = lens_lets y.rhs in
        let x_bound = Option.cata id cons x.var x_bound in
        let y_bound = Option.cata id cons y.var y_bound in
        let common, xs, ys =
          let is_free_in bound = function
            | {expr = Var x} when not (List.mem x bound) -> Some x
            | _ -> None
          in
          let same_free_var x y =
            match (x.expr, y.expr) with
            | Var x, Var y
              when List.(x = y && (not (mem x x_bound)) && not (mem y y_bound))
              ->
                Some x
            | _ -> None
          in
          match (x_rhs.expr, y_rhs.expr) with
          (* TODO Treat non-vector case of a single variable. *)
          | Vector xs, Vector ys ->
              (List.map2 ~err:"common" same_free_var xs ys, Some xs, Some ys)
          | Vector xs, Prim1 (Failwith, _) ->
              (List.map (is_free_in x_bound) xs, Some xs, None)
          | Prim1 (Failwith, _), Vector ys ->
              (List.map (is_free_in y_bound) ys, None, Some ys)
          | _ -> ([], None, None)
        in
        if List.exists Option.is_some common
        then
          let commons_only xs =
            let f p x = if Option.is_none p then None else Some x in
            List.somes (List.map2 ~err:"remove" f common xs)
          in
          let non_commons_only xs =
            let f p x = if Option.is_some p then None else Some x in
            List.somes (List.map2 ~err:"remove" f common xs)
          in
          let f x = vector1 (non_commons_only x) in
          let xs = Option.map f xs in
          let ys = Option.map f ys in
          let x = {x with rhs = Option.cata x.rhs close_x xs} in
          let y = {y with rhs = Option.cata y.rhs close_y ys} in
          let_in
            (commons_only rs)
            (vector1 (List.map var (List.somes common)))
            (let_in
               (non_commons_only rs)
               (match_variant scrutinee Binary_tree.(Node (Leaf x, Leaf y)))
               e)
        else {expr}
    | expr -> {expr}
  in
  cata_expr f

let elim_drops = mk "elim_drops" elim_drops [pre_post bindings_unique]

let simplify st ~tparameter env =
  let main =
    Sequence
      [ unfold_ifs
      ; unfold_stack_ops ~discarded_only:false
      ; inline ~mode:`All
      ; fold_constants
      ; mark_unused
      ; unvectortize_lets
      ; field_access
      ; flatten_matches
      ; Label ("fix_mangle", Fixpoint (mangle st))
      ; infer_constructors
      ; remove_invariant_loop_state st
      ; unused_loop_state st
      ; factorise_clauses st
      ; reify_booleans ]
  in
  let t = Fixpoint main in
  run_transformer ~tparameter env t

let smartMLify st ~tparameter env =
  let main =
    Sequence
      [ unfold_ifs
      ; unfold_stack_ops ~discarded_only:false
      ; inline ~mode:`All
      ; fold_constants
      ; mark_unused
      ; unvectortize_lets
      ; field_access
      ; flatten_matches
      ; Label ("fix_mangle", Fixpoint (mangle st))
      ; infer_constructors
      ; inline_invariant_loop_state
      ; remove_invariant_loop_state st
      ; unused_loop_state st
      ; factorise_clauses st
      ; reify_booleans ]
  in
  let post =
    Sequence [binarize_matches st; binarize_projs; name_all st; single_lets st]
  in
  let t = Sequence [Label ("main", Fixpoint main); Label ("post", post)] in
  run_transformer ~tparameter env t

let michelsonify st ~tparameter env =
  let t =
    Sequence
      [ Label
          ( "pre_mich"
          , Fixpoint
              (Sequence
                 [ mark_unused
                 ; unfold_ifs
                 ; elim_drops
                 ; inline ~mode:`Vars_only
                 ; remove_invariant_loop_state st
                 ; unfold_stack_ops ~discarded_only:true
                 ; chop_stack_ops
                 ; elim_drops ]) )
      ; Label ("linearize", Sequence [unfold_ifs; linearize st; fold_ifs]) ]
  in
  fun e -> run_transformer ~tparameter env t e

let on_contract f {tparameter; tstorage; body} =
  let env = [("parameter_and_storage", t_pair tparameter tstorage)] in
  {tparameter; tstorage; body = f ~tparameter env body}
