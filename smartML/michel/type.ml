(* Copyright 2019-2020 Smart Chain Arena LLC. *)
open Utils

type type0 =
  | T_nat
  | T_int
  | T_mutez
  | T_string
  | T_bytes
  | T_chain_id
  | T_timestamp
  | T_address
  | T_key
  | T_key_hash
  | T_signature
  | T_operation
  | T_sapling_state
  | T_sapling_transaction
  | T_never
  | T_unit
[@@deriving eq, show {with_path = false}]

type type1 =
  | T_list
  | T_set
  | T_contract
[@@deriving eq, show {with_path = false}]

type type2 =
  | T_lambda
  | T_map
  | T_big_map
[@@deriving eq, show {with_path = false}]

type ty =
  | T0        of type0
  | T1        of type1 * ty
  | T2        of type2 * ty * ty
  | T_record  of row
  | T_variant of row
[@@deriving eq, show {with_path = false}]

(* inviariant: not a singleton *)
and row = (string option * ty) Binary_tree.t
[@@deriving eq, show {with_path = false}]

type tys =
  | Stack_ok     of ty list
  | Stack_failed
[@@deriving eq, show {with_path = false}]

let mk_t1 t t1 = T1 (t, t1)

let mk_t2 t t1 t2 = T2 (t, t1, t2)

let mk_leaf l t = Binary_tree.Leaf (l, t)

let mk_node n1 n2 = Binary_tree.Node (n1, n2)

let t_record r = T_record r

let t_variant r = T_variant r

let rec lub t1 t2 =
  let open Option in
  match (t1, t2) with
  | T0 t, T0 u when equal_type0 t u -> Some (T0 t)
  | T1 (t, t1), T1 (u, u1) when equal_type1 t u -> mk_t1 t <$> lub t1 u1
  | T2 (t, t1, t2), T2 (u, u1, u2) when equal_type2 t u ->
      mk_t2 t <$> lub t1 u1 <*> lub t2 u2
  | T_record r, T_record s -> t_record <$> lub_row true r s
  | T_variant r, T_variant s -> t_variant <$> lub_row false r s
  | _ -> None

and lub_row is_record r1 r2 =
  let open Option in
  match (r1, r2) with
  | Leaf (Some l1, t1), Leaf (Some l2, t2) ->
      if l1 = l2 then mk_leaf (Some l1) <$> lub t1 t2 else None
  | Leaf (_, t1), Leaf (_, t2) -> mk_leaf None <$> lub t1 t2
  | Node (r1, r2), Node (s1, s2) ->
      mk_node <$> lub_row is_record r1 s1 <*> lub_row is_record r2 s2
  | Leaf (None, T_record r1), Node _ when is_record -> lub_row is_record r1 r2
  | Node _, Leaf (None, T_record r2) when is_record -> lub_row is_record r1 r2
  | Leaf (None, T_variant r1), Node _ when not is_record ->
      lub_row is_record r1 r2
  | Node _, Leaf (None, T_variant r2) when not is_record ->
      lub_row is_record r1 r2
  | _ -> None

let lubs s1 s2 =
  let open Option in
  match (s1, s2) with
  | Stack_ok ts1, Stack_ok ts2 ->
      if List.length ts1 = List.length ts2
      then (fun x -> Stack_ok x) <$> Option.mapA2_list lub ts1 ts2
      else None
  | Stack_failed, t2 -> Some t2
  | t1, Stack_failed -> Some t1

let compatible t1 t2 = Option.is_some (lub t1 t2)

let t_unit = T0 T_unit

let t_nat = T0 T_nat

let t_int = T0 T_int

let t_mutez = T0 T_mutez

let t_string = T0 T_string

let t_bytes = T0 T_bytes

let t_chain_id = T0 T_chain_id

let t_timestamp = T0 T_timestamp

let t_address = T0 T_address

let t_key = T0 T_key

let t_key_hash = T0 T_key_hash

let t_signature = T0 T_signature

let t_operation = T0 T_operation

let t_sapling_state = T0 T_sapling_state

let t_sapling_transaction = T0 T_sapling_transaction

let t_never = T0 T_never

let t_list t = T1 (T_list, t)

let t_set t = T1 (T_set, t)

let t_contract t = T1 (T_contract, t)

let t_map k v = T2 (T_map, k, v)

let t_big_map k v = T2 (T_big_map, k, v)

let t_lambda a b = T2 (T_lambda, a, b)

let t_variant_node r1 r2 = T_variant (Node (r1, r2))

let t_record_node r1 r2 = T_record (Node (r1, r2))

let r_or t1 t2 = Binary_tree.Node (Leaf (None, t1), Leaf (None, t2))

let t_or t1 t2 = T_variant (r_or t1 t2)

let leaf ?lbl t = Binary_tree.Leaf (lbl, t)

let t_pair t1 t2 = t_record_node (Leaf (None, t1)) (Leaf (None, t2))

let r_bool = Binary_tree.Node (leaf ~lbl:"True" t_unit, leaf ~lbl:"False" t_unit)

let r_option t = Binary_tree.Node (leaf ~lbl:"Some" t, leaf ~lbl:"None" t_unit)

let t_option t = T_variant (r_option t)

let t_bool = T_variant r_bool

let show_t0 = function
  | T_nat -> "nat"
  | T_int -> "int"
  | T_mutez -> "mutez"
  | T_string -> "string"
  | T_bytes -> "bytes"
  | T_chain_id -> "chain_id"
  | T_timestamp -> "timestamp"
  | T_address -> "address"
  | T_key -> "key"
  | T_key_hash -> "key_hash"
  | T_signature -> "signature"
  | T_operation -> "operation"
  | T_sapling_state -> "sapling_state"
  | T_sapling_transaction -> "sapling_transaction"
  | T_never -> "never"
  | T_unit -> "unit"

let show_t1 = function
  | T_list -> "list"
  | T_set -> "set"
  | T_contract -> "contract"

let show_t2 = function
  | T_lambda -> "lambda"
  | T_map -> "map"
  | T_big_map -> "big_map"

let rec print_row ~sep =
  let rec f protect ppf : row -> _ = function
    | Leaf (None, t) -> Format.fprintf ppf "%a" print_ty t
    | Leaf (Some s, t) -> Format.fprintf ppf "%s : %a" s print_ty t
    | Node (r1, r2) ->
        let r ppf = Format.fprintf ppf "%a %s %a" (f true) r1 sep (f true) r2 in
        if protect then Format.fprintf ppf "(%t)" r else r ppf
  in
  f false

and print_ty ppf = function
  | T_record r -> Format.fprintf ppf "{ %a }" (print_row ~sep:";") r
  | T_variant r -> Format.fprintf ppf "< %a >" (print_row ~sep:"|") r
  | T0 t -> Format.fprintf ppf "%s" (show_t0 t)
  | T1 (t, t1) -> Format.fprintf ppf "%s(%a)" (show_t1 t) print_ty t1
  | T2 (t, t1, t2) ->
      Format.fprintf ppf "%s(%a, %a)" (show_t2 t) print_ty t1 print_ty t2

let print_tys ppf = function
  | Stack_ok ts -> Format.fprintf ppf "%a" (List.pp print_ty) ts
  | Stack_failed -> Format.fprintf ppf "failed"

let get1 = function
  | Stack_ok [t] -> Ok t
  | tys -> Error (Format.asprintf "not a singleton: %a" print_tys tys)

let unleaf_variant =
  Binary_tree.(
    function
    | Leaf (_, t) -> t
    | Node _ as t -> T_variant t)

let view_option =
  let open Binary_tree in
  function
  | T_variant (Node (Leaf (Some "Some", t), Leaf (Some "None", T0 T_unit))) ->
      Some t
  | _ -> None

let view_or =
  let open Binary_tree in
  function
  | T_variant (Node (tl, tr)) -> Some (unleaf_variant tl, unleaf_variant tr)
  | _ -> None
