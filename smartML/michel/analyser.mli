(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Expr

type 'a var_count_mode

val mode_occurrences : int var_count_mode

val mode_consumptions : [ `Zero | `One | `Unknown ] var_count_mode

val is_linear : expr -> bool

val loops_closed : expr -> bool

val bindings_used : expr -> bool

val count_free_vars_f :
  'a var_count_mode -> 'a Map.String.t Expr.expr_f -> 'a Map.String.t

val count_free_vars : 'a var_count_mode -> Expr.expr -> 'a Map.String.t
