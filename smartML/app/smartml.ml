(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Base

module Io = struct
  let sexp_of_file_exn path f =
    Parsexp.Conv_single.parse_string_exn
      Caml.(
        let i = open_in path in
        let b = Buffer.create 42 in
        try
          while true do
            Buffer.add_char b (input_char i)
          done;
          "<nope>"
        with
        | _ ->
            close_in i;
            Buffer.contents b)
      f

  let contract_of_file_exn path =
    let scenario_state = Smart_ml.Basics.scenario_state () in
    try
      let contract =
        sexp_of_file_exn
          path
          (Smart_ml.Import.import_contract
             ~do_fix:true
             ~primitives:
               (module Smart_ml.Fake_primitives : Smart_ml.Primitives.Primitives)
             ~scenario_state
             (Smart_ml.Typing.init_env ()))
      in
      Smart_ml.Basics.to_value_tcontract
        (fun name x ->
          Smart_ml.Interpreter.interpret_expr_external
            ~primitives:
              (module Smart_ml.Fake_primitives : Smart_ml.Primitives.Primitives)
            ~no_env:[`Text ("Compute " ^ name)]
            ~scenario_state
            x)
        contract
    with
    | exn -> failwith (Smart_ml.Printer.exception_to_string false exn)

  let pp_to_file path f =
    let open Caml in
    let open Format in
    let o = open_out path in
    let ppf = formatter_of_out_channel o in
    pp_set_margin ppf 80;
    f ppf;
    pp_print_flush ppf ();
    flush o;
    close_out o;
    ()
end

let say fmt = Fmt.epr Caml.(fmt ^^ "\n%!")

module Tezos_michelson = Babylonian_michelson

module Primitives = struct
  open Smart_ml.Primitives
  module Tezize = Smart_ml.Fake_primitives.Tezize

  module Crypto = struct
    include Smart_ml.Fake_primitives.Crypto

    let sign ~secret_key bytes =
      let open Tezos_crypto.Ed25519 in
      let key =
        Secret_key.of_b58check_exn
          ( match String.chop_prefix secret_key ~prefix:"unencrypted:" with
          | None -> secret_key
          | Some sk -> sk )
      in
      sign key (Bytes.of_string bytes) |> to_b58check

    let blake2b bytes = Tezos_crypto.Blake2B.(hash_string [bytes] |> to_string)

    let account_of_seed s =
      let open Flextesa.Tezos_protocol.Account in
      let a = of_name s in
      {pk = pubkey a; pkh = pubkey_hash a; sk = private_key a}
  end

  module Storage = Smart_ml.Fake_primitives.Storage
  module Parser = Smart_ml.Fake_primitives.Parser
end

module Tezos_scenario_unix = Smart_ml_unix.Run_tezos_scenario.Make (Primitives)

let () =
  let open Cmdliner in
  let help = Term.(ret (pure (`Help (`Auto, None))), info "smartml") in
  let open Term in
  let input_smart_ml_arg () =
    Arg.(
      required
        (pos
           0
           (some file)
           None
           (info
              []
              ~docv:"INPUT-FILE"
              ~docs:"ARGUMENTS"
              ~doc:"Input file, by default, a SmartML S-Expression.")))
  in
  let input_micheline_arg () =
    Arg.(
      required
        (pos
           0
           (some file)
           None
           (info
              []
              ~docv:"INPUT-FILE"
              ~docs:"ARGUMENTS"
              ~doc:"Micheline input file")))
  in
  let output_michel_arg () =
    Arg.(
      required
        (pos
           1
           (some string)
           None
           (info
              []
              ~docv:"OUTPUT-FILE"
              ~docs:"ARGUMENTS"
              ~doc:"Michel output file")))
  in
  let output_michelson_arg () =
    Arg.(
      pure (fun output_format ->
        function
        | Some "-" -> (Option.value ~default:`Michelson output_format, None)
        | x ->
          ( match output_format with
          | Some s -> (s, x)
          | None ->
            ( match Option.map ~f:Caml.Filename.extension x with
            | Some ".json" -> (`Json, x)
            | Some ".tz" -> (`Michelson, x)
            | _ -> (`Michelson, x) ) ))
      $ value
          (opt
             (enum [("json", `Json); ("michelson", `Michelson)] |> some)
             None
             (info
                ["output-format"; "F"]
                ~doc:
                  "Output format of the michelson (by default guess from the \
                   extension or .tz if none)."))
      $ value
          (pos
             1
             (some string)
             None
             (info
                []
                ~docv:"OUTPUT-FILE"
                ~docs:"ARGUMENTS"
                ~doc:"Output .tz or .json file (or `-` for stdout).")))
  in
  let compile_cmd =
    ( pure
        (fun input_file
             (output_format, output_file_opt)
             output_binary_size
             options
             ()
             ->
          let smartml_contract = Io.contract_of_file_exn input_file in
          let compiled =
            Tezos_michelson.of_smart_ml ~options smartml_contract
          in
          let output ppf =
            match output_format with
            | `Michelson -> Fmt.pf ppf "@[%a@]@\n%!" Tezos_michelson.pp compiled
            | `Json ->
                Fmt.pf
                  ppf
                  "%s"
                  (Ezjsonm.to_string
                     ~minify:false
                     (Tezos_michelson.to_json compiled))
          in
          ( match output_file_opt with
          | Some s -> Io.pp_to_file s output
          | None -> output Fmt.stdout );
          Option.iter output_binary_size ~f:(fun file ->
              Io.pp_to_file
                file
                Fmt.(
                  fun ppf ->
                    pf ppf "%d\n" (Tezos_michelson.binary_size compiled)));
          say
            "Done (options: %a): %s → %a (%d bytes)."
            Tezos_michelson.Of_smart_ml_michelson.Options.pp
            options
            input_file
            Fmt.(option ~none:(const string "stdout") string)
            output_file_opt
            (Tezos_michelson.binary_size compiled))
      $ input_smart_ml_arg ()
      $ output_michelson_arg ()
      $ Arg.(
          value
            (opt
               (some string)
               None
               (info
                  ["write-binary-size"]
                  ~doc:"Write computed binary size to a file.")))
      $ Tezos_michelson.Of_smart_ml_michelson.Options.cli_term ()
      $ pure ()
    , Term.info "compile" ~doc:"Compiler from SmartML to Michelson" )
  in
  let init_storage_cmd =
    ( pure (fun input_file (output_format, output_file_opt) () ->
          let smartml_contract = Io.contract_of_file_exn input_file in
          let compiled =
            Tezos_michelson.storage_initialization smartml_contract
          in
          let output ppf =
            match output_format with
            | `Michelson -> Fmt.pf ppf "@[%a@]@\n%!" Tezos_michelson.pp compiled
            | `Json ->
                Fmt.pf
                  ppf
                  "%s"
                  (Ezjsonm.to_string
                     ~minify:false
                     (Tezos_michelson.to_json compiled))
          in
          ( match output_file_opt with
          | Some s -> Io.pp_to_file s output
          | None -> output Fmt.stdout );
          say
            "Done: %s → %a."
            input_file
            Fmt.(option ~none:(const string "stdout") string)
            output_file_opt)
      $ input_smart_ml_arg ()
      $ output_michelson_arg ()
      $ pure ()
    , Term.info "initialization" ~doc:"Get the initialization of the storage" )
  in
  let decompile_cmd =
    ( pure (fun input_file output_file ->
          let open Stdlib in
          let open Smart_ml in
          try
            let verbosity = 0 in
            if verbosity >= 1 then prerr_endline "Parsing JSON...";
            let mich = Micheline.parse (Yojson.Basic.from_file input_file) in
            if verbosity >= 1 then prerr_endline "Parsing Micheline...";
            let contract = Michelson.Michelson_contract.of_micheline mich in
            let has_error =
              Michelson.Michelson_contract.has_error
                ~accept_missings:false
                contract
            in
            if verbosity >= 2 || has_error
            then
              Printf.eprintf
                "Parsed contract:\n%s\n"
                (Michelson.Michelson_contract.to_string contract);
            if has_error then failwith "Error in contract.";

            ( if false
            then
              let Michelson.Michelson_contract.
                    {tparameter; tstorage; code; lazy_entry_points; storage} =
                contract
              in
              let contract =
                Michelson.Michelson_contract.typecheck_and_make
                  ~tparameter
                  ~tstorage
                  ~lazy_entry_points
                  ~storage
                  (Michelson_rewriter.default_simplify
                     (Michelson.forget_types code))
              in
              Printf.eprintf
                "Simplified contract:\n%s\n"
                (Michelson.Michelson_contract.to_string contract) );

            if verbosity >= 1 then prerr_endline "Decompiling...";
            let c_michel = open_out output_file in
            let c_py =
              open_out (Filename.remove_extension output_file ^ ".py")
            in
            let c_michel2 =
              open_out
                (Filename.remove_extension output_file ^ "_smartML.michel")
            in
            let ppf_michel = Format.formatter_of_out_channel c_michel in
            let ppf_michel2 = Format.formatter_of_out_channel c_michel2 in
            let ppf_py = Format.formatter_of_out_channel c_py in
            Format.pp_set_margin ppf_michel 120;
            Format.pp_set_margin ppf_py 120;
            let open Michel.Transformer in
            let st = {var_counter = ref 0} in
            ( match Michel_decompiler.michel_of_michelson st contract with
            | Ok c ->
                let c1 = on_contract (simplify st) c in
                let c2 = on_contract (smartMLify st) c in
                Format.fprintf ppf_michel "%a" Michel.Expr.print_contract c1;
                Format.fprintf ppf_michel2 "%a" Michel.Expr.print_contract c2;
                ( try
                    let c2 = Decompiler.smartML_of_michel c2 in
                    Format.fprintf ppf_py "%s" (Printer.tcontract_to_string c2);
                    Format.fprintf ppf_py "\n\n";
                    Format.fprintf ppf_py "@sp.add_test(name = \"Test\")\n";
                    Format.fprintf ppf_py "def test():\n";
                    Format.fprintf ppf_py "    s = sp.test_scenario()\n";
                    Format.fprintf ppf_py "    s += Contract()\n";
                    Format.fprintf ppf_py "    s += Contract()"
                  with
                | exn ->
                    Printf.eprintf
                      "Error: %s\n"
                      (Printer.exception_to_string false exn) )
            | Error msg -> Printf.eprintf "%s\n" msg );

            Format.pp_print_flush ppf_michel ();
            Format.pp_print_flush ppf_michel2 ();
            Format.pp_print_flush ppf_py ();
            close_out c_michel;
            close_out c_michel2;
            close_out c_py
          with
          | exn ->
              prerr_endline (Printer.exception_to_string false exn);
              Term.exit (`Error `Exn))
      $ input_micheline_arg ()
      $ output_michel_arg ()
    , Term.info "decompile" ~doc:"Decompile a Michelson contract to SmartPy" )
  in
  let pack_value_cmd =
    ( ( pure (fun literal using ->
            let sexp = Parsexp.Conv_single.parse_string_exn literal Fn.id in
            let primitives =
              (module Smart_ml.Fake_primitives : Smart_ml.Primitives.Primitives)
            in
            let scenario_state = Smart_ml.Basics.scenario_state () in
            let env = Smart_ml.Typing.init_env () in
            let expr =
              Smart_ml.Import.import_expr
                env
                (Smart_ml.Import.early_env primitives scenario_state)
                sexp
            in
            let _ = Smart_ml.Checker.check_expr ~env [] expr in
            Smart_ml.Solver.apply env;
            let value =
              Smart_ml.Interpreter.interpret_expr_external
                ~primitives
                ~no_env:[`Text "Computing expression"; `Expr expr; `Line expr.el]
                ~scenario_state
                expr
            in
            let from_tezos =
              Smart_ml.Compiler.compile_value value
              |> Tezos_michelson.Of_smart_ml_michelson.literal
              |> Tezos_michelson.of_node
            in
            say
              "Packing equivalent to Michelson literal: %a"
              Tezos_michelson.pp
              from_tezos;
            Fmt.pr
              "0x%a\n%!"
              (fun ppf () ->
                match using with
                | `Tezos_protocol ->
                    let hex =
                      from_tezos
                      |> Data_encoding.Binary.to_bytes_exn
                           Tezos_michelson.Tz_proto.Script_repr.expr_encoding
                      |> Tezos_stdlib.MBytes.to_hex
                    in
                    Fmt.pf ppf "05%a" Hex.pp hex
                | `Smart_ml ->
                    let hex =
                      Smart_ml.Interpreter.pack_value None value
                      |> Hex.of_string
                    in
                    Fmt.pf ppf "%a" Hex.pp hex)
              ())
      $ Arg.(
          required
            (pos
               0
               (some string)
               None
               (info [] ~doc:"Expression literal (S-Expr)")))
      $ Arg.(
          value
            (opt
               (enum
                  [("tezos-protocol", `Tezos_protocol); ("smartml", `Smart_ml)])
               `Smart_ml
               (info
                  ["using"]
                  ~doc:"How to do the generation (should be always equal).")))
      )
    , Term.info
        "pack-value"
        ~doc:"Output michelson-binary bytes for a given value." )
  in
  let pretty_print_sexp_cmd =
    ( ( pure (fun input_path output_path ->
            Io.pp_to_file output_path (fun ppf ->
                Base.Sexp.pp_hum ppf (Io.sexp_of_file_exn input_path Fn.id)))
      $ Arg.(required (pos 0 (some string) None (info [] ~doc:"Input file")))
      $ Arg.(required (pos 1 (some string) None (info [] ~doc:"Output file")))
      )
    , Term.info "pp-sexp" ~doc:"Pretty print an s-expression file." )
  in
  let key_of_name_command () =
    let open Cmdliner in
    let open Term in
    ( ( pure (fun n ->
            let open Flextesa.Tezos_protocol.Account in
            let account = of_name n in
            Fmt.pr
              "%s,%s,%s,%s\n%!"
              (name account)
              (pubkey account)
              (pubkey_hash account)
              (private_key account))
      $ Arg.(
          required
            (pos
               0
               (some string)
               None
               (info [] ~docv:"NAME" ~doc:"String to generate the data from.")))
      )
    , info
        "key-of-name"
        ~doc:"Make an unencrypted key-pair deterministically from a string."
        ~man:
          [ `P
              "`smartml key-of-name hello-world` generates a key-pair of the \
               `unencrypted:..` kind and outputs it as a 4 values separated by \
               commas: `name,pub-key,pub-key-hash,private-uri` (hence \
               compatible with the `--add-bootstrap-account` option of some of \
               the flextesa-sandbox scenarios)." ] )
  in
  let ocaml_cmd =
    ( Term.(
        pure (fun input_file output_file () ->
            let smartml_contract = Io.contract_of_file_exn input_file in
            Io.pp_to_file
              output_file
              Fmt.(
                fun ppf ->
                  pf
                    ppf
                    "@[<2>let contract =@,%a@]@."
                    Smart_ml.Basics.pp_value_tcontract
                    smartml_contract);
            say "Done: %s." input_file)
        $ Arg.(
            required
              (pos
                 0
                 (some file)
                 None
                 (info
                    []
                    ~docv:"INPUT-FILE"
                    ~docs:"ARGUMENTS"
                    ~doc:"Input file, by default, a SmartML S-Expression.")))
        $ Arg.(
            required
              (pos
                 1
                 (some string)
                 None
                 (info
                    []
                    ~docv:"OUTPUT-FILE"
                    ~docs:"ARGUMENTS"
                    ~doc:"Output .ml file.")))
        $ pure ())
    , Term.info
        "ocaml"
        ~doc:
          "Try to output a contract as an OCaml module (syntactic validity not \
           guaranteed)." )
  in
  Term.exit
    (Term.eval_choice
       (help : unit Term.t * _)
       [ (* Sandbox.cmd_smartbox ()
            ; Sandbox.cmd_scenario_example ()
            ; *)
         ocaml_cmd
       ; compile_cmd
       ; decompile_cmd
       ; init_storage_cmd
       ; key_of_name_command ()
       ; pack_value_cmd
       ; pretty_print_sexp_cmd
       ; Tezos_scenario_unix.cmd () ])
