(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Base

module Date = struct
  include Ptime

  let opt_exn msg o = Option.value_exn ~message:(Fmt.str "Date: %s" msg) o

  let float_to_string f =
    f |> Ptime.of_float_s |> opt_exn "now failed" |> Ptime.to_rfc3339

  let now () = Unix.gettimeofday () |> Ptime.of_float_s |> opt_exn "now failed"
end

let debug = ref false

let () =
  match Caml.Sys.getenv_opt "smartpy_debug" with
  | Some "true" -> debug := true
  | _ -> ()

let dbg fmt =
  ( if !debug
  then
    Fmt.kstr (fun s ->
        Fmt.epr "[DBG:%a]: %s\n%!" Date.(pp_human ~frac_s:3 ()) Date.(now ()) s)
  else Caml.Format.ikfprintf (fun _ -> ()) Caml.Format.err_formatter )
    fmt

module System = struct
  let pp_to_file path f =
    let open Caml in
    let open Format in
    let o = open_out path in
    let ppf = formatter_of_out_channel o in
    pp_set_margin ppf 80;
    f ppf;
    pp_print_flush ppf ();
    flush o;
    close_out o;
    ()

  let read_file path =
    let open Caml in
    let i = open_in path in
    let b = Buffer.create 1020 in
    dbg "read_file: %S" path;
    let rec loop () =
      match input_char i with
      | c ->
          Buffer.add_char b c;
          loop ()
      | exception e ->
          dbg "read_file: %a (%d bytes)" Fmt.exn e (Buffer.length b)
    in
    loop ();
    close_in i;
    Buffer.contents b
end

module More_fmt = struct
  include Fmt

  let vertical_box ?indent ppf f = vbox ?indent (fun ppf () -> f ppf) ppf ()

  let wrapping_box ?indent ppf f = box ?indent (fun ppf () -> f ppf) ppf ()

  let wf ppf fmt = Fmt.kstr (fun s -> box (fun ppf () -> text ppf s) ppf ()) fmt

  let markdown_verbatim_list ppf l =
    vertical_box ~indent:0 ppf (fun ppf ->
        cut ppf ();
        string ppf (String.make 45 '`');
        List.iter l ~f:(fun l ->
            cut ppf ();
            string ppf l);
        cut ppf ();
        string ppf (String.make 45 '`'))

  let long_string ?(max = 30) ppf s =
    match String.sub s ~pos:0 ~len:(max - 2) with
    | s -> pf ppf "%S" (s ^ "...")
    | exception _ -> pf ppf "%S" s

  let json ppf json =
    markdown_verbatim_list
      ppf
      (Ezjsonm.value_to_string ~minify:false json |> String.split ~on:'\n')
end

module Make (Primitives : Smart_ml.Primitives.Primitives) = struct
  module Smartml_scenario = struct
    type t =
      { name : string
      ; sc : Smart_ml.Scenario.t
      ; scenario_state : Smart_ml.Basics.scenario_state
      ; typing_env : Smart_ml.Typing.env }

    let make name sc scenario_state typing_env =
      {name; sc; scenario_state; typing_env}

    let pp_quick ppf t =
      Fmt.pf ppf "{%s %d actions}" t.name (List.length t.sc.actions)

    let of_json_file filename =
      let scenarios =
        Yojson.Basic.Util.to_list (Yojson.Basic.from_file filename)
      in
      List.map
        ~f:(fun scenario ->
          let scenario_state = Smart_ml.Basics.scenario_state () in
          let typing_env =
            Smart_ml.Typing.init_env
              ~contract_data_types:scenario_state.contract_data_types
              ()
          in
          make
            ( Caml.Filename.(basename filename |> chop_extension)
            ^ "-"
            ^ Yojson.Basic.Util.to_string
                (Yojson.Basic.Util.member "shortname" scenario) )
            (Smart_ml.Scenario.load_from_string
               ~primitives:(module Primitives : Smart_ml.Primitives.Primitives)
               ~scenario_state
               ~typing_env
               (Yojson.Basic.Util.member "scenario" scenario))
            scenario_state
            typing_env)
        scenarios

    let cmdliner_term () =
      let open Cmdliner in
      let open Term in
      pure (fun jsons ->
          let scenarios = List.map jsons ~f:of_json_file in
          scenarios)
      $ Arg.(
          value
            (opt_all
               file
               []
               (info ["scenario"] ~doc:"Run a JSON/SmartML test scenarios.")))
  end

  module This_client = struct
    module Io = struct
      type 'a t = 'a

      let return x = x

      let bind x ~f = f x
    end

    module Connection = struct
      type t =
        | Node of
            { address : string option
            ; port : int
            ; tls : bool }

      let node ?address ?(tls = false) port = Node {address; port; tls}

      let default = node 20_000

      let cmdliner_term () =
        let open Cmdliner in
        let open Term in
        let warn fmt = Fmt.kstr (Fmt.epr "WARNNING: %s\n%!") fmt in
        pure (fun uris ->
            let uri = Uri.of_string uris in
            let address = Uri.host uri in
            let port =
              let default = 42_000 in
              match (Uri.port uri, Uri.path uri) with
              | None, "" -> default
              | Some p, "" -> p
              | Some p, more ->
                  warn "not using %S from %a" more Uri.pp uri;
                  p
              | None, s ->
                ( try Int.of_string s with
                | _ ->
                  ( try Int.of_string (String.chop_prefix_exn s ~prefix:":") with
                  | _ ->
                      warn
                        "cannot understand %S from %a, using port %d"
                        s
                        Uri.pp
                        uri
                        default;
                      default ) )
            in
            match Uri.scheme uri with
            | None | Some "http" -> node ?address port
            | Some "https" -> node ~tls:true ?address port
            | Some other -> Fmt.failwith "Unknown URI scheme: %S" other)
        $ Arg.(
            value
              (opt
                 string
                 ":42000"
                 (info
                    ["connection"]
                    ~doc:"The URI to use to connect to the node.")))
    end

    module R = Monad.Make (struct
      include Smart_ml.Tezos_scenario.Decorated_result

      let map = `Define_using_bind
    end)

    open R

    let fail m =
      Smart_ml.Tezos_scenario.Decorated_result.fail
        (Smart_ml.Tezos_scenario.Error.make m)

    let attach = Smart_ml.Tezos_scenario.Decorated_result.attach

    let ( // ) = Caml.Filename.concat

    type state =
      { client_command : string
      ; connection : Connection.t
      ; mutable funder : string
      ; mutable root_dir : string
      ; confirmation : [ `Bake | `Wait of int ]
      ; mutable command : int
      ; originator : string option
      ; translate_paths : string -> string }

    let state
        ?(client_command = "tezos-client")
        ?(funder = "funder")
        ?(confirmation = `Wait 1)
        ~connection
        ?(translate_paths = Fn.id)
        root_dir =
      { client_command
      ; connection
      ; funder
      ; root_dir
      ; command = 0
      ; confirmation
      ; originator = None
      ; translate_paths }

    module Command_result = struct
      type t = {id : int}

      let make id = {id}

      let pp ppf i = Fmt.pf ppf "{command:%d}" i.id

      let output_path state {id} = state.root_dir // Fmt.str "command_%04d" id

      let get_stdout state i =
        System.read_file (output_path state i // "out.txt")
    end

    let command ?(succeed = true) state args =
      let id = Command_result.make state.command in
      state.command <- state.command + 1;
      let outpath = Command_result.output_path state id in
      let base_dir = state.root_dir // "client-dir" in
      let command_string =
        let exec =
          let connect_args =
            match state.connection with
            | Node {address; port; tls} ->
                (if tls then ["--tls"] else [])
                @ ["--port"; Int.to_string port]
                @ Option.value_map address ~default:[] ~f:(fun a ->
                      ["--addr"; a])
          in
          ["-d"; state.translate_paths base_dir]
          @ connect_args
          @ List.map args ~f:(function
                | f when Caml.Sys.file_exists f -> state.translate_paths f
                | f -> f)
        in
        String.concat
          ~sep:" "
          (state.client_command :: List.map exec ~f:Caml.Filename.quote)
      in
      let decorated =
        let outq = Caml.Filename.quote outpath in
        Fmt.str
          "mkdir -p %s %s && {  %s > %s/out.txt 2> %s/err.txt ; }"
          (Caml.Filename.quote base_dir)
          outq
          command_string
          outq
          outq
      in
      let ret = Caml.Sys.command decorated in
      System.pp_to_file
        (outpath // "cmd.txt")
        Fmt.(fun ppf -> string ppf decorated);

      (* dbg "cmd: %s → %d" decorated ret; *)
      ( if succeed && ret <> 0
      then Fmt.kstr fail "Command %a failed" Command_result.pp id
      else return id )
      |> attach
           ~a:
             [ `O
                 [ ("id", `Int id.id)
                 ; ("command", `Path (outpath // "cmd.txt"))
                 ; ("stdout", `Path (outpath // "out.txt"))
                 ; ("stderr", `Path (outpath // "err.txt"))
                 ; ("ret", `Int ret) ] ]

    let default_originator = "smartml-originator"

    let make_account ?sk state name =
      let secret_key =
        let preclean =
          match sk with
          | Some s -> s
          | None ->
              let account = Primitives.Crypto.account_of_seed name in
              account.sk
        in
        match String.is_prefix preclean ~prefix:"unencrypted:" with
        | true -> preclean
        | false -> "unencrypted:" ^ preclean
      in
      command state ["import"; "secret"; "key"; name; secret_key; "--force"]
      >>= fun _ -> return name

    let potential_bake state =
      match state.confirmation with
      | `Wait _ ->
          dbg "not baking here";
          return ()
      | `Bake ->
          command
            state
            ["bake"; "for"; state.funder; "--force"; "--minimal-timestamp"]
          >>= fun _ -> return ()

    let init state =
      begin
        if String.is_prefix state.funder ~prefix:"unencrypted:"
        then (
          dbg "We need to import %s" state.funder;
          let name = "the-funding-authority" in
          make_account ~sk:state.funder state name
          >>= fun _ ->
          state.funder <- name;
          return () )
        else return ()
      end
      >>= fun () ->
      List.init 3 ~f:Fn.id
      |> List.fold ~init:(return ()) ~f:(fun pm _ ->
             pm >>= fun () -> potential_bake state)

    let wait_arg state =
      match state.confirmation with
      | `Wait n -> Int.to_string n
      | `Bake -> "none"

    let account_name (acc : Smart_ml.Primitives.account) =
      "Acc-" ^ String.prefix acc.pkh 10

    let rec ensure_account state (acc : Smart_ml.Primitives.account) ~balance =
      make_account state ~sk:acc.sk (account_name acc)
      >>= fun name ->
      command state ["get"; "balance"; "for"; name]
      >>= fun balance_cmd ->
      begin
        match
          Option.(
            String.lsplit2 (Command_result.get_stdout state balance_cmd) ~on:' '
            >>= fun (o, _) ->
            try_with (fun () -> Float.(of_string o * 1_000_000. |> to_int)))
        with
        | Some o -> return o
        | None -> Fmt.kstr fail "Failed to get the balance for %s" acc.pkh
      end
      >>= fun current_balance ->
      if balance <= current_balance
      then return (account_name acc)
      else
        transfer state ~dst:name ~amount:(balance - current_balance)
        >>= fun () ->
        dbg "Made %s with %d (cur: %d)" name balance current_balance;
        return name

    and transfer ?arg ?entry_point ?(amount = 0) ?sender ?source state ~dst =
      let extras =
        Option.value_map arg ~default:[] ~f:(fun a -> ["--arg"; a])
        @ Option.value_map entry_point ~default:[] ~f:(fun e ->
              ["--entrypoint"; e])
      in
      begin
        match (sender, source) with
        | None, None -> return None
        | Some e, Some o when Poly.equal e o -> return (Some e)
        | Some _, Some _ ->
            Fmt.kstr fail "source and sender but different: Not Implemented"
        | Some e, None -> return (Some e)
        | None, Some o -> return (Some o)
      end
      >>= (function
            | None -> return state.funder
            | Some (aoa : Smart_ml.Basics.account_or_address) ->
                Smart_ml.Basics.(
                  ( match aoa with
                  | Account acc ->
                      ensure_account
                        state
                        acc
                        ~balance:(10_000_000 + (20 * amount))
                  | Address (Smart_ml.Literal.Real _)
                   |Address (Smart_ml.Literal.Local _) ->
                      Fmt.kstr fail "source|sender = address: not implemented"
                  )))
      >>= fun src ->
      attach
        ~a:
          [ `O
              [ ( "Transfer"
                , `O
                    [ ("amount", `Int amount)
                    ; ("from", `Code [src])
                    ; ("to", `Code [dst]) ] ) ] ]
        (command
           state
           ( [ "-wait"
             ; wait_arg state
             ; "transfer"
             ; (let tez = amount / 1_000_000 in
                let decimals = amount % 1_000_000 in
                Fmt.str
                  "%d%s"
                  tez
                  (if decimals = 0 then "" else Fmt.str ".%06d" decimals))
             ; "from"
             ; src
             ; "to"
             ; dst
             ; "--burn-cap"
             ; "1" ]
           @ extras ))
      >>= fun _ -> potential_bake state

    let originator state =
      match state.originator with
      | None ->
          make_account state default_originator
          >>= fun _ ->
          transfer state ~dst:default_originator ~amount:10_000_000_000
          >>= fun () -> return default_originator
      | Some s -> return s

    let originate state ~id ~contract ~storage =
      dbg
        "Supposed to originate:@ `%s...`@ with `%s...`"
        (String.prefix contract 20)
        (String.prefix storage 20);

      originator state
      >>= fun orig_account ->
      let name =
        Fmt.str
          "c%03d-%s"
          id
          Caml.Digest.(
            Fmt.kstr string "%s %s %f" contract storage (Unix.gettimeofday ())
            |> to_hex)
      in
      let balance = "10" in
      let burn_cap = "20" in
      let contract_path = state.root_dir // Fmt.str "contract-%s.tz" name in
      System.pp_to_file contract_path Fmt.(fun ppf -> string ppf contract);
      attach
        ~a:[`O [("origination", `Text name); ("code", `Path contract_path)]]
        (command
           state
           [ "--wait"
           ; wait_arg state
           ; "originate"
           ; "contract"
           ; name
           ; "transferring"
           ; balance
           ; "from"
           ; orig_account
           ; "running"
           ; contract_path
           ; "--init"
           ; storage
           ; "--burn-cap"
           ; burn_cap ])
      >>= fun _origination ->
      potential_bake state
      >>= fun () ->
      command state ["show"; "known"; "contract"; name]
      >>= fun show ->
      let addr = Command_result.get_stdout state show |> String.strip in
      dbg "Contract: %S" addr;
      return addr

    let get_contract_storage state ~address =
      command state ["get"; "contract"; "storage"; "for"; address]
      >>= fun cmd_id -> return (Command_result.get_stdout state cmd_id)

    let run_script state ~contract ~parameter =
      command
        state
        [ "run"
        ; "script"
        ; contract
        ; "on"
        ; "storage"
        ; "Unit"
        ; "and"
        ; "input"
        ; parameter
        ; "-G"
        ; "1000000000" ]
      >>= fun _ -> return ()

    let cmdliner_term () =
      let open Cmdliner in
      let open Term in
      pure (fun client_command connection confirmation funder root_dir ->
          state ~confirmation ~client_command ~funder ~connection root_dir)
      $ Arg.(
          value
            (opt
               string
               "tezos-client"
               (info
                  ["client-command"]
                  ~docv:"CMD"
                  ~doc:"Use $(docv) as tezos-client command prefix")))
      $ Connection.cmdliner_term ()
      $ Arg.(
          pure (fun bake wait ->
              match (bake, wait) with
              | false, None -> `Wait 1
              | false, Some w -> `Wait w
              | true, None -> `Bake
              | true, Some _ ->
                  Fmt.failwith "Cannot give --bake and --wait together.")
          $ value
              (flag (info ["bake"] ~doc:"Make the client to bake by itself."))
          $ value
              (opt
                 (some int)
                 None
                 (info
                    ["wait"]
                    ~doc:"Make the client to wait for $(docv) confirmations."
                    ~docv:"N")))
      $ Arg.(
          value
            (opt
               string
               "funder"
               (info
                  ["funder"]
                  ~doc:"The account that has a ton of funds to use.")))
      $ Arg.(
          value
            (opt
               string
               "/tmp/smartml-tezos-scenario"
               (info ["root-path"] ~doc:"Root path for all outputs.")))
  end

  module Scenario_interpreter =
    Smart_ml.Tezos_scenario.Make_interpreter (This_client) (Primitives)

  let pp_state ?(full = false) ppf s =
    let open Scenario_interpreter.State in
    let open More_fmt in
    let history_item ppf item =
      let status ppf = function
        | `Success -> pf ppf "✔"
        | `Failure -> pf ppf "✖"
        | `None -> pf ppf "━"
      in
      let action ppf =
        let open Smart_ml.Basics in
        let pp_line ppf x = pf ppf " (l.%d)" x in
        function
        | New_contract {id; line_no; _} ->
            pf
              ppf
              "New contract %s%a"
              (Smart_ml.Printer.string_of_contract_id id)
              pp_line
              line_no
        | Set_delegate {id; line_no} ->
            pf
              ppf
              "Set delegate %s%a"
              (Smart_ml.Printer.string_of_contract_id id)
              pp_line
              line_no
        | Message {id; message; line_no; title; valid; _} ->
            wrapping_box ~indent:2 ppf (fun ppf ->
                pf
                  ppf
                  "Message %s#%s"
                  (Smart_ml.Printer.string_of_contract_id id)
                  message;
                pp_line ppf line_no;
                if String.is_empty title then () else pf ppf "@ “%s”" title;
                if valid
                then pf ppf "@ (should succeed)"
                else pf ppf "@ (should fail)")
        | Compute {id; line_no; _} ->
            pf ppf "Compute %d" id;
            pp_line ppf line_no
        | Simulation {id; line_no; _} ->
            pf ppf "Simulation %s" (Smart_ml.Printer.string_of_contract_id id);
            pp_line ppf line_no
        | ScenarioError {message} -> pf ppf "Error: %s" message
        | Html _ -> pf ppf "<html/>"
        | Verify {line_no; _} ->
            pf ppf "Verify";
            pp_line ppf line_no
        | Show {line_no; _} ->
            pf ppf "Show";
            pp_line ppf line_no
        | Exception l ->
            pf
              ppf
              "Exception (%a)"
              text
              (String.concat
                 ~sep:" "
                 (List.map l ~f:(Smart_ml.Printer.pp_smart_except false)))
        | DynamicContract {id = {dynamic_id}; tparameter = _; tstorage = _} ->
            pf ppf "DynamicContract Dyn_%d [...]" dynamic_id
      in
      let actions ppf = function
        | [] -> ()
        | [one] -> action ppf one
        | multi ->
            vertical_box ~indent:2 ppf (fun ppf ->
                pf ppf "Multiple actions:";
                cut ppf ();
                List.iter multi ~f:(fun a -> pf ppf "* %a" action a))
      in
      let rec pp_attachment ppf att =
        let pp_obj header ppf obj =
          vertical_box ~indent:2 ppf (fun ppf ->
              pf ppf "\\_%s" header;
              List.iter obj ~f:(fun (k, v) ->
                  cut ppf ();
                  wrapping_box ~indent:3 ppf (fun ppf ->
                      pf ppf "|- %s:@ %a" k pp_attachment v)))
        in
        match att with
        | `Path s -> pf ppf "`%s`" s
        | `Text t -> wf ppf "%s" t
        | `Code [] -> pf ppf "<EMPTY>"
        | `Code [line] -> pf ppf "`%s`" line
        | `Code (first :: lines) ->
            vertical_box ppf (fun ppf ->
                pf ppf "|| %s" first;
                List.iter lines ~f:(fun l ->
                    cut ppf ();
                    pf ppf "|| %s" l))
        | `Int i -> pf ppf "%d" i
        | `Error err ->
            wrapping_box ~indent:4 ppf (fun ppf ->
                pf
                  ppf
                  "Error:@ “%a”"
                  Smart_ml.Tezos_scenario.Error.pp_quick
                  err)
        | `O [(onek, `O obj)] -> pp_obj (str "%s:" onek) ppf obj
        | `O obj -> pp_obj "" ppf obj
      in
      let open Smart_ml.Tezos_scenario.History_event in
      vertical_box ~indent:2 ppf (fun ppf ->
          wrapping_box ppf ~indent:2 (fun ppf ->
              wf ppf "%a %a" status item.status actions item.actions;
              match item.status with
              | (`None | `Success) when not full -> string ppf "."
              | _ -> pf ppf ", details:");
          match item.status with
          | (`None | `Success) when not full -> ()
          | _ when Poly.equal item.attachements [] -> pf ppf " <none>."
          | _ ->
              List.iter item.attachements ~f:(fun att ->
                  cut ppf ();
                  pp_attachment ppf att))
    in
    let filtered_history =
      let open Smart_ml.Tezos_scenario.History_event in
      let seq = Caml.Queue.to_seq s.history in
      if full
      then seq
      else
        Caml.Seq.filter
          (fun item ->
            List.for_all item.actions ~f:(function
                | Html _ | Show _ -> false
                | _ -> true))
          seq
    in
    vertical_box ~indent:2 ppf (fun ppf ->
        let q_lgth = Caml.Queue.length s.history in
        wf
          ppf
          "Scenario interpretation state: (%a history item%s):"
          int
          q_lgth
          (if q_lgth = 1 then "" else "s");
        Caml.Seq.iter
          (fun item ->
            cut ppf ();
            history_item ppf item)
          filtered_history)

  let pp_all_results ?full ppf results =
    let open Smartml_scenario in
    List.iter results ~f:(fun (scen, state_opt, res, dur) ->
        let open More_fmt in
        vertical_box ppf (fun ppf ->
            wf
              ppf
              "#====== Test Results for %s: %a ======#"
              scen.name
              (fun ppf -> function
                | Ok () -> pf ppf "All OK!"
                | Error _ -> pf ppf "FAILED ☹")
              res;
            cut ppf ();
            pf ppf "⏰ Total Time: %0.2f s." dur;
            cut ppf ();
            Result.iter_error
              res
              ~f:(fun {Smart_ml.Tezos_scenario.Error.message} ->
                pf ppf "Error: %s@," message);
            Option.iter state_opt ~f:(fun state -> pp_state ?full ppf state);
            cut ppf ()))

  let run ?(advancement = Fmt.epr "%s\n%!") ~scenarios ~client () =
    dbg "To-do:@ %a" Fmt.(list ~sep:sp Smartml_scenario.pp_quick) scenarios;

    let _ =
      Fmt.kstr
        Caml.Sys.command
        "rm -fr %s; mkdir -p %s"
        client.This_client.root_dir
        client.This_client.root_dir
    in
    let global_start = Unix.gettimeofday () in
    let results =
      List.map
        scenarios
        ~f:(fun ({name; sc; scenario_state; typing_env} as scen) ->
          dbg "Running %s" name;
          Fmt.kstr advancement "Running scenario %s ..." name;
          let start_time = Unix.gettimeofday () in
          let stop () = Unix.gettimeofday () -. start_time in
          let client_dir = This_client.(client.root_dir // name) in
          let client = This_client.{client with root_dir = client_dir} in
          let write_result_file ?client r =
            (* Here we make a deterministic result file.
                     - For successes we record the number of steps.
                     - For failures this number is (for now) not fully
                       deterministic because it depends on the value of
                       `NOW` for instance. *)
            let open Scenario_interpreter.State in
            let result_summary =
              Option.value_map ~default:"no-history" client ~f:(fun x ->
                  let open Smart_ml.Tezos_scenario.History_event in
                  Caml.Queue.fold
                    (fun prev item ->
                      match item.status with
                      | `Failure ->
                          (if String.(prev = "") then "" else prev ^ ",")
                          ^ "F:"
                          ^ ( List.map
                                item.actions
                                ~f:
                                  Smart_ml.Basics.(
                                    function
                                    | New_contract _ -> "O"
                                    | Message _ -> "M"
                                    | Verify _ -> "V"
                                    | _ -> "_")
                            |> String.concat ~sep:"" )
                      | _ -> prev)
                    ""
                    x.history
                  |> function
                  | "" -> Int.to_string (Caml.Queue.length x.history)
                  | x -> x)
            in
            System.pp_to_file
              (client_dir ^ "/result.tsv")
              Fmt.(
                fun ppf ->
                  pf
                    ppf
                    "%s\t%s\t%s\n%!"
                    scen.name
                    ( match r with
                    | Ok () -> "OK"
                    | Error _ -> "ERROR" )
                    result_summary)
          in
          match This_client.init client with
          | {result = Ok (); _} ->
              Fmt.kstr advancement "Client initialized.";
              let state =
                Scenario_interpreter.State.of_smartml
                  scenario_state
                  ~client
                  ~log_advancement:advancement
              in
              begin
                match Scenario_interpreter.run state typing_env sc with
                | Ok () ->
                    write_result_file ~client:state (Ok ());
                    (scen, Some state, Ok (), stop ())
                | Error err ->
                    Fmt.epr
                      "ERROR: %a\n%!"
                      Smart_ml.Tezos_scenario.Error.pp_quick
                      err;
                    write_result_file ~client:state (Error err);
                    (scen, Some state, Error err, stop ())
                | exception e ->
                    Fmt.epr "ERROR: %a\n%!" Fmt.exn e;
                    let message =
                      Fmt.str
                        "Exception: %s"
                        (Smart_ml.Printer.exception_to_string false e)
                    in
                    let e = Error {Smart_ml.Tezos_scenario.Error.message} in
                    write_result_file ~client:state e;
                    (scen, Some state, e, stop ())
              end
          | {result = Error err; _} ->
              Fmt.kstr
                advancement
                "Client initialization error: %a."
                Smart_ml.Tezos_scenario.Error.pp_quick
                err;
              write_result_file (Error err);
              (scen, None, Error err, stop ()))
    in
    Fmt.kstr advancement "Writing results.";
    let global_end = Unix.gettimeofday () in
    let global_time = global_end -. global_start in
    let pp_header ppf () =
      let open More_fmt in
      vertical_box ppf (fun ppf ->
          string ppf (String.make 80 '#');
          cut ppf ();
          let nb = List.length results in
          pf ppf "* Ran %d test%s.@," nb (if nb = 1 then "" else "s");
          pf ppf "* Total time: %f s.@," global_time;
          pf ppf "  * Start: %s@," Date.(float_to_string global_start);
          pf ppf "  * End:   %s@," Date.(float_to_string global_end);
          string ppf (String.make 80 '#'));
      cut ppf ()
    in
    let results_dot_txt =
      Caml.Filename.concat client.This_client.root_dir "results.txt"
    in
    System.pp_to_file results_dot_txt (fun ppf ->
        pp_header ppf ();
        pp_all_results ppf results);
    let results_full_dot_txt =
      Caml.Filename.concat client.This_client.root_dir "results-full.txt"
    in
    System.pp_to_file results_full_dot_txt (fun ppf ->
        pp_header ppf ();
        pp_all_results ~full:true ppf results);
    let results_dot_json =
      Caml.Filename.concat client.This_client.root_dir "results.json"
    in
    let results_save =
      let open Ezjsonm in
      let open Smartml_scenario in
      let open Scenario_interpreter.State in
      let one_result (scen, state, res, (_ : float)) =
        dict
          [ ("name", string scen.name)
          ; ( "result"
            , string
                ( match res with
                | Ok () -> "OK"
                | Error _ -> "ERROR" ) )
          ; ( "items"
            , int
                (Option.value_map ~default:0 state ~f:(fun x ->
                     Caml.Queue.length x.history)) ) ]
      in
      dict [("tezos-scenario-result.v0", list one_result results)]
    in
    System.pp_to_file results_dot_json (fun ppf ->
        Fmt.string ppf (Ezjsonm.value_to_string ~minify:false results_save));
    dbg "%s, %s" results_dot_txt results_full_dot_txt;
    object
      method success =
        List.for_all results ~f:(function
            | _, state, Ok (), _ ->
              ( match state with
              | None -> true
              | Some {history; _} ->
                  Caml.Queue.fold
                    (fun prev h ->
                      let open Smart_ml.Tezos_scenario.History_event in
                      prev
                      &&
                      match h.status with
                      | `Failure -> false
                      | `Success | `None -> true)
                    true
                    history )
            | _ -> false)

      method results_dot_txt = results_dot_txt

      method results_full_dot_txt = results_full_dot_txt

      method results_dot_json = results_dot_json
    end

  let cmd ?docs ?(cmd_name = "run-tezos-scenario") () =
    let open Cmdliner in
    let open Term in
    let term =
      pure (fun scenarios client () ->
          let scenarios = List.concat scenarios in
          let res = run ~scenarios ~client () in
          Fmt.pr
            "\n%a\n%!"
            Fmt.(
              vbox
                ~indent:2
                (pair
                   ~sep:cut
                   string
                   (list ~sep:cut (fun ppf -> pf ppf "* `%s`"))))
            ( Fmt.str
                "Tests %s, results:"
                (if res#success then "SUCCEEDED" else "FAILED")
            , [ res#results_dot_txt
              ; res#results_dot_json
              ; res#results_full_dot_txt ] ))
      $ Smartml_scenario.cmdliner_term ()
      $ This_client.cmdliner_term ()
      $ pure ()
    in
    (term, info cmd_name ?docs ~doc:"Run a SmartML scenario using tezos-client.")
end
