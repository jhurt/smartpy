(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils

let pp = print_endline

module Hashtbl_ = Hashtbl
open Base
open Smart_ml

let dbg f : unit =
  Fmt.(
    epr
      "%a@.@?"
      (fun ppf () ->
        box
          (fun ppf () ->
            string ppf "|DBG-test>";
            sp ppf ();
            f ppf)
          ppf
          ())
      ())

(*
   A first experiment of OCaml EDSL.

   - Very simple wrapping.
   - No types/GADTs.
   - Some very incomplete type inference.

   Cf, https://gitlab.com/SmartPy/smartpy-private/issues/19
*)
module EDSL_exp0 = struct
  open Basics

  module Ty = struct
    let bool = Type.bool

    let string = Type.string

    let unit = Type.unit

    let int = Type.int ()

    let record = Type.record

    let address = Type.address

    let token = Type.token
  end

  let get_type_or_try_to_infer infer_fun t v =
    match t with
    | Some s -> s
    | None -> infer_fun v

  let infer_from_expression storage_type = function
    | EPrim1 (ENot, _) -> Ty.bool
    | EPrim0 (ELocal "__storage__") -> storage_type
    | EPrim0 (ELocal "__operations__") -> Type.list Type.operation
    | ETransfer _ -> Type.operation
    | ECons (_, l) -> l.et
    | other ->
        Fmt.kstrf
          failwith
          "expresssion: cannot infer type from expr: %a"
          pp_uexpr
          other

  let expression ?t storage_type e =
    { e
    ; el = -1
    ; et = get_type_or_try_to_infer (infer_from_expression storage_type) t e }

  let infer_from_value = function
    | Literal Unit -> Ty.unit
    | Literal (String _) -> Ty.string
    | Literal (Int _) -> Ty.int
    | Literal (Address _) -> Ty.address
    | Literal (Mutez _) -> Ty.token
    | other ->
        Fmt.kstrf failwith "value: cannot infer type from %a" pp_uvalue other

  let value ?t v = {v; t = get_type_or_try_to_infer infer_from_value t v}

  let big_int = Bigint.of_int

  module Val = struct
    let unit = Value.unit

    let string s = Value.string s

    let record ~t l = value ~t (Record l)

    let address s = Value.address s

    let int i = Value.int (big_int i)

    let token t = Value.mutez (big_int t)
  end

  module Var = struct
    let string v = (v, Ty.string)
  end

  let message channel params = {channel; params}

  let channel channel ?(t = Ty.unit) body =
    {channel; paramsType = t; originate = true; body}

  let command c ?(line = 0) () =
    {line_no = line; c; ct = Type.unit; has_operations = HO_none}

  module Cmd = struct
    let make = command

    let seq ?(line = 0) l = Smart_ml.Command.seq ~line_no:line l

    let set_var var e =
      Smart_ml.Command.set
        ~line_no:(-1)
        (Smart_ml.Expr.local ~line_no:(-1) var)
        e

    let transfer ~destination ~amount:amount_ () =
      let op f = f ~line_no:(-1) in
      let open Smart_ml.Expr in
      let ops = op operations in
      op
        Command.set
        ops
        (op
           cons
           (op transfer ~arg:(op cst Literal.unit) ~amount:amount_ ~destination)
           ops)
  end

  let contract
      ?(unknown_parts = None)
      ?(balance = 0)
      ?(storage = Val.unit)
      ?(flags = [])
      entry_points =
    let entry_points_layout = ref (Type.UnUnknown "") in
    let tparameter =
      Type.variant
        entry_points_layout
        (List.map
           ~f:(fun (channel : _ entry_point) ->
             (channel.channel, channel.paramsType))
           entry_points)
    in
    { value_tcontract =
        { balance = Value.mutez (big_int balance)
        ; storage = Some storage
        ; baker = Value.none Type.key_hash
        ; tstorage = storage.t
        ; tparameter
        ; entry_points
        ; entry_points_layout
        ; unknown_parts
        ; flags
        ; global_variables = [] } }

  module Example = struct
    module Send_channel = struct
      let params_type =
        Ty.(
          record
            (ref (Type.UnUnknown ""))
            [("address", address); ("amount", Type.token)])

      let channel =
        let params_field name =
          Expr.attr ~line_no:(-1) (Expr.params ~line_no:(-1) params_type) name
        in
        let destination =
          params_field "address"
          |> Expr.contract ~line_no:(-1) None (Type.full_unknown ())
          |> Expr.openVariant ~line_no:(-1) "Some"
        in
        channel
          "send"
          ~t:params_type
          (Cmd.seq
             [Cmd.transfer ~destination ~amount:(params_field "amount") ()])
    end
  end
end

(** Additional constructs for {!Alcotest}. *)
module Testing_helpers = struct
  (** Alcotest.testable for {!Basics.Execution.error}. *)
  let testable_exec_error =
    Alcotest.of_pp
      (Fmt.option ~none:Fmt.(const string "No-error") Basics.Execution.pp_error)

  (** Alcotest.testable for {!Basics.toperation}. *)
  let testable_toperation =
    Alcotest.of_pp (Basics.pp_operation Basics.pp_tvalue)

  (** [does_raise "interesting thing to say"
        ~p:predicate_on_exn ~f:function_supposed_to_raise]. *)
  let does_raise : string -> p:(exn -> bool) -> f:(unit -> unit) -> unit =
   fun msg ~p ~f ->
    try
      f ();
      Alcotest.failf "%s: did not raise an exception" msg
    with
    | e when p e -> ()
    | other ->
        Alcotest.failf
          "%s: did not raise the right exception: %a"
          msg
          Exn.pp
          other

  (** Match any exception in the [~p] argument of {!does_raise}. *)
  let any_failure = function
    | Failure _ | Basics.SmartExcept _ -> true
    | _ -> false

  (** Do not use this directly. *)
  exception Found

  (** [check_find m f] calls f with a function [found : unit -> 'a]
      that has to be called for the test to be considered successful. *)
  let check_find msg f =
    Alcotest.check_raises msg Found (fun () -> f (fun () -> raise Found))
end

(** Tests for {!Smart_ml.Contract.eval}. *)
module Contract_eval_test = struct
  let do_test_of_contract_eval ?context ~scenario_state ~env contract message =
    let ctx =
      Option.value
        context
        ~default:
          (Interpreter.context
             ~sender:(Literal.Real "tz1sender")
             ~time:42
             ~amount:(Bigint.of_int 10)
             ~line_no:10
             ()
             ~debug:false)
    in
    let contract_cmd_option, operations, error_opt, _ =
      Interpreter.interpret_message
        ~primitives:(module Fake_primitives : Primitives.Primitives)
        ~scenario_state
        ~env
        ctx
        contract
        message
    in
    ( match error_opt with
    | None ->
        pp (Smart_ml.Printer.type_to_string message.params.t);
        ( try Solver.apply env with
        | Basics.SmartExcept l ->
            failwith (Smart_ml.Printer.pp_smart_except false (`Rec l)) );
        ignore (Checker.check_value_contract ~env [] contract)
    | Some _ -> () );
    dbg
      Fmt.(
        fun ppf ->
          vbox
            ~indent:2
            (fun ppf () ->
              string ppf "Testing Contract.eval:";
              sp ppf ();
              pf
                ppf
                "Contract-commands:@, %a"
                (box
                   (option (pair Basics.pp_value_tcontract Basics.pp_tcommand)))
                contract_cmd_option;
              cut ppf ();
              pf
                ppf
                "Operations: %d:@, %a."
                (List.length operations)
                (list (Basics.pp_operation Basics.pp_tvalue))
                operations;
              cut ppf ();
              pf
                ppf
                "Error: %a."
                (option ~none:(const string "none") Basics.Execution.pp_error)
                error_opt)
            ppf
            ());
    (contract_cmd_option, operations, error_opt)

  let do_test_of_contract_eval_ignore
      ?context ~scenario_state ~env contract message =
    let _, _, _ =
      do_test_of_contract_eval ?context ~scenario_state ~env contract message
    in
    ()

  let run () =
    let open EDSL_exp0 in
    let open Testing_helpers in
    let () =
      let scenario_state = Smart_ml.Basics.scenario_state () in
      let env =
        Typing.init_env
          ~contract_data_types:scenario_state.contract_data_types
          ()
      in
      (* Completely empty contract called on an inexistent entry-point *)
      let _, _, error_opt =
        do_test_of_contract_eval
          ~scenario_state
          ~env
          (contract [])
          (message "hello_no_entrypoint" Val.unit)
      in
      Alcotest.check
        testable_exec_error
        "entry-point not found"
        (Some (Basics.Execution.Exec_channel_not_found "hello_no_entrypoint"))
        error_opt
    in
    let hello_channel_sets_storage =
      channel
        "hello"
        ~t:Ty.string
        (Cmd.seq
           [Cmd.set_var "__storage__" (Expr.params ~line_no:(-1) Ty.string)])
    in
    Alcotest.check_raises
      "Duplicate channels"
      (Failure "Too many channels")
      (fun () ->
        let scenario_state = Smart_ml.Basics.scenario_state () in
        let env =
          Typing.init_env
            ~contract_data_types:scenario_state.contract_data_types
            ()
        in

        let contract =
          contract
            ~balance:10
            ~storage:(Val.string "world")
            [hello_channel_sets_storage; hello_channel_sets_storage]
        in
        let message = message "hello" (Val.string "foo") in
        do_test_of_contract_eval_ignore ~scenario_state ~env contract message);
    does_raise "wrong parameter type" ~p:any_failure ~f:(fun () ->
        let scenario_state = Smart_ml.Basics.scenario_state () in
        let env =
          Typing.init_env
            ~contract_data_types:scenario_state.contract_data_types
            ()
        in
        do_test_of_contract_eval_ignore
          ~scenario_state
          ~env
          (contract
             ~balance:10
             ~storage:(Val.string "world")
             [hello_channel_sets_storage])
          (message "hello" Val.unit));
    let () =
      let scenario_state = Smart_ml.Basics.scenario_state () in
      let env =
        Typing.init_env
          ~contract_data_types:scenario_state.contract_data_types
          ()
      in

      let new_value = Val.(string "foo") in
      let new_contract, operations, error_opt =
        do_test_of_contract_eval
          ~scenario_state
          ~env
          (contract
             ~balance:10
             ~storage:(Val.string "world")
             [hello_channel_sets_storage])
          (message "hello" new_value)
      in
      Alcotest.check testable_exec_error "no error" None error_opt;
      Alcotest.(check (list testable_toperation)) "no operation" [] operations;
      check_find "storage got changed" (fun found ->
          match new_contract with
          | None -> failwith "not here"
          | Some ({value_tcontract = {storage = Some storage; _}}, _) ->
              if Poly.equal storage new_value then found () else ()
          | Some ({value_tcontract = {storage = None; _}}, _) ->
              failwith "missing storage in contract")
    in
    let () =
      let scenario_state = Smart_ml.Basics.scenario_state () in
      let env =
        Typing.init_env
          ~contract_data_types:scenario_state.contract_data_types
          ()
      in
      let new_value =
        Val.(
          record
            ~t:Example.Send_channel.params_type
            [("address", address "KT1blabla"); ("amount", token 42)])
      in
      let _new_contract, operations, error_opt =
        try
          Solver.apply env;
          let c =
            contract
              ~balance:10
              ~storage:(Val.string "world")
              [Example.Send_channel.channel; hello_channel_sets_storage]
          in
          Solver.apply env;
          let m = message "send" new_value in
          Solver.apply env;
          do_test_of_contract_eval ~scenario_state ~env c m
        with
        | exn -> failwith (Smart_ml.Printer.exception_to_string false exn)
      in
      Alcotest.check testable_exec_error "no error" None error_opt;
      check_find "one operation" (fun found ->
          List.iter operations ~f:(function
              | Transfer
                  { arg = _
                  ; destination = _
                  ; amount = {v = Smart_ml.Basics.Literal (Mutez tok); _} }
                when Big_int.eq_big_int tok (big_int 42) ->
                  (* TODO: also check left side *)
                  found ()
              | _ -> ()))
    in
    (* Alcotest.(check bool) "JUST MAKING TEST FAIL" true false ; *)
    ()
end

(** Tests for {!Michelson_compiler.michelson_contract}. *)
module Michelson_compiler_test = struct
  let run () =
    let open EDSL_exp0 in
    let open Testing_helpers in
    let run_compiler msg contract =
      let res =
        Result.try_with (fun () -> Compiler.michelson_contract contract)
      in
      dbg
        Fmt.(
          fun ppf ->
            vbox
              (fun ppf () ->
                pf ppf "Test: %s" msg;
                cut ppf ();
                box
                  ~indent:2
                  (fun ppf () ->
                    pf ppf "# Compiler on:";
                    sp ppf ();
                    Basics.pp_value_tcontract ppf contract)
                  ppf
                  ();
                cut ppf ();
                box
                  ~indent:2
                  (fun ppf () ->
                    pf ppf "# Result:";
                    sp ppf ();
                    match res with
                    | Ok o -> pf ppf "OK:@ %a" Michelson.Michelson_contract.pp o
                    | Error e -> pf ppf "ERROR:@ %a" Exn.pp e)
                  ppf
                  ())
              ppf
              ());
      res
    in
    let some_string = "Some-string" in
    let res =
      contract ~storage:(Val.string some_string) [] |> run_compiler "first test"
    in
    check_find "empty contract" (fun found ->
        match res with
        | Ok
            { tstorage = {mt = MTstring; _}
            ; tparameter = {mt = MTunit; _}
            ; code = _
            ; lazy_entry_points = _
            ; storage = _ } ->
            found ()
        | _ -> ());
    let contract =
      contract ~storage:(Val.string some_string) [Example.Send_channel.channel]
      |> run_compiler "with send channel"
    in
    check_find "send-channel-basic" (fun found ->
        match contract with
        | Ok
            { tstorage = {mt = MTstring; _}
            ; tparameter = {mt = MTpair _; annot_singleton = Some "send"; _}
            ; code = _
            ; lazy_entry_points = _
            ; storage = _ } ->
            found ()
        | _ -> ());
    ()
end

module Command_dot_fix = struct
  let run () =
    let open Testing_helpers in
    let open EDSL_exp0 in
    let scenario_state = Smart_ml.Basics.scenario_state () in
    let env =
      Typing.init_env ~contract_data_types:scenario_state.contract_data_types ()
    in
    let homogenize_command c =
      Smart_ml.Basics.{(c : tcommand) with line_no = -1}
    in
    let try_fix cmd =
      let mangler_env = Mangler.init_env ~reducer:(fun ~line_no:_ x -> x) () in
      try
        Solver.apply env;
        ignore (Checker.check_command ~env [] cmd);
        Solver.apply env;
        let cmd = Smart_ml.Mangler.mangle_command mangler_env env cmd in
        Smart_ml.Closer.close_command cmd |> homogenize_command
      with
      | exn -> failwith (Smart_ml.Printer.exception_to_string false exn)
    in
    does_raise "Duplicate local variables" ~p:any_failure ~f:(fun () ->
        let cmd =
          try_fix
            (Cmd.seq
               [ Smart_ml.Command.defineLocal
                   ~line_no:(-1)
                   "aa"
                   Smart_ml.Expr.now
               ; Smart_ml.Command.defineLocal
                   ~line_no:(-1)
                   "aa"
                   Smart_ml.Expr.now ])
        in
        dbg
          Fmt.(
            fun ppf ->
              pf ppf "command result %a" Smart_ml.Basics.pp_tcommand cmd));
    let testable_command =
      Alcotest.testable Basics.pp_tcommand (fun c1 c2 ->
          let open Smart_ml.Basics in
          equal_command (erase_types_command c1) (erase_types_command c2))
    in
    let while_loop ~ok ~in_seq =
      let while_thing =
        if ok
        then
          Command.whileLoop
            ~line_no:1
            (Smart_ml.Expr.cst ~line_no:(-1) (Literal.bool false))
            (Command.seq ~line_no:(-1) [])
        else
          Command.whileLoop
            ~line_no:1
            (Smart_ml.Expr.cst ~line_no:(-1) (Literal.small_nat 2))
            (Command.seq ~line_no:(-1) [])
      in
      if in_seq then Cmd.(seq [while_thing]) else while_thing
    in
    Alcotest.check
      testable_command
      "OK while loop"
      (try_fix (while_loop ~ok:true ~in_seq:true))
      (while_loop ~ok:true ~in_seq:false |> homogenize_command);
    does_raise "Wrong while" ~p:any_failure ~f:(fun () ->
        try_fix (while_loop ~ok:false ~in_seq:true) |> ignore);
    ()

  let alcotest_test =
    ("command-dot-fix", [Alcotest.test_case "Command.fix" `Quick run])
end

module Interpreter_on_expressions = struct
  let test_interpret_expr_early () =
    let do_check name expr expected =
      let testable_value = Alcotest.testable Value.pp Value.equal in
      let env = Typing.init_env () in
      let _ = Smart_ml.Checker.check_expr ~env [] expr in
      Smart_ml.Solver.apply env;
      Alcotest.(check testable_value)
        (Fmt.str "interpret_expr_early:%s" name)
        (Interpreter.interpret_expr_external
           ~primitives:(module Fake_primitives : Primitives.Primitives)
           ~no_env:[`Text "Computing expression"; `Expr expr; `Line expr.el]
           ~scenario_state:(Smart_ml.Basics.scenario_state ())
           expr)
        expected
    in
    do_check
      "zero"
      Expr.(cst ~line_no:0 (Literal.bool false))
      Value.(bool false);
    let of_which which =
      match which with
      | `SB_string -> ("String", Literal.string, Value.string, Type.string)
      | `SB_bytes -> ("Bytes", Literal.bytes, Value.bytes, Type.bytes)
    in
    let check_slice ~which ~ofs ~len s =
      let ks, lit, value, t = of_which which in
      let res =
        match String.sub s ~pos:ofs ~len with
        | s -> Value.some (value s)
        | exception _ -> Value.none t
      in
      do_check
        (Fmt.str "slice%s:%S:%d--%d" ks s ofs len)
        Expr.(
          slice
            ~line_no:0
            ~offset:(cst ~line_no:0 @@ Literal.small_nat ofs)
            ~length:(cst ~line_no:0 @@ Literal.small_nat len)
            ~buffer:(cst ~line_no:0 @@ lit s))
        res
    in
    let both f =
      f ~which:`SB_string;
      f ~which:`SB_bytes
    in
    both @@ check_slice ~ofs:0 ~len:0 "";
    both @@ check_slice "" ~ofs:0 ~len:0;
    both @@ check_slice "a" ~ofs:0 ~len:1;
    both @@ check_slice "a" ~ofs:1 ~len:0;
    both @@ check_slice "abcdef" ~ofs:1 ~len:3;
    both @@ check_slice "abcdef" ~ofs:1 ~len:5;
    both @@ check_slice "abcdef" ~ofs:(-1) ~len:5;
    both @@ check_slice "abcdef" ~ofs:0 ~len:50;
    let check_concat ~which l =
      let ks, lit, value, _t = of_which which in
      let res = String.concat ~sep:"" l in
      let t =
        match which with
        | `SB_string -> Type.string
        | `SB_bytes -> Type.bytes
      in
      let e =
        Expr.(
          concat_list
            ~line_no:0
            (type_annotation
               ~line_no:0
               (build_list
                  ~line_no:0
                  ~elems:(List.map l ~f:(fun s -> cst ~line_no:0 @@ lit s)))
               (Type.list t)))
      in
      let env = Typing.init_env () in
      let _ = Smart_ml.Checker.check_expr ~env [] e in
      Solver.apply env;
      try do_check (Fmt.str "concat%s:%S" ks res) e (value res) with
      | exn -> failwith (Smart_ml.Printer.exception_to_string false exn)
    in
    both @@ check_concat [];
    both @@ check_concat ["a"];
    both @@ check_concat ["a"; "b"];
    both @@ check_concat (List.init 42 ~f:(Fmt.str "%d"));
    let check_size ~which s =
      let ks, lit, _, _ = of_which which in
      let res = String.length s in
      do_check
        (Fmt.str "size%s:%S" ks s)
        Expr.(size ~line_no:0 (cst ~line_no:0 @@ lit s))
        (Value.nat (Bigint.of_int res))
    in
    both @@ check_size "";
    both @@ check_size "a";
    both @@ check_size "aaaaaaaaaa";

    (* Alcotest.(check bool) "JUST MAKING TEST FAIL" true false ; *)
    ()

  let alcotest_test =
    ( "interpreter-on-expressions"
    , [ Alcotest.test_case
          "interpret_expr_early"
          `Quick
          test_interpret_expr_early ] )
end

let () =
  Misc.Dbg.on := true;
  Alcotest.run
    ~argv:Sys.(get_argv ())
    "smart-ml-library"
    [ ( "contract-eval"
      , [Alcotest.test_case "Contract.eval" `Quick Contract_eval_test.run] )
    ; ( "michompilation"
      , [ Alcotest.test_case
            "Michelson_compiler.michelson_contract"
            `Quick
            Michelson_compiler_test.run ] )
    ; Command_dot_fix.alcotest_test
    ; Interpreter_on_expressions.alcotest_test
    ; ( "fake-primitive-implementations"
      , [ Alcotest.test_case
            "fake-primitive-implementations-self-test"
            `Quick
            (fun () ->
              List.iter
                ~f:(function
                  | (Ok s | Error s) as res ->
                      Alcotest.(check bool)
                        (Fmt.str "fake-prim: %s" s)
                        ( match res with
                        | Ok _ -> true
                        | _ -> false )
                        true)
                (Primitives.test_primitives
                   (module Fake_primitives : Primitives.Primitives))) ] ) ]
