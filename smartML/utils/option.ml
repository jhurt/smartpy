(* Copyright 2019-2020 Smart Chain Arena LLC. *)
open Control

type 'a t = 'a option [@@deriving eq, show {with_path = false}]

let cata x f = function
  | None -> x
  | Some x -> f x

let some x = Some x

include Monad (struct
  type nonrec 'a t = 'a t

  let map f = function
    | Some x -> Some (f x)
    | None -> None

  let return = some

  let apply f x =
    match (f, x) with
    | Some f, Some x -> Some (f x)
    | _ -> None

  let bind x f = cata None f x
end)

let default d = cata d id

let is_none = function
  | None -> true
  | Some _ -> false

let is_some = function
  | None -> false
  | Some _ -> true

let of_some = function
  | Some x -> x
  | None -> failwith "of_some"
