(* Copyright 2019-2020 Smart Chain Arena LLC. *)

let id x = x

let pair x y = (x, y)

let flip f x y = f y x

let map_fst f (x, y) = (f x, y)

let map_snd f (x, y) = (x, f y)

module type TYPE = sig
  type t
end

module type TYPE1 = sig
  type 'a t
end

module type FUNCTOR_CORE = sig
  include TYPE1

  val map : ('a -> 'b) -> 'a t -> 'b t
end

module type APPLICATIVE_CORE = sig
  include FUNCTOR_CORE

  val return : 'a -> 'a t

  val apply : ('a -> 'b) t -> 'a t -> 'b t
end

module type MONAD_CORE = sig
  include APPLICATIVE_CORE

  val bind : 'a t -> ('a -> 'b t) -> 'b t
end

module type APPLICATIVE_SYNTAX = sig
  type 'a t

  val ( let+ ) : 'a t -> ('a -> 'b) -> 'b t

  val ( and+ ) : 'a t -> 'b t -> ('a * 'b) t
end

module type MONAD_SYNTAX = sig
  type 'a t

  val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t

  val ( and* ) : 'a t -> 'b t -> ('a * 'b) t
end

module type FUNCTOR = sig
  include FUNCTOR_CORE

  val ( <$> ) : ('a -> 'b) -> 'a t -> 'b t
end

module type APPLICATIVE = sig
  include APPLICATIVE_CORE

  include FUNCTOR with type 'a t := 'a t

  include APPLICATIVE_SYNTAX with type 'a t := 'a t

  module Applicative_syntax : APPLICATIVE_SYNTAX with type 'a t := 'a t

  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t

  val void : 'a t -> unit t

  val map2 : ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t

  val mapA_list : ('a -> 'b t) -> 'a list -> 'b list t

  val mapA_list_ : ('a -> unit t) -> 'a list -> unit t

  val mapA2_list : ('a -> 'b -> 'c t) -> 'a list -> 'b list -> 'c list t

  val mapA2_list_ : ('a -> 'b -> unit t) -> 'a list -> 'b list -> unit t

  val sequenceA_list : 'a t list -> 'a list t

  val sequenceA_list_ : unit t list -> unit t
end

module type MONAD = sig
  include MONAD_CORE

  include APPLICATIVE with type 'a t := 'a t

  include MONAD_SYNTAX with type 'a t := 'a t

  module Monad_syntax : MONAD_SYNTAX with type 'a t := 'a t

  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

  val ( >> ) : unit t -> 'a t -> 'a t

  val join : 'a t t -> 'a t

  val bind2 : ('a -> 'b -> 'c t) -> 'a t -> 'b t -> 'c t

  val unless : bool -> unit t -> unit t
end

module Functor (A : FUNCTOR_CORE) : FUNCTOR with type 'a t := 'a A.t = struct
  include A

  let ( <$> ) = map
end

module Applicative (A : APPLICATIVE_CORE) :
  APPLICATIVE with type 'a t := 'a A.t = struct
  include A
  include Functor (A)

  let ( <*> ) = apply

  let void x = map (fun _ -> ()) x

  let map2 f x y = f <$> x <*> y

  module Applicative_syntax = struct
    let ( let+ ) x = flip map x

    let ( and+ ) x = map2 pair x
  end

  include Applicative_syntax

  let cons x xs = x :: xs

  let rec mapA_list f = function
    | [] -> return []
    | x :: xs -> cons <$> f x <*> mapA_list f xs

  let rec mapA2_list f xs ys =
    match (xs, ys) with
    | [], [] -> return []
    | x :: xs, y :: ys -> cons <$> f x y <*> mapA2_list f xs ys
    | _ -> failwith "mapA2_list: differing lengths"

  let mapA_list_ f xs = void (mapA_list f xs)

  let mapA2_list_ f xs ys = void (mapA2_list f xs ys)

  let sequenceA_list xs = mapA_list id xs

  let sequenceA_list_ xs = void (sequenceA_list xs)
end

module Monad (M : MONAD_CORE) : MONAD with type 'a t := 'a M.t = struct
  include M
  include Applicative (M)

  let ( >>= ) = bind

  let ( >> ) x y = x >>= fun _ -> y

  let join x = x >>= id

  let bind2 f x y = x >>= fun x -> y >>= fun y -> f x y

  let unless c x = if c then return () else x

  module Monad_syntax = struct
    let ( let* ) = ( >>= )

    let ( and* ) x y = x >>= fun x -> y >>= fun y -> return (x, y)
  end

  include Monad_syntax
end

module Either = struct
  type ('a, 'b) t =
    | Left  of 'a
    | Right of 'b

  let of_left = function
    | Left x -> Some x
    | Right _ -> None

  let of_right = function
    | Left _ -> None
    | Right x -> Some x

  let is_left = function
    | Left _ -> true
    | Right _ -> false

  let is_right = function
    | Left _ -> false
    | Right _ -> true
end

module Result = struct
  type 'a t = ('a, string) result [@@deriving eq, show {with_path = false}, map]

  let error x = Error x

  let cata ok err = function
    | Ok r -> ok r
    | Error x -> err x

  include Monad (struct
    type nonrec 'a t = 'a t

    let map = map

    let return x = Ok x

    let apply f x =
      match (f, x) with
      | Ok f, Ok x -> Ok (f x)
      | (Error _ as e), _ -> e
      | _, (Error _ as e) -> e

    let bind x f = cata f error x
  end)
end

module State (T : TYPE) = struct
  type 'a t = T.t -> 'a * T.t

  include Monad (struct
    type nonrec 'a t = 'a t

    let map f x s = map_fst f (x s)

    let return x s = (x, s)

    let bind x f s =
      let x, s = x s in
      f x s

    let apply f x = bind f (fun f -> bind x (fun x -> return (f x)))
  end)

  let get s = (s, s)

  let set s _ = ((), s)

  let modify f = get >>= fun s -> set (f s)

  let run f s = f s

  let exec f s = fst (run f s)
end

module Reader (T : TYPE) = struct
  type 'a t = T.t -> 'a

  include Monad (struct
    type nonrec 'a t = 'a t

    let map f x s = f (x s)

    let return x _ = x

    let bind x f s =
      let x = x s in
      f x s

    let apply f x = bind f (fun f -> bind x (fun x -> return (f x)))
  end)

  let ask s = s

  let local f x s = x (f s)

  let run f s = f s
end

module Writer (T : TYPE) = struct
  type 'a t = T.t list * 'a

  include Monad (struct
    type nonrec 'a t = 'a t

    let map f (a, x) = (a, f x)

    let return x = ([], x)

    let bind (a, x) f =
      let a', x = f x in
      (a' @ a, x)

    let apply f x = bind f (fun f -> bind x (fun x -> return (f x)))
  end)

  let write a = ([a], ())

  let run (a, x) = (List.rev a, x)
end

let print_list xs ppf =
  Format.pp_print_list
    ~pp_sep:(fun ppf () -> Format.fprintf ppf "; ")
    (fun ppf x -> Format.fprintf ppf "%t" x)
    ppf
    xs

let pp_int ppf = Format.fprintf ppf "%d"

let pp_string ppf = Format.fprintf ppf "%s"

let curry f x y = f (x, y)

let uncurry f (x, y) = f x y

let eq3 x y z = x = y && y = z

let pp_no_breaks f () =
  let fs = Format.pp_get_formatter_out_functions f () in
  Format.pp_set_formatter_out_functions
    f
    {fs with out_newline = (fun () -> ()); out_indent = (fun _ -> ())}

type ('a, 'b) pair = 'a * 'b [@@deriving eq, show {with_path = false}, map]
