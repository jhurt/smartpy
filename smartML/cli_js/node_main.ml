(* Copyright 2019-2020 Smart Chain Arena LLC. *)
module Meta = struct
  include Build_information

  let _app_name =
    lazy
      ( try Caml.Sys.getenv "smartml_app_name" with
      | _ -> Caml.Filename.basename Caml.Sys.argv.(0) )

  let app_name () = Lazy.force _app_name

  let this_is_smartpy_dot_sh () = String.equal (app_name ()) "SmartPy.sh"

  let high_level_compile_cmd = "compile-smartpy-class"

  let high_level_simulate_cmd = "run-smartpy-test-in-interpreter"

  let high_level_sandbox_cmd = "run-smartpy-test-with-docker-sandbox"

  let smartpy_dot_sh_aliases =
    [(high_level_compile_cmd, "compile"); (high_level_simulate_cmd, "test")]
end

module Cmd_doc = struct
  open Base

  let docs_low_level = "PLUMBING COMMANDS"

  let docs_docker_sandbox = "DOCKER SANDBOX COMMANDS"

  let toplevel_man ~env =
    let open Cmdliner.Manpage in
    let intro = "Welcome to the command-line entry-point for SmartPy." in
    let environment =
      match env with
      | [] -> []
      | _ ->
          [ `S s_environment
          ; `P
              (Fmt.str
                 "This application uses on the following environment \
                  variable%s:"
                 ( match env with
                 | [_] -> ""
                 | _ -> "s" )) ]
          @ [ `Blocks
                (List.map env ~f:(fun (k, doc) ->
                     `I (Fmt.str "\\${$(b,%s)}" k, doc))) ]
    in
    [ `P intro
    ; `S s_commands
    ; `P "These are high-level user commands for every-day usage of SmartPy."
    ; `Blocks
        ( if Meta.this_is_smartpy_dot_sh ()
        then
          `P "There are short aliases for some commands:"
          :: List.map Meta.smartpy_dot_sh_aliases ~f:(fun (c, als) ->
                 `P (Fmt.str "* $(b,%S) is `%s`." als c))
        else [] ) ]
    @ [ `S docs_docker_sandbox
      ; `P
          (Fmt.str
             "%s manages a Flextesa sandbox using docker containers, the \
              following commands allow one to manage it."
             (Meta.app_name ())) ]
    @ environment
    @ [ `S docs_low_level
      ; `P
          "These commands are lower-level and perform smaller steps than the \
           main commands; they can be useful for instance to script more \
           complex builds."
      ; `S s_options ]
end

let cmd_self_test () =
  let open Cmdliner in
  let open Term in
  ( pure (fun () ->
        (* console_log "hello from node"; *)
        let open Js_of_ocaml in
        let dbgf fmt =
          Format.kasprintf
            (fun s ->
              Js_of_ocaml.Firebug.console##log
                (Fmt.kstr Js_of_ocaml.Js.string "DBG: %s" s)
              |> ignore)
            fmt
        in
        let sep () = Printf.eprintf "%s\n%!" (String.make 72 '=') in
        dbgf
          "Using firebug console: %s"
          (String.concat " -- " (Array.to_list Sys.argv));
        sep ();
        Printf.eprintf
          "$0 -> %s\n%!"
          ( if Sys.file_exists Sys.argv.(0)
          then "exists!"
          else "DOES NOT SEEM TO EXIST" );
        sep ();
        dbgf "Crypto-primitives test:";
        Smart_ml_js.Real_primitives.test (module Smart_ml_js.Real_primitives);
        sep ();
        Smart_ml_unix.Run_tezos_scenario.(
          dbg "Now: %s" Date.(now () |> to_rfc3339));
        sep ();
        Smart_ml_unix.Run_tezos_scenario.System.pp_to_file
          "/tmp/boooh"
          Fmt.(fun ppf -> pf ppf "world :thumbs_up:\n");
        dbgf "Hello?...";
        let ret = Sys.command "cat /tmp/boooh >&2" in
        dbgf "cat >&2 returned %d" ret;
        let ret = Sys.command "cat /tmp/boooh" in
        dbgf "cat returned %d" ret;
        sep ();
        let tmp2 = Filename.temp_file "node_self_test" ".md" in
        Smart_ml_unix.Run_tezos_scenario.System.pp_to_file
          tmp2
          Fmt.(fun ppf -> pf ppf "more\nstuff\n");
        let i = open_in tmp2 in
        let rec f acc =
          try f (input_char i :: acc) with
          | _ -> acc
        in
        dbgf
          "tmp2:@ %s@ [%a]"
          tmp2
          Fmt.(box ~indent:2 (list ~sep:comma (fun ppf -> pf ppf "%C")))
          (f []);
        close_in i;

        sep ();
        ignore
          (Js.Unsafe.eval_string
             (Printf.sprintf "console.log(\"console.log(but totally unsafe)\")")))
    $ pure ()
  , info
      "self-test"
      ~docs:Cmd_doc.docs_low_level
      ~doc:"Perform a quick portability test." )

open! Base

let dbg = Smart_ml_unix.Run_tezos_scenario.dbg

module Tezos_scenario_node_unix =
  Smart_ml_unix.Run_tezos_scenario.Make (Smart_ml_js.Real_primitives)

module Path = struct
  include Caml.Filename

  let ( // ) = concat
end

module Install_path = struct
  type t = Is_at of string

  let _varname = "smartpy_install_path"

  let default =
    Is_at
      Caml.(
        try Sys.getenv _varname with
        | _ -> Sys.getenv "HOME" ^ "/.smartpy-installation")

  let wrap f o = f (o#install_path : t)

  let smartpycli_dot_py s =
    wrap (fun (Is_at p) -> Path.concat p "smartpy_cli.py") s

  let pythonpath ?extra s =
    wrap
      (fun (Is_at p) ->
        p
        ^ Option.value_map ~default:"" ~f:(Fmt.str ":%s") extra
        ^ Option.value_map
            ~default:""
            ~f:(Fmt.str ":%s")
            (Caml.Sys.getenv_opt "PYTHONPATH"))
      s

  let doc = (_varname, "The installation path of SmartPy.")
end

module Output_path = struct
  let preprocessed_python p ~base = Path.concat p (Fmt.str "%s_pure.py" base)

  let scenario p ~base = Path.concat p (Fmt.str "%s_scenario.sc" base)

  let smartml_smlse s ~base = Path.concat s (Fmt.str "%s.smlse" base)

  let storage_init_tz s ~base =
    Path.concat s (Fmt.str "%s_storage_init.tz" base)

  let types_sp s ~base = Path.concat s (Fmt.str "%s_types.sp" base)

  let compiled_code_tz s ~base = Path.concat s (Fmt.str "%s_compiled.tz" base)

  let compiled_code_json s ~base =
    Path.concat s (Fmt.str "%s_compiled.json" base)

  let smartml_scenario_output s = Path.(s // "scenario-interpreter-log.txt")

  let interpreted_scenario s ~base:_ = Path.(s // "interpreted-scenario")

  let sandbox_scenario_root s ~base:_ = Path.(s // "sandbox-scenario")
end

module State = struct
  let default () =
    object
      method install_path = Install_path.default
    end
end

module Sys_io = struct
  include Smart_ml_unix.Run_tezos_scenario.System

  exception
    Error of
      { command : string
      ; return_value : int }

  let run s =
    dbg "Sys_io.run %S" s;
    match Caml.Sys.command s with
    | 0 -> ()
    | n -> raise (Error {command = s; return_value = n})

  let get_output s =
    let out = Path.temp_file "cmd" "out" in
    let err = Path.temp_file "cmd" "err" in
    Fmt.kstr run " { %s ; } > %s 2> %s" s (Path.quote out) (Path.quote err);
    (read_file out, read_file err)

  let make_exec ?(env = []) l =
    ( List.map env ~f:(fun (k, v) -> Fmt.str "%s=%s " k (Path.quote v))
    |> String.concat ~sep:"" )
    ^ (List.map l ~f:Caml.Filename.quote |> String.concat ~sep:" ")

  let exec ?env l = run (make_exec ?env l)

  let mkdir_p s = exec ["mkdir"; "-p"; s]
end

module Console = struct
  let say f = Fmt.epr "@[<2>[[%s:]]@ %a@]\n%!" (Meta.app_name ()) f ()
end

module Run_python = struct
  let run ?class_call ?extra_pythonpath state ~contract ~output_path =
    let base = Path.(basename contract |> chop_extension) in
    let extras =
      Option.value_map class_call ~default:[] ~f:(fun s ->
          [ "--class_call"
          ; s
          ; "--sexprfile"
          ; Output_path.smartml_smlse output_path ~base ])
    in
    Sys_io.mkdir_p output_path;
    Sys_io.exec
      ~env:
        [("PYTHONPATH", Install_path.pythonpath ?extra:extra_pythonpath state)]
      ( [ "python3"
        ; Install_path.smartpycli_dot_py state
        ; contract
        ; "--scenario"
        ; Output_path.scenario output_path ~base
        ; "--pyadaptedfile"
        ; Output_path.preprocessed_python output_path ~base ]
      @ extras )

  let cmd_make_all () =
    let open Cmdliner in
    let open Term in
    let ex_addr = "tz1hKWTx3hoVAZMAa989fZ7Qa2LujtP9kJed" in
    ( pure (fun contract output_path class_call extra_pythonpath () ->
          dbg "Run_python.cmd_make_all %s %s" contract output_path;
          run
            (State.default ())
            ?class_call
            ~contract
            ~output_path
            ?extra_pythonpath)
      $ Arg.(
          required
            (pos
               0
               (some string)
               None
               (info [] ~docv:"INPUT_PATH.py" ~doc:"Input file")))
      $ Arg.(
          required
            (pos
               1
               (some string)
               None
               (info [] ~docv:"OUTPUT_PATH" ~doc:"Output directory.")))
      $ Arg.(
          let doc =
            Fmt.str
              "Output a smartML contract constructed by $(docv), e.g. \
               'My_contract(42,%S)'"
              ex_addr
          in
          value
            (opt
               (some string)
               None
               (info ["class-call"; "C"] ~docv:"PYTHON-EXPR" ~doc)))
      $ Arg.(
          value
            (opt
               (some string)
               None
               (info
                  ["python-path"]
                  ~docs:"COLON-SEPARATED"
                  ~doc:"Add extra paths to the `PYTHONPATH` variable.")))
      $ pure ()
    , info
        "run-smartpy-script"
        ~docs:Cmd_doc.docs_low_level
        ~doc:"Run a SMartPy script (perform SmartML code-generation)." )
end

module Docker_sandbox = struct
  let _env_vars = ref []

  let env_var suffix ~default ~doc =
    let n = "smartpy_docker_sandbox_" ^ suffix in
    _env_vars := (n, doc) :: !_env_vars;
    let value =
      lazy
        (let v = Caml.Sys.getenv_opt n |> Option.value ~default in
         v)
    in
    fun () -> Lazy.force value

  let container_name =
    env_var
      "name"
      ~default:"smartpy-sandbox"
      ~doc:"The name of the (detached) container."

  let image =
    env_var
      "image"
      ~default:"registry.gitlab.com/tezos/flextesa:07fdc9f2-run"
      ~doc:"The docker image to use."

  let port =
    env_var
      "port"
      ~default:"15348"
      ~doc:"The RPC-port to make the node listen on."

  let extra_verbose () =
    String.equal
      "true"
      (env_var
         "verbose"
         ~default:"false"
         ~doc:"Make interactions more verbose."
         ())

  let docker_run ?(detach = false) ?(entry_point = "flextesa") ?name () =
    ["docker"; "run"; "--rm"]
    @ Option.value_map ~f:(fun n -> ["--name"; n]) ~default:[] name
    @ (if detach then ["--detach"] else [])
    @ [ "--network"
      ; "host"
      ; "--entrypoint"
      ; entry_point
      ; "-v"
      ; Fmt.str "/:/work" (* (Caml.Sys.getcwd ()) *)
      ; image () ]

  let embed_path p =
    let open Path in
    "/work" // if is_relative p then Caml.Sys.getcwd () // p else p

  (** Made with [flextesa key smartpy-funder]. *)
  let funder_name = "smartpy-funder"

  let funder_pk = "edpkv1f4oJoRx1sP51gB3BbH5DXjntT8AgCfy8cPd6BnBxGLLQDSkH"

  let funder_pkh = "tz1fABh66UfN5vm653bha1Qc8FZ9gaGEUYy3"

  let funder_sk =
    "unencrypted:edsk3YjiNjNRuxuwxjeAeEcfxNQbxmHooKQ8YEZ9Na9QD262PqGXub"

  let local_tezos_client_command ?(with_port = true) args =
    [ "docker"
    ; "exec"
    ; "-e"
    ; "TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y"
    ; container_name ()
    ; "tezos-client" ]
    @ (if with_port then ["-P"; port ()] else [])
    @ args

  let get_container_status () =
    try
      Some
        Sys_io.(
          get_output
            (make_exec
               [ "docker"
               ; "inspect"
               ; "--format"
               ; "{{.Name}}, {{.State.Status}}, {{.State.StartedAt}}"
               ; container_name () ]))
    with
    | _ -> None

  let get_bootstrapped () =
    try
      Some
        Sys_io.(
          get_output (make_exec (local_tezos_client_command ["bootstrapped"])))
    with
    | _ -> None

  let get_level_1 () =
    try
      Some
        Sys_io.(
          get_output
            (make_exec
               (local_tezos_client_command
                  ["rpc"; "get"; "/chains/main/blocks/1/header"])))
    with
    | _ -> None

  let start () =
    let open Sys_io in
    exec
      ( docker_run
          ~detach:true
          ~entry_point:"flextesa"
          ~name:(container_name ())
          ()
      @ [ "mini"
        ; "--size=1"
        ; "--base-port"
        ; port ()
        ; "--number-of-boot=2"
        ; "--add-boot"
        ; Fmt.str
            "%s,%s,%s,%s@20_000_000_000_000"
            funder_name
            funder_pk
            funder_pkh
            funder_sk
        ; "--no-baking"
        ; "--until-level=2_000_000" ] )

  let kill () = Sys_io.exec ["docker"; "kill"; container_name ()]

  let ensure () =
    match get_level_1 () with
    | Some _ ->
        Console.say Fmt.(const string "Docker-sandbox already up.");
        ()
    | None ->
        Console.say Fmt.(const string "Starting the Docker-sandbox ...");
        start ();
        let attempts = 30 in
        let rec wait n =
          Console.say
            Fmt.(
              fun ppf () ->
                pf ppf "Waiting for sandbox to bootstrap ... [%d/%d]" n attempts);
          match get_level_1 () with
          | Some _ -> ()
          | None when n >= attempts ->
              Console.say
                Fmt.(
                  box
                    ~indent:2
                    (kstr
                       (const text)
                       "ERROR: The docker-sandbox did not manage to start \
                        after %d attempts."
                       attempts));
              kill ();
              failwith "Failed to ensure the presence of the docker-sandbox."
          | None ->
              (* Unix.sleep seems absent in node.js *)
              if extra_verbose ()
              then
                Console.say
                  Fmt.(
                    vbox
                      ~indent:2
                      ( const text "Container-status:"
                      ++ cut
                      ++ const
                           (pair string (cut ++ string))
                           (kstr
                              Sys_io.get_output
                              "docker ps | grep %s || echo FAILED"
                              (container_name ())) )
                    ++ vbox
                         ~indent:2
                         ( const text "Container-logs:"
                         ++ cut
                         ++ const
                              (pair string (cut ++ string))
                              (kstr
                                 Sys_io.get_output
                                 "docker logs %s || echo FAILED"
                                 (container_name ())) )
                    ++ vbox
                         ~indent:2
                         ( const text "Level 1:"
                         ++ cut
                         ++ const
                              (pair string (cut ++ string))
                              (kstr
                                 Sys_io.get_output
                                 "curl \
                                  https://localhost:%s/chains/main/blocks/1/header \
                                  || echo FAILED"
                                 (port ())) ));
              Sys_io.exec ["sleep"; "1"];
              wait (n + 1)
        in
        wait 1;
        ()

  let cmd_start () =
    let open Cmdliner in
    let open Term in
    ( pure (fun () ->
          start ();
          ())
      $ pure ()
    , info
        "start-docker-sandbox"
        ~docs:Cmd_doc.docs_docker_sandbox
        ~doc:"Start a Tezos sandbox to run SmartPy scenarios." )

  let cmd_kill () =
    let open Cmdliner in
    let open Term in
    ( pure (fun () -> kill ()) $ pure ()
    , info
        "kill-docker-sandbox"
        ~docs:Cmd_doc.docs_docker_sandbox
        ~doc:"Destroy the Tezos sandbox." )

  let cmd_tezos_client () =
    let open Cmdliner in
    let open Term in
    ( ( pure (fun args ->
            Sys_io.(run (make_exec (local_tezos_client_command args))))
      $ Arg.(
          value
            (pos_all
               string
               []
               (info
                  []
                  ~docv:"ARGS"
                  ~doc:
                    "Arguments passed to tezos-client (you may need '--' to \
                     pass options)."))) )
    , info
        "tezos-client-in-docker-sandbox"
        ~docs:Cmd_doc.docs_docker_sandbox
        ~doc:"Run a tezos-client command in the docker sandbox." )

  let cmd_status () =
    let open Cmdliner in
    let open Term in
    ( pure (fun () ->
          let psout, pserr =
            match get_container_status () with
            | Some (out, err) -> (out, err)
            | None ->
                Console.say
                  Fmt.(
                    kstr
                      (const text)
                      "The sandbox appears to be down (container `%s`)."
                      (container_name ()));
                Stdlib.exit 3
          in
          let bootout, booterr =
            Sys_io.(
              get_output
                (make_exec (local_tezos_client_command ["bootstrapped"])))
          in
          Console.say
            Fmt.(
              let cli_output out =
                const text (String.tr ~target:'\n' ~replacement:' ' out)
              in
              vbox
                ~indent:2
                ( const text "Docker-Sandbox Status:"
                ++ cut
                ++ box
                     ~indent:2
                     ( const string "* Container:"
                     ++ sp
                     ++ cli_output (psout ^ pserr) )
                ++ cut
                ++ box
                     ~indent:2
                     ( const string "* Node status:"
                     ++ sp
                     ++ cli_output (bootout ^ booterr) ) )))
      $ pure ()
    , info
        "status-of-docker-sandbox"
        ~docs:Cmd_doc.docs_docker_sandbox
        ~doc:"Start a Tezos sandbox to run SmartPy scenarios." )

  let cmds () = [cmd_start (); cmd_status (); cmd_kill (); cmd_tezos_client ()]
end

module Smartml = struct
  open Smart_ml

  let primitives = (module Smart_ml_js.Real_primitives : Primitives.Primitives)

  module Smlse = struct
    let import_file path =
      let sexp_string = Sys_io.read_file path in
      let scenario_state = Basics.scenario_state () in
      let contract =
        Import.import_contract
          ~do_fix:true
          ~primitives
          (Typing.init_env ())
          (Parsexp.Single.parse_string_exn sexp_string)
          ~scenario_state
      in
      let conv name x =
        Interpreter.interpret_expr_external
          ~primitives
          ~no_env:[`Text ("Compute " ^ name)]
          ~scenario_state
          x
      in
      Basics.to_value_tcontract conv contract
  end

  module Scenario = struct
    let run_scenario scenario ~output_path =
      let scenario_state = Basics.scenario_state () in
      let typing_env =
        Typing.init_env
          ~contract_data_types:scenario_state.contract_data_types
          ()
      in
      let open Smartml_scenario in
      let scenario =
        Scenario.load_from_string
          ~primitives
          ~scenario_state
          ~typing_env
          scenario
      in
      let res, errors =
        run ~primitives ~scenario_state ~typing_env scenario (Some output_path)
      in
      let res_file = Output_path.smartml_scenario_output output_path in
      Sys_io.pp_to_file res_file Fmt.(fun ppf -> string ppf res);
      errors

    let run_scenarios path ~output_path =
      let scenario = Yojson.Basic.from_file path in
      let scenarios = Yojson.Basic.Util.to_list scenario in
      List.concat
        (List.map
           ~f:(fun scenario ->
             let output_path =
               Path.(
                 ( output_path
                 // Yojson.Basic.Util.to_string
                      (Yojson.Basic.Util.member "shortname" scenario) )
                 ^ "_interpreted")
             in
             Sys_io.mkdir_p output_path;
             run_scenario
               ~output_path
               (Yojson.Basic.Util.member "scenario" scenario))
           scenarios)

    let pp_interpreter_result ~output_path ~errors : unit Fmt.t =
      let res_file = Output_path.smartml_scenario_output output_path in
      let log_fmt =
        Fmt.(
          parens
            ( const text "see also log:"
            ++ sp
            ++ const (quote ~mark:"`" string) res_file ))
      in
      match errors with
      | [] ->
          Fmt.(
            box
              ~indent:2
              (const text "Scenario-interpretation: All Good" ++ sp ++ log_fmt))
      | more ->
          Fmt.(
            hvbox
              ~indent:2
              ( box
                  ~indent:2
                  ( const text "Scenario-interpretation: There were ERRORS :("
                  ++ sp
                  ++ log_fmt
                  ++ const string ":" )
              ++ cut
              ++ const
                   (list
                      ~sep:cut
                      (box (fun ppf x ->
                           text ppf (Smart_ml.Printer.pp_smart_except false x))))
                   (List.concat more) ))

    let cmd_interpret_scenario () =
      let open Cmdliner in
      let open Term in
      ( pure (fun contract output_path () ->
            dbg "Smartml_smlse.interpret %s %s" contract output_path;
            let errors = run_scenarios contract ~output_path in
            Console.say (pp_interpreter_result ~output_path ~errors);
            if Poly.(errors <> []) then Stdlib.exit 1)
        $ Arg.(
            required
              (pos
                 0
                 (some string)
                 None
                 (info
                    []
                    ~docv:"INPUT_PATH.sc"
                    ~doc:"Input SmartML scenario (JSON).")))
        $ Arg.(
            required
              (pos
                 1
                 (some string)
                 None
                 (info [] ~docv:"OUTPUT_PATH" ~doc:"Output directory.")))
        $ pure ()
      , info
          "run-smartml-scenario-in-interpreter"
          ~docs:Cmd_doc.docs_low_level
          ~doc:"Run a SmartML scenario with the interpreter." )
  end
end

module Compile = struct
  open Smart_ml

  let of_smartml _state smartml_smlse ~output_path =
    let smartml = Smartml.Smlse.import_file smartml_smlse in
    let michocode = Compiler.michelson_contract smartml in
    let michocode_tz = Michelson.Michelson_contract.to_string michocode in
    let michocode_micheline =
      Michelson.Michelson_contract.to_micheline michocode
    in
    let types =
      Printf.sprintf
        "Storage: %s\nParams: %s"
        (Printer.type_to_string smartml.value_tcontract.tstorage)
        (Printer.type_to_string smartml.value_tcontract.tparameter)
    in
    let base = Path.(basename smartml_smlse |> chop_extension) in
    Sys_io.pp_to_file
      (Output_path.storage_init_tz output_path ~base)
      Fmt.(
        fun ppf ->
          string
            ppf
            (Base.Option.value_map
               michocode.storage
               ~default:"missing storage"
               ~f:Michelson.string_of_instr_mliteral));
    Sys_io.pp_to_file
      (Output_path.types_sp output_path ~base)
      Fmt.(fun ppf -> string ppf types);
    Sys_io.pp_to_file
      (Output_path.compiled_code_tz output_path ~base)
      Fmt.(fun ppf -> string ppf michocode_tz);
    Sys_io.pp_to_file
      (Output_path.compiled_code_json output_path ~base)
      (fun ppf -> (Micheline.pp_as_json ()) ppf michocode_micheline);
    ()

  let cmd_of_smartml () =
    let open Cmdliner in
    let open Term in
    ( pure (fun contract output_path () ->
          dbg "Compile.of_smartml %s %s" contract output_path;
          of_smartml (State.default ()) contract ~output_path)
      $ Arg.(
          required
            (pos
               0
               (some string)
               None
               (info
                  []
                  ~docv:"INPUT_PATH.smlse"
                  ~doc:"Input SmartML file (S-Expression).")))
      $ Arg.(
          required
            (pos
               1
               (some string)
               None
               (info [] ~docv:"OUTPUT_PATH" ~doc:"Output directory.")))
      $ pure ()
    , info
        "compile-smartml-contract"
        ~docs:Cmd_doc.docs_low_level
        ~doc:"Compile a SmartML contract to Michelson." )

  let pp_output_of_compilation ~base ~output_path : unit Fmt.t =
    let open Fmt in
    const
      (vbox
         ~indent:2
         (prefix
            (any "Compilation Result:@,")
            (list
               ~sep:cut
               (box
                  ~indent:2
                  (pair
                     ~sep:(any ":@ ")
                     (prefix (any "* ") string)
                     (fmt "`%s`"))))))
      [ ("Concrete micheline", Output_path.compiled_code_tz output_path ~base)
      ; ("JSON micheline", Output_path.compiled_code_json output_path ~base)
      ; ( "Concrete storage initialization"
        , Output_path.storage_init_tz output_path ~base ) ]

  let cmd_of_smartpy () =
    let open Cmdliner in
    let open Term in
    ( pure (fun contract class_call output_path () ->
          let base = Path.(basename contract |> chop_extension) in
          dbg "Compile.of_smartpy %s %s %s" contract class_call output_path;
          let state = State.default () in
          try
            Run_python.run state ~class_call ~contract ~output_path;
            let smlse = Output_path.smartml_smlse output_path ~base in
            let base = Path.(basename smlse |> chop_extension) in
            of_smartml state smlse ~output_path;
            Console.say (pp_output_of_compilation ~base ~output_path)
          with
          | Smart_ml.Basics.SmartExcept l ->
              Console.say
                Fmt.(
                  const string "Errors:"
                  ++ sp
                  ++ const
                       text
                       ( List.map l ~f:(Smart_ml.Printer.pp_smart_except false)
                       |> String.concat ~sep:", " )))
      $ Arg.(
          required
            (pos
               0
               (some string)
               None
               (info
                  []
                  ~docv:"INPUT_PATH.py"
                  ~doc:"Input SmartPy file (contract builder).")))
      $ Arg.(
          required
            (pos
               1
               (some string)
               None
               (info [] ~docv:"MyContract(42)" ~doc:"Class call.")))
      $ Arg.(
          required
            (pos
               2
               (some string)
               None
               (info [] ~docv:"OUTPUT_PATH" ~doc:"Output directory.")))
      $ pure ()
    , info
        "compile-smartpy-class"
        ~docs:Manpage.s_commands
        ~doc:"Compile a SmartPy contract to Michelson." )
end

let cmd_interpret_smartpy_scenario () =
  let open Cmdliner in
  let open Term in
  ( pure (fun contract output_path () ->
        let base = Path.(basename contract |> chop_extension) in
        dbg "cmd_run_smartpy_scenario  %s %s" contract output_path;
        let state = State.default () in
        let errors =
          try
            Run_python.run state ~contract ~output_path;
            let sc = Output_path.scenario output_path ~base in
            Smartml.Scenario.run_scenarios sc ~output_path
          with
          | Smart_ml.Basics.SmartExcept l -> [l]
        in
        let interp_result =
          Smartml.Scenario.pp_interpreter_result ~output_path ~errors
        in
        Console.say
          Fmt.(
            vbox
              ~indent:2
              ( kstr (const string) "## SmartPy Scenario `%s`" base
              ++ cut
              ++ interp_result ));
        if Poly.(errors <> []) then Stdlib.exit 1 else ())
    $ Arg.(
        required
          (pos
             0
             (some string)
             None
             (info
                []
                ~docv:"INPUT_PATH.py"
                ~doc:"Input SmartPy file (contract builder).")))
    $ Arg.(
        required
          (pos
             1
             (some string)
             None
             (info [] ~docv:"OUTPUT_PATH" ~doc:"Output directory.")))
    $ pure ()
  , info
      "run-smartpy-test-in-interpreter"
      ~doc:"Run a SmartPy scenario with the interpreter." )

let cmd_run_smartpy_scenario_in_docker_sandbox () =
  let open Cmdliner in
  let open Term in
  ( pure (fun keep_sandbox contract output_path () ->
        let base = Path.(basename contract |> chop_extension) in
        dbg "cmd_run_smartpy_scenario  %s %s" contract output_path;
        let state = State.default () in
        Run_python.run state ~contract ~output_path;
        let _cc_output = Compile.pp_output_of_compilation ~base ~output_path in
        let sc = Output_path.scenario output_path ~base in
        let module Scn = Tezos_scenario_node_unix.Smartml_scenario in
        let scenarios = Scn.of_json_file sc in
        let module TC = Tezos_scenario_node_unix.This_client in
        let module Conn = Tezos_scenario_node_unix.This_client.Connection in
        let root_path = Output_path.sandbox_scenario_root output_path ~base in
        Sys_io.mkdir_p root_path;
        let client_command =
          Docker_sandbox.local_tezos_client_command ~with_port:false []
          |> String.concat ~sep:" "
        in
        dbg "Client_Command: %s" client_command;
        let client =
          TC.state
            ~funder:Docker_sandbox.funder_sk
            ~client_command
            ~connection:(Conn.node (Docker_sandbox.port () |> Int.of_string))
            ~confirmation:`Bake
            ~translate_paths:Docker_sandbox.embed_path
            root_path
        in
        Docker_sandbox.ensure ();
        let res =
          Tezos_scenario_node_unix.run
            ~scenarios
            ~client
            ()
            ~advancement:
              Fmt.(
                fun s ->
                  epr "%a\n%!" (box ~indent:4 (const string "  * " ++ string)) s)
        in
        if not keep_sandbox
        then (
          Console.say Fmt.(const string "Killing Sandbox ...");
          Docker_sandbox.kill () );
        Console.say
          Fmt.(
            vbox
              ~indent:2
              ( box
                  (kstr
                     (const text)
                     "End of scenario: %s"
                     (if res#success then "Success \\o/" else "FAILED :("))
              ++ cut
              ++ const
                   (list
                      ~sep:cut
                      (pair
                         (const string "* " ++ string)
                         (fun ppf -> pf ppf " `%s`.")))
                   [ ("Results summary", res#results_dot_txt)
                   ; ("Result with extra details", res#results_full_dot_txt)
                   ; ("Results JSON", res#results_dot_json) ] ));
        if res#success then Stdlib.exit 0 else Stdlib.exit 4)
    $ Arg.(
        value (flag (info ["keep-sandbox-up"] ~doc:"Do not kill the sandbox.")))
    $ Arg.(
        required
          (pos
             0
             (some string)
             None
             (info
                []
                ~docv:"INPUT_PATH.py"
                ~doc:"Input SmartPy file (contract builder).")))
    $ Arg.(
        required
          (pos
             1
             (some string)
             None
             (info [] ~docv:"OUTPUT_PATH" ~doc:"Output directory.")))
    $ pure ()
  , info
      "run-smartpy-test-with-docker-sandbox"
      ~doc:"Run a SmartPy scenario with tezos-client against a docker sandbox."
  )

module Local_sandbox = struct
  type t =
    { root : string
    ; port : int
    ; mini_net_cmd : string }

  let make ?(mini_net_cmd = "tezos-sandbox mini") ?(port = 20_102) root =
    {root; port; mini_net_cmd}

  let pid_file t = Path.(t.root // "pid")

  let log_file t = Path.(t.root // "log.txt")

  let funder_name = Docker_sandbox.funder_name

  let funder_pk = Docker_sandbox.funder_pk

  let funder_pkh = Docker_sandbox.funder_pkh

  let funder_sk = Docker_sandbox.funder_sk

  let start t =
    let cmd =
      Fmt.str
        "%s --size 1 --base-port %d --number-of-boot 2 --no-baking \
         --until-level 2_000_000 --add-boot %s,%s,%s,%s@20_000_000_000_000 2> \
         %s & echo $! > %s"
        t.mini_net_cmd
        t.port
        funder_name
        funder_pk
        funder_pkh
        funder_sk
        (log_file t)
        (pid_file t)
    in
    Sys_io.run cmd

  let status t =
    try
      let _, _ = Fmt.kstr Sys_io.get_output "ps $(cat %s)" (pid_file t) in
      let _, _ =
        Sys_io.(
          get_output
            (make_exec
               [ "tezos-client"
               ; "-P"
               ; Int.to_string t.port
               ; "rpc"
               ; "get"
               ; "/chains/main/blocks/1/header" ]))
      in
      true
    with
    | _ -> false

  let kill t =
    let ko, ke =
      Sys_io.(
        Fmt.kstr
          get_output
          "ps \"$(cat %s)\" && kill \"$(cat %s)\""
          (pid_file t)
          (pid_file t))
    in
    dbg "Killing local sandbox:\nout: %S\nerr: %S" ko ke;
    ()

  let ensure t =
    match status t with
    | true ->
        Console.say Fmt.(const string "Local-sandbox already up.");
        ()
    | false ->
        Console.say Fmt.(const string "Starting the Local-sandbox ...");
        Sys_io.mkdir_p t.root;
        let _ = start t in
        let attempts = 15 in
        let rec wait n =
          Console.say
            Fmt.(
              fun ppf () ->
                pf ppf "Waiting for sandbox to bootstrap ... [%d/%d]" n attempts);
          match status t with
          | true -> ()
          | false when n >= attempts ->
              Console.say
                Fmt.(
                  box
                    ~indent:2
                    (kstr
                       (const text)
                       "ERROR: The local-sandbox did not manage to start after \
                        %d attempts."
                       attempts));
              ( try kill t with
              | _ -> () );
              failwith "Failed to ensure the presence of the local-sandbox."
          | false ->
              (* Unix.sleep seems absent in node.js *)
              Sys_io.exec ["sleep"; "1"];
              wait (n + 1)
        in
        wait 1;
        ()
end

let cmd_run_scenario_local_sandbox () =
  let open Cmdliner in
  let open Term in
  ( pure (fun sc output_path () ->
        let module Scn = Tezos_scenario_node_unix.Smartml_scenario in
        let scenarios = Scn.of_json_file sc in
        let base = Path.(basename sc |> chop_extension) in
        let module TC = Tezos_scenario_node_unix.This_client in
        let module Conn = Tezos_scenario_node_unix.This_client.Connection in
        let root_path = Output_path.sandbox_scenario_root output_path ~base in
        Sys_io.mkdir_p root_path;
        let open Local_sandbox in
        let local_sandbox =
          Local_sandbox.make Path.(output_path // "sandbox-root")
        in
        let client =
          TC.state
            ~funder:Local_sandbox.funder_sk
            ~connection:(Conn.node local_sandbox.port)
            ~confirmation:`Bake
            root_path
        in
        Local_sandbox.ensure local_sandbox;
        let res =
          Tezos_scenario_node_unix.run
            ~scenarios
            ~client
            ()
            ~advancement:
              Fmt.(
                fun s ->
                  epr "%a\n%!" (box ~indent:4 (const string "  * " ++ string)) s)
        in
        Console.say Fmt.(const string "Killing Sandbox ...");
        Local_sandbox.kill local_sandbox;
        Console.say
          Fmt.(
            vbox
              ~indent:2
              ( box
                  (kstr
                     (const text)
                     "End of scenario: %s"
                     (if res#success then "Success \\o/" else "FAILED :("))
              ++ cut
              ++ const
                   (list
                      ~sep:cut
                      (pair
                         (const string "* " ++ string)
                         (fun ppf -> pf ppf " `%s`.")))
                   [ ("Results summary", res#results_dot_txt)
                   ; ("Result with extra details", res#results_full_dot_txt)
                   ; ("Results JSON", res#results_dot_json) ] ));
        if res#success then Stdlib.exit 0 else Stdlib.exit 4)
    $ Arg.(
        required
          (pos
             0
             (some string)
             None
             (info [] ~docv:"INPUT_PATH.sc" ~doc:"Input SmartML scenario file.")))
    $ Arg.(
        required
          (pos
             1
             (some string)
             None
             (info [] ~docv:"OUTPUT_PATH" ~doc:"Output directory.")))
    $ pure ()
  , info
      "run-smartml-scenario-with-local-sandbox"
      ~docs:Cmd_doc.docs_low_level
      ~doc:"Run a SmartML scenario with a local sandbox setup." )

let () =
  let open Cmdliner in
  let version =
    Fmt.str
      "Version %S, %s, %s."
      Meta.version
      Meta.git_commit_desc
      Meta.configuration_date
  in
  let help =
    Term.
      ( ret (pure (`Help (`Auto, None)))
      , info
          (Meta.app_name ())
          ~version
          ~man:
            (let env = Install_path.doc :: !Docker_sandbox._env_vars in
             Cmd_doc.toplevel_man ~env) )
  in
  Term.exit
    (Term.eval_choice
       (help : unit Term.t * _)
       ( [ Compile.cmd_of_smartpy ()
         ; cmd_interpret_smartpy_scenario ()
         ; cmd_run_smartpy_scenario_in_docker_sandbox ()
         ; cmd_self_test ()
         ; Tezos_scenario_node_unix.cmd
             ()
             ~docs:Cmd_doc.docs_low_level
             ~cmd_name:"run-scenario-against-node"
         ; cmd_run_scenario_local_sandbox ()
         ; Run_python.cmd_make_all ()
         ; Smartml.Scenario.cmd_interpret_scenario ()
         ; Compile.cmd_of_smartml () ]
       @ Docker_sandbox.cmds () ))
