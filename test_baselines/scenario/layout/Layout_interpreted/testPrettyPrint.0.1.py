import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(a = 1, b = 12)

  @sp.entry_point
  def run_record(self, params):
    sp.set_type(params, sp.TRecord(a = sp.TInt, b = sp.TString, c = sp.TBool).layout(("b", ("a", "c"))))

  @sp.entry_point
  def run_record_2(self, params):
    sp.set_type(params, sp.TRecord(a = sp.TInt, b = sp.TString, c = sp.TBool, d = sp.TNat).layout(("a", ("b", ("c", "d")))))

  @sp.entry_point
  def run_type_record(self, params):
    sp.set_type(params, sp.TRecord(a = sp.TInt, b = sp.TString).layout(("b", "a")))

  @sp.entry_point
  def run_type_variant(self, params):
    sp.set_type(params, sp.TVariant(a = sp.TInt, b = sp.TString).layout(("b", "a")))

  @sp.entry_point
  def run_variant(self, params):
    sp.set_type(params, sp.TVariant(a = sp.TInt, b = sp.TString, c = sp.TBool).layout(("b", ("a", "c"))))