import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(last_acc = '', last_sum = 0, operator_support = False)

  @sp.entry_point
  def receive_balances(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance"))))
    self.data.last_sum = 0
    sp.for resp in params:
      self.data.last_sum += resp.balance

  @sp.entry_point
  def receive_metadata(self, params):
    self.data.last_acc = ''
    sp.for resp in params:
      sp.set_type(resp, sp.TRecord(decimals = sp.TNat, extras = sp.TMap(sp.TString, sp.TString), name = sp.TString, symbol = sp.TString, token_id = sp.TNat).layout(("token_id", ("symbol", ("name", ("decimals", "extras"))))))
      self.data.last_acc += resp.symbol

  @sp.entry_point
  def receive_metadata_registry(self, params):
    sp.verify(sp.sender == params)

  @sp.entry_point
  def receive_permissions_descriptor(self, params):
    sp.set_type(params, sp.TRecord(custom = sp.TOption(sp.TRecord(config_api = sp.TOption(sp.TAddress), tag = sp.TString).layout(("config_api", "tag"))), operator = sp.TVariant(no_transfer = sp.TUnit, owner_or_operator_transfer = sp.TUnit, owner_transfer = sp.TUnit).layout(("no_transfer", ("owner_transfer", "owner_or_operator_transfer"))), receiver = sp.TVariant(optional_owner_hook = sp.TUnit, owner_no_hook = sp.TUnit, required_owner_hook = sp.TUnit).layout(("owner_no_hook", ("optional_owner_hook", "required_owner_hook"))), sender = sp.TVariant(optional_owner_hook = sp.TUnit, owner_no_hook = sp.TUnit, required_owner_hook = sp.TUnit).layout(("owner_no_hook", ("optional_owner_hook", "required_owner_hook")))).layout(("operator", ("receiver", ("sender", "custom")))))
    sp.if params.operator.is_variant('owner_or_operator_transfer'):
      self.data.operator_support = True
    sp.else:
      self.data.operator_support = False