import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(x = sp.none)

  @sp.entry_point
  def create1(self, params):
    create_contract_20 = sp.local("create_contract_20", create contract ...)
    sp.operations().push(create_contract_20.value.operation)
    self.data.x = sp.some(create_contract_20.value.address)

  @sp.entry_point
  def create2(self, params):
    create_contract_24 = sp.local("create_contract_24", create contract ...)
    sp.operations().push(create_contract_24.value.operation)
    create_contract_25 = sp.local("create_contract_25", create contract ...)
    sp.operations().push(create_contract_25.value.operation)

  @sp.entry_point
  def create3(self, params):
    create_contract_29 = sp.local("create_contract_29", create contract ...)
    sp.operations().push(create_contract_29.value.operation)
    self.data.x = sp.some(create_contract_29.value.address)

  @sp.entry_point
  def create4(self, params):
    sp.for x in params:
      create_contract_35 = sp.local("create_contract_35", create contract ...)
      sp.operations().push(create_contract_35.value.operation)