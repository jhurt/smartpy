import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(x = sp.none)

  @sp.entry_point
  def push(self, params):
    self.data.x = sp.some(params)