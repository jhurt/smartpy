Comment...
 h1: Hash Functions
Creating contract
 -> (Pair (Pair 0x 0x) (Pair 0x (Pair "tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr" 0x)))
 => test_baselines/scenario/testHashFunctions/HashFunctions_interpreted/testContractTypes.0.1.tz 2
 => test_baselines/scenario/testHashFunctions/HashFunctions_interpreted/testContractCode.0.1.tz 87
 => test_baselines/scenario/testHashFunctions/HashFunctions_interpreted/testContractCode.0.1.tz.json 112
 => test_baselines/scenario/testHashFunctions/HashFunctions_interpreted/testPrettyPrint.0.1.py 16
Executing new_value(sp.bytes('0x001234'))...
 -> (Pair (Pair 0xfffdfd672ff9075528f51a30408cf768a093d8c67fb3c5c8782dff49eab0724d 0x61a706dfe2ddb1339d7b1d6f10c15a26786dcd1c99b743e0b0e351a6a168d99f) (Pair 0x0110f7f5dc329eed3f1d0e8d1ae204cc58b2a790506acfb793200fc60ba22525de2c5147fff19128807352a3a33c44d673cbda3b9840973fdfe4ad6516a73a49 (Pair "tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr" 0x001234)))
Executing new_key(sp.reduce(sp.test_account("Robert").public_key))...
 -> (Pair (Pair 0xfffdfd672ff9075528f51a30408cf768a093d8c67fb3c5c8782dff49eab0724d 0x61a706dfe2ddb1339d7b1d6f10c15a26786dcd1c99b743e0b0e351a6a168d99f) (Pair 0x0110f7f5dc329eed3f1d0e8d1ae204cc58b2a790506acfb793200fc60ba22525de2c5147fff19128807352a3a33c44d673cbda3b9840973fdfe4ad6516a73a49 (Pair "tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP" 0x001234)))
Verifying sp.pack(sp.set_type_expr(sp.contract_data(0), sp.TRecord(b2b = sp.TBytes, s256 = sp.TBytes, s512 = sp.TBytes, tz1 = sp.TKeyHash, v = sp.TBytes).layout((("b2b", "s256"), ("s512", ("tz1", "v")))))) == sp.pack(sp.set_type_expr(sp.record(b2b = sp.bytes('0xfffdfd672ff9075528f51a30408cf768a093d8c67fb3c5c8782dff49eab0724d'), s256 = sp.bytes('0x61a706dfe2ddb1339d7b1d6f10c15a26786dcd1c99b743e0b0e351a6a168d99f'), s512 = sp.bytes('0x0110f7f5dc329eed3f1d0e8d1ae204cc58b2a790506acfb793200fc60ba22525de2c5147fff19128807352a3a33c44d673cbda3b9840973fdfe4ad6516a73a49'), tz1 = sp.reduce(sp.test_account("Robert").public_key_hash), v = sp.bytes('0x001234')), sp.TRecord(b2b = sp.TBytes, s256 = sp.TBytes, s512 = sp.TBytes, tz1 = sp.TKeyHash, v = sp.TBytes).layout((("b2b", "s256"), ("s512", ("tz1", "v"))))))...
 => test_baselines/scenario/testHashFunctions/HashFunctions_interpreted/testVerify.4.tz 25
 OK