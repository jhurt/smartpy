const Networks = {
  MAINNET: 'Mainnet',
  ZERONET: 'Zeronet',
  BABYLONNET: 'Babylonnet',
  CARTHAGENET: 'Carthagenet',
  LABNET: 'Labnet',
  DALPHANET: 'Dalphanet',
};

const EXPLORER = {
  MAINNET: {
    tzstats: 'https://tzstats.com',
    tzkt: 'https://tzkt.io',
    bcd: 'https://better-call.dev',
  },
  CARTHAGENET: {
    tzstats: 'https://carthagenet.tzstats.com',
    tzkt: 'https://carthage.tzkt.io',
    bcd: 'https://better-call.dev/carthagenet',
  },
  BABYLONNET: {
    tzstats: 'https://babylonnet.tzstats.com',
    tzkt: 'https://babylon.tzkt.io',
    bcd: 'https://better-call.dev/babylonnet',
  },
  ZERONET: {
    tzstats: 'https://zeronet.tzstats.com',
    tzkt: 'https://zeronet.tzkt.io',
    bcd: 'https://better-call.dev/zeronet',
  },
};

const API = {
  MAINNET: {
    tzkt: 'https://api.tzkt.io/v1',
  },
  CARTHAGENET: {
    tzkt: 'https://api.carthage.tzkt.io/v1',
  },
  BABYLONNET: {
    tzkt: 'https://api.babylon.tzkt.io/v1',
  },
  ZERONET: {
    tzkt: 'https://api.zeronet.tzkt.io/v1',
  },
};

const smartPyNodes = {
  'https://mainnet.smartpy.io': 'Mainnet',
  'https://carthagenet.smartpy.io': 'Carthagenet',
  'https://babylonnet.smartpy.io': 'Babylonnet',
  'https://zeronet.smartpy.io': 'Zeronet',
  'https://labnet.smartpy.io': 'Labnet',
  'https://dalphanet.smartpy.io': 'Dalphanet',
};

const gigaNodes = {
  'https://mainnet-tezos.giganode.io': 'Mainnet',
  'https://testnet-tezos.giganode.io': 'Carthagenet',
  'https://labnet-tezos.giganode.io': 'Labnet',
};

const networkFilterByFeature = {
  origination: ['babylonnet'],
  faucet: ['babylonnet', 'mainnet'],
};

/**
 * Filter network by feature
 *
 * @param {string} feature
 * @param {string} node
 *
 * @returns true to skip, false otherwise
 */
const shouldSkipNetwork = (feature, node) => {
  const featureFilter = networkFilterByFeature[feature];
  return featureFilter && featureFilter.some((filter) => node.toLowerCase().includes(filter));
};

/*
OTHER NODES

https://rpcalpha.tzbeta.net
https://tezos-dev.cryptonomic-infra.tech
http://babylonnet.tezos.cryptium.ch:8732
http://carthagenet.tezos.cryptium.ch:8732
*/

/**
 *  GET Request
 */
const requestGET = (url) =>
  new Promise((resolve, reject) => {
    let req = new XMLHttpRequest();
    req.timeout = 2000;
    req.onreadystatechange = () => {
      if (req.readyState == 4) {
        if (req.status == 200) {
          resolve(JSON.parse(req.response));
        } else {
          reject(req.responseText);
        }
      }
    };
    req.open('GET', url, true);
    req.send();
  });
